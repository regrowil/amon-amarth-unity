﻿namespace Fabric.Internal
{
	using UnityEngine;
	using System.Collections;
	
	public class FabricInit : MonoBehaviour
	{
	    public void Awake()
	    {
	        Fabric.Runtime.Fabric.Initialize();
	    }
	}
}
