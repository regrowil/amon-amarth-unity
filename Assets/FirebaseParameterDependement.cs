﻿using System.Collections;
using System.Collections.Generic;
using Firebase.RemoteConfig;
using UnityEngine;
using UnityEngine.Events;

public class FirebaseParameterDependement : MonoBehaviour
{

    public string ParameterName;
    public UnityEvent ParameterActivated;

    

    public void OnEnable()
    {
        var param = FirebaseRemoteConfig.GetValue(ParameterName).BooleanValue;
        Debug.Log(param);
        if (param)
        {
            ParameterActivated.Invoke();
        }
    }

}
