﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ScreenshotMaker
{
    public static Texture2D TakeSnapshot()
    {
        int width = Screen.width;
        int height = Screen.height;

#if UNITY_EDITOR
        EditorWindow window = GetMainGameView();
        width = (int)window.position.width;
        height = (int)window.position.height;
#endif

        return TakeSnapshot(width, height);
    }

    public static Texture2D TakeSnapshot(int width, int height)
    {
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGB24, true);
        texture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        texture.Apply();

        return texture;
    }

#if UNITY_EDITOR
    public static EditorWindow GetMainGameView()
    {
        System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
        System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        System.Object Res = GetMainGameView.Invoke(null, null);
        return (EditorWindow)Res;
    }
#endif
}
