using UnityEngine;
using System.Collections;

namespace FreshRocket
{
	public abstract class FRSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
		private static T _instance = null;
		private static bool applicationIsQuitting = false;

		public static T Instance
        {
			get
            {
				if (applicationIsQuitting)
                {
					return null;
				}

				if (_instance == null)
                {
					_instance = GameObject.FindObjectOfType(typeof(T)) as T;

					if (_instance == null)
                    {
						_instance = new GameObject ().AddComponent<T> ();
						_instance.gameObject.name = _instance.GetType ().Name;
					}
				}				

				return _instance;
			}
		}

		public static bool HasInstance
        {
			get
            {
				return !IsDestroyed;
			}
		}

		public static bool IsDestroyed
        {
			get
            {
                return (_instance == null);
			}
		}

		protected virtual void OnDestroy ()
        {
            _instance = null;
			applicationIsQuitting = true;
		}
        
		protected virtual void OnApplicationQuit ()
        {
			_instance = null;
			applicationIsQuitting = true;
        }
    }
}