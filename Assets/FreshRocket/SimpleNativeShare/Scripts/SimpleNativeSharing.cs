﻿using UnityEngine;
using System.IO;

namespace FreshRocket
{
    public class SimpleNativeSharing : FRSingleton<SimpleNativeSharing>
    {
        SimpleNativeSharingHandler handler;

        private void Awake()
        {
            DontDestroyOnLoad(this);

#if UNITY_ANDROID
            handler = new SimpleNativeSharingAndroidHandler();
#endif

#if UNITY_IOS
            handler = new SimpleNativeSharingiOSHandler();
#endif
        }

        public static void ShareText(string message)
        {
            Instance.ShareMessage(message);
        }

        public static void ShareTextAndImage(Texture2D image, string message)
        {
            Instance.ShareTextWithImage(image, message);
        }

        public void ShareMessage(string message)
	    {
            StartCoroutine(handler.ShareText(message));
	    }
	
	    public void ShareTextWithImage (Texture2D image, string message)
	    {
            string date = System.DateTime.Now.ToString("dd-MM-yy");

            byte[] bytes = image.EncodeToPNG();
            string path = Application.temporaryCachePath + "/screenShot-" + date + ".png";
            File.WriteAllBytes(path, bytes);

            StartCoroutine(handler.ShareTextAndImage(path, message));
        }
    }
}