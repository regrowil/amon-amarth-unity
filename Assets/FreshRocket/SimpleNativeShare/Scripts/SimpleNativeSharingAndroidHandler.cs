﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FreshRocket
{
    public class SimpleNativeSharingAndroidHandler : SimpleNativeSharingHandler
    {
        public IEnumerator ShareText(string message)
        {
            yield return new WaitForEndOfFrame();

#if UNITY_ANDROID && !UNITY_EDITOR
		    AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		    AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		
		    intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		    intentObject.Call<AndroidJavaObject>("setType", "image/*");
		    intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), message);
		
		    AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		    AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		    AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Select Sharing App");
            currentActivity.Call("startActivity", jChooser);
//		    currentActivity.Call("startActivity", intentObject);
#endif
        }

        public IEnumerator ShareTextAndImage(string imagePath, string message)
        {
            yield return new WaitForEndOfFrame();

#if UNITY_ANDROID && !UNITY_EDITOR
		    AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		    AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		
		    intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		    intentObject.Call<AndroidJavaObject>("setType", "image/*");
		    intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), message);
		
		    AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		    AndroidJavaClass fileClass = new AndroidJavaClass("java.io.File");
		
		    AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", imagePath);
		
		    AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromFile", fileObject);
		
		    bool fileExist = fileObject.Call<bool>("exists");
		    if (fileExist)
            {
			    intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            }
		
		    AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		    AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Select Sharing App");
            currentActivity.Call("startActivity", jChooser);
//            currentActivity.Call("startActivity", intentObject);
#endif
        }
    }
}
