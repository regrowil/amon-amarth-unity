﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SimpleNativeSharingHandler
{
    IEnumerator ShareText(string message);
    IEnumerator ShareTextAndImage(string imagePath, string message);
}
