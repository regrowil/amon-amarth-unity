using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

namespace FreshRocket
{
    public class SimpleNativeSharingiOSHandler : SimpleNativeSharingHandler
    {

#if UNITY_IOS
	
        [DllImport("__Internal")]
        private static extern void _FRESHROCKET_ShareTextWithImage (string iosPath, string message);
	
        [DllImport("__Internal")]
        private static extern void _FRESHROCKET_ShareSimpleText (string message);
#endif

        public IEnumerator ShareText(string message)
        {
#if UNITY_IOS
            _FRESHROCKET_ShareSimpleText(message);
#endif
            yield return null;
        }

        public IEnumerator ShareTextAndImage(string imagePath, string message)
        {
#if UNITY_IOS
            _FRESHROCKET_ShareTextWithImage(imagePath, message);
#endif
            yield return null;
        }

    }
}