// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using System;
using UnityEngine;
using System.Collections.Generic;

namespace SpriteTile
{

    public class MapData
    {
        const ushort COLLIDERBIT = 32768;
        const ushort ROTATIONBITS = 1024 + 512 + 256 + 128 + 64 + 32 + 16 + 8 + 4 + 2 + 1;
        const ushort ROTATIONBITSINVERSE = 65535 - ROTATIONBITS;    // Since ~ROTATIONBITS refuses to compile
        const ushort FLIPXBIT = 4096;
        const ushort FLIPYBIT = 2048;
        const ushort FLIPBITSINVERSE = 65535 - (FLIPXBIT + FLIPYBIT);
        public Int2 mapSize;
        public short[] map;
        public ushort[] info;   // Bit 16 (32768) = collider, bits 14-15 = reserved, bit 13 = xflip, bit 12 = yflip, bits 1-11 = rotation
        public short[] orderData;
        public byte[] triggerData;
        public byte[] materialIDs;
        public Color32[] colors;
        public ushort[] halfColors;
        public bool useColors;
        public Color32 ambientColor;
        public Dictionary<Int2, PropertyInfo> propertyDictionary;
        public Dictionary<Int2, Light> lightRefs;
        public Int2 maxMapPos;
        public bool useRadiosity;

        static int _setCount = 32;
        public static int setCount
        {
            get { return _setCount; }
        }
        static int _setShift = 10;
        public static int setShift
        {
            get { return _setShift; }
        }
        static int _tileShift = 1024;
        public static int tileShift
        {
            get { return _tileShift; }
        }
        static int _tileShiftI = 1023;
        public static int tileShiftI
        {
            get { return _tileShiftI; }
        }

        public const int missingTile = -3;
        public const int tempTile = -99;

        public static void SetCounts(int tileSetCount)
        {
            _setCount = tileSetCount;
            _tileShift = 32768 / _setCount;
            _tileShiftI = _tileShift - 1;
            _setShift = (int)Mathf.Log(_tileShift, 2);
        }

        public MapData(Int2 size, bool clear)
        {
            InitializeMap(size, clear);
        }

        public void InitializeMap(Int2 size, bool clear)
        {
            if (size.x < 1)
            {
                size.x = 1;
            }
            if (size.y < 1)
            {
                size.y = 1;
            }
            mapSize = size;
            map = new short[size.x * size.y];
            info = new ushort[size.x * size.y];
            orderData = new short[size.x * size.y];
            triggerData = new byte[size.x * size.y];
            useColors = false;
            propertyDictionary = new Dictionary<Int2, PropertyInfo>();
            lightRefs = new Dictionary<Int2, Light>();
            ambientColor = Color.white;
            maxMapPos = new Int2(mapSize.x - 1, mapSize.y - 1);

            if (clear)
            {
                int total = size.x * size.y;
                for (int i = 0; i < total; i++)
                {
                    map[i] = -1;
                }
            }
        }

        public void SetupMaterialIDs()
        {
            materialIDs = new byte[mapSize.x * mapSize.y];
        }

        public int GetMaterialID(Int2 p)
        {
            return materialIDs[p.x + p.y * mapSize.x];
        }

        public void SetMaterialID(Int2 p, byte id)
        {
            materialIDs[p.x + p.y * mapSize.x] = id;
        }

        public void ForceColorReset()
        {
            useColors = false;
            SetupColors();
        }

        public void SetupColors()
        {
            if (useColors) return;

            useColors = true;
            if (Tile.useTrueColor)
            {
                colors = new Color32[mapSize.x * mapSize.y];
                for (int i = 0; i < colors.Length; i++)
                {
                    colors[i] = ambientColor;
                }
                halfColors = null;
            }
            else
            {
                halfColors = new ushort[mapSize.x * mapSize.y];
                var color = (ushort)(((ambientColor.r >> 4) << 12) + ((ambientColor.g >> 4) << 8) + ((ambientColor.b >> 4) << 4) + (ambientColor.a >> 4));
                for (int i = 0; i < halfColors.Length; i++)
                {
                    halfColors[i] = color;
                }
                colors = null;
            }
        }

        public void ConvertColors(bool convertToTrueColor)
        {
            if (convertToTrueColor)
            {
                colors = new Color32[halfColors.Length];
                ushort c;
                for (int i = 0; i < colors.Length; i++)
                {
                    c = halfColors[i];
                    colors[i] = new Color32((byte)((c >> 12) * 17), (byte)(((c & 0x0F00) >> 8) * 17), (byte)(((c & 0x00F0) >> 4) * 17), (byte)((c & 0x000F) * 17));
                }
                halfColors = null;
            }
            else
            {
                halfColors = new ushort[colors.Length];
                Color32 c;
                for (int i = 0; i < halfColors.Length; i++)
                {
                    c = colors[i];
                    halfColors[i] = (ushort)(((c.r >> 4) << 12) + ((c.g >> 4) << 8) + ((c.b >> 4) << 4) + (c.a >> 4));
                }
                colors = null;
            }
        }

        public MapElement GetMapElement(Int2 p)
        {
            return new MapElement(p, map[p.x + p.y * mapSize.x], info[p.x + p.y * mapSize.x], orderData[p.x + p.y * mapSize.x], triggerData[p.x + p.y * mapSize.x], GetPropertyInfo(p), GetLightRef(p));
        }

        public void SetMapElement(MapElement m)
        {
            map[m.position.x + m.position.y * mapSize.x] = m.mapTile;
            info[m.position.x + m.position.y * mapSize.x] = m.info;
            orderData[m.position.x + m.position.y * mapSize.x] = m.order;
            triggerData[m.position.x + m.position.y * mapSize.x] = m.trigger;
            if (m.propertyInfo == null)
            {
                propertyDictionary.Remove(m.position);
            }
            else
            {
                propertyDictionary[m.position] = new PropertyInfo(m.propertyInfo);
            }
            if (m.light == null)
            {
                lightRefs.Remove(m.position);
            }
            else
            {
                lightRefs[m.position] = new Light(m.light);
            }
        }

        public TileInfo GetTile(Int2 p)
        {
            var mapTile = map[p.x + p.y * mapSize.x];
            if (mapTile < 0)
            {
                return new TileInfo(0, mapTile);
            }
            return new TileInfo(mapTile >> _setShift, mapTile & _tileShiftI);
        }

        public void SetTile(Int2 p, int set, short t)
        {
            int pos = p.x + p.y * mapSize.x;
            map[pos] = (short)((t == -1 ? 0 : set * _tileShift) + t);
        }

        public void SetTile(Int2 p, int set, short t, int order, float rotation, bool setCollider, int trigger, Flip flip)
        {
            int pos = p.x + p.y * mapSize.x;
            map[pos] = (short)((t == -1 ? 0 : set * _tileShift) + t);
            info[pos] = GetInfoData(rotation, flip, setCollider);
            orderData[pos] = (short)order;
            triggerData[pos] = (byte)trigger;
        }

        public void SetTileBlock(Int2 p1, Int2 p2, int set, short t)
        {
            Clamp(ref p1, ref p2);
            t = (short)((t == -1 ? 0 : set * _tileShift) + t);
            for (int y = p1.y; y <= p2.y; y++)
            {
                for (int x = p1.x; x <= p2.x; x++)
                {
                    map[x + y * mapSize.x] = t;
                }
            }
        }

        public void SetTileBlock(Int2 p1, Int2 p2, int set, short t, int order, float rotation, bool setCollider, int trigger, Flip flip, bool hasProperty, PropertyInfo propertyInfo, bool eraseLight)
        {
            Clamp(ref p1, ref p2);
            t = (short)((t == -1 ? 0 : set * _tileShift) + t);
            ushort infoData = GetInfoData(rotation, flip, setCollider);
            short oData = (short)order;
            byte tData = (byte)trigger;
            Int2 p;
            for (p.y = p1.y; p.y <= p2.y; p.y++)
            {
                for (p.x = p1.x; p.x <= p2.x; p.x++)
                {
                    int pos = p.x + p.y * mapSize.x;
                    map[pos] = t;
                    info[pos] = infoData;
                    orderData[pos] = oData;
                    triggerData[pos] = tData;
                    SetHasProperty(p, hasProperty);
                    if (hasProperty)
                    {
                        propertyDictionary[p] = propertyInfo;
                    }
                    if (eraseLight)
                    {
                        lightRefs.Remove(p);
                    }
                }
            }
        }

        public bool SetMapTileset(int set, int maxTile)
        {
            set *= _tileShift;
            int end = map.Length;
            for (int i = 0; i < end; i++)
            {
                if (map[i] < 0)
                {
                    continue;
                }
                int tile = map[i] & _tileShiftI;
                if (tile >= maxTile)
                {
                    return false;
                }
                map[i] = (short)(tile + set);
            }
            return true;
        }

        ushort GetInfoData(float rotation, Flip flip, bool setCollider)
        {
            ushort infoData = (ushort)(Mathf.Repeat(rotation, 360.0f) * 5);
            if (flip == Flip.X || flip == Flip.XY)
            {
                infoData |= FLIPXBIT;
            }
            if (flip == Flip.Y || flip == Flip.XY)
            {
                infoData |= FLIPYBIT;
            }
            if (setCollider)
            {
                infoData |= COLLIDERBIT;
            }
            return infoData;
        }

        public void CopyBlockTo(Int2 p1, Int2 p2, MapData dest, Int2 destPos, bool skipEmpties, bool delete)
        {
            Int2 p;
            Int2 dpos = new Int2(0, destPos.y - 1);
            for (p.y = p1.y; p.y <= p2.y; p.y++)
            {
                if (++dpos.y < 0) continue;
                if (dpos.y >= dest.mapSize.y) break;

                dpos.x = destPos.x - 1;
                for (p.x = p1.x; p.x <= p2.x; p.x++)
                {
                    if (++dpos.x < 0) continue;
                    if (dpos.x >= dest.mapSize.x) break;

                    int sIntPos = p.x + p.y * mapSize.x;
                    int dIntPos = dpos.x + dpos.y * dest.mapSize.x;
                    if (skipEmpties && map[sIntPos] < 0)
                    {
                        continue;
                    }
                    dest.SetHasProperty(dpos, false);   // Will be overwritten below if there are properties
                    dest.lightRefs.Remove(dpos);
                    if (!delete)
                    {
                        dest.map[dIntPos] = map[sIntPos];
                        dest.info[dIntPos] = info[sIntPos];
                        dest.orderData[dIntPos] = orderData[sIntPos];
                        dest.triggerData[dIntPos] = triggerData[sIntPos];
                    }
                    else
                    {
                        dest.map[dIntPos] = -1;
                        dest.info[dIntPos] = 0;
                        dest.orderData[dIntPos] = 0;
                        dest.triggerData[dIntPos] = 0;
                    }
                }
            }

            if (!delete)
            {
                var pDictionary = GetPropertyDictionaryInArea(p1, p2);
                if (pDictionary != null)
                {
                    foreach (var item in pDictionary)
                    {
                        dest.propertyDictionary[new Int2(item.Key.x + destPos.x, item.Key.y + destPos.y)] = item.Value;
                    }
                }
                var lReferences = GetLightReferencesInArea(p1, p2);
                if (lReferences != null)
                {
                    foreach (var item in lReferences)
                    {
                        dest.lightRefs[new Int2(item.Key.x + destPos.x, item.Key.y + destPos.y)] = item.Value;
                    }
                }
            }
        }

        Dictionary<Int2, PropertyInfo> GetPropertyDictionaryInArea(Int2 p1, Int2 p2)
        {
            if (propertyDictionary.Count > 0)
            {
                var newDictionary = new Dictionary<Int2, PropertyInfo>();
                Int2 p;
                for (p.y = p1.y; p.y <= p2.y; p.y++)
                {
                    for (p.x = p1.x; p.x <= p2.x; p.x++)
                    {
                        if (HasProperty(p))
                        {
                            newDictionary[new Int2(p.x - p1.x, p.y - p1.y)] = GetPropertyInfo(p);
                        }
                    }
                }
                return newDictionary;
            }
            return null;
        }

        Dictionary<Int2, Light> GetLightReferencesInArea(Int2 p1, Int2 p2)
        {
            if (lightRefs.Count > 0)
            {
                var newDictionary = new Dictionary<Int2, Light>();
                Int2 p;
                for (p.y = p1.y; p.y <= p2.y; p.y++)
                {
                    for (p.x = p1.x; p.x <= p2.x; p.x++)
                    {
                        if (HasLight(p))
                        {
                            newDictionary[new Int2(p.x - p1.x, p.y - p1.y)] = GetLightRef(p);
                        }
                    }
                }
                return newDictionary;
            }
            return null;
        }

        public MapData GetBlock(Int2 p1, Int2 p2)
        {
            var newMapData = new MapData(p2 - p1 + Int2.one, false);
            bool doMaterial = (materialIDs != null);
            if (doMaterial)
            {
                newMapData.SetupMaterialIDs();
            }
            int mapDataY = 0;
            for (int y = p1.y; y <= p2.y; y++)
            {
                int pos = p1.x + y * mapSize.x;
                int mapDataPos = mapDataY++ * newMapData.mapSize.x;
                System.Array.Copy(map, pos, newMapData.map, mapDataPos, newMapData.mapSize.x);
                System.Array.Copy(info, pos, newMapData.info, mapDataPos, newMapData.mapSize.x);
                System.Array.Copy(orderData, pos, newMapData.orderData, mapDataPos, newMapData.mapSize.x);
                System.Array.Copy(triggerData, pos, newMapData.triggerData, mapDataPos, newMapData.mapSize.x);
                if (doMaterial)
                {
                    System.Array.Copy(materialIDs, pos, newMapData.materialIDs, mapDataPos, newMapData.mapSize.x);
                }
            }
            if (propertyDictionary.Count > 0)
            {
                newMapData.propertyDictionary = GetPropertyDictionaryInArea(p1, p2);
            }
            if (lightRefs.Count > 0)
            {
                newMapData.lightRefs = GetLightReferencesInArea(p1, p2);
            }

            return newMapData;
        }

        public void SetBlock(Int2 p, MapData newMapData)
        {
            bool doMaterial = (newMapData.materialIDs != null);
            if (doMaterial && materialIDs == null)
            {
                SetupMaterialIDs();
            }
            int h = p.y + newMapData.mapSize.y;
            int mapDataY = 0;
            for (int y = p.y; y < h; y++)
            {
                int pos = p.x + y * mapSize.x;
                int mapDataPos = mapDataY++ * newMapData.mapSize.x;
                System.Array.Copy(newMapData.map, mapDataPos, map, pos, newMapData.mapSize.x);
                System.Array.Copy(newMapData.info, mapDataPos, info, pos, newMapData.mapSize.x);
                System.Array.Copy(newMapData.orderData, mapDataPos, orderData, pos, newMapData.mapSize.x);
                System.Array.Copy(newMapData.triggerData, mapDataPos, triggerData, pos, newMapData.mapSize.x);
                if (doMaterial)
                {
                    System.Array.Copy(newMapData.materialIDs, mapDataPos, materialIDs, pos, newMapData.mapSize.x);
                }
            }

            if (newMapData.propertyDictionary.Count > 0)
            {
                foreach (var item in newMapData.propertyDictionary)
                {
                    propertyDictionary[new Int2(item.Key.x + p.x, item.Key.y + p.y)] = item.Value;
                }
            }
            if (newMapData.lightRefs.Count > 0)
            {
                foreach (var item in newMapData.lightRefs)
                {
                    lightRefs[new Int2(item.Key.x + p.x, item.Key.y + p.y)] = item.Value;
                }
            }
        }

        public bool GetCollider(Int2 p)
        {
            return (info[p.x + p.y * mapSize.x] & COLLIDERBIT) != 0;
        }

        public void SetCollider(Int2 p, bool active)
        {
            if (active)
            {
                info[p.x + p.y * mapSize.x] |= COLLIDERBIT;
            }
            else
            {
                info[p.x + p.y * mapSize.x] &= COLLIDERBIT - 1;
            }
        }

        public void SetColliderBlock(Int2 p1, Int2 p2, bool active)
        {
            Clamp(ref p1, ref p2);
            for (int y = p1.y; y <= p2.y; y++)
            {
                for (int x = p1.x; x <= p2.x; x++)
                {
                    if (active)
                    {
                        info[x + y * mapSize.x] |= COLLIDERBIT;
                    }
                    else
                    {
                        info[x + y * mapSize.x] &= COLLIDERBIT - 1;
                    }
                }
            }
        }

        public int GetOrder(Int2 p)
        {
            return (int)orderData[p.x + p.y * mapSize.x];
        }

        public void SetOrder(Int2 p, int order)
        {
            orderData[p.x + p.y * mapSize.x] = (short)order;
        }

        public void SetOrderBlock(Int2 p1, Int2 p2, int order)
        {
            Clamp(ref p1, ref p2);
            short o = (short)order;
            for (int y = p1.y; y <= p2.y; y++)
            {
                for (int x = p1.x; x <= p2.x; x++)
                {
                    orderData[x + y * mapSize.x] = o;
                }
            }
        }

        public float GetRotation(Int2 p)
        {
            return (info[p.x + p.y * mapSize.x] & ROTATIONBITS) / 5.0f;
        }

        public void SetRotation(Int2 p, float rotation)
        {
            info[p.x + p.y * mapSize.x] &= ROTATIONBITSINVERSE;
            info[p.x + p.y * mapSize.x] |= (ushort)(Mathf.Repeat(rotation, 360.0f) * 5);
        }

        public void SetRotationBlock(Int2 p1, Int2 p2, float rotation)
        {
            Clamp(ref p1, ref p2);
            ushort r = (ushort)(Mathf.Repeat(rotation, 360.0f) * 5);
            for (int y = p1.y; y <= p2.y; y++)
            {
                for (int x = p1.x; x <= p2.x; x++)
                {
                    info[x + y * mapSize.x] &= ROTATIONBITSINVERSE;
                    info[x + y * mapSize.x] |= r;
                }
            }
        }

        public Flip GetFlip(Int2 p)
        {
            if ((info[p.x + p.y * mapSize.x] & FLIPXBIT) != 0)
            {
                if ((info[p.x + p.y * mapSize.x] & FLIPYBIT) != 0)
                {
                    return Flip.XY;
                }
                return Flip.X;
            }
            if ((info[p.x + p.y * mapSize.x] & FLIPYBIT) != 0)
            {
                return Flip.Y;
            }
            return Flip.None;
        }

        public bool GetFlipX(Int2 p)
        {
            return (info[p.x + p.y * mapSize.x] & FLIPXBIT) != 0;
        }

        public bool GetFlipY(Int2 p)
        {
            return (info[p.x + p.y * mapSize.x] & FLIPYBIT) != 0;
        }

        public void SetFlip(Int2 p, Flip flip)
        {
            info[p.x + p.y * mapSize.x] &= FLIPBITSINVERSE;
            if (flip == Flip.XY)
            {
                info[p.x + p.y * mapSize.x] |= FLIPXBIT;
                info[p.x + p.y * mapSize.x] |= FLIPYBIT;
            }
            else if (flip == Flip.X)
            {
                info[p.x + p.y * mapSize.x] |= FLIPXBIT;
            }
            else if (flip == Flip.Y)
            {
                info[p.x + p.y * mapSize.x] |= FLIPYBIT;
            }
        }

        public void SetFlipBlock(Int2 p1, Int2 p2, Flip flip)
        {
            bool flipX = (flip == Flip.X || flip == Flip.XY);
            bool flipY = (flip == Flip.Y || flip == Flip.XY);
            Clamp(ref p1, ref p2);
            for (int y = p1.y; y <= p2.y; y++)
            {
                for (int x = p1.x; x <= p2.x; x++)
                {
                    info[x + y * mapSize.x] &= FLIPBITSINVERSE;
                    if (flipX)
                    {
                        info[x + y * mapSize.x] |= FLIPXBIT;
                    }
                    if (flipY)
                    {
                        info[x + y * mapSize.x] |= FLIPYBIT;
                    }
                }
            }
        }

        public bool HasProperty(Int2 p)
        {
            return propertyDictionary.ContainsKey(p);
        }

        public void SetHasProperty(Int2 p, bool active)
        {
            if (active)
            {
                if (!propertyDictionary.ContainsKey(p))
                {
                    propertyDictionary[p] = new PropertyInfo();
                }
            }
            else
            {
                propertyDictionary.Remove(p);
            }
        }

        public PropertyInfo GetPropertyInfo(Int2 p)
        {
            if (propertyDictionary.ContainsKey(p))
            {
                return new PropertyInfo(propertyDictionary[p]);
            }
            return null;
        }

        public T GetProperty<T>(Int2 p)
        {
            if (typeof(T) == typeof(float))
            {
                return (T)((object)propertyDictionary[p].f);
            }
            else if (typeof(T) == typeof(string))
            {
                return (T)((object)propertyDictionary[p].s);
            }
            else if (typeof(T) == typeof(GameObject))
            {
                return (T)((object)propertyDictionary[p].go);
            }
            return default(T);
        }

        public void SetProperty<T>(Int2 p, object value)
        {
            SetHasProperty(p, true);

            if (typeof(T) == typeof(float))
            {
                propertyDictionary[p].f = (float)value;
            }
            else if (typeof(T) == typeof(string))
            {
                propertyDictionary[p].s = (string)value;
            }
            else if (typeof(T) == typeof(GameObject))
            {
                propertyDictionary[p].go = (GameObject)value;
            }
        }

        public int GetPropertyID(Int2 p)
        {
            return propertyDictionary[p].guid;
        }

        public void SetPropertyID(Int2 p, int guid)
        {
            propertyDictionary[p].guid = guid;
        }

        public int GetTrigger(Int2 p)
        {
            return triggerData[p.x + p.y * mapSize.x];
        }

        public void SetTrigger(Int2 p, int trigger)
        {
            triggerData[p.x + p.y * mapSize.x] = (byte)trigger;
        }

        public void SetTriggerBlock(Int2 p1, Int2 p2, int trigger)
        {
            Clamp(ref p1, ref p2);
            byte t = (byte)trigger;
            for (int y = p1.y; y <= p2.y; y++)
            {
                for (int x = p1.x; x <= p2.x; x++)
                {
                    triggerData[x + y * mapSize.x] = t;
                }
            }
        }

        public Color32 GetColor(Int2 p)
        {
            if (Tile.useTrueColor)
            {
                return colors[p.x + p.y * mapSize.x];
            }
            var c = halfColors[p.x + p.y * mapSize.x];
            return new Color32((byte)((c >> 12) * 17), (byte)(((c & 0x0F00) >> 8) * 17), (byte)(((c & 0x00F0) >> 4) * 17), (byte)((c & 0x000F) * 17));
        }

        public void GetColor(ref Int2 p, ref Color32 color)
        {
            if (Tile.useTrueColor)
            {
                color = colors[p.x + p.y * mapSize.x];
                return;
            }
            var c = halfColors[p.x + p.y * mapSize.x];
            color = new Color32((byte)((c >> 12) * 17), (byte)(((c & 0x0F00) >> 8) * 17), (byte)(((c & 0x00F0) >> 4) * 17), (byte)((c & 0x000F) * 17));
        }

        public void SetColor(Int2 p, Color32 color)
        {
            if (Tile.useTrueColor)
            {
                colors[p.x + p.y * mapSize.x] = color;
            }
            else
            {
                halfColors[p.x + p.y * mapSize.x] = (ushort)(((color.r >> 4) << 12) + ((color.g >> 4) << 8) + ((color.b >> 4) << 4) + (color.a >> 4));
            }
        }

        public void SetColorBlock(Int2 p1, Int2 p2, Color32 color)
        {
            if (Tile.useTrueColor)
            {
                for (int y = p1.y; y <= p2.y; y++)
                {
                    for (int x = p1.x; x <= p2.x; x++)
                    {
                        colors[x + y * mapSize.x] = color;
                    }
                }
            }
            else
            {
                var thisColor = (ushort)(((color.r >> 4) << 12) + ((color.g >> 4) << 8) + ((color.b >> 4) << 4) + (color.a >> 4));
                for (int y = p1.y; y <= p2.y; y++)
                {
                    for (int x = p1.x; x <= p2.x; x++)
                    {
                        halfColors[x + y * mapSize.x] = thisColor;
                    }
                }
            }
        }

        public int GetLightNumber(Int2 p)
        {
            if (lightRefs.ContainsKey(p))
            {
                return lightRefs[p].number;
            }
            return -1;
        }

        public float GetLightIntensity(Int2 p)
        {
            if (lightRefs.ContainsKey(p))
            {
                return lightRefs[p].intensity;
            }
            return 0.0f;
        }

        public bool HasLight(Int2 p)
        {
            if (lightRefs.ContainsKey(p))
            {
                return true;
            }
            return false;
        }

        Light GetLightRef(Int2 p)
        {
            if (lightRefs.ContainsKey(p))
            {
                return new Light(lightRefs[p]);
            }
            return null;
        }

        Light GetLightRefNoAlloc(Int2 p)
        {
            if (lightRefs.ContainsKey(p))
            {
                return lightRefs[p];
            }
            return null;
        }

        public void RemoveLightNumber(int lightNumber)
        {
            var removeList = new List<Int2>();
            var changeList = new List<Int2>();
            foreach (var item in lightRefs)
            {
                if (item.Value.number == lightNumber)
                {
                    removeList.Add(item.Key);
                }
                else if (item.Value.number > lightNumber)
                {
                    changeList.Add(item.Key);
                }
            }
            for (int j = 0; j < removeList.Count; j++)
            {
                lightRefs.Remove(removeList[j]);
            }
            for (int j = 0; j < changeList.Count; j++)
            {
                lightRefs[changeList[j]].number--;
            }
        }

        public void SetLight(Int2 p, bool active, int lightNumber, float intensity, List<LightInfo> lightList, bool dontAddOrRemove)
        {
            if (active)
            {
                if (!dontAddOrRemove)
                {
                    lightRefs[p] = new Light(lightNumber, intensity);
                }
                else
                {
                    lightRefs[p].intensity = intensity;
                }
                ComputeLight(p, lightList[lightNumber], Int2.zero, maxMapPos);
            }
            else
            {   // Delete
                Int2 p1 = GetStartPosClamped(p, lightList[lightNumber]);
                Int2 p2 = GetEndPosClamped(p, lightList[lightNumber]);
                SetColorBlock(p1, p2, ambientColor);
                if (!dontAddOrRemove)
                {
                    lightRefs.Remove(p);
                }
                // Check overlap with other lights and redraw as needed
                Int2 pos1, pos2;
                var enumerator = lightRefs.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    var item = enumerator.Current;
                    if (item.Key == p) continue;

                    if (p1.x <= item.Key.x + lightList[item.Value.number].radiusR.x && p2.x >= item.Key.x - lightList[item.Value.number].radius.x &&
                        p1.y <= item.Key.y + lightList[item.Value.number].radiusR.y && p2.y >= item.Key.y - lightList[item.Value.number].radius.y)
                    {
                        // Get intersecting rect
                        pos1.x = Mathf.Max(p1.x, item.Key.x - lightList[item.Value.number].radius.x);
                        pos1.y = Mathf.Max(p1.y, item.Key.y - lightList[item.Value.number].radius.y);
                        pos2.x = Mathf.Min(p2.x, item.Key.x + lightList[item.Value.number].radiusR.x);
                        pos2.y = Mathf.Min(p2.y, item.Key.y + lightList[item.Value.number].radiusR.y);
                        ComputeLight(item.Key, lightList[item.Value.number], pos1, pos2);
                    }
                }
            }
        }

        public void MoveLight(Int2 p1, Int2 p2, List<LightInfo> lightList)
        {
            var light = GetLightRefNoAlloc(p1);
            light.losComputed = false;
            lightRefs.Remove(p1);
            SetLight(p1, false, light.number, 0.0f, lightList, true);
            lightRefs[p2] = light;
            SetLight(p2, true, light.number, light.intensity, lightList, true);
        }

        void ComputeLight(Int2 p, LightInfo lightInfo, Int2 startClamp, Int2 endClamp)
        {
            Int2 radius = lightInfo.radius;
            Int2 startP = new Int2(-radius.x + (Mathf.Max(startClamp.x, p.x - radius.x) - (p.x - radius.x)), -radius.y + (Mathf.Max(startClamp.y, p.y - radius.y) - (p.y - radius.y)));
            Int2 endP = new Int2(radius.x - ((p.x + radius.x) - Mathf.Min(endClamp.x, p.x + radius.x)), radius.y - ((p.y + radius.y) - Mathf.Min(endClamp.y, p.y + radius.y)));
            Int2 sRadius = radius;
            if (sRadius.x > sRadius.y)
            {
                sRadius.y -= sRadius.y - sRadius.x;
            }
            else if (sRadius.x < sRadius.y)
            {
                sRadius.x -= sRadius.x - sRadius.y;
            }

            // Compute shadowmap if needed
            if (lightInfo.shadowType != ShadowType.None)
            {
                int thisRadius = (lightInfo.radiusR.x > lightInfo.radiusR.y) ? lightInfo.radiusR.x : lightInfo.radiusR.y;
                int w = thisRadius * 2 + 1;
                if (lightRefs[p].NeedsReset(thisRadius))
                {
                    lightRefs[p].SetupShadow(thisRadius);
                }
                if (!lightRefs[p].losComputed)
                {
                    if (GetCollider(p))
                    {
                        for (int i = 0; i < lightRefs[p].shadowMap.Length; i++)
                        {
                            lightRefs[p].shadowMap[i] = 0;
                        }
                    }
                    else
                    {
                        Int2 mapP = p - new Int2(thisRadius, thisRadius);

                        int r = thisRadius + 1;
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.up, Int2.upLeft, r, Mathf.Min(r, maxMapPos.y - p.y + 1), Mathf.Min(r, p.x + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.up, Int2.upRight, r, Mathf.Min(r, maxMapPos.y - p.y + 1), Mathf.Min(r, maxMapPos.x - p.x + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.right, Int2.upRight, r, Mathf.Min(r, maxMapPos.x - p.x + 1), Mathf.Min(r, maxMapPos.y - p.y + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.right, Int2.downRight, r, Mathf.Min(r, maxMapPos.x - p.x + 1), Mathf.Min(r, p.y + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.down, Int2.downRight, r, Mathf.Min(r, p.y + 1), Mathf.Min(r, maxMapPos.x - p.x + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.down, Int2.downLeft, r, Mathf.Min(r, p.y + 1), Mathf.Min(r, p.x + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.left, Int2.downLeft, r, Mathf.Min(r, p.x + 1), Mathf.Min(r, p.y + 1));
                        ComputeLOSOctant(lightRefs[p].shadowMap, w, mapP, Int2.left, Int2.upLeft, r, Mathf.Min(r, p.x + 1), Mathf.Min(r, maxMapPos.y - p.y + 1));
                        if (useRadiosity)
                        {
                            var thisStartP = new Int2(-(-radius.x - startP.x), -(-radius.y - startP.y));
                            var thisEndP = new Int2(lightInfo.size.x - (radius.x - endP.x), lightInfo.size.y - (radius.y - endP.y));
                            if ((lightInfo.size.x & 1) == 0)
                            {
                                if (lightInfo.style == LightStyle.Box)
                                {
                                    thisStartP.x++;
                                    thisEndP.x++;
                                }
                                else
                                {
                                    thisEndP.x--;
                                }
                            }
                            if ((lightInfo.size.y & 1) == 0)
                            {
                                if (lightInfo.style == LightStyle.Box)
                                {
                                    thisStartP.y++;
                                    thisEndP.y++;
                                }
                                else
                                {
                                    thisEndP.y--;
                                }
                            }
                            ComputeRadiosity(lightRefs[p].shadowMap, w, mapP, thisStartP, thisEndP);
                        }
                        lightRefs[p].losComputed = true;
                    }
                }
            }

            Int2 pos;
            Color32 mapColor = Color.white;
            Color32 lightColor = lightInfo.color;
            float inner = lightInfo.innerIntensity * lightRefs[p].intensity;
            float outerMinusInner = lightInfo.outerIntensity * lightRefs[p].intensity - inner;

            if (lightInfo.style == LightStyle.Box)
            {
                var add = Vector2.zero;
                if ((lightInfo.size.x & 1) == 0)
                {   // Even size
                    add.x = 0.5f;
                    sRadius += (lightInfo.size.x > lightInfo.size.y) ? Int2.one : Int2.zero;
                    endP.x += ((p.x + endP.x >= maxMapPos.x) ? 0 : 1);
                }
                if ((lightInfo.size.y & 1) == 0)
                {
                    add.y = 0.5f;
                    sRadius += (lightInfo.size.y > lightInfo.size.x) ? Int2.one : Int2.zero;
                    endP.y += ((p.y + endP.y >= maxMapPos.y) ? 0 : 1);
                }
                float xt, yt;
                if (lightInfo.shadowType != ShadowType.None)
                {
                    var shadowMap = lightRefs[p].shadowMap;
                    Int2 shadowPos;
                    int w = ((lightInfo.radiusR.x > lightInfo.radiusR.y) ? lightInfo.radiusR.x : lightInfo.radiusR.y) * 2 + 1;
                    byte litVal;
                    for (int y = startP.y; y <= endP.y; y++)
                    {
                        yt = y - add.y;
                        yt = inner + outerMinusInner * ((yt > 0.0f ? yt : -yt) / radius.y);
                        for (int x = startP.x; x <= endP.x; x++)
                        {
                            shadowPos.x = sRadius.x + x;
                            shadowPos.y = sRadius.y + y;
                            litVal = shadowMap[shadowPos.x + shadowPos.y * w];
                            if (litVal > 0)
                            {
                                xt = x - add.x;
                                xt = inner + outerMinusInner * ((xt > 0.0f ? xt : -xt) / radius.x);
                                pos.x = p.x + x;
                                pos.y = p.y + y;
                                ColorBlend(ref pos, ref lightColor, ref mapColor, (xt > yt ? yt : xt) * (litVal == 2 ? 0.6f : 1.0f));
                            }
                        }
                    }
                }
                else
                {
                    for (int y = startP.y; y <= endP.y; y++)
                    {
                        yt = y - add.y;
                        yt = inner + outerMinusInner * ((yt > 0.0f ? yt : -yt) / radius.y);
                        for (int x = startP.x; x <= endP.x; x++)
                        {
                            xt = x - add.x;
                            xt = inner + outerMinusInner * ((xt > 0.0f ? xt : -xt) / radius.x);
                            pos.x = p.x + x;
                            pos.y = p.y + y;
                            ColorBlend(ref pos, ref lightColor, ref mapColor, xt > yt ? yt : xt);
                        }
                    }
                }
            }
            else
            {   // LightStyle.Radial
                float radiusSquare = radius.x * radius.x * radius.y * radius.y + radius.x * radius.y;   // Add a bit for better-looking circles
                float d;
                if (lightInfo.shadowType != ShadowType.None)
                {
                    var shadowMap = lightRefs[p].shadowMap;
                    Int2 shadowPos;
                    int w = ((lightInfo.radiusR.x > lightInfo.radiusR.y) ? lightInfo.radiusR.x : lightInfo.radiusR.y) * 2 + 1;
                    byte litVal;
                    for (int y = startP.y; y <= endP.y; y++)
                    {
                        for (int x = startP.x; x <= endP.x; x++)
                        {
                            d = x * x * radius.y * radius.y + y * y * radius.x * radius.x;
                            if (d <= radiusSquare)
                            {
                                shadowPos.x = sRadius.x + x;
                                shadowPos.y = sRadius.y + y;
                                litVal = shadowMap[shadowPos.x + shadowPos.y * w];
                                if (litVal > 0)
                                {
                                    pos.x = p.x + x;
                                    pos.y = p.y + y;
                                    ColorBlend(ref pos, ref lightColor, ref mapColor, (inner + outerMinusInner * (d / radiusSquare)) * (litVal == 2 ? (1.5f - (d / radiusSquare)) * .5f : 1.0f));
                                }
                            }
                        }
                    }
                }
                else
                {   // Not shadow caster
                    for (int y = startP.y; y <= endP.y; y++)
                    {
                        for (int x = startP.x; x <= endP.x; x++)
                        {
                            d = x * x * radius.y * radius.y + y * y * radius.x * radius.x;
                            if (d <= radiusSquare)
                            {
                                pos.x = p.x + x;
                                pos.y = p.y + y;
                                ColorBlend(ref pos, ref lightColor, ref mapColor, inner + outerMinusInner * (d / radiusSquare));
                            }
                        }
                    }
                }
            }
        }

        void ComputeLOSOctant(byte[] shadow, int w, Int2 mapPos, Int2 dir, Int2 angleDir, int radius, int jCount, int iCount)
        {
            int jStart = 1;
            int blockedPos = radius + 1;
            int jAdd = radius;
            bool blocked = false;
            Int2 origin = new Int2(radius - 1, radius - 1);
            Int2 p;
            int iStart = 0;
            byte lit = 1;   // Partially-lit = 2, set in ComputeRadiosity
            byte unlit = 0;
            for (int i = iStart; i < iCount; i++)
            {   // i = "across", j = "up", actual orientation depends on dir and angleDir
                p = origin;
                bool hitCollider = false;
                for (int j = jStart++; j <= jCount; j++)
                {
                    shadow[p.x + p.y * w] = (j >= blockedPos) ? unlit : lit;
                    if (!hitCollider && GetCollider(mapPos + p))
                    {
                        if (j < blockedPos)
                        {
                            blockedPos = j;
                        }
                        hitCollider = true;
                        blocked = true;
                        int thisAdd = (int)Mathf.Lerp(1, radius, Mathf.InverseLerp(jStart, radius, blockedPos));    // Shallower angle for corners farther away
                        if (thisAdd < jAdd)
                        {
                            jAdd = thisAdd;
                        }
                    }
                    p += dir;
                }
                origin += angleDir;
                if (blocked)
                {
                    blockedPos += jAdd;
                }
            }
        }

        void ComputeRadiosity(byte[] shadow, int w, Int2 mapPos, Int2 start, Int2 end)
        {
            Int2 p;
            Int2 right = Int2.right;
            Int2 up = Int2.up;
            for (p.y = start.y; p.y < end.y; p.y++)
            {
                for (p.x = start.x; p.x < end.x; p.x++)
                {
                    if (shadow[p.x + p.y * w] == 1)
                    {
                        if (p.x < end.x - 1 && shadow[p.x + 1 + p.y * w] == 0 && !GetCollider(mapPos + p))
                        {
                            shadow[p.x + 1 + p.y * w] = 2;
                        }
                        if (p.y < end.y - 1 && shadow[p.x + (p.y + 1) * w] == 0 && !GetCollider(mapPos + p))
                        {
                            shadow[p.x + (p.y + 1) * w] = 2;
                        }
                    }
                    else if (shadow[p.x + p.y * w] == 0)
                    {
                        if (p.x < end.x - 1 && shadow[p.x + 1 + p.y * w] == 1 && !GetCollider(mapPos + p + right))
                        {
                            shadow[p.x + p.y * w] = 2;
                        }
                        if (p.y < end.y - 1 && shadow[p.x + (p.y + 1) * w] == 1 && !GetCollider(mapPos + p + up))
                        {
                            shadow[p.x + p.y * w] = 2;
                        }
                    }
                }
            }
        }

        void ColorBlend(ref Int2 pos, ref Color32 c1, ref Color32 c2, float t)
        {
            GetColor(ref pos, ref c2);
            float r = c1.r * t + c2.r;
            float g = c1.g * t + c2.g;
            float b = c1.b * t + c2.b;
            float a = c1.a * t + c2.a;
            c2.r = (r > 255) ? (byte)255 : (byte)r;
            c2.g = (g > 255) ? (byte)255 : (byte)g;
            c2.b = (b > 255) ? (byte)255 : (byte)b;
            c2.a = (a > 255) ? (byte)255 : (byte)a;
            SetColor(pos, c2);
        }

        public Int2 GetStartPosClamped(Int2 p, LightInfo lightInfo)
        {
            return new Int2(Mathf.Max(0, p.x - lightInfo.radius.x), Mathf.Max(0, p.y - lightInfo.radius.y));
        }

        public Int2 GetEndPosClamped(Int2 p, LightInfo lightInfo)
        {
            return new Int2(Mathf.Min(maxMapPos.x, p.x + lightInfo.radiusR.x), Mathf.Min(maxMapPos.y, p.y + lightInfo.radiusR.y));
        }

        public void ResetLighting(List<LightInfo> lightList)
        {
            SetupColors();
            SetColorBlock(Int2.zero, maxMapPos, ambientColor);
            foreach (var item in lightRefs)
            {
                if (item.Value.number >= lightList.Count)
                {
                    continue;
                }
                item.Value.losComputed = false;
                ComputeLight(item.Key, lightList[item.Value.number], Int2.zero, maxMapPos);
            }
        }

        public void ResetShadowCastLights(List<LightInfo> lightList, bool redrawLights)
        {
            SetupColors();
            foreach (var item in lightRefs)
            {
                if (lightList[item.Value.number].shadowType != ShadowType.None)
                {
                    item.Value.losComputed = false;
                    if (redrawLights)
                    {
                        SetLight(item.Key, false, item.Value.number, 0.0f, lightList, true);
                        SetLight(item.Key, true, item.Value.number, item.Value.intensity, lightList, true);
                    }
                }
            }
        }

        public bool AreaContainsColliders(Int2 p1, Int2 p2)
        {
            Int2 p;
            for (p.y = p1.y; p.y <= p2.y; p.y++)
            {
                for (p.x = p1.x; p.x <= p2.x; p.x++)
                {
                    if (GetCollider(p))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool AreaContainsLighting(Int2 p1, Int2 p2)
        {
            Int2 p;
            for (p.y = p1.y; p.y <= p2.y; p.y++)
            {
                for (p.x = p1.x; p.x <= p2.x; p.x++)
                {
                    if (lightRefs.ContainsKey(p))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool RemoveTile(int set, int tile)
        {
            bool changed = false;
            int t = set * _tileShift + tile;
            int maxT = (set + 1) * _tileShift;
            int end = map.Length;
            for (int i = 0; i < end; i++)
            {
                if (map[i] == t)
                {
                    map[i] = -1;
                    changed = true;
                }
                else if (map[i] > t && map[i] < maxT)
                {
                    map[i]--;
                    changed = true;
                }
            }
            return changed;
        }

        public void DeleteSpriteSetTiles(int set)
        {
            short t1 = (short)(set * _tileShift);
            short t2 = (short)(set * _tileShift + _tileShift);
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i] >= t1 && map[i] < t2)
                {
                    map[i] = -1;
                }
                else if (map[i] >= t2)
                {
                    map[i] -= (short)_tileShift;
                }
            }
        }

        public void Clamp(ref Int2 p1, ref Int2 p2)
        {
            p1.x = Mathf.Clamp(p1.x, 0, mapSize.x - 1);
            p1.y = Mathf.Clamp(p1.y, 0, mapSize.y - 1);
            p2.x = Mathf.Clamp(p2.x, 0, mapSize.x - 1);
            p2.y = Mathf.Clamp(p2.y, 0, mapSize.y - 1);
        }
    }

}