// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SpriteTile {

public class SpriteFile {
	public static int LEVELVERSION = 4;
	
	public static void AddToData (List<byte> bytes, object data) {
		if (data.GetType() == typeof(string)) {
			bytes.AddRange (Encoding.UTF8.GetBytes (System.Convert.ToString (data)));
		}
		else if (data.GetType() == typeof(int)) {
			AddInt (bytes, System.Convert.ToInt32 (data));
		}
		else if (data.GetType() == typeof(float)) {
			AddFloat (bytes, System.Convert.ToSingle (data));
		}
		else if (data.GetType() == typeof(bool)) {
			AddBool (bytes, System.Convert.ToBoolean (data));
		}
		else if (data.GetType() == typeof(Color32)) {
			AddColor32 (bytes, (Color32)data);
		}
		else if (data.GetType() == typeof(LayerData)) {
			var lData = data as LayerData;
			AddFloat (bytes, lData.tileSize.x);
			AddFloat (bytes, lData.tileSize.y);
			AddInt (bytes, (int)lData.scrollPos.x);
			AddInt (bytes, (int)lData.scrollPos.y);
			AddInt (bytes, lData.previewSize);
			AddFloat (bytes, lData.zPosition);
			AddInt (bytes, System.Convert.ToInt32(lData.layerLock));
			AddInt (bytes, lData.addBorder);
			AddMapData (bytes, lData.mapData);
		}
		else if (data.GetType() == typeof(SpriteGroupData)) {
			var sgData = (data as SpriteGroupData).spriteGroups;
			AddInt (bytes, sgData.Count);
			for (int i = 0; i < sgData.Count; i++) {
				var tNumbers = sgData[i].tileNumbers;
				AddInt (bytes, tNumbers.Count);
				for (int j = 0; j < tNumbers.Count; j++) {
					AddShort (bytes, (short)tNumbers[j].set);
					AddShort (bytes, (short)tNumbers[j].tile);
				}
				AddMapData (bytes, sgData[i].mapData);
				AddFloat (bytes, sgData[i].tileSize.x);
				AddFloat (bytes, sgData[i].tileSize.y);
			}
		}
		else if (data.GetType() == typeof(RandomGroupData)) {
			var rgData = (data as RandomGroupData).randomGroups;
			AddInt (bytes, rgData.Count);
			for (int i = 0; i < rgData.Count; i++) {
				var tNumbers = rgData[i];
				AddInt (bytes, tNumbers.Count);
				for (int j = 0; j < tNumbers.Count; j++) {
					AddShort (bytes, (short)tNumbers[j].set);
					AddShort (bytes, (short)tNumbers[j].tile);
				}
			}
		}
		else if (data.GetType() == typeof(TerrainGroupData)) {
			var tgData = (data as TerrainGroupData).terrainGroups;
			AddInt (bytes, tgData.Count);
			for (int i = 0; i < tgData.Count; i++) {
				AddBool (bytes, tgData[i].active);
				int tileArraysCount = tgData[i].tileNumbers.Count;
				AddInt (bytes, tileArraysCount);
				for (int j = 0; j < tileArraysCount; j++) {
					var tNumbers = tgData[i].tileNumbers[j];
					AddInt (bytes, tNumbers.Length);
					for (int k = 0; k < tNumbers.Length; k++) {
						AddShort (bytes, (short)tNumbers[k].set);
						AddShort (bytes, (short)tNumbers[k].tile);
					}
				}
			}
		}
		else if (data.GetType() == typeof(Bookmark[])) {
			var bookmarks = data as Bookmark[];
			AddInt (bytes, bookmarks.Length);
			for (int i = 0; i < bookmarks.Length; i++) {
				AddFloat (bytes, bookmarks[i].scrollPos.x);
				AddFloat (bytes, bookmarks[i].scrollPos.y);
				AddInt (bytes, bookmarks[i].zoom);
				AddBool (bytes, bookmarks[i].defined);
			}
		}
		else if (data.GetType() == typeof(List<int>)) {
			var intList = data as List<int>;
			AddInt (bytes, intList.Count);
			for (int i = 0; i < intList.Count; i++) {
				AddInt (bytes, intList[i]);
			}
		}
		else if (data.GetType() == typeof(List<LightInfo>)) {
			var lightList = data as List<LightInfo>;
			AddInt (bytes, lightList.Count);
			for (int i = 0; i < lightList.Count; i++) {
				AddByte (bytes, (byte)lightList[i].style);
				AddInt (bytes, lightList[i].size.x);
				AddInt (bytes, lightList[i].size.y);
				AddColor32 (bytes, lightList[i].color);
				AddFloat (bytes, lightList[i].innerIntensity);
				AddFloat (bytes, lightList[i].outerIntensity);
				AddByte (bytes, (byte)lightList[i].shadowType);
			}
		}
	}
	
	public static List<List<MapData>> LoadGroupSets (byte[] bytes, ref int fIdx, int fileVersion, TileManager tileManager) {
		int numberOfSets = GetInt (bytes, ref fIdx);
		var thisGroupSets = new List<List<MapData>>(numberOfSets);
		for (int i = 0; i < numberOfSets; i++) {
			if (GetUTF8String (bytes, ref fIdx, 7) != "grpdata") {
				return null;
			}
			int groupCount = GetInt (bytes, ref fIdx);
			var thisGroups = new List<MapData>(groupCount);
			for (int j = 0; j < groupCount; j++) {
				// Skip tileNumbers
				int tNumberCount = GetInt (bytes, ref fIdx);
				fIdx += 4 * tNumberCount;
				// MapData
				thisGroups.Add (GetMapData (bytes, ref fIdx, fileVersion, tileManager));
				if (fileVersion < 3) {
					// Skip texture
					int texSizeX = SpriteFile.GetInt (bytes, ref fIdx);
					int texSizeY = SpriteFile.GetInt (bytes, ref fIdx);
					fIdx += (texSizeX * texSizeY) * 4;
				}
				else {
					// Skip tileSize
					fIdx += 8;
				}
			}
			thisGroupSets.Add (thisGroups);
		}
		return thisGroupSets;
	}

	public static List<RandomGroupData> LoadRandomGroups (byte[] bytes, ref int fIdx) {
		int numberOfSets = GetInt (bytes, ref fIdx);
		var thisRandomSets = new List<RandomGroupData>(numberOfSets);
		for (int i = 0; i < numberOfSets; i++) {
			if (GetUTF8String (bytes, ref fIdx, 7) != "rnddata") {
				return null;
			}
			int rgDataCount = GetInt (bytes, ref fIdx);
			var thisRGDatas = new List<List<TileInfo>>(rgDataCount);
			for (int j = 0; j < rgDataCount; j++) {
				int tNumberCount = GetInt (bytes, ref fIdx);
				var tNumbers = new List<TileInfo>(tNumberCount);
				for (int k = 0; k < tNumberCount; k++) {
					tNumbers.Add (new TileInfo(GetShort (bytes, ref fIdx), GetShort (bytes, ref fIdx)) );
				}
				thisRGDatas.Add (tNumbers);
			}
			thisRandomSets.Add (new RandomGroupData(thisRGDatas));
		}
		return thisRandomSets;
	}

	public static List<TerrainGroupData> LoadTerrainGroups (byte[] bytes, ref int fIdx) {
		int numberOfSets = GetInt (bytes, ref fIdx);
		var thisTerrainSets = new List<TerrainGroupData>(numberOfSets);
		for (int i = 0; i < numberOfSets; i++) {
			if (GetUTF8String (bytes, ref fIdx, 7) != "terdata") {
				return null;
			}
			int tgDataCount = GetInt (bytes, ref fIdx);
			var thisTGDatas = new List<TerrainGroup>(tgDataCount);
			for (int j = 0; j < tgDataCount; j++) {
				bool tActive = GetBool (bytes, ref fIdx);
				int tNumberArrayCount = GetInt (bytes, ref fIdx);
				var tNumbersList = new List<TileInfo[]>(tNumberArrayCount);
				for (int k = 0; k < tNumberArrayCount; k++) {
					int arrayLength = GetInt (bytes, ref fIdx);
					var tInfoArray = new TileInfo[arrayLength];
					for (int ii = 0; ii < arrayLength; ii++) {
						tInfoArray[ii] = new TileInfo(GetShort (bytes, ref fIdx), GetShort (bytes, ref fIdx));
					}
					tNumbersList.Add (tInfoArray);
				}
				thisTGDatas.Add (new TerrainGroup(tNumbersList, tActive));
			}
			thisTerrainSets.Add (new TerrainGroupData(thisTGDatas));
		}
		return thisTerrainSets;
	}
	
	static void AddMapData (List<byte> bytes, MapData mData) {
		AddInt (bytes, mData.mapSize.x);
		AddInt (bytes, mData.mapSize.y);
		AddInt (bytes, mData.propertyDictionary.Count);
		AddInt (bytes, mData.lightRefs.Count);
		AddColor32 (bytes, mData.ambientColor);
		AddBool (bytes, mData.useRadiosity);
		
		int w = mData.mapSize.x;
		// Write from bottom up so it's in the correct order (0, 0 should be lower left)
		for (int y = 0; y < mData.mapSize.y; y++) {
			for (int x = 0; x < w; x++) {
				int pos = x + y*w;
				AddShort (bytes, mData.map[pos]);
				AddUshort (bytes, mData.info[pos]);
				AddShort (bytes, mData.orderData[pos]);
				AddByte (bytes, mData.triggerData[pos]);
			}
		}
		
		foreach (var item in mData.propertyDictionary) {
			AddInt (bytes, item.Key.x);
			AddInt (bytes, item.Key.y);
			AddFloat (bytes, item.Value.f);
			AddString (bytes, item.Value.s);
			AddInt (bytes, item.Value.guid);
		}
		
		foreach (var item in mData.lightRefs) {
			AddInt (bytes, item.Key.x);
			AddInt (bytes, item.Key.y);
			AddInt (bytes, item.Value.number);
			AddFloat (bytes, item.Value.intensity);
		}
	}

	static void AddByte (List<byte> bytes, byte b) {
		bytes.Add (b);
	}

	static void AddInt (List<byte> bytes, int i) {
		bytes.AddRange (System.BitConverter.GetBytes (i));
	}

	static void AddFloat (List<byte> bytes, float f) {
		bytes.AddRange (System.BitConverter.GetBytes (f));
	}

	static void AddShort (List<byte> bytes, short s) {
		bytes.AddRange (System.BitConverter.GetBytes (s));
	}
	
	static void AddUshort (List<byte> bytes, ushort u) {
		bytes.AddRange (System.BitConverter.GetBytes (u));
	}
	
	static void AddBool (List<byte> bytes, bool b) {
		bytes.Add (System.Convert.ToByte (b));
	}
	
	static void AddColor32 (List<byte> bytes, Color32 c) {
		bytes.Add (c.r);
		bytes.Add (c.g);
		bytes.Add (c.b);
		bytes.Add (c.a);
	}
	
	static void AddString (List<byte> bytes, string s) {
		if (System.String.IsNullOrEmpty (s)) {
			AddInt (bytes, 0);
			return;
		}
		var sBytes = Encoding.Unicode.GetBytes (s);
		AddInt (bytes, sBytes.Length);
		bytes.AddRange (sBytes);
	}
	
	public static void CopyIntToListAtIndex (int value, List<byte> bytes, int index) {
		var intBytes = System.BitConverter.GetBytes (value);
		for (int i = 0; i < 4; i++) {
			bytes[index++] = intBytes[i];
		}
	}
	
	public static MapData GetMapData (byte[] bytes, ref int fIdx, int fileVersion, TileManager tileManager) {
		var thisMapSize = new Int2(GetInt (bytes, ref fIdx), GetInt (bytes, ref fIdx));
		int pDictionaryCount = 0;
		int lReferencesCount = 0;
		Color32 ambientColor = Color.white;
		bool useRadiosity = false;
		if (fileVersion >= 4) {
			pDictionaryCount = GetInt (bytes, ref fIdx);
			lReferencesCount = GetInt (bytes, ref fIdx);
			ambientColor = GetColor32 (bytes, ref fIdx);
			useRadiosity = GetBool (bytes, ref fIdx);
		}
		
		var thisMapData = new MapData(thisMapSize, false);
		int w = thisMapSize.x;
		for (int y = 0; y < thisMapSize.y; y++) {
			for (int x = 0; x < w; x++) {
				int pos = x + y*w;
				thisMapData.map[pos] = GetShort (bytes, ref fIdx);
				thisMapData.info[pos] = GetUshort (bytes, ref fIdx);
				thisMapData.orderData[pos] = GetShort (bytes, ref fIdx);
				thisMapData.triggerData[pos] = GetByte (bytes, ref fIdx);
			}
		}
		
		thisMapData.propertyDictionary = new Dictionary<Int2, PropertyInfo>();
		for (int i = 0; i < pDictionaryCount; i++) {
			var key = new Int2(GetInt (bytes, ref fIdx), GetInt (bytes, ref fIdx));
			var value = new PropertyInfo();
			value.f = GetFloat (bytes, ref fIdx);
			value.s = GetString (bytes, ref fIdx);
			value.guid = GetInt (bytes, ref fIdx);
			value.go = tileManager.GetGameObject (value.guid);
			thisMapData.propertyDictionary[key] = value;
		}
		
		thisMapData.lightRefs = new Dictionary<Int2, Light>();
		for (int i = 0; i < lReferencesCount; i++) {
			var key = new Int2(GetInt (bytes, ref fIdx), GetInt (bytes, ref fIdx));
			thisMapData.lightRefs[key] = new Light(GetInt (bytes, ref fIdx), GetFloat (bytes, ref fIdx));
		}
		
		thisMapData.ambientColor = ambientColor;
		thisMapData.useRadiosity = useRadiosity;
		return thisMapData;
	}
	
	public static List<LightInfo> GetLightInfoList (byte[] bytes, ref int fIdx) {
		int listSize = GetInt (bytes, ref fIdx);
		var lightList = new List<LightInfo>(listSize);
		for (int i = 0; i < listSize; i++) {
			lightList.Add (new LightInfo((LightStyle)GetByte (bytes, ref fIdx),
										 new Int2(GetInt (bytes, ref fIdx), GetInt (bytes, ref fIdx)),
										 GetColor32 (bytes, ref fIdx),
										 GetFloat (bytes, ref fIdx),
										 GetFloat (bytes, ref fIdx),
										 (ShadowType)GetByte (bytes, ref fIdx)));
		}
		return lightList;
	}
	
	public static string GetTag (byte[] bytes, ref int tIdx, ref int fIdx) {
		string s = GetUTF8String (bytes, ref tIdx, 7);
		fIdx = GetInt (bytes, ref tIdx);
		return s;
	}
	
	public static byte GetByte (byte[] bytes, ref int fIdx) {
		return bytes[fIdx++];
	}
	
	public static int GetInt (byte[] bytes, ref int fIdx) {
		int i = System.BitConverter.ToInt32 (bytes, fIdx);
		fIdx += 4;
		return i;
	}

	public static float GetFloat (byte[] bytes, ref int fIdx) {
		float f = System.BitConverter.ToSingle (bytes, fIdx);
		fIdx += 4;
		return f;
	}

	public static short GetShort (byte[] bytes, ref int fIdx) {
		short s = System.BitConverter.ToInt16 (bytes, fIdx);
		fIdx += 2;
		return s;
	}

	public static ushort GetUshort (byte[] bytes, ref int fIdx) {
		ushort u = System.BitConverter.ToUInt16 (bytes, fIdx);
		fIdx += 2;
		return u;
	}

	public static bool GetBool (byte[] bytes, ref int fIdx) {
		return System.Convert.ToBoolean (bytes[fIdx++]);
	}
	
	public static Color32 GetColor32 (byte[] bytes, ref int fIdx) {
		return new Color32(bytes[fIdx++], bytes[fIdx++], bytes[fIdx++], bytes[fIdx++]);
	}

	public static string GetUTF8String (byte[] bytes, ref int fIdx, int length) {
		string s = Encoding.UTF8.GetString (bytes, fIdx, length);
		fIdx += length;
		return s;
	}
	
	public static string GetString (byte[] bytes, ref int fIdx) {
		int sLen = GetInt (bytes, ref fIdx);
		if (sLen == 0) {
			return null;
		}
		string s = Encoding.Unicode.GetString (bytes, fIdx, sLen);
		fIdx += sLen;
		return s;
	}
	
	public static bool MatchTag (string line, string tag, ref string matchString) {
		var match = Regex.Match (line, tag+"\\s*=\\s*\"(.*?)\"", RegexOptions.IgnoreCase);
		matchString = match.Groups[1].ToString();
		return match.Success;
	}
}

}