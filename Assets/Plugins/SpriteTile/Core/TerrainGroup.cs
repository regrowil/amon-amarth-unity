// Version 2.13
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using UnityEngine;
using System.Collections.Generic;

namespace SpriteTile {

public class TerrainGroup {
	public List<TileInfo[]> tileNumbers;
	public bool active;
	public bool oldActive;
	
	public static TileInfo defaultTile = new TileInfo(0, -10);
	
	public TerrainGroup () {
		this.tileNumbers = new List<TileInfo[]>();
		this.tileNumbers.Add (new TileInfo[13]);
		for (int i = 0; i < this.tileNumbers[0].Length; i++) {
			this.tileNumbers[0][i] = TerrainGroup.defaultTile;
		}
		this.active = false;
		this.oldActive = false;
	}

	public TerrainGroup (List<TileInfo[]> tileNumbersList, bool active) {
		this.tileNumbers = new List<TileInfo[]>();
		for (int i = 0; i < tileNumbersList.Count; i++) {
			if (tileNumbersList[i].Length != 13) {
				Debug.LogError ("TerrainGroup: tileNumbers array length must be 13");
				return;
			}
			this.tileNumbers.Add (tileNumbersList[i]);
		}
		this.active = active;
		this.oldActive = active;
	}
	
	public bool Changed () {
		if (tileNumbers == null) {
			return false;
		}
		for (int j = 0; j < tileNumbers.Count; j++) {
			for (int i = 0; i < tileNumbers[j].Length; i++) {
				if (tileNumbers[j][i] != TerrainGroup.defaultTile) {
					return true;
				}
			}
		}
		return false;
	}
	
	public bool Contains (TileInfo tInfo) {
		if (tileNumbers == null) {
			return false;
		}
		for (int i = 0; i < tileNumbers.Count; i++) {
			if (System.Array.IndexOf (tileNumbers[i], tInfo) != -1) {
				return true;
			}
		}
		return false;
	}
}

public class TerrainGroupData {
	public List<TerrainGroup> terrainGroups;
	public int selectedGroup;
	
	public TerrainGroupData () {
		terrainGroups = new List<TerrainGroup>();
		selectedGroup = 0;
	}
	
	public TerrainGroupData (List<TerrainGroup> terrainGroups) {
		this.terrainGroups = terrainGroups;
		selectedGroup = 0;
	}
}

public class TerrainLookup {
	public Int2[] fills;
	public Int2[] empties;
	
	public TerrainLookup (Int2[] fills, Int2[] empties) {
		this.fills = fills;
		this.empties = empties;
	}
	
	public bool FillsContain (Int2 p, MapData map, TerrainGroup tGroup) {
		for (int i = 0; i < fills.Length; i++) {
			if (!tGroup.Contains (CheckTile (p + fills[i], map))) {
				return false;
			}
		}
		return true;
	}

	public bool EmptiesContain (Int2 p, MapData map, TerrainGroup tGroup) {
		for (int i = 0; i < empties.Length; i++) {
			if (tGroup.Contains (CheckTile (p + empties[i], map))) {
				return true;
			}
		}
		return false;
	}
	
	static TileInfo CheckTile (Int2 p, MapData map) {
		if (p.x < 0 || p.y < 0 || p.x >= map.mapSize.x || p.y >= map.mapSize.y) {
			return new TileInfo(-1, -1);
		}
		return map.GetTile (p);
	}
	
	public static TerrainLookup[] InitializeTerrainLookups () {
		var terrainLookups = new TerrainLookup[13];
		terrainLookups[0] = new TerrainLookup(new[] {Int2.right, Int2.down, Int2.downRight}, new[] {Int2.left, Int2.up});
		terrainLookups[1] = new TerrainLookup(new[] {Int2.left, Int2.down, Int2.downLeft}, new[] {Int2.right, Int2.up});
		terrainLookups[2] = new TerrainLookup(new[] {Int2.right, Int2.up, Int2.upRight}, new[] {Int2.left, Int2.down});
		terrainLookups[3] = new TerrainLookup(new[] {Int2.left, Int2.up, Int2.upLeft}, new[] {Int2.right, Int2.down});
		terrainLookups[4] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.up, Int2.down}, new[] {Int2.downRight});
		terrainLookups[5] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.up, Int2.down}, new[] {Int2.downLeft});
		terrainLookups[6] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.up, Int2.down}, new[] {Int2.upRight});
		terrainLookups[7] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.up, Int2.down}, new[] {Int2.upLeft});
		terrainLookups[8] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.down, Int2.downLeft, Int2.downRight}, new[] {Int2.up});
		terrainLookups[9] = new TerrainLookup(new[] {Int2.right, Int2.up, Int2.down, Int2.upRight, Int2.downRight}, new[] {Int2.left});
		terrainLookups[10] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.up, Int2.upLeft, Int2.upRight}, new[] {Int2.down});
		terrainLookups[11] = new TerrainLookup(new[] {Int2.left, Int2.up, Int2.down, Int2.upLeft, Int2.downLeft}, new[] {Int2.right});
		terrainLookups[12] = new TerrainLookup(new[] {Int2.left, Int2.right, Int2.up, Int2.down, Int2.upLeft, Int2.upRight, Int2.downLeft, Int2.downRight}, new Int2[0]);
		return terrainLookups;
	}
}

}