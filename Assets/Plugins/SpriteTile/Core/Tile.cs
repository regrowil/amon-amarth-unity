// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace SpriteTile {

public enum LayerLock {None, X, Y, XandY}
public enum AnimType {Loop, Reverse, PingPong}
public enum Dispose {Destroy, Deactivate, LeaveAlone}
public enum CamRotation {X, Y, Z, None}
public enum Flip {None, X, Y, XY}
public enum LightStyle {Box = 0, Radial = 1}
public enum ShadowType {None = 0, Fast = 1}

public class Tile {
	static Camera[] cams;
	static Transform[] camTransforms;
	static float[] previousCamPosZs;
	static TileSceneManager sceneManager;
	static bool _useTrueColor = true;
	public static bool useTrueColor {
		get {return _useTrueColor;}
	}
	static bool willDisableSceneManager = false;
	
	static List<TileLayer> tileLayers;
	static List<List<List<List<SpriteCached>>>> tileSprites;
	static List<List<Sprite>> spriteSets;
	static int[][] tileRefSets;
	static int[][] tileRefReverseSets;
	static List<List<ColliderData>> colliderSets;
	static List<List<MapData>> groupSets;
	
	static List<RandomGroupData> randomGroupSets;
	static List<TileInfo> randomGroup;
	static bool useRandomGroup = false;
	
	static List<TerrainGroupData> terrainGroupSets;
	static List<Int2> terrainGroupActiveList;
	static bool useTerrainGroup = false;
	static TerrainLookup[] terrainLookups;
	
	static Transform colliderContainer;
	static List<List<List<PolygonCollider2D>>> colliderBlocks;
	static List<bool[][]> colliderBlockUpdates;
	static int colliderBlockSize = 5;
	static List<Material> materialList;
	static bool useLayerMaterials = false;
	static List<Material> layerMaterials;
	static Dictionary<TileInfo, Material> materialDictionary;
	static Material defaultMaterial;
	static Material opaqueMaterial;
	static Material opaqueLitMaterial;
	static Material transparentLitMaterial;
	
	static float[] tileScales;
	static float defaultScale = 1.0f;
	
	static int tileRenderLayer = 0;
	static int defaultColliderLayer = 0;
	static List<ColliderInfo> colliderInfoList;
	static string defaultColliderTag = "Untagged";
	static PhysicsMaterial2D defaultColliderMaterial;
	static bool defaultColliderTrigger = false;
	
	static bool levelInitialized = false;
	static bool spritesInitialized = false;
	static bool cameraInitialized = false;
	static bool usePhysicsColliders;
	static bool useEditorDefaults = false;
	static bool useIndividualTileMaterials = false;
	static bool setTileTypeMaterial = false;
	
	static bool[][] doesTileAnimate;
	static AnimationInfo[][] animationInfo;
	static List<TileInfo> animatedTiles;
	static int tileAnimationCount = 0;
	static List<List<AnimationPosition>> animationPositions;
	static int positionAnimationCount = 0;
	
	static List<List<Int2>> watchedTiles;
	public delegate void TileWatchFunction(Int2 p, int layer, int camNumber, bool active);
	static List<List<TileWatchFunction>> tileWatchFunctions;
	
	static string[] sortingLayerNames;
	static List<int> sortingLayerIndices;
	
	static List<LightInfo> lightList;
	static bool useRadiosity = false;
	
	static TileManager tileManager;

    // RuntimeColliders and GameObjects
    public static List<List<Transform>> Colliders;
    public static List<GameObject> RuntimeGameObjects;
    public static float CameraBorder=64;

	public const int TRIGGERNUMBER = 256;
	public const int LAYERNUMBER = 32;
	
	static bool InitializeSprites () {	
		tileManager = Resources.Load ("TileManager", typeof(TileManager)) as TileManager;
		if (tileManager == null) {
			Debug.LogError ("No TileManager found in Resources");
			return false;
		}
		sortingLayerNames = tileManager.GetSortingLayerNames();
		
		defaultMaterial = Resources.Load ("Default") as Material;
		if (defaultMaterial == null) {
			Debug.LogError ("Default material not found in Resources");
			return false;
		}
		transparentLitMaterial = Resources.Load ("Diffuse") as Material;
		if (transparentLitMaterial == null) {
			Debug.LogError ("Diffuse material not found in Resources");
			return false;
		}
		opaqueMaterial = Resources.Load ("DefaultNonTransparent") as Material;
		if (opaqueMaterial == null) {
			Debug.LogError ("DefaultNonTransparent material not found in Resources");
			return false;
		}
		opaqueLitMaterial = Resources.Load ("DiffuseNonTransparent") as Material;
		if (opaqueLitMaterial == null) {
			Debug.LogError ("DiffuseNonTransparent material not found in Resources");
			return false;
		}
		
		int setCount = tileManager.tilesetCount;
		if (setCount == 0) {
			setCount = 32;
		}
		MapData.SetCounts (setCount);
		
		int numberOfSets = tileManager.SpriteSetCount();
		spriteSets = new List<List<Sprite>>(numberOfSets);
		tileRefSets = new int[numberOfSets][];
		tileRefReverseSets = new int[numberOfSets][];
		colliderSets = new List<List<ColliderData>>(numberOfSets);
		usePhysicsColliders = false;
		for (int i = 0; i < numberOfSets; i++) {
			int numberOfSprites = tileManager.SpriteListCount (i);
			if (numberOfSets == 1 && numberOfSprites == 0) {
				Debug.LogError ("No tiles have been set up. Use the TileEditor to set up tiles.");
				return false;
			}
			var sprites = new List<Sprite>(numberOfSprites);
			var colliderData = new List<ColliderData>(numberOfSprites);
			var tileNumbers = new List<int>(numberOfSprites);
			for (int j = 0; j < numberOfSprites; j++) {
				var spriteData = tileManager.GetSpriteData (i, j);
				if (!spriteData.sprite) {
					Debug.LogWarning ("Sprite " + j + " in set " + i + " is missing");
					continue;
				}
				sprites.Add (spriteData.sprite);
				tileNumbers.Add (spriteData.realTileNumber);
				if (spriteData.usePhysics) {
					if (spriteData.paths == null || spriteData.paths.Length == 0) {
						Debug.LogWarning ("Collider set but no paths for set " + i + ", sprite " + j);
						colliderData.Add (new ColliderData(0));
					}
					else {
						int pathCount = spriteData.paths.Length;
						var points = new Vector2[pathCount][];
						for (int k = 0; k < pathCount; k++) {
							points[k] = new Vector2[spriteData.paths[k].path.Length];
							System.Array.Copy (spriteData.paths[k].path, points[k], points[k].Length);
						}
						colliderData.Add (new ColliderData(pathCount, points));
						usePhysicsColliders = true;
					}
				}
				else {
					colliderData.Add (new ColliderData(0));
				}
			}
			
			var tileRefs = new int[numberOfSprites];
			var tileRefsReverse = new int[numberOfSprites];
			for (int j = 0; j < numberOfSprites; j++) {
				tileRefs[tileNumbers[j]] = j;
				tileRefsReverse[j] = tileNumbers[j];
			}
			tileRefSets[i] = tileRefs;
			tileRefReverseSets[i] = tileRefsReverse;
			
			spriteSets.Add (sprites);
			colliderSets.Add (colliderData);
		}
		
		for (int i = 0; i < numberOfSets; i++) {
			int numberOfSprites = tileManager.SpriteListCount (i);
			for (int j = 0; j < numberOfSprites; j++) {
				var spriteData = tileManager.GetSpriteData (i, j);
				if (spriteData.useNonTransparent) {
					if (spriteData.useLighting) {
						SetTileMaterial (new TileInfo(i, j), opaqueLitMaterial, false);
					}
					else {
						SetTileMaterial (new TileInfo(i, j), opaqueMaterial, false);
					}
				}
				else if (spriteData.useLighting) {
					SetTileMaterial (new TileInfo(i, j), transparentLitMaterial, false);
				}
			}
		}
		
		return true;
	}
	
	static void SetupTileScales () {
		if (tileScales == null || tileScales.Length != tileLayers.Count) {
			tileScales = new float[tileLayers.Count];
			for (int i = 0; i < tileScales.Length; i++) {
				tileScales[i] = defaultScale;
			}
		}
	}
	
	public static void SetCamera () {
		var mainCameraObjects = GameObject.FindGameObjectsWithTag ("MainCamera");
		if (mainCameraObjects.Length == 0) {
			Debug.LogError ("Tile.SetCamera: no camera tagged 'MainCamera' found. Call SetCamera with a specific camera if you don't have one tagged 'MainCamera'.");
			return;
		}
		var mainCameras = new Camera[mainCameraObjects.Length];
		for (int i = 0; i < mainCameras.Length; i++) {
			mainCameras[i] = mainCameraObjects[i].GetComponent<Camera>();
		}
		SetCamera (mainCameras);
	}
	
	public static void SetCamera (Camera camera) {
		SetCamera (new Camera[1] {camera});
	}
	
	public static void SetCamera (Camera[] cameras) {
		bool oldCameraInitialized = (cameraInitialized && cams != null);
		if (cameras == null || cameras.Length < 1) {
			Debug.LogError ("Tile.SetCamera: Cameras array must not be null");
			return;
		}
		for (int i = 0; i < cameras.Length; i++) {
			if (cameras[i] == null) {
				Debug.LogError ("Tile.SetCamera: Camera must not be null");
				return;
			}
		}
		cams = cameras;
		camTransforms = new Transform[cameras.Length];
		previousCamPosZs = new float[cameras.Length];
		for (int i = 0; i < camTransforms.Length; i++) {
			camTransforms[i] = cameras[i].transform;
			previousCamPosZs[i] = camTransforms[i].position.z;	
		}
		cameraInitialized = true;
		if (sceneManager != null) {
			sceneManager.SetCamera (camTransforms);
		}
		if (!spritesInitialized) {
			if (!InitializeSprites()) {
				return;
			}
			spritesInitialized = true;
		}
		if (oldCameraInitialized) {	// In case SetCamera is used again in the same scene
			UpdateCamera();
		}
	}
	
	public static void CameraRotationX (float xRotation) {
		CameraRotationX (xRotation, 0);
	}
	
	public static void CameraRotationX (float xRotation, int camNumber) {
		if (!CheckCameras(camNumber)) return;
		
		xRotation = Mathf.Clamp (xRotation, -90.0f, 90.0f);
		sceneManager.CameraRotation (CamRotation.X, xRotation, camNumber);
		MoveTileCamera (Vector3.zero, camNumber);
	}
	
	public static void CameraRotationY (float yRotation) {
		CameraRotationY (yRotation, 0);
	}
	
	public static void CameraRotationY (float yRotation, int camNumber) {
		if (!CheckCameras(camNumber)) return;
		
		yRotation = Mathf.Clamp (yRotation, -90.0f, 90.0f);
		sceneManager.CameraRotation (CamRotation.Y, yRotation, camNumber);
		MoveTileCamera (Vector3.zero, camNumber);
	}
	
	public static void CameraRotationZ (float zRotation) {
		CameraRotationZ (zRotation, 0);
	}
	
	public static void CameraRotationZ (float zRotation, int camNumber) {
		if (!CheckCameras(camNumber)) return;
		
		sceneManager.CameraRotation (CamRotation.Z, zRotation, camNumber);
	}
	
	static bool CheckCameras (int camNumber) {
		if (cams == null) {
			Debug.LogError ("Tile.CameraRotation: SetCamera must be called first");
			return false;
		}
		if (camNumber < 0 || camNumber >= cams.Length) {
			Debug.LogError ("Tile.CameraRotation: camNumber must be at least 0 and < the number of cameras (" + camTransforms.Length + ")");
			return false;
		}
		if (sceneManager == null) {
			Debug.LogError ("Tile.CameraRotation: a level must be loaded or created before camera rotation can be set");
			return false;
		}
		return true;
	}
	
	static void UpdateCamera () {
		SetTileObjects (true, -1, true);
		UpdateScreenSprites (false, 0);
	}
	
	static bool SetTileObjects (bool update, int layerNumber, bool cameraChange) {
		if (sceneManager == null) {
			sceneManager = new GameObject("SpriteTileSceneManager").AddComponent<TileSceneManager>();
			sceneManager.SetCamera (camTransforms, MoveTileCamera, UpdateColliderBlocks, UpdateCamera);
			sceneManager.SetAnimationFunction (AnimateScreenTiles);
		}
		
		int layerCount = tileLayers.Count;
		bool changed = false;
		int layerStart = (layerNumber >= 0)? layerNumber : 0;
		int layerEnd = (layerNumber >= 0)? layerNumber+1 : tileLayers.Count;
		bool singleLayer = (layerNumber >= 0);
		SetupTileScales();
		var camName = "";

		// Set default sorting layers in case there are none loaded
		if (sortingLayerIndices == null) {
			sortingLayerIndices = new List<int>(tileLayers.Count);
			for (int i = 0; i < tileLayers.Count; i++) {
				sortingLayerIndices.Add (i);
			}
		}
		
		for (int cam = 0; cam < cams.Length; cam++) {
			var originalCamPos = camTransforms[cam].position;
			if (cams.Length > 1) {
				camName = " cam " + cam;
			}
			for (int layer = layerStart; layer < layerEnd; layer++) {
				if (sortingLayerIndices[layer] >= sortingLayerNames.Length) {
					Debug.LogError ("Error creating layer: requested sorting layer index " + sortingLayerIndices[layer] + ", but only " + sortingLayerNames.Length + " sorting layers exist in the project. Add new sorting layers using the \"Tags and Layers\" project setting, then use the \"Assets/Set SpriteTile Sorting Layer Names\" menu item. Or change the layers in the level to use only existing sorting layers.");
					willDisableSceneManager = true;
					return false;
				}
				
				var tLayer = tileLayers[layer];
				float tileSizeX = tLayer.layerData.tileSize.x;
				float tileSizeY = tLayer.layerData.tileSize.y;
				var tileScale = new Vector3(tileScales[layer], tileScales[layer], 1.0f);
				
				// Position camera appropriately if using layerlock
				if (cameraChange && tLayer.layerData.layerLock != LayerLock.None) {
					if (tLayer.layerData.layerLock == LayerLock.XandY) {
						camTransforms[cam].position = tLayer.storedCamPos[cam];
					}
					else if (tLayer.layerData.layerLock == LayerLock.X) {
						camTransforms[cam].position = new Vector3(tLayer.storedCamPos[cam].x, originalCamPos.y, originalCamPos.z);
					}
					else {	// LayerLock.Y
						camTransforms[cam].position = new Vector3(originalCamPos.x, tLayer.storedCamPos[cam].y, originalCamPos.z);
					}
				}
				var storedCamPos = camTransforms[cam].position;
				
				if (tLayer.offset != Vector3.zero) {
					camTransforms[cam].position -= new Vector3(tLayer.offset.x, tLayer.offset.y, 0.0f);
				}
				float relativeZ = -(camTransforms[cam].position.z - tLayer.layerData.zPosition);
				Vector3 screenLL = cams[cam].ScreenToWorldPoint (new Vector3((Screen.width+CameraBorder)*cams[cam].rect.x, Screen.height * cams[cam].rect.y, relativeZ));
				Vector3 screenUR = cams[cam].ScreenToWorldPoint (new Vector3((Screen.width + CameraBorder) * cams[cam].rect.x  + (Screen.width + CameraBorder)* cams[cam].rect.width,
																		   Screen.height * cams[cam].rect.y +Screen.height * cams[cam].rect.height,
																		   relativeZ));
                        
				int xStart = (int)Mathf.Round ((screenLL.x - tileSizeX) / tileSizeX) - tLayer.layerData.addBorder;
				int xEnd = (int)Mathf.Round ((screenUR.x + tileSizeX*2) / tileSizeX) + tLayer.layerData.addBorder;
				int yStart = (int)Mathf.Round ((screenLL.y - tileSizeY) / tileSizeY) - tLayer.layerData.addBorder;
				int yEnd = (int)Mathf.Round ((screenUR.y + tileSizeY*2) / tileSizeY) + tLayer.layerData.addBorder;
				
				var thisScreenTileCount = new Int2(xEnd-xStart, yEnd-yStart);
				if (update && !singleLayer && thisScreenTileCount == tLayer.screenTileCount[cam]) {
					camTransforms[cam].position = storedCamPos;
					continue;
				}
				changed = true;
				tLayer.screenTileCount[cam] = thisScreenTileCount;
				var mapData = tLayer.layerData.mapData;
				int w = mapData.mapSize.x;
				int h = mapData.mapSize.y;
				
				tLayer.previousTilePos[cam] = new Int2(xStart, yStart);
				
				// Create tile sprites as needed
				var tileSprs = tileSprites[cam][layer];
				int yCount = 0;
				for (int y = yStart; y < yEnd; y++) {
					List<SpriteCached> tileRowS;
					if (!update || (update && yCount >= tileSprs.Count)) {
						tileRowS = new List<SpriteCached>();
					}
					else {
						tileRowS = tileSprs[yCount];
					}
					int xCount = 0;
					for (int x = xStart; x < xEnd; x++) {
						// Add sprite if new, or if zooming out
						if (!update || (update && xCount >= tileRowS.Count)) {
							var spr = new SpriteCached("Tile " + xCount + "/" + yCount + camName);
							spr.spriteRenderer.sortingLayerName = sortingLayerNames[sortingLayerIndices[layer]];
							if (spr.spriteRenderer.sortingLayerName != sortingLayerNames[sortingLayerIndices[layer]]) {
								if (!(sortingLayerNames[sortingLayerIndices[layer]] == "Default" && spr.spriteRenderer.sortingLayerName == "")) {	// Work-around for bug in Unity 4 where the default layer returns "" instead of "Default"
									Debug.LogError ("Sorting layer name error. Please ensure that there are at least " + layerCount + " sorting layers in your project, and use the \"Assets/Set SpriteTile Sorting Layer Names\" menu item.");
									willDisableSceneManager = true;
									return false;
								}
							}
							spr.transform.gameObject.layer = tileRenderLayer;
							spr.spriteRenderer.material = defaultMaterial;
							spr.enabled = spr.spriteRenderer.enabled = false;
							spr.transform.parent = tLayer.container;
							spr.transform.localScale = tileScale;
							tileRowS.Add (spr);
						}
						xCount++;
					}
					if (!update || (update && yCount >= tileSprs.Count)) {
						tileSprs.Add (tileRowS);
					}
					else {
						if (xCount < tileRowS.Count) {
							for (int x = xCount; x < tileRowS.Count; x++) {
								tileRowS[x].enabled = tileRowS[x].spriteRenderer.enabled = false;
							}
						}
					}
					yCount++;
				}
				// Disable excess sprites if zooming in
				if (update && yCount < tileSprs.Count) {
					for (int y = yCount; y < tileSprs.Count; y++) {
						var tileRowS = tileSprs[y];
						for (int x = 0; x < tileRowS.Count; x++) {
							tileRowS[x].enabled = tileRowS[x].spriteRenderer.enabled = false;
						}
					}
				}
				
				var containerPos = tLayer.container.position;
				// Set sprite positions
				float yPos = yStart * tileSizeY;
				int ySlot = (int)Mathf.Repeat (yStart, tLayer.screenTileCount[cam].y);
				var pos = new Vector3(0.0f, 0.0f, tLayer.layerData.zPosition);
				for (int y = yStart; y < yEnd; y++) {
					float xPos = xStart * tileSizeX;
					int xSlot = (int)Mathf.Repeat (xStart, tLayer.screenTileCount[cam].x);
					for (int x = xStart; x < xEnd; x++) {
						var spr = tileSprs[ySlot][xSlot];
						if (++xSlot == tLayer.screenTileCount[cam].x) {
							xSlot = 0;
						}
						pos.x = xPos;
						pos.y = yPos;
						spr.transform.localPosition = pos;
						if (spr.enabled) {
							spr.enabled = spr.spriteRenderer.enabled = false;
						}
						xPos += tileSizeX;
					}
					yPos += tileSizeY;
					if (++ySlot == tLayer.screenTileCount[cam].y) {
						ySlot = 0;
					}
				}
				// Make sure locked layers get put back in the correct position if necessary
				if (cameraChange && tLayer.layerData.layerLock != LayerLock.None) {
					if (tLayer.layerData.layerLock == LayerLock.XandY) {
						tLayer.container.position = containerPos;
					}
					else if (tLayer.layerData.layerLock == LayerLock.X) {
						tLayer.container.position = new Vector3(containerPos.x, tLayer.offset.y, containerPos.z);
					}
					else {	// LayerLockY
						tLayer.container.position = new Vector3(tLayer.offset.x, containerPos.y, containerPos.z);
					}
				}
				else {
					tLayer.container.position = new Vector3(tLayer.offset.x, tLayer.offset.y, tLayer.container.position.z);
				}
				
				var newLowerLeft = new Int2(Mathf.Max (xStart, 0), Mathf.Max (yStart, 0));
				var newUpperRight = new Int2(Mathf.Min (xEnd-1, w-1), Mathf.Min (yEnd-1, h-1));
				// If watching tiles, see if on-screen now but wasn't before, and vice versa
				if ((watchedTiles != null && watchedTiles[layer].Count > 0)) {
					var oldLowerLeft = tileLayers[layer].pLowerLeft[cam];
					var oldUpperRight = tileLayers[layer].pUpperRight[cam];
					for (int i = 0; i < watchedTiles[layer].Count; i++) {
						var p = watchedTiles[layer][i];
						var wasOnScreen = (p.x >= oldLowerLeft.x && p.y >= oldLowerLeft.y && p.x <= oldUpperRight.x && p.y <= oldUpperRight.y);
						var onScreen = (p.x >= newLowerLeft.x && p.y >= newLowerLeft.y && p.x <= newUpperRight.x && p.y <= newUpperRight.y);
						if (onScreen && !wasOnScreen) {
							tileWatchFunctions[layer][i] (p, layer, cam, true);
						}
						else if (!onScreen && wasOnScreen) {
							tileWatchFunctions[layer][i] (p, layer, cam, false);
						}
					}
				}
				
				tLayer.leftSlot[cam] = (int)Mathf.Repeat (xStart, tLayer.screenTileCount[cam].x);
				tLayer.rightSlot[cam] = (int)Mathf.Repeat (xEnd-1, tLayer.screenTileCount[cam].x);
				tLayer.bottomSlot[cam] = (int)Mathf.Repeat (yStart, tLayer.screenTileCount[cam].y);
				tLayer.topSlot[cam] = (int)Mathf.Repeat (yEnd-1, tLayer.screenTileCount[cam].y);
				tLayer.pLowerLeft[cam] = newLowerLeft;
				tLayer.pUpperRight[cam] = newUpperRight;
				tLayer.storedCamPos[cam] = storedCamPos;
			}
		
			camTransforms[cam].position = originalCamPos;
		}
		
		if (!update) {
			SetLevelColliders();
		}
		return changed;
	}

	static void MoveTileCamera (Vector3 translation, int camNumber) {
		if (camTransforms == null) {
			Debug.LogError ("SetCamera has not been called");
			return;
		}
		
		// Update tile objects if the camera moved on Z
		Vector3 thisCamPos = camTransforms[camNumber].position;
		if (thisCamPos.z != previousCamPosZs[camNumber]) {
			previousCamPosZs[camNumber] = thisCamPos.z;
			if (SetTileObjects (true, -1, true)) {
				UpdateScreenSprites (false, 0, camNumber);
			}
		}
		
		var tInfo = new TileInfo();
		var pos = Int2.zero;
		for (int layer = 0; layer < tileLayers.Count; layer++) {
			var tLayer = tileLayers[layer];
			if (tLayer.layerData.layerLock == LayerLock.XandY) {
				tLayer.container.position += new Vector3(translation.x, translation.y, 0.0f);
				continue;
			}
			if (tLayer.layerData.layerLock == LayerLock.X) {
				tLayer.container.position += new Vector3(translation.x, 0.0f, 0.0f);
				camTransforms[camNumber].position = new Vector3(tLayer.storedCamPos[camNumber].x, thisCamPos.y, thisCamPos.z);
			}
			else if (tLayer.layerData.layerLock == LayerLock.Y) {
				tLayer.container.position += new Vector3(0.0f, translation.y, 0.0f);
				camTransforms[camNumber].position = new Vector3(thisCamPos.x, tLayer.storedCamPos[camNumber].y, thisCamPos.z);
			}
			
			camTransforms[camNumber].position -= tLayer.offset;
			Vector3 screenLL = cams[camNumber].ScreenToWorldPoint (new Vector3((Screen.width + CameraBorder) * cams[camNumber].rect.x,
																			  Screen.height  * cams[camNumber].rect.y,
																			   -(camTransforms[camNumber].position.z - tLayer.layerData.zPosition)));


                float tSizeX = tLayer.layerData.tileSize.x;
			float tSizeY = tLayer.layerData.tileSize.y;
			int thisXPos = (int)Mathf.Round ((screenLL.x - tSizeX) / tSizeX) - tLayer.layerData.addBorder;
			int thisYPos = (int)Mathf.Round ((screenLL.y - tSizeY) / tSizeY) - tLayer.layerData.addBorder;
			camTransforms[camNumber].position = thisCamPos;
			
			if (thisXPos == tLayer.previousTilePos[camNumber].x && thisYPos == tLayer.previousTilePos[camNumber].y) {
				continue;
			}
			
			var mapData = tLayer.layerData.mapData;
			int w = mapData.mapSize.x;
			int h = mapData.mapSize.y;
			var tilePos = new Vector3(0.0f, 0.0f, tLayer.layerData.zPosition);
			var tileSprs = tileSprites[camNumber][layer];
			Int2 tileCount = tLayer.screenTileCount[camNumber];
			
			tLayer.pLowerLeft[camNumber].x = Mathf.Max (thisXPos, 0);
			tLayer.pLowerLeft[camNumber].y = Mathf.Max (thisYPos, 0);
			tLayer.pUpperRight[camNumber].x = Mathf.Min (thisXPos + tLayer.screenTileCount[camNumber].x - 1, w - 1);
			tLayer.pUpperRight[camNumber].y = Mathf.Min (thisYPos + tLayer.screenTileCount[camNumber].y - 1, h - 1);
			
			bool doWatchTiles = (watchedTiles != null && watchedTiles[layer].Count > 0);
			Int2 p;
				
			// Move tile sprites on X axis
			int xStart = tLayer.previousTilePos[camNumber].x;
			if (thisXPos != tLayer.previousTilePos[camNumber].x && tLayer.layerData.layerLock != LayerLock.X) {
				int moveXDir = (thisXPos > tLayer.previousTilePos[camNumber].x)? 1 : -1;
				int xEnd = xStart + tileCount.x - 1;
				int yStart = tLayer.previousTilePos[camNumber].y;
				int yEnd = yStart + tileCount.y - 1;
				
				do {
					xStart += moveXDir;
					xEnd += moveXDir;
					var showNextColumn = true;
					int slot;
					if (moveXDir == 1) {
						tilePos.x = xEnd * tSizeX;
						p.x = xEnd;
						slot = tLayer.leftSlot[camNumber];
						if (p.x >= w) {
							showNextColumn = false;
						}
					}
					else {
						tilePos.x = xStart * tSizeX;
						p.x = xStart;
						slot = tLayer.rightSlot[camNumber];
						if (p.x < 0) {
							showNextColumn = false;
						}
					}
					
					// Recycle column
					if (showNextColumn && (p.x >= 0 && p.x < w)) {
						int yCount = tLayer.bottomSlot[camNumber];
						float yPos = yStart * tSizeY;
						for (p.y = yStart; p.y <= yEnd; p.y++) {
							var spr = tileSprs[yCount][slot];
							if (++yCount == tileCount.y) {
								yCount = 0;
							}
							tilePos.y = yPos;
							spr.transform.localPosition = tilePos;
							if (p.y >= 0 && p.y < h) {
								SetSprite (ref p, ref w, ref layer, mapData, spr, ref tInfo);
								if (doWatchTiles) {
									pos.x = p.x;
									pos.y = p.y;
									int index = watchedTiles[layer].IndexOf (pos);
									if (index >= 0) {
										tileWatchFunctions[layer][index] (pos, layer, camNumber, true);
									}
									pos.x = p.x + (-moveXDir * (xEnd - xStart + 1));
									index = watchedTiles[layer].IndexOf (pos);
									if (index >= 0) {
										tileWatchFunctions[layer][index] (pos, layer, camNumber, false);
									}
								}
							}
							yPos += tSizeY;
						}
					}
					// Disable column
					else {
						int yCount = tLayer.bottomSlot[camNumber];
						for (p.y = yStart; p.y <= yEnd; p.y++) {
							var spr = tileSprs[yCount][slot];
							if (++yCount == tileCount.y) {
								yCount = 0;
							}
							if (spr.enabled) {
								spr.enabled = spr.spriteRenderer.enabled = false;
							}
							if (doWatchTiles) {
								pos.x = p.x + (-moveXDir * (xEnd - xStart + 1));
								pos.y = p.y;
								int index = watchedTiles[layer].IndexOf (pos);
								if (index >= 0) {
									tileWatchFunctions[layer][index] (pos, layer, camNumber, false);
								}
							}
						}
					}
					
					if (moveXDir == 1) {
						if (++tLayer.leftSlot[camNumber] == tileCount.x) tLayer.leftSlot[camNumber] = 0;
						if (++tLayer.rightSlot[camNumber] == tileCount.x) tLayer.rightSlot[camNumber] = 0;
					}
					else {
						if (--tLayer.leftSlot[camNumber] < 0) tLayer.leftSlot[camNumber] = tileCount.x - 1;
						if (--tLayer.rightSlot[camNumber] < 0) tLayer.rightSlot[camNumber] = tileCount.x - 1;
					}
				} while (xStart != thisXPos);
			}
			
			// Move tile sprites on Y axis
			if (thisYPos != tLayer.previousTilePos[camNumber].y && tLayer.layerData.layerLock != LayerLock.Y) {
				int moveYDir = (thisYPos > tLayer.previousTilePos[camNumber].y)? 1 : -1;
				int yStart = tLayer.previousTilePos[camNumber].y;
				int yEnd = yStart + tileCount.y - 1;
				if (thisXPos == tLayer.previousTilePos[camNumber].x) {
					xStart = thisXPos;
				}
				int xEnd = xStart + tileCount.x - 1;
				do {
					yStart += moveYDir;
					yEnd += moveYDir;
					var showNextColumn = true;
					int slot;
					if (moveYDir == 1) {
						tilePos.y = yEnd * tSizeY;
						p.y = yEnd;
						slot = tLayer.bottomSlot[camNumber];
						if (p.y >= h) {
							showNextColumn = false;
						}
					}
					else {
						tilePos.y = yStart * tSizeY;
						p.y = yStart;
						slot = tLayer.topSlot[camNumber];
						if (p.y < 0) {
							showNextColumn = false;
						}
					}
					
					// Recycle row
					if (showNextColumn && (p.y >= 0 && p.y < h)) {
						int xCount = tLayer.leftSlot[camNumber];
						float xPos = xStart * tSizeX;
						for (p.x = xStart; p.x <= xEnd; p.x++) {
							var spr = tileSprs[slot][xCount];
							if (++xCount == tileCount.x) {
								xCount = 0;
							}
							tilePos.x = xPos;
							spr.transform.localPosition = tilePos;
							if (p.x >= 0 && p.x < w) {
								SetSprite (ref p, ref w, ref layer, mapData, spr, ref tInfo);
								if (doWatchTiles) {
									pos.x = p.x;
									pos.y = p.y;
									int index = watchedTiles[layer].IndexOf (pos);
									if (index >= 0) {
										tileWatchFunctions[layer][index] (pos, layer, camNumber, true);
									}
									pos.y = p.y + (-moveYDir * (yEnd - yStart + 1));
									index = watchedTiles[layer].IndexOf (pos);
									if (index >= 0) {
										tileWatchFunctions[layer][index] (pos, layer, camNumber, false);
									}
								}
							}
							xPos += tSizeX;
						}
					}
					// Disable row
					else {
						int xCount = tLayer.leftSlot[camNumber];
						for (p.x = xStart; p.x <= xEnd; p.x++) {
							var spr = tileSprs[slot][xCount];
							if (++xCount == tileCount.x) {
								xCount = 0;
							}
							if (spr.enabled) {
								spr.enabled = spr.spriteRenderer.enabled = false;
							}
							if (doWatchTiles) {
								pos.x = p.x;
								pos.y = p.y + (-moveYDir * (yEnd - yStart + 1));
								int index = watchedTiles[layer].IndexOf (pos);
								if (index >= 0) {
									tileWatchFunctions[layer][index] (pos, layer, camNumber, false);
								}
							}
						}
					}
					
					if (moveYDir == 1) {
						if (++tLayer.bottomSlot[camNumber] == tileCount.y) tLayer.bottomSlot[camNumber] = 0;
						if (++tLayer.topSlot[camNumber] == tileCount.y) tLayer.topSlot[camNumber] = 0;
					}
					else {
						if (--tLayer.bottomSlot[camNumber] < 0) tLayer.bottomSlot[camNumber] = tileCount.y - 1;
						if (--tLayer.topSlot[camNumber] < 0) tLayer.topSlot[camNumber] = tileCount.y - 1;
					}
				} while (yStart != thisYPos);
			}
			
			tLayer.previousTilePos[camNumber].x = thisXPos;
			tLayer.previousTilePos[camNumber].y = thisYPos;
		}
	}
	
	static void ForceCameraUpdate () {
		sceneManager.ForceUpdate();
	}
	
	static void SetSprite (ref Int2 p, ref int w, ref int layer, MapData mapData, SpriteCached spr, ref TileInfo thisTileInfo) {
		int tile = mapData.map[p.x + p.y*w];
		var noAnim = true;
		if (positionAnimationCount > 0) {
			for (int i = 0; i < animationPositions[layer].Count; i++) {
				if (animationPositions[layer][i].p.x == p.x && animationPositions[layer][i].p.y == p.y) {
					var animInfo = animationPositions[layer][i].animationInfo;
					thisTileInfo = animInfo.tileInfoArray[animInfo.frame];
					spr.spriteRenderer.sprite = spriteSets[thisTileInfo.set][thisTileInfo.tile];
					noAnim = false;
					break;
				}
			}
		}
		if (tile < 0 && noAnim) {	// Tile is empty and is not doing single tile animation
			if (spr.enabled) {
				spr.enabled = spr.spriteRenderer.enabled = false;
			}
			return;
		}
		if (tileAnimationCount > 0 && noAnim) {
			if (doesTileAnimate[tile >> MapData.setShift][tile & MapData.tileShiftI]) {
				var animInfo = animationInfo[tile >> MapData.setShift][tile & MapData.tileShiftI];
				thisTileInfo = animInfo.tileInfoArray[animInfo.frame];
				spr.spriteRenderer.sprite = spriteSets[thisTileInfo.set][thisTileInfo.tile];
				noAnim = false;
			}
		}
		if (noAnim) {
			spr.spriteRenderer.sprite = spriteSets[tile >> MapData.setShift][tile & MapData.tileShiftI];
			thisTileInfo.set = tile >> MapData.setShift;
			thisTileInfo.tile = tile & MapData.tileShiftI;
		}
		spr.storedPosition = spr.transform.localPosition;
		spr.spriteRenderer.sortingOrder = mapData.GetOrder (p);
		bool flipX = mapData.GetFlipX (p);
		bool flipY = mapData.GetFlipY (p);
		spr.transform.eulerAngles = new Vector3(flipY? 180.0f : 0.0f, flipX? 180.0f : 0.0f, mapData.GetRotation (p) );
		if (flipX || flipY) {
			SetSpriteFlipPos (spr, flipX, flipY);
		}
		if (!spr.enabled) {
			spr.enabled = spr.spriteRenderer.enabled = true;
		}
		SetSpriteMaterial (ref layer, ref p, thisTileInfo, spr);
		if (mapData.useColors) {
			spr.spriteRenderer.color = mapData.GetColor (p);
		}
	}
	
	static void SetSpriteFlipPos (SpriteCached spr, bool flipX, bool flipY) {
		float pixelsPerUnit = spr.spriteRenderer.sprite.pixelsPerUnit;
		Vector3 size = spr.spriteRenderer.sprite.bounds.size;
		Vector2 pivot = spr.spriteRenderer.sprite.pivot;
		Vector3 pos = spr.storedPosition;
		if (flipX) {
			float pivotXNormalized = ((1.0f / pixelsPerUnit) * pivot.x) / size.x;
			pos.x += (((1.0f - pivotXNormalized) * 2) - 1.0f) * size.x;
		}
		if (flipY) {
			float pivotYNormalized = ((1.0f / pixelsPerUnit) * pivot.y) / size.y;
			pos.y += (((1.0f - pivotYNormalized) * 2) - 1.0f) * size.y;
		}
		spr.transform.localPosition = pos;
	}
	
	static void SetSpriteMaterial (ref int layer, ref Int2 p, TileInfo tInfo, SpriteCached spr) {
		if (setTileTypeMaterial || useIndividualTileMaterials || useLayerMaterials) {
			spr.spriteRenderer.material = GetSpriteMaterial (ref layer, ref p, ref tInfo);
		}
		// Otherwise do nothing since we just leave it as the default material
	}
	
	static Material GetSpriteMaterial (ref int layer, ref Int2 p, ref TileInfo tInfo) {
		if (useLayerMaterials) {
			return layerMaterials[layer];
		}
		if (setTileTypeMaterial) {
			if (useIndividualTileMaterials) {
				int materialID = tileLayers[layer].layerData.mapData.GetMaterialID (p);
			 	if (materialID > 0) {
					return materialList[materialID];
				}
				else {
					return materialDictionary[tInfo];
				}
			}
			else {
				return materialDictionary[tInfo];
			}
		}
		else if (useIndividualTileMaterials) {
			return materialList[tileLayers[layer].layerData.mapData.GetMaterialID (p)];
		}
		return defaultMaterial;
	}

	public static void SetLayerPosition (Vector2 position) {
		SetLayerPosition (0, position, false);
	}

	public static void SetLayerPosition (Vector2 position, bool includeColliders) {
		SetLayerPosition (0, position, includeColliders);
	}

	public static void SetLayerPosition (int layer, Vector2 position) {
		SetLayerPosition (layer, position, false);
	}

	public static void SetLayerPosition (int layer, Vector2 position, bool includeColliders) {
		if (!CheckLayerCount (layer)) return;
		
		tileLayers[layer].offset = position;
		SetTileObjects (true, layer, false);
		if (willDisableSceneManager) return;
		UpdateScreenSprites (true, layer);
		
		if (includeColliders) {
			Vector3 v3position = position;
			for (int y = 0; y < colliderBlocks[layer].Count; y++) {
				for (int x = 0; x < colliderBlocks[layer][y].Count; x++) {
					if (colliderBlocks[layer][y][x] != null) {
						colliderBlocks[layer][y][x].transform.position = new Vector3(x * colliderBlockSize * tileLayers[layer].layerData.tileSize.x, y * colliderBlockSize * tileLayers[layer].layerData.tileSize.y, 0.0f) + v3position;
					}
				}
			}
		}
	}
	
	public static void SetColliderBlockSize (int size) {
		if (levelInitialized) {
			Debug.LogError ("SetColliderBlockSize: Collider tile size can only be set before the level is initialized");
			return;
		}
		colliderBlockSize = Mathf.Max (size, 1);
	}
	
	public static void SetTileMaterial (Material material) {
		for (int i = 0; i < tileLayers.Count; i++) {
			SetLayerMaterial (i, material);
		}
	}
	
	public static void SetLayerMaterial (int layer, Material material) {
		if (!cameraInitialized) {
			Debug.LogError ("SetLayerMaterial: SetCamera must be called first");
			return;
		}
		if (material == null) {
			Debug.LogError ("SetLayerMaterial: material should not be null");
			return;
		}
		if (!CheckParams (layer)) {
			return;
		}
		
		useLayerMaterials = true;
		useIndividualTileMaterials = false;
		if (layerMaterials == null) {
			layerMaterials = new List<Material>(tileLayers.Count);
			for (int i = 0; i < tileLayers.Count; i++) {
				layerMaterials.Add (defaultMaterial);
			}
		}
		layerMaterials[layer] = material;
		UpdateScreenSprites (true, layer);
	}
	
	public static void SetTileMaterial (Int2 p, Material material) {
		SetTileMaterial (0, p, material);
	}
	
	public static void SetTileMaterial (int layer, Int2 p, Material material) {
		if (!CheckParams (p, layer)) {
			return;
		}
		if (!cameraInitialized) {
			Debug.LogError ("SetTileMaterial: SetCamera must be called first");
			return;
		}
		if (material == null) {
			Debug.LogError ("SetTileMaterial: material should not be null");
			return;
		}
		
		useLayerMaterials = false;
		if (!useIndividualTileMaterials) {
			for (int i = 0; i < tileLayers.Count; i++) {
				tileLayers[i].layerData.mapData.SetupMaterialIDs();
			}
			materialList = new List<Material>();
			materialList.Add (defaultMaterial);
			useIndividualTileMaterials = true;
		}
		
		int materialID = materialList.IndexOf (material);
		if (materialID == -1) {
			if (materialList.Count == 256) {
				Debug.LogError ("SetTileMaterial: There can be no more than 255 materials");
				return;
			}
			materialList.Add (material);
			materialID = materialList.Count - 1;
		}
		tileLayers[layer].layerData.mapData.SetMaterialID (p, (byte)materialID);
		UpdateScreenSprites (false, 0);
	}
	
	public static void SetTileMaterial (int set, int tile, Material material) {
		SetTileMaterial (new TileInfo(set, tile), material, true);
	}
	
	public static void SetTileMaterial (TileInfo tileInfo, Material material) {
		SetTileMaterial (tileInfo, material, true);
	}
	
	static void SetTileMaterial (TileInfo tileInfo, Material material, bool updateScreenSprites) {
		if (!cameraInitialized) {
			Debug.LogError ("SetTileMaterial: SetCamera must be called first");
			return;
		}
		if (!CheckParams (tileInfo.set, tileInfo.tile)) {
			return;
		}
		if (material == null) {
			Debug.LogError ("SetTileMaterial: material should not be null");
			return;
		}
		
		useLayerMaterials = false;
		if (!setTileTypeMaterial) {
			SetDefaultMaterialEntries();
			setTileTypeMaterial = true;
		}
		materialDictionary[tileInfo] = material;
		if (levelInitialized && updateScreenSprites) {
			UpdateScreenSprites (false, 0);
		}
	}
	
	static void SetDefaultMaterialEntries () {
		materialDictionary = new Dictionary<TileInfo, Material>();
		for (int s = 0; s < spriteSets.Count; s++) {
			for (int t = 0; t < spriteSets[s].Count; t++) {
				materialDictionary[new TileInfo(s, t)] = defaultMaterial;
			}
		}
	}
	
	public static void SetTileRenderLayer (int value) {
		if (value < 0 || value >= LAYERNUMBER) {
			Debug.LogError ("SetTileRenderLayer: layer value is " + value + ", but must be >= 0 and < " + LAYERNUMBER);
			return;
		}
		
		tileRenderLayer = value;
		for (int i = 0; i < tileSprites.Count; i++) {
			for (int j = 0; j < tileSprites[i].Count; j++) {
				for (int y = 0; y < tileSprites[i][j].Count; y++) {
					for (int x = 0; x < tileSprites[i][j][y].Count; x++) {
						tileSprites[i][j][y][x].transform.gameObject.layer = value;
					}
				}
			}
		}
	}
	
	public static void SetColliderLayer (int value) {
		if (!CheckLayerValue (value)) return;
		
		defaultColliderLayer = value;
		if (colliderInfoList == null) return;
		
		for (int i = 0; i < colliderInfoList.Count; i++) {
			colliderInfoList[i].layer = value;
		}
		UpdateColliders (0, colliderBlocks.Count);
	}
	
	public static void SetColliderLayer (int value, int layer) {
		if (!CheckLayerValue (value) || !CheckLayerCount (layer)) return;
		
		colliderInfoList[layer].layer = value;
		UpdateColliders (layer, layer + 1);
	}
	
	static bool CheckLayerValue (int value) {
		if (value < 0 || value >= LAYERNUMBER) {
			Debug.LogError ("Layer value is " + value + ", but must be >= 0 and < " + LAYERNUMBER);
			return false;
		}
		return true;
	}

	public static void SetColliderTag (string tag) {
		defaultColliderTag = tag;
		if (colliderInfoList == null) return;
		
		for (int i = 0; i < colliderInfoList.Count; i++) {
			colliderInfoList[i].tag = tag;
		}
		UpdateColliders (0, colliderBlocks.Count);
	}
	
	public static void SetColliderTag (string tag, int layer) {
		if (!CheckLayerCount (layer)) return;
		
		colliderInfoList[layer].tag = tag;
		UpdateColliders (layer, layer + 1);
	}
	
	public static void SetColliderMaterial (PhysicsMaterial2D material) {
		defaultColliderMaterial = material;
		if (colliderInfoList == null) return;
		
		for (int i = 0; i < colliderInfoList.Count; i++) {
			colliderInfoList[i].material = material;
		}
		UpdateColliders (0, colliderBlocks.Count);
	}
	
	public static void SetColliderMaterial (PhysicsMaterial2D material, int layer) {
		if (!CheckLayerCount (layer)) return;
		
		colliderInfoList[layer].material = material;
		UpdateColliders (layer, layer + 1);
	}
	
	public static void SetColliderTrigger (bool isTrigger) {
		defaultColliderTrigger = isTrigger;
		if (colliderInfoList == null) return;
		
		for (int i = 0; i < colliderInfoList.Count; i++) {
			colliderInfoList[i].isTrigger = isTrigger;
		}
		UpdateColliders (0, colliderBlocks.Count);
	}
	
	public static void SetColliderTrigger (bool isTrigger, int layer) {
		if (!CheckLayerCount (layer)) return;
		
		colliderInfoList[layer].isTrigger = isTrigger;
		UpdateColliders (layer, layer + 1);
	}
	
	static void UpdateColliders (int layerStart, int layerEnd) {
		if (colliderBlocks == null) return;
		
		for (int i = layerStart; i < layerEnd; i++) {
			for (int y = 0; y < colliderBlocks[i].Count; y++) {
				for (int x = 0; x < colliderBlocks[i][y].Count; x++) {
					if (colliderBlocks[i][y][x] != null) {
						colliderBlocks[i][y][x].gameObject.layer = colliderInfoList[i].layer;
						colliderBlocks[i][y][x].gameObject.tag = colliderInfoList[i].tag;
						colliderBlocks[i][y][x].sharedMaterial = colliderInfoList[i].material;
						colliderBlocks[i][y][x].isTrigger = colliderInfoList[i].isTrigger;
						colliderBlocks[i][y][x].enabled = false;	// Work-around for Unity bug where changing collider materials won't work
						colliderBlocks[i][y][x].enabled = true;
					}
				}
			}
		}
	}
	
	static void UpdateColliderIfNeeded (Int2 p, int layer) {
		if (usePhysicsColliders && tileLayers[layer].layerData.mapData.GetCollider (p)) {
			colliderBlockUpdates[layer][(int)(p.y / colliderBlockSize)][(int)(p.x / colliderBlockSize)] = true;
			sceneManager.SetColliderUpdate();
		}
	}
	
	static bool ColliderExistsInBlock (Int2 p1, Int2 p2, int layer) {
		Int2 p;
		for (p.y = p1.y; p.y <= p2.y; p.y++) {
			for (p.x = p1.x; p.x <= p2.x; p.x++) {
				if (tileLayers[layer].layerData.mapData.GetCollider (p)) {
					return true;
				}
			}
		}
		return false;
	}
	
	static void SetLevelColliders () {
		if (colliderContainer) {
			foreach (Transform tr in colliderContainer) {
				Object.Destroy (tr.gameObject);
			}
		}
		else {
			colliderContainer = new GameObject("SpriteTileColliders").transform;
			MonoBehaviour.DontDestroyOnLoad (colliderContainer);
		}
		
		colliderBlocks = new List<List<List<PolygonCollider2D>>>();
		for (int i = 0; i < tileLayers.Count; i++) {
                Colliders.Add(new List<Transform>());
                AddColliderLayer(i);
		}
	}
	
	static void AddColliderLayer (int i) {
		colliderInfoList.Add (new ColliderInfo(defaultColliderLayer, defaultColliderTag, defaultColliderMaterial, defaultColliderTrigger));
		var thisLayerBlocks = new List<List<PolygonCollider2D>>();
		var mapData = tileLayers[i].layerData.mapData;
		int w = mapData.mapSize.x;
		int h = mapData.mapSize.y;
		for (int y = 0; y < h; y += colliderBlockSize) {
			var colliderBlockRow = new List<PolygonCollider2D>();
			for (int x = 0; x < w; x += colliderBlockSize) {
				int yEnd = Mathf.Min (y + colliderBlockSize, h);
				int xEnd = Mathf.Min (x + colliderBlockSize, w);
				colliderBlockRow.Add (MakeColliderBlock (x, y, xEnd, yEnd, w, h, tileLayers[i].layerData.tileSize, mapData, i));
			}
			thisLayerBlocks.Add (colliderBlockRow);
		}
		colliderBlocks.Add (thisLayerBlocks);
	}
	
	static PolygonCollider2D MakeColliderBlock (int x, int y, int xEnd, int yEnd, int w, int h, Vector2 tileSize, MapData mapData, int layer) {
		int totalPathCount = GetTotalPathCount (x, y, xEnd, yEnd, w, mapData);
		if (totalPathCount == 0) {
			return null;
		}
		var points = GetPathPoints (totalPathCount, x, y, xEnd, yEnd, w, tileSize, mapData);
		return MakeObjectWithCollider (totalPathCount, layer, x, y, tileSize, points);
	}
	
	static int GetTotalPathCount (int x1, int y1, int xEnd, int yEnd, int w, MapData mapData) {
		int totalPathCount = 0;
		short[] map = mapData.map;
		Int2 p;
		for (p.y = y1; p.y < yEnd; p.y++) {
			for (p.x = x1; p.x < xEnd; p.x++) {
				int tile = map[p.x + p.y*w];
				if (tile >= 0 && mapData.GetCollider (p) && colliderSets[tile >> MapData.setShift][tile & MapData.tileShiftI].pathCount > 0) {
					totalPathCount += colliderSets[tile >> MapData.setShift][tile & MapData.tileShiftI].pathCount;
				}
			}
		}
		return totalPathCount;
	}
	
	static Vector2[][] GetPathPoints (int pathCount, int xStart, int yStart, int xEnd, int yEnd, int w, Vector2 tileSize, MapData mapData) {
		var totalPoints = new Vector2[pathCount][];
		int count = 0;
		Int2 p;
		for (p.y = yStart; p.y < yEnd; p.y++) {
			for (p.x = xStart; p.x < xEnd; p.x++) {
				int tile = mapData.map[p.x + p.y*w];
				if (tile >= 0 && mapData.GetCollider (p)) {
					var thisSet = colliderSets[tile >> MapData.setShift][tile & MapData.tileShiftI];
					if (thisSet.pathCount > 0) {
						float xPos = (p.x - xStart) * tileSize.x;
						float yPos = (p.y - yStart) * tileSize.y;
						
						float rotationAngle = mapData.GetRotation (p);
						int flipX = mapData.GetFlipX (p)? -1 : 1;
						int flipY = mapData.GetFlipY (p)? -1 : 1;
						if (flipX == -1 || flipY == -1) {
							var sprite = tileManager.GetSpriteData (tile >> MapData.setShift, tile & MapData.tileShiftI).sprite;
							Vector2 pivot = sprite.pivot;
							Vector3 size = sprite.bounds.size;
							float pixelsPerUnit = sprite.pixelsPerUnit;
							if (flipX == -1) {
								float pivotXNormalized = ((1.0f / pixelsPerUnit) * pivot.x) / size.x;
								xPos += (((1.0f - pivotXNormalized) * 2) - 1.0f) * size.x;
							}
							if (flipY == -1) {
								float pivotYNormalized = ((1.0f / pixelsPerUnit) * pivot.y) / size.y;
								yPos += (((1.0f - pivotYNormalized) * 2) - 1.0f) * size.y;								
							}
						}
						float sin = 0.0f;
						float cos = 0.0f;
						if (rotationAngle != 0.0) {
							rotationAngle *= Mathf.Deg2Rad;
							sin = Mathf.Sin (rotationAngle);
							cos = Mathf.Cos (rotationAngle);
						}
						
						for (int j = 0; j < thisSet.pathCount; j++) {
							var points = thisSet.points[j];
							totalPoints[count] = new Vector2[points.Length];
							if (rotationAngle == 0.0) {
								for (int k = 0; k < points.Length; k++) {
									totalPoints[count][k].x = points[k].x * flipX + xPos;
									totalPoints[count][k].y = points[k].y * flipY + yPos;
								}
							}
							else {
								for (int k = 0; k < points.Length; k++) {
									totalPoints[count][k].x = (points[k].x * cos - points[k].y * sin) * flipX + xPos;
									totalPoints[count][k].y = (points[k].x * sin + points[k].y * cos) * flipY + yPos;
								}
							}
							count++;
						}
					}
				}
			}
		}
		return totalPoints;
	}
	
	static PolygonCollider2D MakeObjectWithCollider (int pathCount, int layer, int x, int y, Vector2 tileSize, Vector2[][] points) {
        var comboCollider = new GameObject("Collider " + layer + " : " + x + "/" + y).AddComponent<PolygonCollider2D>();
        //Save Collider in List
        Colliders[layer].Add(comboCollider.transform);
    	comboCollider.transform.position = new Vector3(x * tileSize.x, y * tileSize.y, 0);
		SetPathPoints (comboCollider, pathCount, points);
		comboCollider.transform.parent = colliderContainer;
		comboCollider.sharedMaterial = colliderInfoList[layer].material;
		comboCollider.gameObject.layer = colliderInfoList[layer].layer;
		comboCollider.gameObject.tag = colliderInfoList[layer].tag;
		comboCollider.isTrigger = colliderInfoList[layer].isTrigger;
		comboCollider.enabled = false;	// Work-around for Unity bug where changing collider materials won't work
		comboCollider.enabled = true;
		return comboCollider;
	}
	
	static void SetPathPoints (PolygonCollider2D collider, int pathCount, Vector2[][] points) {
		collider.pathCount = pathCount;
		for (int j = 0; j < pathCount; j++) {
			collider.SetPath (j, points[j]);
		}
	}
	
	static void DeleteLevelColliders () {
		for (int i = 0; i < tileLayers.Count; i++) {
			for (int y = 0; y < colliderBlocks[i].Count; y++) {
				for (int x = 0; x < colliderBlocks[i][y].Count; x++) {
					if (colliderBlocks[i][y][x] != null) {
						Object.Destroy (colliderBlocks[i][y][x].gameObject);
					}
				}
			}
		}
	}
	
	public static void LoadLevel (TextAsset level) {
		if (!CheckParams (level)) return;
		LoadLevel (level.bytes, false, false, 0);
	}

	public static void LoadLevel (byte[] bytes) {
		if (!CheckParams (bytes)) return;
		LoadLevel (bytes, false, false, 0);
	}
	
	public static void LoadGroups (TextAsset groupFile) {
		if (!CheckParams (groupFile)) return;
		LoadLevel (groupFile.bytes, true, false, 0);
	}
	
	public static void LoadGroups (byte[] bytes) {
		if (!CheckParams (bytes)) return;
		LoadLevel (bytes, true, false, 0);
	}
	
	public static MapData LoadMapBlock (byte[] bytes) {
		if (!CheckParams (bytes)) return null;
		return LoadLevel (bytes, false, true, 0);
	}

	public static MapData LoadMapBlock (byte[] bytes, int layer) {
		if (!CheckParams (bytes) || !CheckParams (layer)) return null;
		return LoadLevel (bytes, false, true, layer);
	}
	
	static MapData LoadLevel (byte[] bytes, bool loadGroup, bool loadBlock, int layer) {
		if (!cameraInitialized) {
			Debug.LogError ("Tile.LoadLevel: SetCamera must be called before LoadLevel");
			return null;
		}
		
		try {
			bytes = CLZF2.Decompress (bytes);
			int fIdx = 0;
			var idString = "SpriteTileLevel";
			if (SpriteFile.GetUTF8String (bytes, ref fIdx, idString.Length) != idString) {
				throw new System.Exception("This doesn't seem to be a SpriteTile level file");
			}
			if (bytes[fIdx++] != 1) {
				throw new System.Exception("File isn't little endian");
			}
			int thisVersion = SpriteFile.GetInt (bytes, ref fIdx);
			if (thisVersion < 1 || thisVersion > SpriteFile.LEVELVERSION) {
				throw new System.Exception("Unsupported level version: " + thisVersion + ". This version of SpriteTile can load version " + SpriteFile.LEVELVERSION + " or lower.");
			}
			
			int tagCount = 0;
			int numberOfTags = SpriteFile.GetInt (bytes, ref fIdx);
			int tIdx = fIdx;
			
			if (numberOfTags == 0) {
				throw new System.Exception("Not a complete level file");
			}
            //Delete RuntimeGameObjects
            if (RuntimeGameObjects != null)
            {
                foreach (var runtimeGameObject in RuntimeGameObjects)
                {
                    GameObject.Destroy(runtimeGameObject);
                }
            }
            RuntimeGameObjects = new List<GameObject>();
            Colliders = new List<List<Transform>>();
            List<LayerData> thisLayerData = null;
			List<List<MapData>> thisGroupSets = null;
			List<RandomGroupData> thisRandomSets = null;
			List<TerrainGroupData> thisTerrainSets = null;
			int numberOfLayers = 0;
			List<int> thisSortingLayerIndices = null;
			List<LightInfo> thisLightList = null;
			var layersFound = false;
			var groupsFound = false;
			var randomGroupsFound = false;
			var terrainGroupsFound = false;
			var sortingIndicesFound = false;
			var foundLightList = false;
			
			while (tagCount < numberOfTags) {
				var tag = SpriteFile.GetTag (bytes, ref tIdx, ref fIdx);
				tagCount++;
				if (tag == "lvlayrs") {
					numberOfLayers = SpriteFile.GetInt (bytes, ref fIdx);
					thisLayerData = new List<LayerData>(numberOfLayers);
					for (int i = 0; i < numberOfLayers; i++) {
						if (SpriteFile.GetUTF8String (bytes, ref fIdx, 7) != "lyrdata") {
							throw new System.Exception("Can't find lyrdata tag");
						}
						float thisTSizeX = SpriteFile.GetFloat (bytes, ref fIdx);
						float thisTSizeY;
						if (thisVersion == 1) {
							thisTSizeY = thisTSizeX;
						}
						else {
							thisTSizeY = SpriteFile.GetFloat (bytes, ref fIdx);
						}
						fIdx += 12;	// Skip layerScrollPos and previewSize
						float thisZPos = SpriteFile.GetFloat (bytes, ref fIdx);
						LayerLock thisLock = (LayerLock)SpriteFile.GetInt (bytes, ref fIdx);
						int thisAddBorder = SpriteFile.GetInt (bytes, ref fIdx);
						MapData thisMapData = SpriteFile.GetMapData (bytes, ref fIdx, thisVersion, tileManager);
						UpdateMapData (thisMapData);
						thisLayerData.Add (new LayerData(thisMapData, thisAddBorder, new Vector2(thisTSizeX, thisTSizeY), thisZPos, thisLock));
					}
					layersFound = true;
				}
				else if (tag == "grpsets") {
					thisGroupSets = SpriteFile.LoadGroupSets (bytes, ref fIdx, thisVersion, tileManager);
					if (thisGroupSets == null) {
						throw new System.Exception("Can't find grpdata tag");
					}
					for (int i = 0; i < thisGroupSets.Count; i++) {
						for (int j = 0; j < thisGroupSets[i].Count; j++) {
							UpdateMapData (thisGroupSets[i][j]);
						}
					}
					groupsFound = true;
				}
				else if (tag == "rndsets") {
					thisRandomSets = SpriteFile.LoadRandomGroups (bytes, ref fIdx);
					if (thisRandomSets == null) {
						throw new System.Exception("Can't find rnddata tag");
					}
					// Check all tiles in random groups
					for (int i = 0; i < thisRandomSets.Count; i++) {
						for (int j = 0; j < thisRandomSets[i].randomGroups.Count; j++) {
							for (int k = 0; k < thisRandomSets[i].randomGroups[j].Count; k++) {
								int set = thisRandomSets[i].randomGroups[j][k].set;
								int tile = thisRandomSets[i].randomGroups[j][k].tile;
								if (!CheckParams (set, tile)) {
									throw new System.Exception("Invalid tile data in random groups");
								}
								// Set tile numbers according to tileRefSets (to account for rearranged tiles)
								thisRandomSets[i].randomGroups[j][k] = new TileInfo(set, tileRefSets[set][tile]);
							}
						}
					}
					randomGroupsFound = true;
				}
				else if (tag == "tersets") {
					thisTerrainSets = SpriteFile.LoadTerrainGroups (bytes, ref fIdx);
					if (thisTerrainSets == null) {
						throw new System.Exception("Can't find terdata tag");
					}
					// Check all tiles in terrain groups
					for (int i = 0; i < thisTerrainSets.Count; i++) {
						for (int j = 0; j < thisTerrainSets[i].terrainGroups.Count; j++) {
							for (int k = 0; k < thisTerrainSets[i].terrainGroups[j].tileNumbers.Count; k++) {
								for (int ii = 0; ii < thisTerrainSets[i].terrainGroups[j].tileNumbers[k].Length; ii++) {
									int set = thisTerrainSets[i].terrainGroups[j].tileNumbers[k][ii].set;
									int tile = thisTerrainSets[i].terrainGroups[j].tileNumbers[k][ii].tile;
									if (!CheckParams (set, tile)) {
										throw new System.Exception("Invalid tile data in terrain groups");
									}
									// Set tile numbers according to tileRefSets (to account for rearranged tiles)
									thisTerrainSets[i].terrainGroups[j].tileNumbers[k][ii] = new TileInfo(set, tileRefSets[set][tile]);
								}
							}
						}
					}
					terrainGroupsFound = true;
				}
				else if (tag == "numsets") {
					int setsInFile = SpriteFile.GetInt (bytes, ref fIdx);
					if (MapData.setCount != setsInFile) {
						Debug.LogWarning ("The max number of tile sets referenced in the file (" + setsInFile + ") does not match the max number of tile sets in this project (" + MapData.setCount + "). Tiles may not be correct.");
					}
				}
				else if (tag == "srtlyrs") {
					int sortLayerCount = SpriteFile.GetInt (bytes, ref fIdx);
					thisSortingLayerIndices = new List<int>(sortLayerCount);
					for (int i = 0; i < sortLayerCount; i++) {
						thisSortingLayerIndices.Add (SpriteFile.GetInt (bytes, ref fIdx));
					}
					sortingIndicesFound = true;
				}
				else if (tag == "litinfo") {
					foundLightList = true;
					thisLightList = SpriteFile.GetLightInfoList (bytes, ref fIdx);
				}
			}
			
			if (!loadGroup) {
				if (layersFound) {
					if (!loadBlock) {
						if (sortingIndicesFound) {
							if (thisSortingLayerIndices.Count != numberOfLayers) {
								Debug.LogWarning ("Sorting layer index count does not match layer count");
								sortingLayerIndices = new List<int>(numberOfLayers);
								for (int i = 0; i < numberOfLayers; i++) {
									sortingLayerIndices.Add (i);
								}
							}
							else {
								sortingLayerIndices = thisSortingLayerIndices;
							}
							var outOfRangeError = false;
							for (int i = 0; i < numberOfLayers; i++) {
								if (sortingLayerIndices[i] >= sortingLayerNames.Length) {
									outOfRangeError = true;
									sortingLayerIndices[i] = sortingLayerNames.Length-1;
								}
							}
							if (outOfRangeError) {
								Debug.LogWarning ("Sorting layer index exceeded sorting layer names array length");
							}
						}
						
						// If a level already existed, either re-use existing stuff if the loaded level is the same size, or create new layer data otherwise
						if (levelInitialized) {
							var sameSize = true;
							if (numberOfLayers != tileLayers.Count) {
								sameSize = false;
							}
							else {
								for (int i = 0; i < numberOfLayers; i++) {
									var layerData = tileLayers[i].layerData;
									var thisData = thisLayerData[i];
									if (layerData.mapData.mapSize != thisData.mapData.mapSize || layerData.tileSize != thisData.tileSize || layerData.addBorder != thisData.addBorder || layerData.zPosition != thisData.zPosition || layerData.layerLock != thisData.layerLock) {
										sameSize = false;
										break;
									}
								}
							}
							if (!sameSize) {
								// Create new layer data
								DeleteLevelObjects();
								InitializeLevel (numberOfLayers, thisLayerData);
								SetTileObjects (false, -1, false);
							}
							else {
								// Use existing layer data, but with the loaded map data
								for (int i = 0; i < tileLayers.Count; i++) {
									tileLayers[i].layerData.mapData = thisLayerData[i].mapData;
								}
								SetLevelColliders();
								Resources.UnloadUnusedAssets();
								ResetLayerContainers();
							}
						}
						// Level has not been initialized
						else {
							InitializeLevel (numberOfLayers, thisLayerData);
							SetTileObjects (false, -1, false);
						}
						// Set up lights
						if (foundLightList) {
							lightList = thisLightList;
							for (int i = 0; i < tileLayers.Count; i++) {
								if (tileLayers[i].layerData.mapData.lightRefs.Count > 0) {
									tileLayers[i].layerData.mapData.ResetLighting (lightList);
								}
							}
							useRadiosity = tileLayers[0].layerData.mapData.useRadiosity;
						}
						else {
							lightList = new List<LightInfo>();
						}
						
						if (willDisableSceneManager) {
							sceneManager.enabled = false;
							return null;
						}
						InstantiatePropertyObjects (thisLayerData);
						UpdateScreenSprites (false, 0);
					}
					
					// Load map block
					else {
						if (layer >= thisLayerData.Count) {
							Debug.LogError ("Tile.LoadMapBlock: requested layer is " + layer + " but the maximum layer number for the loaded file is " + (thisLayerData.Count-1));
							return null;
						}
						return thisLayerData[layer].mapData;
					}
				}
				else {
					throw new System.Exception("No layers found in file");
				}
				ResetLevelUpdates();
			}
			
			// Load groups
			else {
				if (groupsFound) {
					groupSets = thisGroupSets;
				}
				if (randomGroupsFound) {
					randomGroupSets = thisRandomSets;
					useRandomGroup = false;
				}
				if (terrainGroupsFound) {
					terrainGroupSets = thisTerrainSets;
					terrainGroupActiveList = null;
					useTerrainGroup = false;
				}
				if (!groupsFound && !randomGroupsFound && !terrainGroupsFound) {
					throw new System.Exception("No groups found in file");
				}
			}
		}
		catch (System.Exception err) {
			Debug.LogError ("Error loading file: " + err.Message);
			return null;
		}
		
		useIndividualTileMaterials = false;
		if (!loadGroup) {
			ForceCameraUpdate();	// Set camera position now, so SetLayerPosition can work immediately without having to wait a frame
		}
		return null;
	}
	
	static void InstantiatePropertyObjects (List<LayerData> layerData) {
		GameObject go;
		for (int i = 0; i < layerData.Count; i++) {
			var mapData = layerData[i].mapData;
			if (mapData.propertyDictionary.Count == 0) {
				continue;
			}
			int xSize = mapData.mapSize.x;
			int ySize = mapData.mapSize.y;
			Int2 p;
			for (p.y = 0; p.y < ySize; p.y++) {
				for (p.x = 0; p.x < xSize; p.x++) {
					if (mapData.HasProperty (p)) {
						go = mapData.GetProperty <GameObject>(p);
						if (go != null) {
							var pos = new Vector3(p.x * layerData[i].tileSize.x, p.y * layerData[i].tileSize.y, layerData[i].zPosition);
							var rotation = Quaternion.Euler (0.0f, 0.0f, mapData.GetRotation (p));
                            //Save RuntimeObjects
                            var obj = GameObject.Instantiate(go, pos, rotation);
                            if (!string.IsNullOrEmpty(mapData.GetProperty<string>(p)))
                            {
                                InstanceParameters instanceParameters = obj.AddComponent<InstanceParameters>();
                                instanceParameters.ReadFromJson(mapData.GetProperty<string>(p));
                            }
                            mapData.SetProperty<GameObject>(p, obj);
                            RuntimeGameObjects.Add(obj);
                        }
					}
				}
			}
		}
	}
	
	static void ResetLevelUpdates () {
		animationPositions = null;
		positionAnimationCount = 0;
		if (tileAnimationCount == 0) {
			sceneManager.SetAnimation (false);
		}
		
		watchedTiles = null;
	}
	
	static void UpdateMapData (MapData mapData) {
		// Make sure all sets/tiles exist (set to -1 if not)
		var map = mapData.map;
		var mapSize = mapData.mapSize;
		for (int i = 0; i < map.Length; i++) {
			if (map[i] >= 0) {
				int setNum = map[i] >> MapData.setShift;
				int tileNum = map[i] & MapData.tileShiftI;
				if (setNum >= spriteSets.Count || tileNum >= spriteSets[setNum].Count) {
					Debug.LogWarning ("Set " + setNum + ", tile " + tileNum + " doesn't exist, at location (" + i % mapSize.x + ", " + i / mapSize.y + ")");
					map[i] = -1;
				}
				// Set tile numbers according to tileRefSets (to account for rearranged tiles)
				else {
					tileNum = tileRefSets[setNum][tileNum];
					map[i] = (short)(setNum*MapData.tileShift + tileNum);
				}
			}
		}
	}
	
	static void ResetLayerContainers () {
		for (int i = 0; i < tileLayers.Count; i++) {
			if (tileLayers[i].offset != Vector3.zero) {
				tileLayers[i].container.position = Vector3.zero;
				tileLayers[i].offset = -tileLayers[i].offset;
				SetTileObjects (true, i, false);
			}
		}
	}
	
	static void InitializeLevel (int numberOfLayers, List<LayerData> tileData) {
		tileLayers = new List<TileLayer>(numberOfLayers);
		colliderInfoList = new List<ColliderInfo>(numberOfLayers);
		for (int i = 0; i < numberOfLayers; i++) {
			tileLayers.Add (new TileLayer(tileData[i], i, cams.Length));
			colliderInfoList.Add (new ColliderInfo(defaultColliderLayer, defaultColliderTag, defaultColliderMaterial, defaultColliderTrigger));
		}
		InitializeTileSprites (numberOfLayers);
		SetupColliderBlockUpdates();
		lightList = new List<LightInfo>();
		levelInitialized = true;
	}
	
	static void InitializeTileSprites (int numberOfLayers) {
		tileSprites = new List<List<List<List<SpriteCached>>>>(cams.Length);
		for (int i = 0; i < cams.Length; i++) {
			tileSprites.Add (new List<List<List<SpriteCached>>>(numberOfLayers) );
			for (int j = 0; j < numberOfLayers; j++) {
				tileSprites[i].Add (new List<List<SpriteCached>>() );
			}
		}
	}
	
	static void SetupColliderBlockUpdates () {
		if (usePhysicsColliders) {
			colliderBlockUpdates = new List<bool[][]>(tileLayers.Count);
			for (int i = 0; i < tileLayers.Count; i++) {
				int ySize = (int)Mathf.Ceil (tileLayers[i].layerData.mapData.mapSize.y / (float)colliderBlockSize);
				colliderBlockUpdates.Add (new bool[ySize][]);
				for (int y = 0; y < ySize; y++) {
					colliderBlockUpdates[i][y] = new bool[(int)Mathf.Ceil (tileLayers[i].layerData.mapData.mapSize.x / (float)colliderBlockSize)];
				}
			}
		}
	}
	
	public static void NewLevel (Int2 mapSize, int addBorder, float tileSize, float zPosition, LayerLock layerLock) {
		LevelData[] levelData = {new LevelData(mapSize, addBorder, new Vector2(tileSize, tileSize), zPosition, layerLock)};
		NewLevel (levelData);
	}
	
	public static void NewLevel (Int2 mapSize, int addBorder, Vector2 tileSize, float zPosition, LayerLock layerLock) {
		LevelData[] levelData = {new LevelData(mapSize, addBorder, tileSize, zPosition, layerLock)};
		NewLevel (levelData);
	}
	
	public static void NewLevel (LevelData[] levelData) {
		if (!cameraInitialized) {
			Debug.LogError ("SetCamera must be called before NewLevel");
			return;
		}
		for (int i = 0; i < levelData.Length; i++) {
			if (!CheckLevelData (levelData[i])) return;
		}
		if (levelData.Length > sortingLayerNames.Length) {
			Debug.LogError ("Error creating level: " + levelData.Length + " layers specified when calling NewLevel, but only " + sortingLayerNames.Length + " sorting layers exist in the project. Add new sorting layers using the \"Tags and Layers\" project setting, then use the \"Assets/Set SpriteTile Sorting Layer Names\" menu item.");
			return;
		}
		
		sortingLayerIndices = new List<int>();
		for (int i = 0; i < levelData.Length; i++) {
			sortingLayerIndices.Add (i);
		}
		colliderInfoList = new List<ColliderInfo>(levelData.Length);
		
		var sameSize = true;
		if (tileLayers == null || levelData.Length != tileLayers.Count) {
			sameSize = false;
		}
		else {
			for (int i = 0; i < levelData.Length; i++) {
				var layerData = tileLayers[i].layerData;
				if (layerData.mapData.mapSize != levelData[i].mapSize || layerData.tileSize != levelData[i].tileSize || layerData.addBorder != levelData[i].addBorder || layerData.zPosition != levelData[i].zPosition || layerData.layerLock != levelData[i].layerLock) {
					sameSize = false;
					break;
				}
			}
		}
		
		if (sameSize) {
			EraseLevel();
			ResetLayerContainers();
		}
		else {
			// Initialize level
			if (levelInitialized) {
				DeleteLevelObjects();
			}
			
			int numberOfLayers = levelData.Length;
			tileLayers = new List<TileLayer>(numberOfLayers);
			for (int i = 0; i < numberOfLayers; i++) {
				var mapData = new MapData(levelData[i].mapSize, true);
				var layerData = new LayerData(mapData, levelData[i].addBorder, levelData[i].tileSize, levelData[i].zPosition, levelData[i].layerLock);
				tileLayers.Add (new TileLayer(layerData, i, cams.Length));
			}
			
			InitializeTileSprites (numberOfLayers);
			levelInitialized = true;
			SetTileObjects (false, -1, false);
			SetupColliderBlockUpdates();
		}
		
		for (int i = 0; i < tileLayers.Count; i++) {
			tileLayers[i].layerData.mapData.useRadiosity = useRadiosity;
		}
		
		if (lightList == null) {
			lightList = new List<LightInfo>();
		}
		useIndividualTileMaterials = false;
		ForceCameraUpdate();	// Set camera position now, so SetLayerPosition can work immediately without having to wait a frame
		ResetLevelUpdates();
	}
	
	public static byte[] GetLevelBytes () {
		if (!levelInitialized) {
			Debug.LogError ("Tile.GetLevelBytes: level not initialized");
			return null;
		}
		
		int layerDataLocation;
		var saveBytes = FileHeaderBytes (out layerDataLocation, true);
		SpriteFile.AddToData (saveBytes, lightList);
		
		SpriteFile.CopyIntToListAtIndex (saveBytes.Count, saveBytes, layerDataLocation);
		SpriteFile.AddToData (saveBytes, tileLayers.Count);
		for (int i = 0; i < tileLayers.Count; i++) {
			SpriteFile.AddToData (saveBytes, "lyrdata");
			// Convert map data to account for rearranged tiles, add to data, then convert back again
			ConvertMapData (tileLayers[i].layerData, tileRefReverseSets);
			SpriteFile.AddToData (saveBytes, tileLayers[i].layerData);
			ConvertMapData (tileLayers[i].layerData, tileRefSets);
		}
		return CLZF2.Compress (saveBytes.ToArray());
	}
	
	public static byte[] GetMapBlockBytes (MapData mapData) {
		if (mapData == null) {
			Debug.LogError ("Tile.SaveMapBlock: mapData must not be null");
			return null;
		}
		
		int layerDataLocation;
		var saveBytes = FileHeaderBytes (out layerDataLocation, false);
		SpriteFile.AddToData (saveBytes, 1);	// Number of layers
		var tempLayer = new LayerData(mapData, 0, Vector2.one, 0.0f, LayerLock.None);
		SpriteFile.AddToData (saveBytes, "lyrdata");
		ConvertMapData (tempLayer, tileRefReverseSets);
		SpriteFile.AddToData (saveBytes, tempLayer);
		tempLayer = null;
		return CLZF2.Compress (saveBytes.ToArray());
	}
	
	static List<byte> FileHeaderBytes (out int layerDataLocation, bool doWholeLevel) {
		var saveBytes = new List<byte>();
		saveBytes.AddRange (System.Text.Encoding.UTF8.GetBytes ("SpriteTileLevel"));
		SpriteFile.AddToData (saveBytes, System.BitConverter.IsLittleEndian);
		SpriteFile.AddToData (saveBytes, SpriteFile.LEVELVERSION);
		
		SpriteFile.AddToData (saveBytes, 3);	// Number of tags
		SpriteFile.AddToData (saveBytes, "numsets");
		SpriteFile.AddToData (saveBytes, saveBytes.Count + (doWholeLevel? 26 : 15));
		SpriteFile.AddToData (saveBytes, "lvlayrs");
		layerDataLocation = saveBytes.Count;
		SpriteFile.AddToData (saveBytes, saveBytes.Count + 8);	// If doing whole level, this is overwritten by actual index later
		if (doWholeLevel) {
			SpriteFile.AddToData (saveBytes, "litinfo");
			SpriteFile.AddToData (saveBytes, saveBytes.Count + 8);
		}
		SpriteFile.AddToData (saveBytes, MapData.setCount);
		return saveBytes;
	}
	
	static void ConvertMapData (LayerData layerData, int[][] refArray) {
		var map = layerData.mapData.map;
		for (int i = 0; i < map.Length; i++) {
			if (map[i] >= 0) {
				int setNum = map[i] >> MapData.setShift;
				int tileNum = refArray[setNum][map[i] & MapData.tileShiftI];
				map[i] = (short)(setNum*MapData.tileShift + tileNum);
			}
		}
	}
	
	static void DeleteLevelObjects () {
		DeleteLevelColliders();
		// Remove sprite GameObjects
		for (int i = 0; i < tileLayers.Count; i++) {
			Object.Destroy (tileLayers[i].container.gameObject);
		}
		Resources.UnloadUnusedAssets();
	}

	static void UpdateScreenSprites (bool singleLayer, int layer) {
		UpdateScreenSprites (singleLayer, layer, 0, cams.Length);
	}

	static void UpdateScreenSprites (bool singleLayer, int layer, int camNumber) {
		UpdateScreenSprites (singleLayer, layer, camNumber, camNumber+1);
	}
	
	static void UpdateScreenSprites (bool singleLayer, int layer, int camStart, int camEnd) {
		var tInfo = new TileInfo();
		for (int camNumber = camStart; camNumber < camEnd; camNumber++) {
			int layerStart = singleLayer? layer : 0;
			int layerEnd = singleLayer? layer + 1 : tileLayers.Count;
			for (int i = layerStart; i < layerEnd; i++) {
				var mapData = tileLayers[i].layerData.mapData;
				int yEnd = tileLayers[i].pUpperRight[camNumber].y;
				int xEnd = tileLayers[i].pUpperRight[camNumber].x;
				int w = mapData.mapSize.x;
				Int2 p;
				for (p.y = tileLayers[i].pLowerLeft[camNumber].y; p.y <= yEnd; p.y++) {
					for (p.x = tileLayers[i].pLowerLeft[camNumber].x; p.x <= xEnd; p.x++) {
						var spr = tileSprites[camNumber][i][p.y % tileLayers[i].screenTileCount[camNumber].y][p.x % tileLayers[i].screenTileCount[camNumber].x];
						SetSprite (ref p, ref w, ref i, mapData, spr, ref tInfo);
					}
				}
			}
		}
	}
	
	static void UpdateSpritesSortingLayer (int layer) {
		for (int i = 0; i < cams.Length; i++) {
			for (int y = 0; y < tileSprites[i][layer].Count; y++) {
				for (int x = 0; x < tileSprites[i][layer][y].Count; x++) {
					tileSprites[i][layer][y][x].spriteRenderer.sortingLayerName = sortingLayerNames[sortingLayerIndices[layer]];
				}
			}
		}
	}
	
	static void SetScreenSprite (Int2 p, int layer, int set, int tile) {
		for (int i = 0; i < cams.Length; i++) {
			// Check if off-screen
			if (p.x < tileLayers[layer].pLowerLeft[i].x || p.y < tileLayers[layer].pLowerLeft[i].y || p.x > tileLayers[layer].pUpperRight[i].x || p.y > tileLayers[layer].pUpperRight[i].y) {
				continue;
			}
			var spr = tileSprites[i][layer][p.y % tileLayers[layer].screenTileCount[i].y][p.x % tileLayers[layer].screenTileCount[i].x];
			if (tile >= 0) {
				spr.spriteRenderer.sprite = spriteSets[set][tile];
				spr.enabled = spr.spriteRenderer.enabled = true;
				SetSpriteMaterial (ref layer, ref p, new TileInfo(set, tile), spr);
				if (tileLayers[layer].layerData.mapData.useColors) {
					spr.spriteRenderer.color = tileLayers[layer].layerData.mapData.GetColor (p);
				}
			}
			else {
				spr.enabled = spr.spriteRenderer.enabled = false;
			}
		}
	}
	
	static void SetScreenSprite (Int2 p, int layer, int set, int tile, int order, float rotation) {
		for (int i = 0; i < cams.Length; i++) {
			if (p.x < tileLayers[layer].pLowerLeft[i].x || p.y < tileLayers[layer].pLowerLeft[i].y || p.x > tileLayers[layer].pUpperRight[i].x || p.y > tileLayers[layer].pUpperRight[i].y) {
				continue;
			}
			var spr = tileSprites[i][layer][p.y % tileLayers[layer].screenTileCount[i].y][p.x % tileLayers[layer].screenTileCount[i].x];
			if (tile >= 0) {
				spr.spriteRenderer.sprite = spriteSets[set][tile];
				spr.spriteRenderer.sortingOrder = order;
				spr.transform.eulerAngles = new Vector3(0.0f, 0.0f, rotation);
				spr.enabled = spr.spriteRenderer.enabled = true;
				SetSpriteMaterial (ref layer, ref p, new TileInfo(set, tile), spr);
				if (tileLayers[layer].layerData.mapData.useColors) {
					spr.spriteRenderer.color = tileLayers[layer].layerData.mapData.GetColor (p);
				}
			}
			else {
				spr.enabled = spr.spriteRenderer.enabled = false;
			}
		}
	}
	
	static void SetScreenSpriteOrder (Int2 p, int layer, int order) {
		for (int i = 0; i < cams.Length; i++) {
			if (p.x < tileLayers[layer].pLowerLeft[i].x || p.y < tileLayers[layer].pLowerLeft[i].y || p.x > tileLayers[layer].pUpperRight[i].x || p.y > tileLayers[layer].pUpperRight[i].y) {
				continue;
			}
			tileSprites[i][layer][p.y % tileLayers[layer].screenTileCount[i].y][p.x % tileLayers[layer].screenTileCount[i].x].spriteRenderer.sortingOrder = order;
		}
	}
	
	static void SetScreenSpriteRotation (Int2 p, int layer, float rotation) {
		for (int i = 0; i < cams.Length; i++) {
			if (p.x < tileLayers[layer].pLowerLeft[i].x || p.y < tileLayers[layer].pLowerLeft[i].y || p.x > tileLayers[layer].pUpperRight[i].x || p.y > tileLayers[layer].pUpperRight[i].y) {
				continue;
			}
			bool flipX = tileLayers[layer].layerData.mapData.GetFlipX (p);
			bool flipY = tileLayers[layer].layerData.mapData.GetFlipY (p);
			SpriteCached spr = tileSprites[i][layer][p.y % tileLayers[layer].screenTileCount[i].y][p.x % tileLayers[layer].screenTileCount[i].x];
			spr.transform.eulerAngles = new Vector3(flipY? 180.0f : 0.0f, flipX? 180.0f : 0.0f, rotation);
			if (flipX || flipY) {
				SetSpriteFlipPos (spr, flipX, flipY);
			}
			else {
				spr.transform.localPosition = spr.storedPosition;	// In case tile had a flip previously
			}
		}
	}
	
	static void SetScreenSpriteColor (Int2 p, int layer, Color32 color) {			
		for (int i = 0; i < cams.Length; i++) {
			if (p.x < tileLayers[layer].pLowerLeft[i].x || p.y < tileLayers[layer].pLowerLeft[i].y || p.x > tileLayers[layer].pUpperRight[i].x || p.y > tileLayers[layer].pUpperRight[i].y) {
				continue;
			}
			tileSprites[i][layer][p.y % tileLayers[layer].screenTileCount[i].y][p.x % tileLayers[layer].screenTileCount[i].x].spriteRenderer.color = color;
		}
	}
	
	static void SetScreenSpriteColorBlock (int layer, Int2 startPos, Int2 endPos) {
		Color32 color = Color.white;
		var mapData = tileLayers[layer].layerData.mapData;
		for (int cam = 0; cam < cams.Length; cam++) {
			var pos1 = startPos;
			var pos2 = endPos;
			pos1.x = Mathf.Max (pos1.x, tileLayers[layer].pLowerLeft[cam].x);
			pos1.y = Mathf.Max (pos1.y, tileLayers[layer].pLowerLeft[cam].y);
			pos2.x = Mathf.Min (pos2.x, tileLayers[layer].pUpperRight[cam].x);
			pos2.y = Mathf.Min (pos2.y, tileLayers[layer].pUpperRight[cam].y);
			Int2 screenTileCount = tileLayers[layer].screenTileCount[cam];
			Int2 p;
			var tSprite = tileSprites[cam][layer];
			for (p.y = pos1.y; p.y <= pos2.y; p.y++) {
				for (p.x = pos1.x; p.x <= pos2.x; p.x++) {
					mapData.GetColor (ref p, ref color);
					tSprite[p.y % screenTileCount.y][p.x % screenTileCount.x].spriteRenderer.color = color;
				}
			}
		}
	}
	
	public static void SetTileScale (float scale) {
		defaultScale = scale;
		if (tileLayers == null) return;
		
		for (int i = 0; i < tileLayers.Count; i++) {
			SetTileLayerScale (i, scale);
		}
	}
	
	public static void SetTileLayerScale (float scale) {
		SetTileLayerScale (0, scale);
	}
	
	public static void SetTileLayerScale (int layer, float scale) {
		if (!CheckParams (layer)) {
			return;
		}
		
		SetupTileScales();
		tileScales[layer] = scale;
		var spriteScale = new Vector3(tileScales[layer], tileScales[layer], 1.0f);
		for (int i = 0; i < cams.Length; i++) {
			for (int y = 0; y < tileSprites[i][layer].Count; y++) {
				for (int x = 0; x < tileSprites[i][layer][y].Count; x++) {
					tileSprites[i][layer][y][x].transform.localScale = spriteScale;
				}
			}
		}
	}
	
	public static void EraseLevel () {
		if (!levelInitialized) {
			Debug.LogError ("No level exists");
			return;
		}
		for (int camNumber = 0; camNumber < cams.Length; camNumber++) {
			for (int i = 0; i < tileLayers.Count; i++) {
				var mapSize = tileLayers[i].layerData.mapData.mapSize - Int2.one;
				DeleteTileBlock (Int2.zero, mapSize, i, true);
				if (mapSize == Int2.zero) {
					var spr = tileSprites[camNumber][i][0][0];
					spr.enabled = spr.spriteRenderer.enabled = false;
				}
				SetOrderBlock (Int2.zero, mapSize, i, 0);
				SetRotationBlock (Int2.zero, mapSize, i, 0.0f);
			}
		}
	}

	public static void SetTile (Int2 p, int set, int tile) {
		SetTile (p, 0, set, tile, false, false, false);
	}

	public static void SetTile (Int2 p, int layer, int set, int tile) {
		SetTile (p, layer, set, tile, false, false, false);
	}

	public static void SetTile (Int2 p, int set, int tile, bool setCollider) {
		SetTile (p, 0, set, tile, false, setCollider, true);
	}

	public static void SetTile (Int2 p, int layer, int set, int tile, bool setCollider) {
		SetTile (p, layer, set, tile, false, setCollider, true);
	}

	public static void SetTile (Int2 p, TileInfo tileInfo) {
		SetTile (p, 0, tileInfo.set, tileInfo.tile, false, false, false);
	}

	public static void SetTile (Int2 p, int layer, TileInfo tileInfo) {
		SetTile (p, layer, tileInfo.set, tileInfo.tile, false, false, false);
	}

	public static void SetTile (Int2 p, TileInfo tileInfo, bool setCollider) {
		SetTile (p, 0, tileInfo.set, tileInfo.tile, false, setCollider, true);
	}

	public static void SetTile (Int2 p, int layer, TileInfo tileInfo, bool setCollider) {
		SetTile (p, layer, tileInfo.set, tileInfo.tile, false, setCollider, true);
	}

	public static void DeleteTile (Int2 p) {
		SetTile (p, 0, 0, -1, true, false, false);
	}

	public static void DeleteTile (Int2 p, int layer) {
		SetTile (p, layer, 0, -1, true, false, false);
	}

	public static void DeleteTile (Int2 p, bool removeCollider) {
		SetTile (p, 0, 0, -1, true, false, removeCollider);
	}

	public static void DeleteTile (Int2 p, int layer, bool removeCollider) {
		SetTile (p, layer, 0, -1, true, false, removeCollider);
	}
	
	static void SetTile (Int2 p, int layer, int set, int tile, bool delete, bool setCollider, bool doSetCollider) {
		if (delete) {
			if (!CheckParams (p, layer)) {
				return;
			}
		}
		else {
			if (!CheckParams (p, layer, set, tile)) {
				return;
			}
		}
		
		if (useRandomGroup) {
			var tInfo = randomGroup[Random.Range (0, randomGroup.Count)];
			set = tInfo.set;
			tile = tInfo.tile;
		}
		
		tileLayers[layer].layerData.mapData.SetTile (p, set, (short)tile);
		
		if (useEditorDefaults && !delete) {
			bool useCollider = tileManager.GetSpriteData (set, tile).useCollider;
			if (useCollider) {
				SetCollider (p, layer, true);
			}
			int orderInLayer = tileManager.GetSpriteData (set, tile).orderInLayer;
			if (orderInLayer != 0) {
				SetOrder (p, layer, orderInLayer);
			}
			float rotation = tileManager.GetSpriteData (set, tile).rotation;
			if (rotation != 0.0f) {
				SetRotation (p, layer, rotation);
			}
		}
		
		if (doSetCollider) {
			SetCollider (p, layer, setCollider);
		}
		
		SetScreenSprite (p, layer, set, tile);
		
		if (useTerrainGroup) {
			TerrainGroupCheck (p, tileLayers[layer].layerData.mapData, layer);
		}
	}
	
	static void TerrainGroupCheck (Int2 p, MapData map, int layer) {
		CheckPosition (p, map, layer);
		CheckPosition (p+Int2.up, map, layer);
		CheckPosition (p+Int2.down, map, layer);
		CheckPosition (p+Int2.left, map, layer);
		CheckPosition (p+Int2.right, map, layer);
		CheckPosition (p+Int2.upLeft, map, layer);
		CheckPosition (p+Int2.upRight, map, layer);
		CheckPosition (p+Int2.downLeft, map, layer);
		CheckPosition (p+Int2.downRight, map, layer);
	}
	
	static void CheckPosition (Int2 p, MapData map, int layer) {
		if (p.x < 0 || p.y < 0 || p.x >= map.mapSize.x || p.y >= map.mapSize.y) {
			return;
		}
		TileInfo newTInfo = CheckAdjacentPositions (p, map.GetTile (p), map);
		if (newTInfo.set >= 0 && newTInfo.tile >= 0) {
			map.SetTile (p, newTInfo.set, (short)newTInfo.tile);
			SetScreenSprite (p, layer, newTInfo.set, newTInfo.tile);
		}
	}
	
	static TileInfo CheckAdjacentPositions (Int2 p, TileInfo tInfo, MapData map) {
		for (int i = 0; i < terrainGroupActiveList.Count; i++) {
			var tGroup = terrainGroupSets[terrainGroupActiveList[i].x].terrainGroups[terrainGroupActiveList[i].y];
			if (tGroup.Contains (tInfo)) {
				for (int j = 0; j < terrainLookups.Length; j++) {
					if (terrainLookups[j].FillsContain (p, map, tGroup) && !terrainLookups[j].EmptiesContain (p, map, tGroup)) {
						return tGroup.tileNumbers[0][j];
					}
				}
			}
		}
		return new TileInfo(-1, -1);
	}
	
	public static void SetTileBlock (Int2 p1, Int2 p2, int set, int tile) {
		SetTileBlock (p1, p2, 0, set, tile, false, false, false);
	}
	
	public static void SetTileBlock (Int2 p1, Int2 p2, int layer, int set, int tile) {
		SetTileBlock (p1, p2, layer, set, tile, false, false, false);
	}
	
	public static void SetTileBlock (Int2 p1, Int2 p2, int set, int tile, bool setCollider) {
		SetTileBlock (p1, p2, 0, set, tile, false, setCollider, true);
	}
	
	public static void SetTileBlock (Int2 p1, Int2 p2, int layer, int set, int tile, bool setCollider) {
		SetTileBlock (p1, p2, layer, set, tile, false, setCollider, true);
	}
	
	public static void SetTileBlock (Int2 p1, Int2 p2, TileInfo tileInfo) {
		SetTileBlock (p1, p2, 0, tileInfo.set, tileInfo.tile, false, false, false);
	}

	public static void SetTileBlock (Int2 p1, Int2 p2, int layer, TileInfo tileInfo) {
		SetTileBlock (p1, p2, layer, tileInfo.set, tileInfo.tile, false, false, false);
	}

	public static void SetTileBlock (Int2 p1, Int2 p2, TileInfo tileInfo, bool setCollider) {
		SetTileBlock (p1, p2, 0, tileInfo.set, tileInfo.tile, false, setCollider, true);
	}

	public static void SetTileBlock (Int2 p1, Int2 p2, int layer, TileInfo tileInfo, bool setCollider) {
		SetTileBlock (p1, p2, layer, tileInfo.set, tileInfo.tile, false, setCollider, true);
	}
	
	public static void DeleteTileBlock (Int2 p1, Int2 p2) {
		SetTileBlock (p1, p2, 0, 0, 0, true, false, false);
	}
	
	public static void DeleteTileBlock (Int2 p1, Int2 p2, int layer) {
		SetTileBlock (p1, p2, layer, 0, 0, true, false, false);
	}

	public static void DeleteTileBlock (Int2 p1, Int2 p2, bool removeCollider) {
		SetTileBlock (p1, p2, 0, 0, 0, true, false, removeCollider);
	}
	
	public static void DeleteTileBlock (Int2 p1, Int2 p2, int layer, bool removeCollider) {
		SetTileBlock (p1, p2, layer, 0, 0, true, false, removeCollider);
	}
	
	static void SetTileBlock (Int2 p1, Int2 p2, int layer, int set, int tile, bool delete, bool setCollider, bool doSetCollider) {
		if (!CheckParams (layer, set, tile)) {
			return;
		}
		if (delete) {
			tile = -1;
		}
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		
		if (!useRandomGroup || delete) {
			tileLayers[layer].layerData.mapData.SetTileBlock (thisP1, thisP2, set, (short)tile);
			
			if (useEditorDefaults) {
				bool useCollider = tileManager.GetSpriteData (set, tile).useCollider;
				if (useCollider) {
					SetColliderBlock (p1, p2, layer, true);
				}
				int orderInLayer = tileManager.GetSpriteData (set, tile).orderInLayer;
				if (orderInLayer != 0) {
					SetOrderBlock (p1, p2, layer, orderInLayer);
				}
				float rotation = tileManager.GetSpriteData (set, tile).rotation;
				if (rotation != 0.0f) {
					SetRotationBlock (p1, p2, layer, rotation);
				}
			}
			
			if (doSetCollider) {
				SetColliderBlock (p1, p2, layer, setCollider);
			}
			
			var pos = Int2.zero;
			for (pos.y = thisP1.y; pos.y <= thisP2.y; pos.y++) {
				for (pos.x = thisP1.x; pos.x <= thisP2.x; pos.x++) {
					SetScreenSprite (pos, layer, set, tile);
				}
			}
		}
		else {	// Set random tiles (SetTile picks the random tile, so we just use 0, 0 here)
			var pos = Int2.zero;
			for (pos.y = thisP1.y; pos.y <= thisP2.y; pos.y++) {
				for (pos.x = thisP1.x; pos.x <= thisP2.x; pos.x++) {
					SetTile (pos, layer, 0, 0, setCollider);
				}
			}
		}
	}
	
	public static void SetMapBlock (Int2 p, MapData mapData) {
		SetMapBlock (p, 0, mapData, false);
	}

	public static void SetMapBlock (Int2 p, MapData mapData, bool instantiateGameObjects) {
		SetMapBlock (p, 0, mapData, instantiateGameObjects);
	}
	
	public static void SetMapBlock (Int2 p, int layer, MapData blockMapData) {
		SetMapBlock (p, layer, blockMapData, false);
	}
	
	public static void SetMapBlock (Int2 p, int layer, MapData blockMapData, bool instantiateGameObjects) {
		if (!CheckParams (p, layer)) {
			return;
		}
		var layerMapData = tileLayers[layer].layerData.mapData;
		if (p.x + blockMapData.mapSize.x > layerMapData.mapSize.x || p.y + blockMapData.mapSize.y > layerMapData.mapSize.y) {
			Debug.LogError ("SetLayerBlock: position " + p + " is out of bounds for the supplied map data. Map size for layer " + layer + " is " + layerMapData.mapSize + ", supplied mapData block size is " + blockMapData.mapSize);
			return;
		}
		
		layerMapData.SetBlock (p, blockMapData);
		int w = layerMapData.mapSize.x;
		var pos = Int2.zero;
		for (pos.y = p.y; pos.y < p.y + blockMapData.mapSize.y; pos.y++) {
			for (pos.x = p.x; pos.x < p.x + blockMapData.mapSize.x; pos.x++) {
				int idx = pos.x + pos.y*w;
				if (layerMapData.map[idx] >= 0) {
					SetScreenSprite (pos, layer, layerMapData.map[idx] >> MapData.setShift, layerMapData.map[idx] & MapData.tileShiftI, layerMapData.orderData[idx], layerMapData.GetRotation (pos));
				}
				else {
					SetScreenSprite (pos, layer, 0, -1, layerMapData.orderData[idx], layerMapData.GetRotation (pos));
				}
			}
		}
		
		int p2X = Mathf.Min (p.x + blockMapData.mapSize.x - 1, layerMapData.mapSize.x - 1);
		int p2Y = Mathf.Min (p.y + blockMapData.mapSize.y - 1, layerMapData.mapSize.y - 1);
		SetColliderBlockUpdates (p, new Int2(p2X, p2Y), layer);
		
		if (instantiateGameObjects && blockMapData.propertyDictionary.Count > 0) {
			Vector2 size = tileLayers[layer].layerData.tileSize;
			for (pos.y = p.y; pos.y < p.y + blockMapData.mapSize.y; pos.y++) {
				for (pos.x = p.x; pos.x < p.x + blockMapData.mapSize.x; pos.x++) {
					if (layerMapData.HasProperty (pos)) {
						var go = layerMapData.GetProperty <GameObject>(pos);
						if (go != null) {
							var worldPos = new Vector3(pos.x * size.x, pos.y * size.y, tileLayers[layer].layerData.zPosition);
							var rotation = Quaternion.Euler (0.0f, 0.0f, layerMapData.GetRotation (pos));
							GameObject.Instantiate (go, worldPos, rotation);
						}
					}
				}
			}
		}
		
		if (layerMapData.lightRefs.Count > 0) {
			layerMapData.ResetLighting (lightList);
			SetScreenSpriteColorBlock (layer, Int2.zero, layerMapData.maxMapPos);
		}
	}
	
	static void SetColliderBlockUpdates (Int2 p1, Int2 p2, int layer) {
		if (colliderBlockUpdates == null) return;
		
		int x1 = p1.x / colliderBlockSize;
		int y1 = p1.y / colliderBlockSize;
		int x2 = p2.x / colliderBlockSize;
		int y2 = p2.y / colliderBlockSize;
		for (int y = y1; y <= y2; y++) {
			for (int x = x1; x <= x2; x++) {
				colliderBlockUpdates[layer][y][x] = true;
			}
		}
		sceneManager.SetColliderUpdate();
	}
	
	public static void SetMapTileset (int set) {
		SetMapTileset (0, set);
	}
	
	public static void SetMapTileset (int layer, int set) {
		if (set < 0 || set >= spriteSets.Count) {
			Debug.LogError ("set must be >= 0 and < " + spriteSets.Count);
			return;
		}
		if (!CheckParams (layer)) {
			return;
		}
		
		if (!tileLayers[layer].layerData.mapData.SetMapTileset (set, spriteSets[set].Count)) {
			Debug.LogError ("MapSet: Attempting to switch map in layer " + layer + " to set " + set + " exceeds the number of tiles in that set");
			return;
		}
		UpdateScreenSprites (true, layer);
	}

	public static void SetOrder (Int2 p, int order) {
		SetOrder (p, 0, order);
	}
	
	public static void SetOrder (Int2 p, int layer, int order) {
		if (!CheckParams (p, layer)) {
			return;
		}
		if (order < -32768 || order >= 32767) {
			Debug.LogError ("SetOrder: order is " + order + ", but must be >= -32768 and < 32768");
			return;
		}
		tileLayers[layer].layerData.mapData.SetOrder (p, order);
		SetScreenSpriteOrder (p, layer, order);
	}

	public static void SetOrderBlock (Int2 p1, Int2 p2, int order) {
		SetOrderBlock (p1, p2, 0, order);
	}
	
	public static void SetOrderBlock (Int2 p1, Int2 p2, int layer, int order) {
		if (!CheckParams (layer)) {
			return;
		}
		if (order < -32768 || order >= 32767) {
			Debug.LogError ("SetOrderBlock: order is " + order + ", but must be >= -32768 and < 32768");
			return;
		}
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		tileLayers[layer].layerData.mapData.SetOrderBlock (thisP1, thisP2, order);
		
		Int2 pos = Int2.zero;
		for (pos.y = thisP1.y; pos.y <= thisP2.y; pos.y++) {
			for (pos.x = thisP1.x; pos.x <= thisP2.x; pos.x++) {
				SetScreenSpriteOrder (pos, layer, order);
			}
		}
	}

	public static void SetRotation (Int2 p, float rotation) {
		SetRotation (p, 0, rotation);
	}
	
	public static void SetRotation (Int2 p, int layer, float rotation) {
		if (!CheckParams (p, layer)) {
			return;
		}
		rotation = Mathf.Repeat (rotation, 360.0f);
		tileLayers[layer].layerData.mapData.SetRotation (p, rotation);
		SetScreenSpriteRotation (p, layer, rotation);
		UpdateColliderIfNeeded (p, layer);
	}
	
	public static void SetRotationBlock (Int2 p1, Int2 p2, float rotation) {
		SetRotationBlock (p1, p2, 0, rotation);
	}
	
	public static void SetRotationBlock (Int2 p1, Int2 p2, int layer, float rotation) {
		if (!CheckParams (layer)) {
			return;
		}
		rotation = Mathf.Repeat (rotation, 360.0f);
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		tileLayers[layer].layerData.mapData.SetRotationBlock (thisP1, thisP2, rotation);
		
		Int2 pos = Int2.zero;
		for (pos.y = thisP1.y; pos.y <= thisP2.y; pos.y++) {
			for (pos.x = thisP1.x; pos.x <= thisP2.x; pos.x++) {
				SetScreenSpriteRotation (pos, layer, rotation);
			}
		}
		
		if (usePhysicsColliders && ColliderExistsInBlock (thisP1, thisP2, layer)) {
			SetColliderBlockUpdates (thisP1, thisP2, layer);
		}
	}
	
	public static void SetFlip (Int2 p, Flip flip) {
		SetFlip (p, 0, flip);
	}
	
	public static void SetFlip (Int2 p, int layer, Flip flip) {
		if (!CheckParams (p, layer)) {
			return;
		}
		tileLayers[layer].layerData.mapData.SetFlip (p, flip);
		SetScreenSpriteRotation (p, layer, tileLayers[layer].layerData.mapData.GetRotation (p));
		UpdateColliderIfNeeded (p, layer);
	}
	
	public static void SetFlipBlock (Int2 p1, Int2 p2, Flip flip) {
		SetFlipBlock (p1, p2, 0, flip);
	}
	
	public static void SetFlipBlock (Int2 p1, Int2 p2, int layer, Flip flip) {
		if (!CheckParams (layer)) {
			return;
		}
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		tileLayers[layer].layerData.mapData.SetFlipBlock (thisP1, thisP2, flip);
		
		Int2 pos = Int2.zero;
		for (pos.y = thisP1.y; pos.y <= thisP2.y; pos.y++) {
			for (pos.x = thisP1.x; pos.x <= thisP2.x; pos.x++) {
				SetScreenSpriteRotation (pos, layer, tileLayers[layer].layerData.mapData.GetRotation (pos));
			}
		}
		
		if (usePhysicsColliders && ColliderExistsInBlock (thisP1, thisP2, layer)) {
			SetColliderBlockUpdates (thisP1, thisP2, layer);
		}
	}
	
	public static void SetCollider (Int2 p, bool active) {
		SetCollider (p, 0, active);
	}
	
	public static void SetCollider (Int2 p, int layer, bool active) {
		if (!CheckParams (p, layer)) {
			return;
		}
		tileLayers[layer].layerData.mapData.SetCollider (p, active);
		if (usePhysicsColliders) {
			colliderBlockUpdates[layer][(int)(p.y / colliderBlockSize)][(int)(p.x / colliderBlockSize)] = true;
			sceneManager.SetColliderUpdate();
		}
	}
	
	public static void SetColliderBlock (Int2 p1, Int2 p2, bool active) {
		SetColliderBlock (p1, p2, 0, active);
	}
	
	public static void SetColliderBlock (Int2 p1, Int2 p2, int layer, bool active) {
		if (!CheckParams (layer)) {
			return;
		}
		var mapData = tileLayers[layer].layerData.mapData;
		mapData.Clamp (ref p1, ref p2);
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, mapData.mapSize);
		mapData.SetColliderBlock (thisP1, thisP2, active);
		
		if (usePhysicsColliders) {
			SetColliderBlockUpdates (thisP1, thisP2, layer);
		}
	}
	
	static void UpdateColliderBlocks () {
		for (int i = 0; i < colliderBlockUpdates.Count; i++) {
			var mapData = tileLayers[i].layerData.mapData;
			int w = mapData.mapSize.x;
			int h = mapData.mapSize.y;
			
			for (int y = 0; y < colliderBlockUpdates[i].Length; y++) {
				for (int x = 0; x < colliderBlockUpdates[i][y].Length; x++) {
					if (!colliderBlockUpdates[i][y][x]) {
						continue;
					}
					
					colliderBlockUpdates[i][y][x] = false;
					
					int xStart = x * colliderBlockSize;
					int yStart = y * colliderBlockSize;
					int xEnd = Mathf.Min (xStart + colliderBlockSize, w);
					int yEnd = Mathf.Min (yStart + colliderBlockSize, h);
					int totalPathCount = GetTotalPathCount (xStart, yStart, xEnd, yEnd, w, mapData);
					if (totalPathCount == 0 && colliderBlocks[i][y][x] == null) {
						continue;
					}
					var points = GetPathPoints (totalPathCount, xStart, yStart, xEnd, yEnd, w, tileLayers[i].layerData.tileSize, mapData);
					if (colliderBlocks[i][y][x] == null) {
						colliderBlocks[i][y][x] = MakeObjectWithCollider (totalPathCount, i, xStart, yStart, tileLayers[i].layerData.tileSize, points);
					}
					else {
						SetPathPoints (colliderBlocks[i][y][x], totalPathCount, points);
					}
				}
			}
		}
	}
	
	public static void SetTrigger (Int2 p, int trigger) {
		SetTrigger (p, 0, trigger);
	}
	
	public static void SetTrigger (Int2 p, int layer, int trigger) {
		if (!CheckParams (p, layer)) {
			return;
		}
		if (trigger < 0 || trigger >= TRIGGERNUMBER) {
			Debug.LogError ("SetTrigger: trigger is " + trigger + ", but must be >= 0 and < " + TRIGGERNUMBER);
			return;
		}
		tileLayers[layer].layerData.mapData.SetTrigger (p, trigger);
	}

	public static void SetTriggerBlock (Int2 p1, Int2 p2, int trigger) {
		SetTriggerBlock (p1, p2, 0, trigger);
	}
	
	public static void SetTriggerBlock (Int2 p1, Int2 p2, int layer, int trigger) {
		if (!CheckParams (layer)) {
			return;
		}
		if (trigger < 0 || trigger >= TRIGGERNUMBER) {
			Debug.LogError ("SetTriggerBlock: trigger is " + trigger + ", but must be >= 0 and < " + TRIGGERNUMBER);
			return;
		}
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		tileLayers[layer].layerData.mapData.SetTriggerBlock (thisP1, thisP2, trigger);
	}
	
	public static void SetColor (Int2 p, Color32 color) {
		SetColor (p, 0, color);
	}
	
	public static void SetColor (Int2 p, int layer, Color32 color) {
		if (!CheckParams (p, layer)) {
			return;
		}
		
		if (!tileLayers[layer].layerData.mapData.useColors) {
			tileLayers[layer].layerData.mapData.SetupColors();
		}
		color.a = 255;
		tileLayers[layer].layerData.mapData.SetColor (p, color);
		SetScreenSpriteColor (p, layer, color);
	}
	
	public static void SetColorBlock (Int2 p1, Int2 p2, Color32 color) {
		SetColorBlock (p1, p2, 0, color);
	}
	
	public static void SetColorBlock (Int2 p1, Int2 p2, int layer, Color32 color) {
		if (!CheckParams (layer)) {
			return;
		}
		
		if (!tileLayers[layer].layerData.mapData.useColors) {
			tileLayers[layer].layerData.mapData.SetupColors();
		}
		color.a = 255;
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		tileLayers[layer].layerData.mapData.SetColorBlock (thisP1, thisP2, color);
		
		Int2 pos = Int2.zero;
		for (pos.y = thisP1.y; pos.y <= thisP2.y; pos.y++) {
			for (pos.x = thisP1.x; pos.x <= thisP2.x; pos.x++) {
				SetScreenSpriteColor (pos, layer, color);
			}
		}
	}
	
	public static void SetProperty <T>(Int2 p, object value) {
		SetProperty <T>(p, 0, value);
	}
	
	public static void SetProperty <T>(Int2 p, int layer, object value) {
		if (!CheckParams (p, layer)) {
			return;
		}
		if (typeof(T) != typeof(float) && typeof(T) != typeof(string) && typeof(T) != typeof(GameObject)) {
			Debug.LogError ("SetProperty: value must be of type float, string, or GameObject");
			return;
		}
		
		tileLayers[layer].layerData.mapData.SetProperty <T>(p, value);
	}
	
	public static void RemoveProperties (Int2 p) {
		RemoveProperties (p, 0);
	}
	
	public static void RemoveProperties (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return;
		}
		
		tileLayers[layer].layerData.mapData.SetHasProperty (p, false);
	}
	
	public static void GetPropertyPositions (ref List<Int2> positions) {
		GetPropertyPositions (0, ref positions);
	}
	
	public static void GetPropertyPositions (int layer, ref List<Int2> positions) {
		if (!CheckParams (layer)) {
			return;
		}
		
		if (positions == null) {
			positions = new List<Int2>();
		}
		else {
			positions.Clear();
		}
		var mapSize = tileLayers[layer].layerData.mapData.mapSize;
		Int2 p;
		for (p.y = 0; p.y < mapSize.y; p.y++) {
			for (p.x = 0; p.x < mapSize.x; p.x++) {
				if (tileLayers[layer].layerData.mapData.HasProperty (p)) {
					positions.Add (p);
				}
			}
		}		
	}
	
	public static void SetBorder (TileInfo tileInfo, bool setCollider) {
		SetBorder (0, tileInfo.set, tileInfo.tile, setCollider);
	}
	
	public static void SetBorder (int layer, TileInfo tileInfo, bool setCollider) {
		SetBorder (layer, tileInfo.set, tileInfo.tile, setCollider);
	}
	
	public static void SetBorder (int set, int tile, bool setCollider) {
		SetBorder (0, set, tile, setCollider);
	}
	
	public static void SetBorder (int layer, int set, int tile, bool setCollider) {
		if (!CheckParams (layer, set, tile)) {
			return;
		}
		
		var mapSize = tileLayers[layer].layerData.mapData.mapSize;
		SetTileBlock (Int2.zero, new Int2(mapSize.x-1, 0), set, tile, setCollider);
		SetTileBlock (new Int2(0, mapSize.y-1), new Int2(mapSize.x-1, mapSize.y-1), set, tile, setCollider);
		SetTileBlock (new Int2(0, 1), new Int2(0, mapSize.y-2), set, tile, setCollider);
		SetTileBlock (new Int2(mapSize.x-1, 1), new Int2(mapSize.x-1, mapSize.y-2), set, tile, setCollider);
	}
	
	// Make thisP1 always smallest and thisP2 always largest, and clamped to the map size
	static void OrderP1P2 (out Int2 thisP1, out Int2 thisP2, Int2 p1, Int2 p2, Int2 mapSize) {
		thisP1 = new Int2(Mathf.Max (0, (p1.x < p2.x)? p1.x : p2.x), Mathf.Max (0, (p1.y < p2.y)? p1.y : p2.y));
		thisP2 = new Int2(Mathf.Min (mapSize.x - 1, (p1.x < p2.x)? p2.x : p1.x), Mathf.Min (mapSize.y - 1, (p1.y < p2.y)? p2.y : p1.y));
	}

	public static TileInfo GetTile (Int2 p) {
		return GetTile (p, 0);
	}
	
	public static TileInfo GetTile (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return new TileInfo(-1, -1);
		}
		return tileLayers[layer].layerData.mapData.GetTile (p);
	}

	public static TileInfo GetTile (Vector2 p) {
		return GetTile (p, 0);
	}

	public static TileInfo GetTile (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return new TileInfo(-1, -1);
		}
		var p2 = WorldToMapPosition (p, layer);
		if (!CheckParams (p2, layer)) {
			return new TileInfo(-1, -1);
		}
		return tileLayers[layer].layerData.mapData.GetTile (p2);
	}

	public static int GetOrder (Int2 p) {
		return GetOrder (p, 0);
	}
	
	public static int GetOrder (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return -1;
		}
		return tileLayers[layer].layerData.mapData.GetOrder (p);
	}
	
	public static int GetOrder (Vector2 p) {
		return GetOrder (p, 0);
	}
	
	public static int GetOrder (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return -1;
		}
		var p2 = WorldToMapPosition (p, layer);
		if (!CheckParams (p2, layer)) {
			return -1;
		}
		return tileLayers[layer].layerData.mapData.GetOrder (p2);
	}

	public static float GetRotation (Int2 p) {
		return GetRotation (p, 0);
	}
	
	public static float GetRotation (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return -1.0f;
		}
		return tileLayers[layer].layerData.mapData.GetRotation (p);
	}
	
	public static float GetRotation (Vector2 p) {
		return GetRotation (p, 0);
	}
	
	public static float GetRotation (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return -1.0f;
		}
		var p2 = WorldToMapPosition (p, layer);
		if (!CheckParams (p2, layer)) {
			return -1.0f;
		}
		return tileLayers[layer].layerData.mapData.GetRotation (p2);
	}
	
	public static Flip GetFlip (Int2 p) {
		return GetFlip (p, 0);
	}

	public static Flip GetFlip (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return Flip.None;
		}
		return tileLayers[layer].layerData.mapData.GetFlip (p);		
	}
	
	public static Flip GetFlip (Vector2 p) {
		return GetFlip (p, 0);
	}
	
	public static Flip GetFlip (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return Flip.None;
		}
		var p2 = WorldToMapPosition (p, layer);
		if (!CheckParams (p2, layer)) {
			return Flip.None;
		}
		return tileLayers[layer].layerData.mapData.GetFlip (p2);
	}

	public static bool GetCollider (Int2 p) {
		return GetCollider (p, 0);
	}
	
	public static bool GetCollider (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return false;
		}
		return tileLayers[layer].layerData.mapData.GetCollider (p);
	}

	public static bool GetCollider (Vector2 p) {
		return GetCollider (p, 0);
	}
	
	public static bool GetCollider (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return false;
		}
		var p2 = WorldToMapPosition (p, layer);
		if (!CheckParams (p2, layer)) {
			return false;
		}
		return tileLayers[layer].layerData.mapData.GetCollider (p2);
	}

	public static int GetTrigger (Int2 p) {
		return GetTrigger (p, 0);
	}
	
	public static int GetTrigger (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return -1;
		}
		return tileLayers[layer].layerData.mapData.GetTrigger (p);
	}

	public static int GetTrigger (Vector2 p) {
		return GetTrigger (p, 0);
	}
	
	public static int GetTrigger (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return -1;
		}
		var p2 = WorldToMapPosition (p, layer);
		if (!CheckParams (p2, layer)) {
			return -1;
		}
		return tileLayers[layer].layerData.mapData.GetTrigger (p2);
	}
	
	public static MapData GetMapBlock (Int2 p1, Int2 p2) {
		return GetMapBlock (p1, p2, 0);
	}
	
	public static MapData GetMapBlock (Int2 p1, Int2 p2, int layer) {
		if (!CheckParams (p1, layer)) {
			return null;
		}
		if (p2.x < 0 || p2.x >= tileLayers[layer].layerData.mapData.mapSize.x || p2.y < 0 || p2.y >= tileLayers[layer].layerData.mapData.mapSize.y) {
			Debug.LogError ("Position " + p2 + " out of bounds. Map size for layer " + layer + " is " + tileLayers[layer].layerData.mapData.mapSize);
			return null;
		}
		
		Int2 thisP1, thisP2;
		OrderP1P2 (out thisP1, out thisP2, p1, p2, tileLayers[layer].layerData.mapData.mapSize);
		return tileLayers[layer].layerData.mapData.GetBlock (thisP1, thisP2);
	}
	
	public static Color32 GetColor (Int2 p) {
		return GetColor (p, 0);
	}
	
	public static Color32 GetColor (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return Color.black;
		}
		
		if (!tileLayers[layer].layerData.mapData.useColors) {
			tileLayers[layer].layerData.mapData.SetupColors();
		}
		return tileLayers[layer].layerData.mapData.GetColor (p);
	}
	
	public static T GetProperty <T>(Int2 p) {
		return GetProperty <T>(p, 0);
	}
	
	public static T GetProperty <T>(Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return default(T);
		}
		if (typeof(T) != typeof(float) && typeof(T) != typeof(string) && typeof(T) != typeof(GameObject)) {
			Debug.LogError ("GetProperty: type must be float, string, or GameObject");
			return default(T);
		}
		if (!tileLayers[layer].layerData.mapData.HasProperty (p)) {
			Debug.LogError ("GetProperty: position " + p + " in layer " + layer + " has no PropertyInfo");
			return default(T);
		}
		
		return tileLayers[layer].layerData.mapData.GetProperty <T>(p);
	}
	
	public static Int2 GetMapSize () {
		return GetMapSize (0);
	}
	
	public static Int2 GetMapSize (int layer) {
		if (!CheckParams (layer)) {
			return Int2.zero;
		}
		return tileLayers[layer].layerData.mapData.mapSize;
	}
	
	public static Vector2 GetTileSize () {
		return GetTileSize (0);
	}
	
	public static Vector2 GetTileSize (int layer) {
		if (!CheckParams (layer)) {
			return Vector2.zero;
		}
		return tileLayers[layer].layerData.tileSize;
	}
	
	public static Int2 GetGroupSize (int groupSet, int groupNumber) {
		if (!CheckGroupParams (groupSet, groupNumber)) return Int2.zero;
		
		var groupMapData = groupSets[groupSet][groupNumber];
		return new Int2(groupMapData.mapSize.x, groupMapData.mapSize.y);
	}
	
	public static float GetZPosition () {
		return GetZPosition (0);
	}
	
	public static float GetZPosition (int layer) {
		if (!CheckParams (layer)) {
			return -1.0f;
		}
		return tileLayers[layer].layerData.zPosition;
	}
	
	public static int GetNumberOfLayers () {
		return tileLayers.Count;
	}
	
	public static bool HasProperty (Int2 p) {
		return HasProperty (p, 0);
	}
	
	public static bool HasProperty (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return false;
		}
		
		return tileLayers[layer].layerData.mapData.HasProperty (p);
	}
	
	public static void SetLayerActive (int layer, bool active) {
		if (!CheckParams (layer)) {
			return;
		}
		tileLayers[layer].container.gameObject.SetActive (active);
	}
	
	public static Int2 WorldToMapPosition (Vector2 p) {
		return WorldToMapPosition (p, 0);
	}

	public static Int2 WorldToMapPosition (Vector2 p, int layer) {
		if (!CheckParams (layer)) {
			return Int2.zero;
		}
		return new Int2((int)Mathf.Round ((p.x - tileLayers[layer].offset.x) / tileLayers[layer].layerData.tileSize.x),
						(int)Mathf.Round ((p.y - tileLayers[layer].offset.y) / tileLayers[layer].layerData.tileSize.y));
	}

	public static Vector3 MapToWorldPosition (Int2 p) {
		return MapToWorldPosition (p, 0);
	}
	
	public static Vector3 MapToWorldPosition (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return Vector3.zero;
		}
		Vector2 tSize = tileLayers[layer].layerData.tileSize;
		return new Vector3(p.x * tSize.x + tileLayers[layer].offset.x, p.y * tSize.y + tileLayers[layer].offset.y, tileLayers[layer].layerData.zPosition);
	}

	public static bool ScreenToMapPosition (Vector2 p, out Int2 mapPos) {
		return ScreenToMapPosition (p, 0, out mapPos, 0);
	}

	public static bool ScreenToMapPosition (Vector2 p, out Int2 mapPos, int camNumber) {
		return ScreenToMapPosition (p, 0, out mapPos, camNumber);
	}

	public static bool ScreenToMapPosition (Vector2 p, int layer, out Int2 mapPos) {
		return ScreenToMapPosition (p, layer, out mapPos, 0);
	}
	
	public static bool ScreenToMapPosition (Vector2 p, int layer, out Int2 mapPos, int camNumber) {
		if (camNumber < 0 || camNumber >= cams.Length) {
			Debug.LogError ("Tile.ScreenToMapPosition: camNumber " + camNumber + " out of range (must be 0 - " + (cams.Length-1) + ")");
			mapPos = Int2.zero;
			return false;
		}
		if (cams[camNumber] == null) {
			Debug.LogError ("Camera not set up");
			mapPos = Int2.zero;
			return false;
		}
		if (!CheckParams (layer)) {
			mapPos = Int2.zero;
			return false;
		}

		mapPos = WorldToMapPosition (cams[camNumber].ScreenToWorldPoint (new Vector3(p.x, p.y, tileLayers[layer].layerData.zPosition - camTransforms[camNumber].position.z)), layer);
		if (mapPos.x < 0 || mapPos.y < 0 || mapPos.x >= tileLayers[layer].layerData.mapData.mapSize.x || mapPos.y >= tileLayers[layer].layerData.mapData.mapSize.y) {
			return false;
		}
		return true;
	}
	
	public static void UseTileEditorDefaults (bool useDefaults) {
		useEditorDefaults = useDefaults;
	}

	public static void CopyGroupToPosition (Int2 p, int groupSet, int groupNumber) {
		CopyGroupToPosition (p, Int2.zero, 0, groupSet, groupNumber);
	}

	public static void CopyGroupToPosition (Int2 p, Int2 offset, int groupSet, int groupNumber) {
		CopyGroupToPosition (p, offset, 0, groupSet, groupNumber);
	}
	
	public static void CopyGroupToPosition (Int2 p, int layer, int groupSet, int groupNumber) {
		CopyGroupToPosition (p, Int2.zero, layer, groupSet, groupNumber);
	}
	
	public static void CopyGroupToPosition (Int2 p, Int2 offset, int layer, int groupSet, int groupNumber) {
		if (!CheckGroupParams (groupSet, groupNumber)) return;
		if (offset.x > 0 || offset.y > 0) {
			Debug.LogError ("CopyGroupToPosition: offset is " + offset + " but offset.x and offset.y must be <= 0");
			return;
		}
		if (!CheckParams (p, layer)) {
			return;
		}
		
		var mapData = tileLayers[layer].layerData.mapData;
		var groupMapData = groupSets[groupSet][groupNumber];
		int w = mapData.mapSize.x;
		int xCrop = 0;
		p += offset;
		if (p.x < 0) {
			xCrop = -p.x;
			p.x = 0;
		}
		int groupW = groupMapData.mapSize.x;
		int groupY = 0;
		int rowWidth = Mathf.Min (p.x + groupMapData.mapSize.x, w) - p.x - xCrop;
		int yEnd = Mathf.Min (p.y + groupMapData.mapSize.y, mapData.mapSize.y);
		Int2 pos = Int2.zero;
		for (pos.y = p.y; pos.y < yEnd; pos.y++) {
			if (pos.y < 0) {
				groupY++;
				continue;
			}
			int idx = p.x + pos.y*w;
			int groupI = groupY*groupW + xCrop;
			
			System.Array.Copy (groupMapData.map, groupI, mapData.map, idx, rowWidth);
			System.Array.Copy (groupMapData.info, groupI, mapData.info, idx, rowWidth);
			System.Array.Copy (groupMapData.orderData, groupI, mapData.orderData, idx, rowWidth);
			System.Array.Copy (groupMapData.triggerData, groupI, mapData.triggerData, idx, rowWidth);
			
			for (pos.x = p.x; pos.x < p.x + rowWidth; pos.x++) {
				int tile = mapData.map[pos.x + pos.y*w];
				if (tile >= 0) {
					SetScreenSprite (pos, layer, tile >> MapData.setShift, tile & MapData.tileShiftI, mapData.orderData[pos.x + pos.y*w], mapData.GetRotation (pos));
				}
				else {
					SetScreenSprite (pos, layer, 0, -1);
				}
			}
			groupY++;
		}
		
		SetColliderBlockUpdates (p, new Int2(p.x + rowWidth - 1, yEnd - 1), layer);
	}
	
	public static void AnimateTilePosition (Int2 p, TileInfo tileInfo, int range, float frameRate) {
		AnimateTilePosition (p, 0, tileInfo, range, frameRate, AnimType.Loop);
	}
	
	public static void AnimateTilePosition (Int2 p, int layer, TileInfo tileInfo, int range, float frameRate) {
		AnimateTilePosition (p, layer, tileInfo, range, frameRate, AnimType.Loop);
	}
	
	public static void AnimateTilePosition (Int2 p, TileInfo tileInfo, int range, float frameRate, AnimType animType) {
		AnimateTilePosition (p, 0, tileInfo, range, frameRate, animType);
	}
	
	public static void AnimateTilePosition (Int2 p, int layer, TileInfo tileInfo, int range, float frameRate, AnimType animType) {
		if (range < 2) {
			Debug.LogError ("AnimateTilePosition: range must be >= 2");
			return;
		}
		if (tileInfo.tile + (range - 1) >= spriteSets[tileInfo.set].Count) {
			Debug.LogError ("AnimateTilePosition: range " + range + " exceeds number of tiles in set " + tileInfo.set + " when using tileInfo " + tileInfo);
			return;
		}
		
		AnimateTilePosition (p, layer, MakeTileInfoArray (tileInfo, range), frameRate, animType);
	}
	
	public static void AnimateTilePosition (Int2 p, TileInfo[] tileInfoArray, float frameRate) {
		AnimateTilePosition (p, 0, tileInfoArray, frameRate, AnimType.Loop);		
	}
	
	public static void AnimateTilePosition (Int2 p, int layer, TileInfo[] tileInfoArray, float frameRate) {
		AnimateTilePosition (p, layer, tileInfoArray, frameRate, AnimType.Loop);		
	}
	
	public static void AnimateTilePosition (Int2 p, TileInfo[] tileInfoArray, float frameRate, AnimType animType) {
		AnimateTilePosition (p, 0, tileInfoArray, frameRate, animType);
	}
	
	public static void AnimateTilePosition (Int2 p, int layer, TileInfo[] tileInfoArray, float frameRate, AnimType animType) {
		for (int i = 0; i < tileInfoArray.Length; i++) {
			if (!CheckParams (tileInfoArray[i].set, tileInfoArray[i].tile)) {
				return;
			}
		}
		if (!CheckParams (p, layer)) {
			return;
		}
		if (frameRate < 0.0f) {
			Debug.LogError ("AnimateTilePosition: frameRate must be >= 0.0");
			return;
		}
		if (tileInfoArray == null || tileInfoArray.Length < 1) {
			Debug.LogError ("AnimateTilePosition: tileInfoArray must contain at least 1 entry");
			return;
		}
		
		if (animationPositions == null) {
			animationPositions = new List<List<AnimationPosition>>();
			for (int i = 0; i < tileLayers.Count; i++) {
				animationPositions.Add (new List<AnimationPosition>());
			}
		}
		else {
			// If position is already in list just replace animation info
			for (int i = 0; i < animationPositions[layer].Count; i++) {
				if (animationPositions[layer][i].p == p) {
					animationPositions[layer][i].animationInfo = new AnimationInfo (tileInfoArray, 1.0f/frameRate, animType);
					return;
				}
			}
		}
		animationPositions[layer].Add (new AnimationPosition(p, new AnimationInfo (tileInfoArray, 1.0f/frameRate, animType)));
		positionAnimationCount++;
		sceneManager.SetAnimation (true);
	}
	
	static TileInfo[] MakeTileInfoArray (TileInfo tileInfo, int range) {
		var tileInfoArray = new TileInfo[range];
		int idx = 0;
		for (int i = tileInfo.tile; i < tileInfo.tile + range; i++) {
			tileInfoArray[idx++] = new TileInfo (tileInfo.set, i);
		}
		return tileInfoArray;
	}
	
	public static void StopAnimatingTilePosition (Int2 p) {
		StopAnimatingTilePosition (p, 0);
	}
	
	public static void StopAnimatingTilePosition (Int2 p, int layer) {
		if (!CheckParams (p, layer)) {
			return;
		}
		
		int foundIndex = -1;
		for (int i = 0; i < animationPositions[layer].Count; i++) {
			if (animationPositions[layer][i].p == p) {
				foundIndex = i;
				break;
			}
		}
		if (foundIndex == -1) {
			Debug.LogError ("StopAnimatingTilePosition: position " + p + " in layer " + layer + " is not animating");
			return;
		}
		
		animationPositions[layer].RemoveAt (foundIndex);
		if (--positionAnimationCount == 0 && tileAnimationCount == 0) {
			sceneManager.SetAnimation (false);
		}
		var tInfo = tileLayers[layer].layerData.mapData.GetTile (p);
		SetScreenSprite (p, layer, tInfo.set, tInfo.tile);
	}
	
	public static void AnimateTile (TileInfo tileInfo, int range, float frameRate) {
		AnimateTile (tileInfo, range, frameRate, AnimType.Loop);
	}
	
	public static void AnimateTile (TileInfo tileInfo, int range, float frameRate, AnimType animType) {
		if (range < 2) {
			Debug.LogError ("AnimateTile: range must be >= 2");
			return;
		}
		if (tileInfo.tile + (range - 1) >= spriteSets[tileInfo.set].Count) {
			Debug.LogError ("AnimateTile: range " + range + " exceeds number of tiles in set " + tileInfo.set + " when using tileInfo " + tileInfo);
			return;
		}
		
		AnimateTile (tileInfo, MakeTileInfoArray (tileInfo, range), frameRate, animType);
	}
	
	public static void AnimateTile (TileInfo tileInfo, TileInfo[] tileInfoArray, float frameRate) {
		AnimateTile (tileInfo, tileInfoArray, frameRate, AnimType.Loop);
	}
	
	public static void AnimateTile (TileInfo tileInfo, TileInfo[] tileInfoArray, float frameRate, AnimType animType) {
		if (!CheckParams (tileInfo.set, tileInfo.tile)) {
			return;
		}
		for (int i = 0; i < tileInfoArray.Length; i++) {
			if (!CheckParams (tileInfoArray[i].set, tileInfoArray[i].tile)) {
				return;
			}
		}
		if (frameRate < 0.0f) {
			Debug.LogError ("AnimateTile: frameRate must be >= 0.0");
			return;
		}
		if (tileInfoArray == null || tileInfoArray.Length < 1) {
			Debug.LogError ("AnimateTile: tileInfoArray must contain at least 1 entry");
			return;
		}
		
		// Set up animation arrays if needed
		if (doesTileAnimate == null) {
			doesTileAnimate = new bool[spriteSets.Count][];
			animationInfo = new AnimationInfo[spriteSets.Count][];
			for (int i = 0; i < spriteSets.Count; i++) {
				doesTileAnimate[i] = new bool[spriteSets[i].Count];
				animationInfo[i] = new AnimationInfo[spriteSets[i].Count];
			}
			animatedTiles = new List<TileInfo>();
		}
		
		if (!doesTileAnimate[tileInfo.set][tileInfo.tile]) {
			doesTileAnimate[tileInfo.set][tileInfo.tile] = true;
			tileAnimationCount++;
			sceneManager.SetAnimation (true);
			animatedTiles.Add (tileInfo);
		}
		animationInfo[tileInfo.set][tileInfo.tile] = new AnimationInfo (tileInfoArray, 1.0f/frameRate, animType);
	}
	
	public static void StopAnimatingTile (TileInfo tileInfo) {
		if (!CheckParams (tileInfo.set, tileInfo.tile)) {
			return;
		}
		
		if (doesTileAnimate[tileInfo.set][tileInfo.tile]) {
			doesTileAnimate[tileInfo.set][tileInfo.tile] = false;
			if (--tileAnimationCount == 0 && positionAnimationCount == 0) {
				sceneManager.SetAnimation (false);
			}
			animatedTiles.Remove (tileInfo);
			UpdateScreenSprites (false, 0);
		}
	}
	
	public static void AnimateTileRange (TileInfo tileInfo, int range, float frameRate) {
		AnimateTileRange (tileInfo, range, frameRate, AnimType.Loop);
	}
	
	public static void AnimateTileRange (TileInfo tileInfo, int range, float frameRate, AnimType animType) {
		if (range < 2) {
			Debug.LogError ("AnimateTileRange: range must be >= 2");
			return;
		}
		var tileInfoArray = new TileInfo[range];
		int idx = 0;
		for (int i = tileInfo.tile; i < tileInfo.tile + range; i++) {
			tileInfoArray[idx++] = new TileInfo (tileInfo.set, i);
		}
		AnimateTileRange (tileInfo, range, tileInfoArray, frameRate, animType);
	}
	
	public static void AnimateTileRange (TileInfo tileInfo, int range, TileInfo[] tileInfoArray, float frameRate) {
		AnimateTileRange (tileInfo, range, tileInfoArray, frameRate, AnimType.Loop);
	}
	
	public static void AnimateTileRange (TileInfo tileInfo, int range, TileInfo[] tileInfoArray, float frameRate, AnimType animType) {
		if (range < 2) {
			Debug.LogError ("AnimateTileRange: range must be >= 2");
			return;
		}
		if (!CheckParams (tileInfo.set, tileInfo.tile)) {
			return;
		}
		if (tileInfo.tile + (range - 1) >= spriteSets[tileInfo.set].Count) {
			Debug.LogError ("AnimateTileRange: range exceeds number of tiles in set " + tileInfo.set);
			return;
		}
		
		var tileInfoArrays = new TileInfo[range][];
		tileInfoArrays[0] = tileInfoArray;
		int idx = 0;
		for (int i = 1; i < range; i++) {
			tileInfoArrays[i] = new TileInfo[tileInfoArray.Length];
			idx = ++idx % tileInfoArray.Length;
			for (int j = 0; j < tileInfoArray.Length; j++) {
				tileInfoArrays[i][j] = tileInfoArray[idx];
				idx = ++idx % tileInfoArray.Length;
			}
		}
		for (int i = 0; i < range; i++) {
			AnimateTile (new TileInfo(tileInfo.set, tileInfo.tile + i), tileInfoArrays[i], frameRate, animType);
		}
	}
	
	public static void StopAnimatingTileRange (TileInfo tileInfo, int range) {
		if (!CheckParams (tileInfo.set, tileInfo.tile)) {
			return;
		}
		if (range < 2) {
			Debug.LogError ("StopAnimatingTileRange: range must be >= 2");
			return;
		}
		if (tileInfo.tile + (range - 1) >= spriteSets[tileInfo.set].Count) {
			Debug.LogError ("StopAnimatingTileRange: range exceeds number of tiles in set " + tileInfo.set);
			return;
		}
		
		for (int i = 0; i < range; i++) {
			StopAnimatingTile (new TileInfo(tileInfo.set, tileInfo.tile + i));
		}
	}
	
	static void AnimateScreenTiles (float deltaTime) {
		var updateTiles = false;
		// See if any animated tiles have advanced a frame
		if (tileAnimationCount > 0) {
			for (int i = 0; i < tileAnimationCount; i++) {
				if (AdvanceAnimation (animationInfo[animatedTiles[i].set][animatedTiles[i].tile], deltaTime)) {
					updateTiles = true;
				}
			}
		}
		var updateTilePositions = false;
		if (positionAnimationCount > 0) {
			for (int layer = 0; layer < animationPositions.Count; layer++) {
				for (int i = 0; i < animationPositions[layer].Count; i++) {
					if (AdvanceAnimation (animationPositions[layer][i].animationInfo, deltaTime)) {
						updateTilePositions = true;
					}
				}
			}
		}
		if (!updateTiles && !updateTilePositions) return;
		
		// Update any sprites that should be animated
		if (updateTiles) {
			TileInfo tInfo;
			MapData mapData;
			AnimationInfo animInfo;
			TileInfo animatedTileInfo;
			int xEnd;
			int yEnd;
			Int2 tileCount, p;
			for (int camNumber = 0; camNumber < cams.Length; camNumber++) {
				for (int layer = 0; layer < tileLayers.Count; layer++) {
					mapData = tileLayers[layer].layerData.mapData;
					yEnd = tileLayers[layer].pUpperRight[camNumber].y;
					xEnd = tileLayers[layer].pUpperRight[camNumber].x;
					tileCount = tileLayers[layer].screenTileCount[camNumber];
					for (p.y = tileLayers[layer].pLowerLeft[camNumber].y; p.y <= yEnd; p.y++) {
						for (p.x = tileLayers[layer].pLowerLeft[camNumber].x; p.x <= xEnd; p.x++) {
							tInfo = mapData.GetTile (p);
							if (tInfo.tile < 0 || !doesTileAnimate[tInfo.set][tInfo.tile]) continue;
							
							animInfo = animationInfo[tInfo.set][tInfo.tile];
							animatedTileInfo = animInfo.tileInfoArray[animInfo.frame];
							tileSprites[camNumber][layer][p.y % tileCount.y][p.x % tileCount.x].spriteRenderer.sprite = spriteSets[animatedTileInfo.set][animatedTileInfo.tile];
						}
					}
				}
			}
		}
		if (updateTilePositions) {
			Int2 p;
			AnimationInfo animInfo;
			TileInfo animatedTileInfo;
			SpriteCached thisTileSprite;
			Int2 tileCount;
			for (int camNumber = 0; camNumber < cams.Length; camNumber++) {
				for (int layer = 0; layer < animationPositions.Count; layer++) {
					for (int i = 0; i < animationPositions[layer].Count; i++) {
						p = animationPositions[layer][i].p;
						// Check if off-screen
						if (p.x < tileLayers[layer].pLowerLeft[camNumber].x || p.y < tileLayers[layer].pLowerLeft[camNumber].y || p.x > tileLayers[layer].pUpperRight[camNumber].x || p.y > tileLayers[layer].pUpperRight[camNumber].y) {
							continue;
						}
						animInfo = animationPositions[layer][i].animationInfo;
						animatedTileInfo = animInfo.tileInfoArray[animInfo.frame];
						tileCount = tileLayers[layer].screenTileCount[camNumber];
						thisTileSprite = tileSprites[camNumber][layer][p.y % tileCount.y][p.x % tileCount.x];
						thisTileSprite.spriteRenderer.sprite = spriteSets[animatedTileInfo.set][animatedTileInfo.tile];
						if (!thisTileSprite.enabled) {	// Since the specified tile can be empty
							thisTileSprite.enabled = thisTileSprite.spriteRenderer.enabled = true;
						}
					}
				}
			}
		}
	}
	
	static bool AdvanceAnimation (AnimationInfo animInfo, float deltaTime) {
		animInfo.timer += deltaTime;
		if (animInfo.timer >= animInfo.frameTime) {
			animInfo.timer -= animInfo.frameTime;
			switch (animInfo.animType) {
				case AnimType.Loop:
					if (++animInfo.frame == animInfo.tileInfoArray.Length) {
						animInfo.frame = 0;
					}
					break;
				case AnimType.Reverse:
					if (--animInfo.frame == -1) {
						animInfo.frame = animInfo.tileInfoArray.Length - 1;
					}
					break;
				case AnimType.PingPong:
					animInfo.frame += animInfo.direction;
					// Logic works if direction == 0, in case the animation array length is 1
					if (animInfo.frame == animInfo.tileInfoArray.Length) {
						animInfo.direction = -1;
						animInfo.frame -= 2;
					}
					else if (animInfo.frame == -1) {
						animInfo.direction = 1;
						animInfo.frame = 1;
					}
					break;
			}
			return true;
		}
		return false;
	}

	public static void AddLayer (Int2 mapSize, int addBorder, float tileSize, float zPosition, LayerLock layerLock) {
		AddLayer (new LevelData(mapSize, addBorder, new Vector2(tileSize, tileSize), zPosition, layerLock));
	}
	
	public static void AddLayer (Int2 mapSize, int addBorder, Vector2 tileSize, float zPosition, LayerLock layerLock) {
		AddLayer (new LevelData(mapSize, addBorder, tileSize, zPosition, layerLock));
	}
	
	public static void AddLayer (LevelData levelData) {
		if (!CheckLevelData (levelData)) return;
		
		var mapData = new MapData(levelData.mapSize, true);
		var layerData = new LayerData(mapData, levelData.addBorder, levelData.tileSize, levelData.zPosition, levelData.layerLock);
		tileLayers.Add (new TileLayer(layerData, tileLayers.Count, cams.Length));
		
		sortingLayerIndices.Add (Mathf.Min (sortingLayerIndices.Count, sortingLayerNames.Length-1));
		
		int layerNum = tileLayers.Count - 1;
		for (int i = 0; i < cams.Length; i++) {
			tileSprites[i].Add (new List<List<SpriteCached>>() );
		}
		SetTileObjects (true, layerNum, false);
		
		if (usePhysicsColliders) {
			AddColliderLayer (layerNum);
			int ySize = (int)Mathf.Ceil (tileLayers[layerNum].layerData.mapData.mapSize.y / (float)colliderBlockSize);
			colliderBlockUpdates.Add (new bool[ySize][]);
			for (int y = 0; y < ySize; y++) {
				colliderBlockUpdates[layerNum][y] = new bool[(int)Mathf.Ceil (tileLayers[layerNum].layerData.mapData.mapSize.x / (float)colliderBlockSize)];
			}
		}
		
		if (useLayerMaterials) {
			layerMaterials.Add (defaultMaterial);
		}
		if (animationPositions != null) {
			animationPositions.Add (new List<AnimationPosition>());
		}
		if (watchedTiles != null) {
			watchedTiles.Add (new List<Int2>());
		}
		tileLayers[tileLayers.Count-1].layerData.mapData.useRadiosity = useRadiosity;
	}

	public static void GetTilePositions (TileInfo tInfo, ref List<Int2> positions) {
		GetTilePositions (tInfo, 0, ref positions);
	}
	
	public static void GetTilePositions (TileInfo tInfo, int layer, ref List<Int2> positions) {
		if (!CheckParams (layer, tInfo.set, tInfo.tile)) {
			return;
		}
		
		if (positions == null) {
			positions = new List<Int2>();
		}
		else {
			positions.Clear();
		}
		var mapSize = tileLayers[layer].layerData.mapData.mapSize;
		Int2 p;
		for (p.y = 0; p.y < mapSize.y; p.y++) {
			for (p.x = 0; p.x < mapSize.x; p.x++) {
				if (tileLayers[layer].layerData.mapData.GetTile (p) == tInfo) {
					positions.Add (p);
				}
			}
		}
	}

	public static void GetTriggerPositions (int t, ref List<Int2> positions) {
		GetTriggerPositions (t, 0, ref positions);
	}
	
	public static void GetTriggerPositions (int t, int layer, ref List<Int2> positions) {
		if (!CheckParams (layer)) {
			return;
		}
		if (t < 0 || t > 255) {
			Debug.LogError ("Tile.GetTriggerPositions: trigger value (" + t + ") must be >= 0 and < 255");
			return;
		}
		
		if (positions == null) {
			positions = new List<Int2>();
		}
		else {
			positions.Clear();
		}
		var mapSize = tileLayers[layer].layerData.mapData.mapSize;
		Int2 p;
		for (p.y = 0; p.y < mapSize.y; p.y++) {
			for (p.x = 0; p.x < mapSize.x; p.x++) {
				if (tileLayers[layer].layerData.mapData.GetTrigger (p) == t) {
					positions.Add (p);
				}
			}
		}
	}

	public static GameObject GrabSprite (Int2 p) {
		return GrabSprite (p, 0, "Sprite", true, null);
	}

	public static GameObject GrabSprite (Int2 p, int layer) {
		return GrabSprite (p, layer, "Sprite", true, null);
	}

	public static GameObject GrabSprite (Int2 p, string name) {
		return GrabSprite (p, 0, name, true, null);
	}
	
	public static GameObject GrabSprite (Int2 p, int layer, string name) {
		return GrabSprite (p, layer, name, true, null);
	}

	public static GameObject GrabSprite (Int2 p, bool deleteTile) {
		return GrabSprite (p, 0, "Sprite", deleteTile, null);
	}

	public static GameObject GrabSprite (Int2 p, int layer, bool deleteTile) {
		return GrabSprite (p, layer, "Sprite", deleteTile, null);
	}

	public static GameObject GrabSprite (Int2 p, string name, bool deleteTile) {
		return GrabSprite (p, 0, name, deleteTile, null);
	}

	public static GameObject GrabSprite (Int2 p, int layer, string name, bool deleteTile) {
		return GrabSprite (p, layer, name, deleteTile, null);
	}
	
	// User-supplied GameObject overloads
	public static void GrabSprite (Int2 p, GameObject go) {
		GrabSprite (p, 0, "", true, go);
	}
	
	public static void GrabSprite (Int2 p, int layer, GameObject go) {
		GrabSprite (p, layer, "", true, go);
	}

	public static void GrabSprite (Int2 p, GameObject go, bool deleteTile) {
		GrabSprite (p, 0, "", deleteTile, go);
	}

	public static void GrabSprite (Int2 p, int layer, GameObject go, bool deleteTile) {
		GrabSprite (p, layer, "", deleteTile, go);
	}
	
	static GameObject GrabSprite (Int2 p, int layer, string name, bool deleteTile, GameObject go) {
		var tileInfo = GetTile (p, layer);
		if (tileInfo.tile < 0) {
			Debug.LogError ("Tile.GrabSprite: can't grab empty tile (position " + p + ")");
			return null;
		}
		
		SpriteRenderer sprRenderer = null;
		// User-supplied GameObject
		if (go != null) {
			sprRenderer = go.GetComponent<SpriteRenderer>();
			if (sprRenderer == null) {
				Debug.LogError ("Tile.GrabSprite: GameObject does not have a SpriteRenderer component");
				return null;
			}
			var tInfoScript = go.GetComponent<SpriteTileInfo>();
			if (tInfoScript == null) {
				Debug.LogError ("Tile.GrabSprite: GameObject does not have a SpriteTileInfo component");
				return null;
			}
			tInfoScript.tileInfo = tileInfo;
			go.SetActive (true);
		}
		// Create new GameObject
		else {
			go = new GameObject(name);		
			go.AddComponent<SpriteTileInfo>().tileInfo = tileInfo;
			sprRenderer = go.AddComponent<SpriteRenderer>();
		}
		// Add collider if tile has a physics collider
		int pathCount = colliderSets[tileInfo.set][tileInfo.tile].pathCount;
		if (GetCollider (p, layer) && pathCount > 0) {
			var sprCollider = go.GetComponent<PolygonCollider2D>();
			if (sprCollider == null) {
				sprCollider = go.AddComponent<PolygonCollider2D>();
			}
			sprCollider.pathCount = pathCount;
			for (int i = 0; i < pathCount; i++) {
				sprCollider.SetPath (i, colliderSets[tileInfo.set][tileInfo.tile].points[i]);
			}
		}
		
		var layerMapData = tileLayers[layer].layerData.mapData;
		go.transform.position = MapToWorldPosition (p, layer);
		go.transform.eulerAngles = new Vector3(0.0f, 0.0f, layerMapData.GetRotation (p));
		sprRenderer.sprite = spriteSets[tileInfo.set][tileInfo.tile];
		sprRenderer.sortingOrder = layerMapData.GetOrder (p);
		sprRenderer.sortingLayerName = sortingLayerNames[sortingLayerIndices[layer]];
		sprRenderer.material = GetSpriteMaterial (ref layer, ref p, ref tileInfo);
		if (deleteTile) {
			DeleteTile (p, layer, true);
		}
		return go;
	}

	public static void PutSprite (GameObject go) {
		PutSprite (go, 0, Dispose.Destroy);
	}

	public static void PutSprite (GameObject go, int layer) {
		PutSprite (go, layer, Dispose.Destroy);
	}
	
	public static void PutSprite (GameObject go, Dispose disposeType) {
		PutSprite (go, 0, disposeType);
	}
	
	public static void PutSprite (GameObject go, int layer, Dispose disposeType) {
		var tInfoScript = go.GetComponent<SpriteTileInfo>();
		if (tInfoScript == null) {
			Debug.LogError ("Tile.PutSprite: no SpriteTileInfo component found on GameObject");
			return;
		}
		var sprRenderer = go.GetComponent<SpriteRenderer>();
		if (sprRenderer == null) {
			Debug.LogError ("Tile.PutSprite: no SpriteRenderer component found on GameObject");
			return;
		}
		var tileInfo = tInfoScript.tileInfo;
		if (!CheckParams (layer, tileInfo.set, tileInfo.tile)) {
			return;
		}
		var p = WorldToMapPosition (go.transform.position, layer);
		var mapSize = tileLayers[layer].layerData.mapData.mapSize;
		if (p.x < 0 || p.x >= mapSize.x || p.y < 0 || p.y >= mapSize.y) {
			Debug.LogError ("Tile.PutSprite: GameObject position is outside the map area for layer " + layer);
			return;
		}
		
		SetTile (p, layer, tileInfo);
		SetRotation (p, layer, go.transform.eulerAngles.z);
		SetOrder (p, layer, sprRenderer.sortingOrder);
		if (go.GetComponent<PolygonCollider2D>() != null) {
			SetCollider (p, layer, true);
		}
		if (disposeType == Dispose.Destroy) {
			MonoBehaviour.Destroy (go);
		}
		else if (disposeType == Dispose.Deactivate) {
			go.SetActive (false);
		}
	}
	
	public static void UseTrueColor (bool trueColor) {
		if (_useTrueColor != trueColor && tileLayers != null) {
			_useTrueColor = trueColor;
			var updated = false;
			for (int i = 0; i < tileLayers.Count; i++) {
				if ((trueColor && tileLayers[i].layerData.mapData.halfColors != null) || (!trueColor && tileLayers[i].layerData.mapData.colors != null)) {
					tileLayers[i].layerData.mapData.ConvertColors (trueColor);
					updated = true;
				}
			}
			if (updated) {
				UpdateScreenSprites (false, 0);
			}
		}
		_useTrueColor = trueColor;
	}
	
	public static string GetSortingLayerName (int layerNumber) {
		if (!CheckLayerNumber (layerNumber)) return null;
		
		return sortingLayerNames[layerNumber];
	}
	
	public static void SetLayerSorting (int layer, int layerNumber) {
		if (!CheckLayerCount (layer)) return;
		if (!CheckLayerNumber (layerNumber)) return;
		if (sortingLayerIndices == null) {
			Debug.LogError ("Sorting layers not set up");
			return;
		}
		sortingLayerIndices[layer] = layerNumber;
		UpdateSpritesSortingLayer (layer);
	}

	public static void SetLayerSorting (int layer, string layerName) {		
		if (!CheckLayerCount (layer)) return;
		if (sortingLayerIndices == null) {
			Debug.LogError ("Sorting layers not set up");
			return;
		}
		int index = System.Array.IndexOf (sortingLayerNames, layerName);
		if (index == -1) {
			Debug.LogError ("Layer name \"" + layerName + "\" not found");
			return;
		}
		sortingLayerIndices[layer] = index;
		UpdateSpritesSortingLayer (layer);
	}

	public static void UseRandomGroup (bool useGroup) {
		UseRandomGroup (useGroup, 0, 0);
	}

	public static void UseRandomGroup (bool useGroup, int groupSet, int groupNumber) {
		if (randomGroupSets == null) {
			Debug.LogError ("UseRandomGroup: No random groups available");
			return;
		}
		if (groupSet < 0 || groupSet >= randomGroupSets.Count) {
			Debug.LogError ("UseRandomGroup: groupSet is " + groupSet + ", but must be >= 0 and < " + randomGroupSets.Count + " (the number of random group sets)");
			return;
		}
		if (groupNumber < 0 || groupNumber >= randomGroupSets[groupSet].randomGroups.Count) {
			Debug.LogError ("UseRandomGroup: groupNumber is " + groupNumber + ", but must be >= 0 and < " + randomGroupSets[groupSet].randomGroups.Count + " (the number of random groups in set " + groupSet + ")");
			return;
		}
		randomGroup = randomGroupSets[groupSet].randomGroups[groupNumber];
		useRandomGroup = useGroup;
	}
	
	public static void UseTerrainGroup (bool useGroup, int groupSet, int groupNumber) {
		if (terrainGroupSets == null) {
			Debug.LogError ("UseTerrainGroup: No terrain groups available");
			return;
		}
		if (groupSet < 0 || groupSet >= terrainGroupSets.Count) {
			Debug.LogError ("UseTerrainGroup: groupSet is " + groupSet + ", but must be >= 0 and < " + terrainGroupSets.Count + " (the number of terrain group sets)");
			return;
		}
		if (groupNumber < 0 || groupNumber >= terrainGroupSets[groupSet].terrainGroups.Count) {
			Debug.LogError ("UseTerrainGroup: groupNumber is " + groupNumber + ", but must be >= 0 and < " + terrainGroupSets[groupSet].terrainGroups.Count + " (the number of terrain groups in set " + groupSet + ")");
			return;
		}
		
		if (terrainGroupActiveList == null) {
			terrainGroupActiveList = new List<Int2>();
		}
		
		var groupID = new Int2(groupSet, groupNumber);
		if (useGroup) {
			if (!terrainGroupActiveList.Contains (groupID)) {
				terrainGroupActiveList.Add (groupID);
			}
			if (terrainLookups == null) {
				InitializeTerrainLookups();
			}
		}
		else {
			terrainGroupActiveList.Remove (groupID);
		}
		useTerrainGroup = (terrainGroupActiveList.Count > 0);
	}
	
	static void InitializeTerrainLookups () {
		terrainLookups = TerrainLookup.InitializeTerrainLookups();
	}
	
	public static void WatchTile (Int2 p, TileWatchFunction tileWatchFunction) {
		WatchTile (p, 0, tileWatchFunction);
	}
	
	public static void WatchTile (Int2 p, int layer, TileWatchFunction tileWatchFunction) {
		if (!CheckParams (p, layer)) return;
		
		if (watchedTiles == null) {
			watchedTiles = new List<List<Int2>>(tileLayers.Count);
			tileWatchFunctions = new List<List<TileWatchFunction>>(tileLayers.Count);
			for (int i = 0; i < tileLayers.Count; i++) {
				watchedTiles.Add (new List<Int2>());
				tileWatchFunctions.Add (new List<TileWatchFunction>());
			}
		}
		int index = watchedTiles[layer].IndexOf (p);
		if (index != -1) {
			tileWatchFunctions[layer][index] = tileWatchFunction;
		}
		else {
			watchedTiles[layer].Add (p);
			tileWatchFunctions[layer].Add (tileWatchFunction);
		}
		
		// See if watched tile is already on-screen
		for (int cam = 0; cam < cams.Length; cam++) {
			if (p.x >= tileLayers[layer].pLowerLeft[cam].x && p.y >= tileLayers[layer].pLowerLeft[cam].y && p.x <= tileLayers[layer].pUpperRight[cam].x && p.y <= tileLayers[layer].pUpperRight[cam].y) {
				tileWatchFunction (p, layer, cam, true);
			}
		}
	}
	
	public static void StopWatchingTile (Int2 p) {
		StopWatchingTile (p, 0);
	}
	
	public static void StopWatchingTile (Int2 p, int layer) {
		if (!CheckParams (p, layer)) return;
		if (watchedTiles == null) {
			Debug.LogError ("StopWatchingTile: tile " + p + " on layer " + layer + " is not being watched");
			return;
		}
		
		int index = watchedTiles[layer].IndexOf (p);
		if (index == -1) {
			Debug.LogError ("StopWatchingTile: tile " + p + " on layer " + layer + " is not being watched");
			return;
		}
		watchedTiles[layer].RemoveAt (index);
		tileWatchFunctions[layer].RemoveAt (index);
	}
	
	public static int AddLightStyle (LightInfo lightInfo) {
		lightList.Add (new LightInfo(lightInfo.style, new Int2(Mathf.Max (lightInfo.size.x, 1), Mathf.Max (lightInfo.size.y, 1)), lightInfo.color, Mathf.Max (lightInfo.innerIntensity, 0.0f), Mathf.Max (lightInfo.outerIntensity, 0.0f), lightInfo.shadowType));
		return lightList.Count - 1;
	}
	
	public static void ChangeLightStyle (int lightNumber, LightInfo lightInfo) {
		if (!CheckLightNumber (lightNumber)) return;
		
		lightList[lightNumber] = new LightInfo(lightInfo.style, new Int2(Mathf.Max (lightInfo.size.x, 1), Mathf.Max (lightInfo.size.y, 1)), lightInfo.color, Mathf.Max (lightInfo.innerIntensity, 0.0f), Mathf.Max (lightInfo.outerIntensity, 0.0f), lightInfo.shadowType);
		for (int i = 0; i < tileLayers.Count; i++) {
			tileLayers[i].layerData.mapData.ResetLighting (lightList);
			foreach (var item in tileLayers[i].layerData.mapData.lightRefs) {
				if (item.Value.number == lightNumber) {
					item.Value.losComputed = false;
				}
			}
			SetScreenSpriteColorBlock (i, Int2.zero, tileLayers[i].layerData.mapData.maxMapPos);
		}
	}
	
	public static void RemoveLightStyle (int lightNumber) {
		if (!CheckLightNumber (lightNumber)) return;
		
		lightList.RemoveAt (lightNumber);
		for (int i = 0; i < tileLayers.Count; i++) {
			tileLayers[i].layerData.mapData.RemoveLightNumber (lightNumber);
			tileLayers[i].layerData.mapData.ResetLighting (lightList);
			SetScreenSpriteColorBlock (i, Int2.zero, tileLayers[i].layerData.mapData.maxMapPos);
		}
	}
	
	public static void SetLight (Int2 p, int lightStyle) {
		SetLight (p, 0, 1.0f, lightStyle, false);
	}
	
	public static void SetLight (Int2 p, float intensity, int lightStyle) {
		SetLight (p, 0, intensity, lightStyle, false);
	}
	
	public static void SetLight (Int2 p, int layer, int lightStyle) {
		SetLight (p, layer, 1.0f, lightStyle, false);
	}
	
	public static void SetLight (Int2 p, float intensity) {
		SetLight (p, 0, intensity, 0, true);
	}
	
	public static void SetLight (Int2 p, int layer, float intensity) {
		SetLight (p, layer, intensity, 0, true);
	}
	
	public static void SetLight (Int2 p, int layer, float intensity, int lightStyle) {
		SetLight (p, layer, intensity, lightStyle, false);
	}
	
	static void SetLight (Int2 p, int layer, float intensity, int lightStyle, bool setStrengthOnly) {
		if (!CheckParams (p, layer) || !CheckLightNumber (lightStyle)) return;
		
		intensity = Mathf.Clamp01 (intensity);
		if (setStrengthOnly) {
			if (!CheckParamsLightExists (p, layer)) return;
			
			lightStyle = tileLayers[layer].layerData.mapData.GetLightNumber (p);
			tileLayers[layer].layerData.mapData.SetLight (p, false, lightStyle, 0.0f, lightList, true);
		}
		Int2 pos1 = tileLayers[layer].layerData.mapData.GetStartPosClamped (p, lightList[lightStyle]);
		Int2 pos2 = tileLayers[layer].layerData.mapData.GetEndPosClamped (p, lightList[lightStyle]);
		if (!setStrengthOnly) {
			int replaceNumber = tileLayers[layer].layerData.mapData.GetLightNumber (p);
			if (replaceNumber != -1) {	// Replace existing light
				tileLayers[layer].layerData.mapData.SetLight (p, false, replaceNumber, 0.0f, lightList, false);
				AdjustScreenCoords (p, layer, replaceNumber, ref pos1, ref pos2);
			}
			if (!tileLayers[layer].layerData.mapData.useColors) {
				tileLayers[layer].layerData.mapData.ResetLighting (lightList);
				pos1 = Int2.zero;
				pos2 = tileLayers[layer].layerData.mapData.maxMapPos;
			}
		}
		tileLayers[layer].layerData.mapData.SetLight (p, true, lightStyle, intensity, lightList, setStrengthOnly);
		SetScreenSpriteColorBlock (layer, pos1, pos2);
	}
	
	static void AdjustScreenCoords (Int2 p, int layer, int oldNumber, ref Int2 pos1, ref Int2 pos2) {
		Int2 oldPos1 = tileLayers[layer].layerData.mapData.GetStartPosClamped (p, lightList[oldNumber]);
		Int2 oldPos2 = tileLayers[layer].layerData.mapData.GetEndPosClamped (p, lightList[oldNumber]);
		pos1.x = Mathf.Min (pos1.x, oldPos1.x);
		pos1.y = Mathf.Min (pos1.y, oldPos1.y);
		pos2.x = Mathf.Max (pos2.x, oldPos2.x);
		pos2.y = Mathf.Max (pos2.y, oldPos2.y);
	}
	
	public static void DeleteLight (Int2 p) {
		DeleteLight (p, 0);
	}
	
	public static void DeleteLight (Int2 p, int layer) {
		if (!CheckParams (p, layer)) return;
		if (!tileLayers[layer].layerData.mapData.lightRefs.ContainsKey (p)) {
			Debug.LogError ("DeleteLight: no light found at position " + p + " in layer " + layer);
			return;
		}
		
		int lightNumber = tileLayers[layer].layerData.mapData.lightRefs[p].number;
		tileLayers[layer].layerData.mapData.SetLight (p, false, lightNumber, 1.0f, lightList, false);
		SetScreenSpriteColorBlock (layer, tileLayers[layer].layerData.mapData.GetStartPosClamped (p, lightList[lightNumber]), tileLayers[layer].layerData.mapData.GetEndPosClamped (p, lightList[lightNumber]));
	}
	
	public static void MoveLight (Int2 fromPos, Int2 toPos) {
		MoveLight (fromPos, toPos, 0);
	}
	
	public static void MoveLight (Int2 pFrom, Int2 pTo, int layer) {
		if (!CheckParams (pFrom, layer) || !CheckParams (pTo, layer)) return;
		if (!tileLayers[layer].layerData.mapData.HasLight (pFrom)) {
			Debug.LogError ("MoveLight: no light found at " + pFrom + " in layer " + layer);
			return;
		}
		if (pFrom == pTo) return;
		
		int lightStyle = tileLayers[layer].layerData.mapData.GetLightNumber (pFrom);
		Int2 toPos1 = tileLayers[layer].layerData.mapData.GetStartPosClamped (pTo, lightList[lightStyle]);
		Int2 toPos2 = tileLayers[layer].layerData.mapData.GetEndPosClamped (pTo, lightList[lightStyle]);
		int replaceNumber = tileLayers[layer].layerData.mapData.GetLightNumber (pTo);
		if (replaceNumber != -1) {
			tileLayers[layer].layerData.mapData.SetLight (pTo, false, replaceNumber, 0.0f, lightList, false);
			AdjustScreenCoords (pTo, layer, replaceNumber, ref toPos1, ref toPos2);
		}
		tileLayers[layer].layerData.mapData.MoveLight (pFrom, pTo, lightList);
		Int2 fromPos1 = tileLayers[layer].layerData.mapData.GetStartPosClamped (pFrom, lightList[lightStyle]);
		Int2 fromPos2 = tileLayers[layer].layerData.mapData.GetEndPosClamped (pFrom, lightList[lightStyle]);
		toPos1.x = Mathf.Min (toPos1.x, fromPos1.x);	// Combined rect
		toPos1.y = Mathf.Min (toPos1.y, fromPos1.y);
		toPos2.x = Mathf.Max (toPos2.x, fromPos2.x);
		toPos2.y = Mathf.Max (toPos2.y, fromPos2.y);
		SetScreenSpriteColorBlock (layer, toPos1, toPos2);
	}
	
	public static void RefreshLight (Int2 p) {
		RefreshLight (p, 0);
	}
	
	public static void RefreshLight (Int2 p, int layer) {
		if (!CheckParamsLightExists (p, layer)) return;
		
		int lightNumber = tileLayers[layer].layerData.mapData.GetLightNumber (p);
		float intensity = tileLayers[layer].layerData.mapData.GetLightIntensity (p);
		tileLayers[layer].layerData.mapData.SetLight (p, false, lightNumber, 0.0f, lightList, true);
		tileLayers[layer].layerData.mapData.lightRefs[p].losComputed = false;
		tileLayers[layer].layerData.mapData.SetLight (p, true, lightNumber, intensity, lightList, true);
		SetScreenSpriteColorBlock (layer, tileLayers[layer].layerData.mapData.GetStartPosClamped (p, lightList[lightNumber]), tileLayers[layer].layerData.mapData.GetEndPosClamped (p, lightList[lightNumber]));
	}
	
	public static bool HasLight (Int2 p) {
		return HasLight (p, 0);
	}
	
	public static bool HasLight (Int2 p, int layer) {
		if (!CheckParams (p, layer)) return false;
		
		return tileLayers[layer].layerData.mapData.HasLight (p);
	}
	
	public static int GetLightStyle (Int2 p) {
		return GetLightStyle (p, 0);
	}
	
	public static int GetLightStyle (Int2 p, int layer) {
		if (!CheckParamsLightExists (p, layer)) return -1;
		
		return tileLayers[layer].layerData.mapData.GetLightNumber (p);
	}
	
	public static float GetLightIntensity (Int2 p) {
		return GetLightIntensity (p, 0);
	}
	
	public static float GetLightIntensity (Int2 p, int layer) {
		if (!CheckParamsLightExists (p, layer)) return 0.0f;
		
		return tileLayers[layer].layerData.mapData.lightRefs[p].intensity;
	}
	
	public static List<Int2> GetLightPositions () {
		return GetLightPositions (0);
	}
	
	public static List<Int2> GetLightPositions (int layer) {
		if (!CheckParams (layer)) return null;
		
		var positions = new List<Int2>(tileLayers[layer].layerData.mapData.lightRefs.Count);
		foreach (var item in tileLayers[layer].layerData.mapData.lightRefs) {
			positions.Add (item.Key);
		}
		return positions;
	}
	
	public static void SetAmbient (Color32 color) {
		SetAmbient (color, 0);
	}
	
	public static void SetAmbient (Color32 color, int layer) {
		if (!CheckParams (layer)) return;
		
		tileLayers[layer].layerData.mapData.ambientColor = color;
		tileLayers[layer].layerData.mapData.ResetLighting (lightList);
		SetScreenSpriteColorBlock (layer, Int2.zero, tileLayers[layer].layerData.mapData.maxMapPos);
	}
	
	public static void UseRadiosity (bool active) {
		useRadiosity = active;
		for (int i = 0; i < tileLayers.Count; i++) {
			tileLayers[i].layerData.mapData.useRadiosity = active;
			if (tileLayers[i].layerData.mapData.lightRefs.Count > 0) {
				tileLayers[i].layerData.mapData.ResetShadowCastLights (lightList, true);
				SetScreenSpriteColorBlock (i, Int2.zero, tileLayers[i].layerData.mapData.maxMapPos);
			}
		}
	}
	
	static bool CheckLevelData (LevelData levelData) {
		if (levelData.mapSize.x < 1 || levelData.mapSize.y < 1) {
			Debug.LogError ("MapSize must be at least 1x1");
			return false;
		}
		if (levelData.addBorder < 0) {
			Debug.LogError ("AddBorder must be >= 0");
			return false;
		}
		if (levelData.tileSize.x < .001f || levelData.tileSize.y < .001f) {
			Debug.LogError ("TileSize must be >= .001");
			return false;
		}
		return true;
	}
	
	static bool CheckLayerNumber (int layerNumber) {
		if (layerNumber < 0 || layerNumber >= sortingLayerNames.Length) {
			Debug.LogError ("LayerNumber " + layerNumber + " is out of bounds...must be >= 0 and < " + sortingLayerNames.Length + " (the number of sorting layer names)");
			return false;
		}
		return true;
	}
	
	static bool CheckLayerCount (int layer) {
		if (layer < 0 || layer >= tileLayers.Count) {
			Debug.LogError ("Layer " + layer + " is out of bounds...must be >= 0 and < " + tileLayers.Count + " (the number of layers)");
			return false;
		}
		return true;
	}
	
	static bool CheckLightNumber (int lightNumber) {
		if (lightList.Count == 0) {
			Debug.LogError ("No lights exist");
			return false;
		}
		if (lightNumber < 0 || lightNumber >= lightList.Count) {
			Debug.LogError ("LightNumber " + lightNumber + " is out of bounds...must be >= 0 and < " + lightList.Count + " (the number of light types in this level)");
			return false;
		}
		return true;
	}
	
	static bool CheckParamsLightExists (Int2 p, int layer) {
		if (!CheckParams (p, layer)) return false;
		if (!tileLayers[layer].layerData.mapData.lightRefs.ContainsKey (p)) {
			Debug.LogError ("No light found at position " + p + " in layer " + layer);
			return false;
		}
		return true;
	}
	
	static bool CheckParams (byte[] bytes) {
		if (bytes == null) {
			Debug.LogError ("bytes must not be null");
			return false;
		}
		return true;
	}

	static bool CheckParams (TextAsset level) {
		if (level == null) {
			Debug.LogError ("Level must not be null");
			return false;
		}
		return true;
	}
	
	static bool CheckParams (int layer) {
		if (tileLayers == null) {
			Debug.LogError ("No tile layers are set up");
			return false;
		}
		if (layer < 0 || layer >= tileLayers.Count) {
			Debug.LogError ("Layer number must be >= 0 and < " + tileLayers.Count);
			return false;
		}
		return true;
	}

	static bool CheckParams (Int2 p, int layer) {
		if (tileLayers == null) {
			Debug.LogError ("No tile layers are set up");
			return false;
		}
		if (layer < 0 || layer >= tileLayers.Count) {
			Debug.LogError ("Layer number must be >= 0 and < " + tileLayers.Count);
			return false;
		}
		if (p.x < 0 || p.x >= tileLayers[layer].layerData.mapData.mapSize.x || p.y < 0 || p.y >= tileLayers[layer].layerData.mapData.mapSize.y) {
			Debug.LogError ("Position " + p + " out of bounds. Map size for layer " + layer + " is " + tileLayers[layer].layerData.mapData.mapSize);
			return false;
		}
		return true;
	}

	static bool CheckParams (int set, int tile) {
		if (set < 0 || set >= spriteSets.Count) {
			Debug.LogError ("set must be >= 0 and < " + spriteSets.Count);
			return false;
		}
		if (tile < 0 || tile >= spriteSets[set].Count) {
			Debug.LogError ("tile must be >= 0 and < " + spriteSets[set].Count + " for set " + set);
			return false;
		}
		return true;
	}

	static bool CheckParams (int layer, int set, int tile) {
		if (tileLayers == null) {
			Debug.LogError ("No tile layers are set up");
			return false;
		}
		if (layer < 0 || layer >= tileLayers.Count) {
			Debug.LogError ("Layer number must be >= 0 and < " + tileLayers.Count);
			return false;
		}
		if (set < 0 || set >= spriteSets.Count) {
			Debug.LogError ("set must be >= 0 and < " + spriteSets.Count);
			return false;
		}
		if (tile < 0 || tile >= spriteSets[set].Count) {
			Debug.LogError ("tile must be >= 0 and < " + spriteSets[set].Count + " for set " + set);
			return false;
		}
		return true;
	}
	
	static bool CheckParams (Int2 p, int layer, int set, int tile) {
		if (tileLayers == null) {
			Debug.LogError ("No tile layers are set up");
			return false;
		}
		if (layer < 0 || layer >= tileLayers.Count) {
			Debug.LogError ("Layer number must be >= 0 and < " + tileLayers.Count);
			return false;
		}
		if (p.x < 0 || p.x >= tileLayers[layer].layerData.mapData.mapSize.x || p.y < 0 || p.y >= tileLayers[layer].layerData.mapData.mapSize.y) {
			Debug.LogError ("Position " + p + " out of bounds. Map size for layer " + layer + " is " + tileLayers[layer].layerData.mapData.mapSize);
			return false;
		}
		if (set < 0 || set >= spriteSets.Count) {
			Debug.LogError ("set must be >= 0 and < " + spriteSets.Count);
			return false;
		}
		if (tile < 0 || tile >= spriteSets[set].Count) {
			Debug.LogError ("tile must be >= 0 and < " + spriteSets[set].Count + " for set " + set);
			return false;
		}
		return true;
	}
	
	static bool CheckGroupParams (int groupSet, int groupNumber) {
		if (groupSets == null) {
			Debug.LogError ("No groups found");
			return false;
		}
		if (groupSet < 0 || groupSet >= groupSets.Count) {
			Debug.LogError ("Group set \"" + groupSet + "\" out of range (group sets are 0 to " + (groupSets.Count-1) + ")");
			return false;
		}
		if (groupNumber < 0 || groupNumber >= groupSets[groupSet].Count) {
			Debug.LogError ("Group number \"" + groupNumber + "\" out of range for group set " + groupSet + " (group numbers are 0 to " + (groupSets[groupSet].Count-1) + ")");
			return false;
		}
		return true;
	}
}

}