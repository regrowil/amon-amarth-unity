namespace SpriteTile {

[System.Serializable]
public struct TileInfo : System.IEquatable<TileInfo> {
	public int set;
	public int tile;
	
	public static readonly TileInfo empty = new TileInfo(0, -1);
	
	public TileInfo (int set, int tile) {
		this.set = set;
		this.tile = tile;
	}
	
	public static TileInfo operator + (TileInfo a, TileInfo b) {
		return new TileInfo (a.set + b.set, a.tile + b.tile);
	}
	
	public static TileInfo operator - (TileInfo a, TileInfo b) {
		return new TileInfo (a.set - b.set, a.tile - b.tile);
	}
	
	public static bool operator == (TileInfo a, TileInfo b) {
		return (a.set == b.set && a.tile == b.tile);
	}

	public static bool operator != (TileInfo a, TileInfo b) {
		return (a.set != b.set || a.tile != b.tile);
	}
	
	public override bool Equals (System.Object obj) {
		if (obj == null || GetType() != obj.GetType()) {
			return false;
		}
		TileInfo i = (TileInfo)obj;
		return (set == i.set && tile == i.tile);
	}
	
	public bool Equals (TileInfo i) {
		return (set == i.set && tile == i.tile);
	}
	
	public override int GetHashCode() {
		return set.GetHashCode () ^ tile.GetHashCode () << 2;
	}
	
	public override string ToString () {
		return "(" + this.set + ", " + this.tile + ")";
	}
}

}