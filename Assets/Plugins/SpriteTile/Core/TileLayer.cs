// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using System;
using UnityEngine;
using System.Collections.Generic;

namespace SpriteTile
{

    public class TileLayer
    {
        public LayerData layerData;
        public Transform container;
        public Int2[] screenTileCount;
        public int[] leftSlot;
        public int[] rightSlot;
        public int[] topSlot;
        public int[] bottomSlot;
        public Int2[] previousTilePos;
        public Vector3[] storedCamPos;
        public Int2[] pLowerLeft;
        public Int2[] pUpperRight;
        public Vector3 offset;

        public TileLayer(LayerData layerData, int layerNumber, int numberOfCameras)
        {
            this.layerData = layerData;
            container = new GameObject("SpriteTileLayer" + layerNumber).transform;
            MonoBehaviour.DontDestroyOnLoad(container);
            screenTileCount = new Int2[numberOfCameras];
            leftSlot = new int[numberOfCameras];
            rightSlot = new int[numberOfCameras];
            bottomSlot = new int[numberOfCameras];
            topSlot = new int[numberOfCameras];
            previousTilePos = new Int2[numberOfCameras];
            storedCamPos = new Vector3[numberOfCameras];
            pLowerLeft = new Int2[numberOfCameras];
            pUpperRight = new Int2[numberOfCameras];
        }

        public void Destroy()
        {
            GameObject.Destroy(container);
        }
    }

    public class LayerData
    {
        public MapData mapData;
        public Vector2 tileSize;
        public Vector2 scrollPos;
        public int previewSize;
        public float zPosition;
        public LayerLock layerLock;
        public int addBorder;

        public LayerData(Int2 size, bool clear)
        {
            this.mapData = new MapData(size, clear);
            this.addBorder = 0;
            this.tileSize = Vector2.zero;
            this.scrollPos = Vector2.zero;
            this.previewSize = 64;
            this.zPosition = 0.0f;
            this.layerLock = LayerLock.None;
        }

        public LayerData(MapData mapData, int addBorder, Vector2 tileSize, float zPosition, LayerLock layerLock)
        {
            this.mapData = mapData;
            this.addBorder = addBorder;
            this.tileSize = tileSize;
            this.zPosition = zPosition;
            this.scrollPos = Vector2.zero;
            this.previewSize = 64;
            this.layerLock = layerLock;
        }

        public LayerData(MapData mapData, int addBorder, Vector2 tileSize, Vector2 scrollPos, int previewSize, float zPosition, LayerLock layerLock)
        {
            this.mapData = mapData;
            this.addBorder = addBorder;
            this.tileSize = tileSize;
            this.scrollPos = scrollPos;
            this.previewSize = previewSize;
            this.zPosition = zPosition;
            this.layerLock = layerLock;
        }
    }

    public class TileData
    {
        public GUIContent content;
        public string baseName; // Name only, no extension
        public Vector2 size;    // In units
        public Vector2 pivot;   // (0, 0) = center, (x, x) = lower left, (-x, -x) = upper right
        public Vector2 displayRatio;
        public Texture texture;
        public Rect textureRect;
        public int orderInLayer;
        public bool useCollider;
        public float rotation;
        public int trigger;
        public bool usePhysics;
        public bool useNonTransparent;
        public bool useLighting;
        public bool customCollider;
        public int spriteAtlasCount;
        public int realTileNumber;
        public Sprite sprite;

        public TileData(string name, string baseName, Texture texture, Vector2 size, Vector2 pivot, Rect textureRect, Vector2 displayRatio, int orderInLayer, float rotation, int trigger, bool useCollider, int spriteAtlasCount, bool usePhysics, bool useNonTransparent, bool useLighting, bool customCollider, int realTileNumber, Sprite sprite)
        {
            this.orderInLayer = orderInLayer;
            this.rotation = rotation;
            this.trigger = trigger;
            this.useCollider = useCollider;
            this.spriteAtlasCount = spriteAtlasCount;
            this.usePhysics = usePhysics;
            this.useNonTransparent = useNonTransparent;
            this.useLighting = useLighting;
            this.customCollider = customCollider;
            this.realTileNumber = realTileNumber;

            SetData(name, baseName, texture, size, pivot, textureRect, displayRatio, sprite);
        }

        public void SetData(string name, string baseName, Texture texture, Vector2 size, Vector2 pivot, Rect textureRect, Vector2 displayRatio, Sprite sprite)
        {
            this.content = new GUIContent(name);
            this.baseName = baseName;
            this.texture = texture;
            this.size = size;
            this.pivot = pivot;
            this.displayRatio = displayRatio;
            this.textureRect = ComputeTextureRect(texture, textureRect);
            this.sprite = sprite;
        }

        public static Rect ComputeTextureRect(Texture texture, Rect rect)
        {
            float w = texture.width;
            float h = texture.height;
            return new Rect(rect.x / w, rect.y / h, rect.width / w, rect.height / h);
        }
    }

    public class TilesetData
    {
        public List<TileData> tileData;
        public int selectedTile;

        public TilesetData()
        {
            tileData = new List<TileData>();
            selectedTile = 0;
        }

        public TilesetData(List<TileData> tileData)
        {
            this.tileData = tileData;
            selectedTile = 0;
        }
    }

    public class SpriteGroup
    {
        public List<TileInfo> tileNumbers;
        public MapData mapData;
        public GUIContent content;
        public Vector2 tileSize;

        public SpriteGroup(List<TileInfo> tileNumbers, MapData mapData, GUIContent content, Vector2 tileSize)
        {
            this.tileNumbers = tileNumbers;
            this.mapData = mapData;
            this.content = content;
            this.tileSize = tileSize;
        }
    }

    public class SpriteGroupData
    {
        public List<SpriteGroup> spriteGroups;
        public int selectedGroup;

        public SpriteGroupData()
        {
            spriteGroups = new List<SpriteGroup>();
            selectedGroup = 0;
        }

        public SpriteGroupData(List<SpriteGroup> spriteGroups)
        {
            this.spriteGroups = spriteGroups;
            selectedGroup = 0;
        }
    }

    public class RandomGroupData
    {
        public List<List<TileInfo>> randomGroups;
        public int selectedGroup;

        public RandomGroupData()
        {
            randomGroups = new List<List<TileInfo>>();
            selectedGroup = 0;
        }

        public RandomGroupData(List<List<TileInfo>> randomGroups)
        {
            this.randomGroups = randomGroups;
            selectedGroup = 0;
        }
    }

    public class LightGroup
    {   //---
        public Int2 position;
        public Int2 size;
        public Color32 colorInner;
        public Color32 colorOuter;
        public float intensity;
        public LightStyle lightStyle;
        public Texture2D texture;
        public string label;

        public LightGroup(Int2 position, Int2 size, Color32 colorInner, Color32 colorOuter, float intensity, LightStyle lightStyle, string label)
        {
            this.position = position;
            this.size = size;
            this.colorInner = colorInner;
            this.colorOuter = colorOuter;
            this.intensity = intensity;
            this.lightStyle = lightStyle;
            this.label = label;
        }
    }

    public class LightGroupData
    {
        public List<LightGroup> lightGroups;
        public int selectedGroup;

        public LightGroupData()
        {
            lightGroups = new List<LightGroup>();
            selectedGroup = 0;
        }

        public LightGroupData(List<LightGroup> lightGroups)
        {
            this.lightGroups = lightGroups;
            selectedGroup = 0;
        }
    }

    public class SpriteCached
    {
        public SpriteRenderer spriteRenderer;
        public Transform transform;
        public bool enabled = true; // Optimization for quickly checking, instead of spriteRenderer.enabled
        public Vector3 storedPosition;

        public SpriteCached(string name)
        {
            var go = new GameObject(name);
            this.spriteRenderer = go.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            this.transform = go.transform;
        }
    }

    public class ColliderData
    {
        public int pathCount;
        public Vector2[][] points;

        public ColliderData(int pathCount, Vector2[][] points)
        {
            this.pathCount = pathCount;
            this.points = points;
        }

        public ColliderData(int pathCount)
        {
            this.pathCount = pathCount;
        }
    }

    public class AnimationInfo
    {
        public TileInfo[] tileInfoArray;
        public float frameTime;
        public float timer;
        public int frame;
        public AnimType animType;
        public int direction;

        public AnimationInfo(TileInfo[] tileInfoArray, float frameTime, AnimType animType)
        {
            this.tileInfoArray = tileInfoArray;
            this.frameTime = frameTime;
            this.animType = animType;
            this.timer = 0.0f;
            this.frame = 0;
            this.direction = (tileInfoArray.Length > 1) ? 1 : 0;
        }
    }

    public class AnimationPosition
    {
        public Int2 p;
        public AnimationInfo animationInfo;

        public AnimationPosition(Int2 p, AnimationInfo animationInfo)
        {
            this.p = p;
            this.animationInfo = animationInfo;
        }
    }

    public class LevelData
    {
        public Int2 mapSize;
        public int addBorder;
        public Vector2 tileSize;
        public float zPosition;
        public LayerLock layerLock;

        public LevelData(Int2 mapSize, int addBorder, Vector2 tileSize, float zPosition, LayerLock layerLock)
        {
            this.mapSize = mapSize;
            this.addBorder = addBorder;
            this.tileSize = tileSize;
            this.zPosition = zPosition;
            this.layerLock = layerLock;
        }
    }

    public class Bookmark
    {
        public Vector2 scrollPos;
        public int zoom;
        public bool defined;

        public Bookmark(Vector2 scrollPos, int zoom, bool defined)
        {
            this.scrollPos = scrollPos;
            this.zoom = zoom;
            this.defined = defined;
        }
    }

    public class ColliderInfo
    {
        public int layer;
        public string tag;
        public PhysicsMaterial2D material;
        public bool isTrigger;

        public ColliderInfo(int layer, string tag, PhysicsMaterial2D material, bool isTrigger)
        {
            this.layer = layer;
            this.tag = tag;
            this.material = material;
            this.isTrigger = isTrigger;
        }
    }

    [Serializable]
    public class PropertyInfo
    {
        public float f;
        public string s;
        public GameObject go;
        public int guid;

        public PropertyInfo()
        {
            this.f = 0.0f;
            this.s = null;
            this.go = null;
            this.guid = -1;
        }

        public PropertyInfo(float f, string s, GameObject go, int guid)
        {
            this.f = f;
            this.s = s;
            this.go = go;
            this.guid = guid;
        }



        public PropertyInfo(PropertyInfo p)
        {
            this.f = p.f;
            this.s = p.s;
            this.go = p.go;
            this.guid = p.guid;
        }
    }

    public class LightInfo
    {
        public LightStyle style;
        public Int2 size;
        public Int2 radius { get; private set; }
        public Int2 radiusR { get; private set; }
        public Color32 color;
        public float innerIntensity;
        public float outerIntensity;
        public ShadowType shadowType;

        public LightInfo()
        {
            this.style = LightStyle.Box;
            this.size = Int2.zero;
            this.radius = Int2.zero;
            this.color = Color.white;
            this.innerIntensity = 0.0f;
            this.outerIntensity = 0.0f;
            this.shadowType = ShadowType.None;
        }

        public LightInfo(LightStyle style, Int2 size, Color32 color, float innerIntensity, float outerIntensity, ShadowType shadowType)
        {
            this.style = style;
            this.color = color;
            this.innerIntensity = innerIntensity;
            this.outerIntensity = outerIntensity;
            this.shadowType = shadowType;
            SetSize(size);
        }

        public void SetSize(Int2 size)
        {
            this.size = size;
            Int2 radius = this.radius;
            Int2 radiusR = this.radiusR;
            if (this.style == LightStyle.Box)
            {
                radiusR = size / 2;
                radius = radiusR;
                if ((size.x & 1) == 0)
                {   // Change by 1 for even sizes
                    radius.x--;
                }
                if ((size.y & 1) == 0)
                {
                    radius.y--;
                }
            }
            else
            {
                radius = size / 2;
                if ((size.x & 1) == 0)
                {
                    radius.x--;
                }
                if ((size.y & 1) == 0)
                {
                    radius.y--;
                }
                radiusR = radius;
            }
            this.radius = radius;
            this.radiusR = radiusR;
            if (size.x <= 3 && size.y <= 3)
            {
                this.shadowType = ShadowType.None;
            }
        }
    }

    public class Light
    {
        public int number;
        public float intensity;
        public bool losComputed;
        public byte[] shadowMap;

        public Light()
        {
            this.number = -1;
            this.intensity = 1.0f;
            this.losComputed = false;
        }

        public Light(int number, float intensity)
        {
            this.number = number;
            this.intensity = intensity;
            this.losComputed = false;
        }

        public Light(Light light)
        {
            this.number = light.number;
            this.intensity = light.intensity;
            this.losComputed = false;
            this.shadowMap = light.shadowMap;
        }

        public void SetupShadow(int radius)
        {
            this.shadowMap = new byte[(radius * 2 + 1) * (radius * 2 + 1)];
            this.losComputed = false;
        }

        public bool NeedsReset(int radius)
        {
            return (this.shadowMap == null || this.shadowMap.Length != (radius * 2 + 1) * (radius * 2 + 1));
        }
    }

    public struct MapElement
    {
        public Int2 position;
        public short mapTile;
        public ushort info;
        public short order;
        public byte trigger;
        public PropertyInfo propertyInfo;
        public Light light;

        public MapElement(Int2 position, short mapTile, ushort info, short order, byte trigger, PropertyInfo propertyInfo, Light light)
        {
            this.position = position;
            this.mapTile = mapTile;
            this.info = info;
            this.order = order;
            this.trigger = trigger;
            this.propertyInfo = propertyInfo;
            this.light = light;
        }
    }

}