// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using SpriteTile;

namespace SpriteTile {

public class TileManager : ScriptableObject {
	public List<SpriteDataList> spriteDataList;
	[HideInInspector]
	public string[] sortingLayerNames;
	[HideInInspector]
	public int tilesetCount;
	[HideInInspector]
	public List<GOid> goList;
	[HideInInspector]
	public int masterID;
	
	public bool DataListInitialized () {
		return spriteDataList != null;
	}
	
	public int SpriteSetCount () {
		if (spriteDataList == null) {
			return 0;
		}
		return spriteDataList.Count;
	}
	
	public int SpriteListCount (int i) {
		return spriteDataList[i].spriteList.Count;
	}
	
	public SpriteData GetSpriteData (int i, int j) {
		return spriteDataList[i].spriteList[j];
	}
	
	public void InitializeList () {
		spriteDataList = new List<SpriteDataList>();
	}
	
	public void AddNewList () {
		spriteDataList.Add (new SpriteDataList());
	}
	
	public void AddSprite (int set, Sprite spr, int spriteCount, int realTileNumber) {
		spriteDataList[set].spriteList.Add (new SpriteData(spr, spriteCount, realTileNumber));
	}
	
	public void UpdateSprite (int set, int i, Sprite spr) {
		spriteDataList[set].spriteList[i].sprite = spr;
	}
	
	public void UpdateSpritePaths (int set, int i, ColliderPath[] paths) {
		spriteDataList[set].spriteList[i].paths = paths;
	}
	
	public void UpdateSpriteNumber (int set, int i, int realTileNumber) {
		spriteDataList[set].spriteList[i].realTileNumber = realTileNumber;
	}
	
	public void DeleteSprite (int set, int i) {
		spriteDataList[set].spriteList.RemoveAt (i);
	}

	public void DeleteSpriteSet (int set) {
		spriteDataList.RemoveAt (set);
	}

	public void ClearList (int set) {
		spriteDataList[set].spriteList.Clear();
	}
	
	public void SetSpriteData (int set, int i, bool useCollider, int order, float rotation, int trigger, ColliderPath[] paths, bool usePhysics, bool useNonTransparent, bool useLighting, bool customCollider) {
		spriteDataList[set].spriteList[i].useCollider = useCollider;
		spriteDataList[set].spriteList[i].orderInLayer = order;
		spriteDataList[set].spriteList[i].rotation = rotation;
		spriteDataList[set].spriteList[i].trigger = trigger;
		if (!customCollider) {
			spriteDataList[set].spriteList[i].paths = paths;
		}
		spriteDataList[set].spriteList[i].usePhysics = usePhysics;
		spriteDataList[set].spriteList[i].useNonTransparent = useNonTransparent;
		spriteDataList[set].spriteList[i].useLighting = useLighting;
		spriteDataList[set].spriteList[i].customCollider = customCollider;
	}
	
	public Sprite GetSprite (int set, int i) {
		return spriteDataList[set].spriteList[i].sprite;
	}
	
	public void MatchTileDataList (int set, List<TileData> tList) {
		var numberArray = new int[tList.Count];
		for (int i = 0; i < tList.Count; i++) {
			numberArray[i] = tList[i].realTileNumber;
		}
		spriteDataList[set].spriteList = spriteDataList[set].spriteList.OrderBy (td => System.Array.IndexOf (numberArray, td.realTileNumber)).ToList();
	}
	
	public void SetCustomCollider (int set, int i, bool customCollider) {
		spriteDataList[set].spriteList[i].customCollider = customCollider;
	}
	
	public string[] GetSortingLayerNames () {
		if (sortingLayerNames == null || sortingLayerNames.Length < 1) {
			sortingLayerNames = new string[1];
			sortingLayerNames[0] = "Default";
		}
		return sortingLayerNames;
	}
	
	public int AddGO (GameObject go) {
		if (goList == null || goList.Count == 0) {
			goList = new List<GOid>();
			masterID = 0;
		}
		
		for (int i = 0; i < goList.Count; i++) {
			if (goList[i].go == go) {
				return goList[i].guid;
			}
		}
		if (masterID == int.MaxValue) {	// User added over 2 billion GOs!?!?!
			return -1;
		}
		goList.Add (new GOid(go, masterID));
		return masterID++;
	}
	
	public GameObject GetGameObject (int guid) {
		if (goList == null) {
			return null;
		}
		
		int index = IDIndex (guid);
		if (index == -1) {
			return null;
		}
		return goList[index].go;
	}
	
	int IDIndex (int id) {
		if (goList == null) {
			return -1;
		}
		
		for (int i = 0; i < goList.Count; i++) {
			if (goList[i].guid == id) {
				return i;
			}
		}
		return -1;
	}
}

[System.Serializable]
public class SpriteDataList {
	public List<SpriteData> spriteList;
	
	public SpriteDataList () {
		spriteList = new List<SpriteData>();
	}
}

[System.Serializable]
public class SpriteData {
	public Sprite sprite;
	public bool useCollider = false;
	public int orderInLayer = 0;
	public float rotation = 0.0f;
	public int trigger = 0;
	public bool usePhysics = false;
	public bool useNonTransparent = false;
	public bool useLighting = false;
	public bool customCollider = false;
	public ColliderPath[] paths;
	public int spriteCount; // Number of sprites in atlas
	public int realTileNumber;
	
	public SpriteData (Sprite sprite, int spriteCount, int realTileNumber) {
		this.sprite = sprite;
		this.spriteCount = spriteCount;
		this.realTileNumber = realTileNumber;
	}
}

[System.Serializable]
public class ColliderPath {
	public Vector2[] path;
	
	public ColliderPath (Vector2[] path) {
		this.path = path;
	}
}

[System.Serializable]
public class GOid {
	public GameObject go;
	public string name;
	public int guid;
	
	public GOid (GameObject go, int guid) {
		this.go = go;
		this.name = go.name;
		this.guid = guid;
	}
}

}