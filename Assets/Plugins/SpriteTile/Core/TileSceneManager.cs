// Version 2.13
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

using UnityEngine;
using System.Collections.Generic;

namespace SpriteTile {

public class TileSceneManager : MonoBehaviour {

	Transform[] camTransforms;
	Camera[] cams;
	Vector3[] previousPos;
	public delegate void CameraMoveFunction(Vector3 position, int camNumber);
	public delegate void ColliderUpdateFunction();
	public delegate void CameraUpdateFunction();
	public delegate void AnimationFunction(float deltaTime);
	CameraMoveFunction cameraMoveFunction;
	ColliderUpdateFunction colliderUpdateFunction;
	CameraUpdateFunction cameraUpdateFunction;
	AnimationFunction animationFunction;
	bool updateColliders = false;
	Int2 previousScreenSize;
	bool[] isOrthographic;
	float[] previousOrthoSize;
	bool doAnimation = false;
	CamRotation[] camRotations;
	Vector3[] rotationAngles;
	Vector3 V3zero = Vector3.zero;
		
	void Awake () {
		var scriptArray = FindObjectsOfType (typeof(TileSceneManager));
		if (scriptArray.Length > 1) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (gameObject);
		previousScreenSize = new Int2(Screen.width, Screen.height);
	}
	
	public void SetCamera (Transform[] camTransforms, CameraMoveFunction cameraMoveFunction, ColliderUpdateFunction colliderUpdateFunction, CameraUpdateFunction cameraUpdateFunction) {
		this.cameraMoveFunction = cameraMoveFunction;
		this.colliderUpdateFunction = colliderUpdateFunction;
		this.cameraUpdateFunction = cameraUpdateFunction;
		SetCamera (camTransforms);
	}
	
	public void SetCamera (Transform[] camTransforms) {
		this.camTransforms = camTransforms;
		this.cams = new Camera[camTransforms.Length];
		this.previousPos = new Vector3[camTransforms.Length];
		this.isOrthographic = new bool[camTransforms.Length];
		this.previousOrthoSize = new float[camTransforms.Length];
		this.camRotations = new CamRotation[camTransforms.Length];
		this.rotationAngles = new Vector3[camTransforms.Length];
		for (int i = 0; i < camTransforms.Length; i++) {
			this.cams[i] = camTransforms[i].GetComponent<Camera>();
			this.previousPos[i] = camTransforms[i].position;
			this.isOrthographic[i] = this.cams[i].orthographic;
			this.previousOrthoSize[i] = this.cams[i].orthographicSize;
			this.camRotations[i] = CamRotation.None;	
		}
	}
	
	public void CameraRotation (CamRotation camRotation, float degrees, int camNumber) {
		if (isOrthographic[camNumber] && (camRotation == CamRotation.X || camRotation == CamRotation.Y)) {
			Debug.LogError ("When using X or Y camera rotation, the camera must be using perspective mode");
			return;
		}
		camRotations[camNumber] = camRotation;
		if (camRotation == CamRotation.X) {
			rotationAngles[camNumber].x = degrees;
			rotationAngles[camNumber].y = 0.0f;
			rotationAngles[camNumber].z = 0.0f;
		}
		else if (camRotation == CamRotation.Y) {
			rotationAngles[camNumber].x = 0.0f;
			rotationAngles[camNumber].y = degrees;
			rotationAngles[camNumber].z = 0.0f;
		}
		else {
			rotationAngles[camNumber].x = 0.0f;
			rotationAngles[camNumber].y = 0.0f;
			rotationAngles[camNumber].z = degrees;
		}		
	}
	
	public void SetColliderUpdate () {
		updateColliders = true;
	}
	
	public void SetAnimation (bool animate) {
		doAnimation = animate;
	}
	
	public void SetAnimationFunction (AnimationFunction animationFunction) {
		this.animationFunction = animationFunction;
	}
	
	void LateUpdate () {
		if (camTransforms == null) return;
		
		for (int i = 0; i < camTransforms.Length; i++) {
			if (camTransforms[i] == null) return;
		
			if (Screen.width != previousScreenSize.x || Screen.height != previousScreenSize.y) {
				previousScreenSize.x = Screen.width;
				previousScreenSize.y = Screen.height;
				cameraUpdateFunction();
			}
			else if (isOrthographic[i] && cams[i].orthographicSize != previousOrthoSize[i]) {
				previousOrthoSize[i] = cams[i].orthographicSize;
				cameraUpdateFunction();
			}
			
			if (camRotations[i] == CamRotation.X || camRotations[i] == CamRotation.Y) {
				camTransforms[i].localEulerAngles = rotationAngles[i];
			}
			else {
				camTransforms[i].localEulerAngles = V3zero;
			}
			if (camTransforms[i].position != previousPos[i]) {
				cameraMoveFunction (camTransforms[i].position - previousPos[i], i);
				previousPos[i] = camTransforms[i].position;
			}
			if (camRotations[i] == CamRotation.Z) {
				camTransforms[i].localEulerAngles = rotationAngles[i];
			}
		}

		if (updateColliders) {
			updateColliders = false;
			colliderUpdateFunction();
		}
		
		if (doAnimation) {
			animationFunction (Time.deltaTime);
		}
	}
	
	public void ForceUpdate () {
		if (camTransforms == null) return;
		
		for (int i = 0; i < camTransforms.Length; i++) {
			if (camTransforms[i] == null) continue;
			
			cameraMoveFunction (camTransforms[i].position - previousPos[i], i);
			previousPos[i] = camTransforms[i].position;
		}
	}
}

}