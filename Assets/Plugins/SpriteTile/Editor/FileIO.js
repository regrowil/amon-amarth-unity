// Version 3.1
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

#pragma strict
import SpriteTile;
import TileEditor;
import UnityEngine.GUILayout;

static function Save (saveGroups : boolean, saveAs : boolean) : boolean {
	if (saveAs || currentFileName == "") {
		var thisSavedLevelPath = savedLevelPath;
		if (!saveGroups && savedLevelPath.EndsWith (".bytes")) {
			thisSavedLevelPath = Path.GetDirectoryName (savedLevelPath) + "/";
			var thisFileName = currentFileName;
		}
		var thisSavedGroupPath = savedGroupPath;
		if (saveGroups && savedGroupPath.EndsWith (".bytes")) {
			thisSavedGroupPath = Path.GetDirectoryName (savedGroupPath) + "/";
			thisFileName = Path.GetFileNameWithoutExtension (savedGroupPath);
		}
		var savePath = EditorUtility.SaveFilePanel ("Save SpriteTile " + (saveGroups? "groups" : "level"), saveGroups? thisSavedGroupPath : thisSavedLevelPath, thisFileName, "bytes");
		if (savePath == "") {
			return false;
		}
	}
	else {
		savePath = saveGroups? savedGroupPath : savedLevelPath;
	}
	
	var saveBytes = new List.<byte>();
	saveBytes.AddRange (Encoding.UTF8.GetBytes ("SpriteTileLevel"));
	SpriteFile.AddToData (saveBytes, System.BitConverter.IsLittleEndian);
	SpriteFile.AddToData (saveBytes, SpriteFile.LEVELVERSION);
	
	if (!saveGroups) {
		thisLayer.scrollPos = mapScrollPos;
		thisLayer.previewSize = pSizeX;
		thisLayer.tileSize.x = tileSizeX;
		thisLayer.tileSize.y = tileSizeY;
		var tags = ["prvsize", "tintclr", "lvlayrs", "divposn", "stlsize", "curlayr", "bkmarks", "panelwd", "numsets", "srtlyrs", "litinfo"];
	}
	else {
		tags = ["grpsets", "rndsets", "tersets"];
	}
	var tagIndices = new int[tags.Length];
	SpriteFile.AddToData (saveBytes, tags.Length);
	for (var i = 0; i < tags.Length; i++) {
		SpriteFile.AddToData (saveBytes, tags[i]);
		tagIndices[i] = saveBytes.Count;
		SpriteFile.AddToData (saveBytes, 0); // Placeholder
	}
	
	if (!saveGroups) {
		// Preview size
		CopyIntToList (saveBytes, tagIndices[0]);	// Write current list index to placeholder
		SpriteFile.AddToData (saveBytes, textureDisplaySize);
		
		// Collider overlay color
		CopyIntToList (saveBytes, tagIndices[1]);
		var colTint32 : Color32 = colliderTint;
		SpriteFile.AddToData (saveBytes, colTint32);
		
		// Layers
		CopyIntToList (saveBytes, tagIndices[2]);
		SpriteFile.AddToData (saveBytes, mapLayers.Count);
		for (i = 0; i < mapLayers.Count; i++) {
			SpriteFile.AddToData (saveBytes, "lyrdata");
			SpriteFile.AddToData (saveBytes, mapLayers[i]);
		}
		
		// Horizontal divider position
		CopyIntToList (saveBytes, tagIndices[3]);
		SpriteFile.AddToData (saveBytes, divPercent);
		
		// Single/dual tile size
		CopyIntToList (saveBytes, tagIndices[4]);
		SpriteFile.AddToData (saveBytes, useSingleTileSize);
		
		// Current selected layer
		CopyIntToList (saveBytes, tagIndices[5]);
		SpriteFile.AddToData (saveBytes, currentLayer);
		
		// Bookmarks
		CopyIntToList (saveBytes, tagIndices[6]);
		SpriteFile.AddToData (saveBytes, bookmarks);
		
		// Panel width
		CopyIntToList (saveBytes, tagIndices[7]);
		SpriteFile.AddToData (saveBytes, panelWidth);
		
		// Number of tile sets
		CopyIntToList (saveBytes, tagIndices[8]);
		SpriteFile.AddToData (saveBytes, MapData.setCount);
		
		// Sorting layer indices
		CopyIntToList (saveBytes, tagIndices[9]);
		SpriteFile.AddToData (saveBytes, sortingLayerIndices);
		
		// Light info
		CopyIntToList (saveBytes, tagIndices[10]);
		SpriteFile.AddToData (saveBytes, lightList);
	}
	else {
		// Group sets
		CopyIntToList (saveBytes, tagIndices[0]);
		SpriteFile.AddToData (saveBytes, groupSets.Count);
		for (i = 0; i < groupSets.Count; i++) {
			SpriteFile.AddToData (saveBytes, "grpdata");
			SpriteFile.AddToData (saveBytes, groupSets[i]);
		}
		
		// Random group sets
		CopyIntToList (saveBytes, tagIndices[1]);
		SpriteFile.AddToData (saveBytes, randomGroupSets.Count);
		for (i = 0; i < randomGroupSets.Count; i++) {
			SpriteFile.AddToData (saveBytes, "rnddata");
			SpriteFile.AddToData (saveBytes, randomGroupSets[i]);
		}
		
		// Terrain group sets
		CopyIntToList (saveBytes, tagIndices[2]);
		SpriteFile.AddToData (saveBytes, terrainGroupSets.Count);
		for (i = 0; i < terrainGroupSets.Count; i++) {
			SpriteFile.AddToData (saveBytes, "terdata");
			SpriteFile.AddToData (saveBytes, terrainGroupSets[i]);
		}
	}
	
	File.WriteAllBytes (savePath, CLZF2.Compress (saveBytes.ToArray()));
	AssetDatabase.Refresh();
	if (saveGroups) {
		groupsChanged = false;
		savedGroupPath = savePath;
		EditorPrefs.SetString (basePathName + "GroupPath", savedGroupPath);
	}
	else {
		levelChanged = false;
		savedLevelPath = savePath;
		EditorPrefs.SetString (basePathName + "LevelPath", savedLevelPath);
		currentFileName = Path.GetFileNameWithoutExtension (savePath);
	}
	return true;
}

static function CopyIntToList (bytes : List.<byte>, index : int) {
	var intBytes = System.BitConverter.GetBytes (bytes.Count);
	for (var i = 0; i < 4; i++) {
		bytes[index++] = intBytes[i];
	}
}

static function ConfirmLoadWithSave (title : String, message : String) : boolean {
	var dialogChoice = EditorUtility.DisplayDialogComplex (title, message, "Save level", "Cancel", "Open without saving");
	if (dialogChoice == 1) {	// Cancel
		return false;
	}
	if (dialogChoice == 0) {
		if (!Save (false, false)) {
			return false;
		}
	}
	return true;
}


static function DrawTilesetSelection () {
	mapScrollPos = BeginScrollView (mapScrollPos, "box");
	
	Label ("Select matching SpriteTile set for each tileset in the TMX file:");
	Space (15);
	
	for (var i = 0; i < tmxTilesets.Count; i++) {
		BeginHorizontal();
		Label (tmxTilesets[i].name, Width(240));
	    
		tmxTilesets[i].tileset = EditorGUILayout.Popup (tmxTilesets[i].tileset, setList, Width(30));
		EndHorizontal();
	}
	
	Space (5);
	setCollidersForImport = Toggle (setCollidersForImport, "Make tiles with the Collider option checked have colliders in the map after import");
	
	Space (15);
	BeginHorizontal();
	if (Button ("Cancel")) {
		selectingTilesets = false;
	}
	Space (174);
	if (Button ("Import")) {
		BuildTMXMap();
	}

    if (Button("Set AutoLayer")) {
        for (i = 0; i < tmxTilesets.Count; i++) {
            tmxTilesets[i].tileset = i;	
        }
    }
	FlexibleSpace();
	EndHorizontal();
	
	EndScrollView();
}

static function Import () {
	if (levelChanged) {
		if (!ConfirmLoadWithSave ("Tiled TMX Import", "The level has unsaved changes; do you want to save it before importing a Tiled file?") ) return;
	}
	
	var path = EditorUtility.OpenFilePanel ("Import Tiled TMX file", "", "tmx");
	if (path == "") {
		return;
	}
		
	var foundMap = false;	
	var matchString = "";
	tmxTilesets = new List.<TMXTilesetData>();
	tmxLines = File.ReadAllLines (path);
	
	for (var i = 0; i < tmxLines.Length; i++) {
		var line = tmxLines[i];
		if (!foundMap && line.IndexOf ("<map ") != -1) {
			foundMap = true;
			// Only import orthogonal maps
			var foundOrthogonal = false;
			if (SpriteFile.MatchTag (line, "orientation", matchString) && matchString == "orthogonal") {
				foundOrthogonal = true;
			}
			// Default render order is right-down; anything else not supported
			var foundRenderOrder = true;
			if (SpriteFile.MatchTag (line, "renderorder", matchString) && matchString != "right-down") {
				foundRenderOrder = false;
			}
			if (!foundOrthogonal) {
				EditorUtility.DisplayDialog ("Error importing file", "The file does not use orthogonal orientation", "", "");
				return;
			}
			if (!foundRenderOrder) {
				EditorUtility.DisplayDialog ("Error importing file", "The file does not use renderorder = right-down", "", "");
				return;
			}
		}
		
		if (line.IndexOf ("<tileset ") != -1) {
			var firstgid : int;			
			if (!SpriteFile.MatchTag (line, "firstgid", matchString) || !int.TryParse (matchString, firstgid)) {
				EditorUtility.DisplayDialog ("Error importing file", "Error reading firstgid property in tileset tag", "", "");
				return;
			}
			if (!SpriteFile.MatchTag (line, "tilewidth", matchString)) {
				EditorUtility.DisplayDialog ("Error importing file", "No tilewidth property found in tileset tag", "", "");
				return;
			}
			var tilewidth = matchString;
			if (!SpriteFile.MatchTag (line, "tileheight", matchString)) {
				EditorUtility.DisplayDialog ("Error importing file", "No tileheight property found in tileset tag", "", "");
				return;
			}
			var tileheight = matchString;
			if (!SpriteFile.MatchTag (line, "name", matchString)) {
				EditorUtility.DisplayDialog ("Error importing file", "No name property found in tileset tag", "", "");
				return;
			}
			tmxTilesets.Add (new TMXTilesetData (firstgid, matchString + " (" + tilewidth + " x " + tileheight + ")"));
		}
		
		if (line.IndexOf ("<layer ") != -1) {
			break;
		}
	}
	
	if (!foundMap) {
		EditorUtility.DisplayDialog ("Error importing file", "No map tag found", "", "");
		return;
	}
	if (tmxTilesets.Count == 0) {
		EditorUtility.DisplayDialog ("Error importing file", "No tileset tags found", "", "");
		return;
	}
	
	selectingTilesets = true;
	mapScrollPos = Vector2.zero;
	selectionActive = false;
}

static function BuildTMXMap () {
	selectingTilesets = false;
	var thisMapLayers = new List.<LayerData>();
	var tiles : int[,];
	var sets : int[,];
	var flips : Flip[,];
	var rotations : int[,];
	var layerCount = 0;
	var foundLayer = false;
	var count : int;
	var layerWidth : int;
	var layerHeight : int;
	var x : int;
	var y : int;
	var matchString = "";
	var FLIPPED_HORIZONTALLY = 0x80000000;
	var FLIPPED_VERTICALLY =   0x40000000;
	var FLIPPED_DIAGONALLY =   0x20000000;
	
	for (var i = 0; i < tmxLines.Length; i++) {
		var line = tmxLines[i];
		if (line.IndexOf ("<layer ") != -1) {
			foundLayer = true;
			// Add previous layer
			if (layerCount++ > 0) {
				AddLayer (layerWidth, layerHeight, tiles, sets, flips, rotations, thisMapLayers);
			}
			// Get layer width & height
			if (!SpriteFile.MatchTag (line, "width", matchString) || !int.TryParse (matchString, layerWidth)) {
				layerWidth = 0;
			}
			if (!SpriteFile.MatchTag (line, "height", matchString) || !int.TryParse (matchString, layerHeight)) {
				layerHeight = 0;
			}
			if (layerWidth == 0 || layerHeight == 0) {
				EditorUtility.DisplayDialog ("Error importing file", "Layer width or height not found", "", "");
				return;
			}
			
			tiles = new int[layerWidth, layerHeight];
			sets = new int[layerWidth, layerHeight];
			flips = new Flip[layerWidth, layerHeight];
			rotations = new int[layerWidth, layerHeight];
			x = 0;
			y = layerHeight-1;
			count = 0;
		}
		
		if (line.IndexOf ("<tile ") != -1) {
			if (line.IndexOf ("gid") == -1) {
				continue;
			}
			if (!foundLayer) {
				EditorUtility.DisplayDialog ("Error importing file", "No layer tag found", "", "");
				return;
			}
			if (count++ == layerWidth * layerHeight) {
				EditorUtility.DisplayDialog ("Error importing file", "Number of tiles in layer exceeds layer width*height", "", "");
				return;
			}
			SpriteFile.MatchTag (line, "gid", matchString);
			var tileID : long;
			if (!long.TryParse (matchString, tileID)) {
				EditorUtility.DisplayDialog ("Error importing file", "Couldn't parse number in tile tag", "", "");
				return;
			}
			
			var xFlip = (tileID & FLIPPED_HORIZONTALLY);
			var yFlip = (tileID & FLIPPED_VERTICALLY);
			var diagonal = (tileID & FLIPPED_DIAGONALLY);
			tileID &= ~(FLIPPED_HORIZONTALLY | FLIPPED_VERTICALLY | FLIPPED_DIAGONALLY);
			
			if (tileID == 0) {	// Empty
				var tileset = 0;
				tileID = -1;
			}
			else {
				tileset = -1;
				for (var j = 0; j < tmxTilesets.Count - 1; j++) {
					if (tileID >= tmxTilesets[j].firstgid && tileID < tmxTilesets[j+1].firstgid) {
						tileset = tmxTilesets[j].tileset;
						break;
					}
				}
				if (tileset == -1) {
					tileset = tmxTilesets[j].tileset;
				}
				tileID -= tmxTilesets[j].firstgid;
				if (tileID >= tileSets[tileset].tileData.Count) {
					EditorUtility.DisplayDialog ("Not enough tiles", "Tileset " + tileset + " does not contain enough tiles. Make sure the tileset configuration is correct for the map you're importing.", "", "");
					return;
				}
			}
			tiles[x, y] = tileID;
			sets[x, y] = tileset;
			if (xFlip && !yFlip && !diagonal) {
				flips[x, y] = Flip.X;
			}
			else if (!xFlip && yFlip && !diagonal) {
				flips[x, y] = Flip.Y;
			}
			else if (xFlip && yFlip && !diagonal) {
				flips[x, y] = Flip.XY;
			}
			else if (!xFlip && !yFlip && diagonal) {
				rotations[x, y] = 90;
				flips[x, y] = Flip.X;
			}
			else if (xFlip && !yFlip && diagonal) {
				rotations[x, y] = 270;
			}
			else if (!xFlip && yFlip && diagonal) {
				rotations[x, y] = 90;
			}
			else if (xFlip && yFlip && diagonal) {
				rotations[x, y] = 90;
				flips[x, y] = Flip.Y;
			}
			
			if (++x == layerWidth) {
				x = 0;
				y--;
			}
		}
	}
	
	AddLayer (layerWidth, layerHeight, tiles, sets, flips, rotations, thisMapLayers);
	mapLayers = thisMapLayers;
	currentLayer = oldCurrentLayer = 0;
	SetDefaultSortingLayerIndices();
	sortingLayerIndex = oldSortingLayerIndex = sortingLayerIndices[currentLayer];
	SetLayerInfo (Int2.zero, false);
	SetLayerNames();
	levelChanged = true;
	lightMode = false;
	editTile = false;
	CalcMinMaxOrder (currentLayer);
}

static function AddLayer (layerWidth : int, layerHeight : int, tiles : int[,], sets : int[,], flips : Flip[,], rotations : int[,], thisMapLayers : List.<LayerData>) {
	var thisMapData = new MapData(Int2(layerWidth, layerHeight), true);
	for (var y = 0; y < layerHeight; y++) {
		for (var x = 0; x < layerWidth; x++) {
			var tInfo = TileInfo(sets[x, y], tiles[x, y]);
			thisMapData.SetTile (Int2(x, y), tInfo.set, tInfo.tile);
			thisMapData.SetFlip (Int2(x, y), flips[x, y]);
			thisMapData.SetRotation (Int2(x, y), rotations[x, y]);
			
			if (setCollidersForImport && tInfo.tile >= 0 && tileSets[tInfo.set].tileData[GetTileNumber (tInfo)].useCollider) {
				thisMapData.SetCollider (Int2(x, y), true);
			}
		}
	}
	thisMapLayers.Add (new LayerData(thisMapData, 0, Vector2(tileSizeX, tileSizeY), zPos, lock));
}

static function Open (openGroups : boolean, usePathDialog : boolean, path : String) {
	if (levelChanged && !openGroups && usePathDialog) {
		if (!ConfirmLoadWithSave ("Unsaved level", "The level has unsaved changes; do you want to save it before opening a new level?") ) return;
	}
	
	if (usePathDialog) {
		path = EditorUtility.OpenFilePanel ("Load SpriteTile " + (openGroups? "groups" : "level"), openGroups? savedGroupPath : savedLevelPath, "bytes");
		if (path == "") {
			return;
		}
	}
	else {
		if (!File.Exists (path)) {
			return;
		}
	}
		
	try {
		var fIdx = 0;
		var loadBytes = CLZF2.Decompress (File.ReadAllBytes (path));
		var idString = "SpriteTileLevel";
		if (SpriteFile.GetUTF8String (loadBytes, fIdx, idString.Length) != idString) {
			throw "This doesn't seem to be a SpriteTile level file";
		}
		if (loadBytes[fIdx++] != 1) {
			throw "File isn't little endian";
		}
		var thisVersion = SpriteFile.GetInt (loadBytes, fIdx);
		if (thisVersion < 1 || thisVersion > SpriteFile.LEVELVERSION) {
			throw "Unsupported level version: " + thisVersion + ". This version of SpriteTile can load version " + SpriteFile.LEVELVERSION + " or lower.";
		}
		
		// Check if any groups are in the file
		var numberOfTags = SpriteFile.GetInt (loadBytes, fIdx);
		var tIdx = fIdx;
		var storedIdx = fIdx;
		var foundGroupTag = false;
		var tagCount = 0;
		while (tagCount < numberOfTags) {
			var tag = SpriteFile.GetTag (loadBytes, tIdx, fIdx);
			if (tag == "grpsets" || tag == "rndsets" || tag == "tersets") {
				foundGroupTag = true;
				break;
			}
			tagCount++;
		}
		if (!openGroups && foundGroupTag) {
			var isSure = EditorUtility.DisplayDialog ("Not a level file", "This file contains only sprite groups; do you want to load them?", "Load groups", "Cancel");
			if (isSure) {
				Open (true, false, path);
			}
			return;
		}
		if (openGroups && !foundGroupTag) {
			EditorUtility.DisplayDialog ("Can't load groups", "This file doesn't seem to contain any groups", "", "");
			return;
		}
			
		fIdx = storedIdx;
		tIdx = fIdx;
		
		tagCount = 0;
		var doSetupGroupTextures = false;
		var foundLayers = false;
		var foundTint = false;
		var foundDivPosition = false;
		var foundPreviewSize = false;
		var foundSingletile = false;
		var foundStandardGroups = false;
		var foundRandomGroups = false;
		var foundTerrainGroups = false;
		var foundCurrentLayer = false;
		var foundBookmarks = false;
		var foundPanelWidth = false;
		var foundSortingLayerIndices = false;
		var foundLightList = false;
		while (tagCount < numberOfTags) {
			tag = SpriteFile.GetTag (loadBytes, tIdx, fIdx);
			switch (tag) {
				case "lvlayrs":
					var numberOfLayers = SpriteFile.GetInt (loadBytes, fIdx);
					var thisMapLayers = new List.<LayerData>(numberOfLayers);
					for (var i = 0; i < numberOfLayers; i++) {
						if (SpriteFile.GetUTF8String (loadBytes, fIdx, 7) != "lyrdata") {
							throw "Can't find lyrdata tag";
						}
						var thisTSizeX = SpriteFile.GetFloat (loadBytes, fIdx);
						if (thisVersion == 1) {
							var thisTSizeY = thisTSizeX;
						}
						else {
							thisTSizeY = SpriteFile.GetFloat (loadBytes, fIdx);
						}
						var thisLayerScrollPos = Vector2(SpriteFile.GetInt (loadBytes, fIdx), SpriteFile.GetInt (loadBytes, fIdx));
						var thisPSize = SpriteFile.GetInt (loadBytes, fIdx);
						var thisZPos = SpriteFile.GetFloat (loadBytes, fIdx);
						var thisLock = SpriteFile.GetInt (loadBytes, fIdx);
						var thisAddBorder = SpriteFile.GetInt (loadBytes, fIdx);
						var thisMapData = SpriteFile.GetMapData (loadBytes, fIdx, thisVersion, tileManager);
						thisMapLayers.Add (new LayerData(thisMapData, thisAddBorder, Vector2(thisTSizeX, thisTSizeY), thisLayerScrollPos, thisPSize, thisZPos, thisLock));
					}
					foundLayers = true;
					break;
				case "grpsets":
					var numberOfSets = SpriteFile.GetInt (loadBytes, fIdx);
					var thisGSets = new List.<SpriteGroupData>(numberOfSets);
					for (i = 0; i < numberOfSets; i++) {
						if (SpriteFile.GetUTF8String (loadBytes, fIdx, 7) != "grpdata") {
							throw "Can't find grpdata tag";
						}
						var gDataCount = SpriteFile.GetInt (loadBytes, fIdx);
						var thisGDatas = new List.<SpriteGroup>(gDataCount);
						for (var j = 0; j < gDataCount; j++) {
							// tileNumbers
							var tNumberCount = SpriteFile.GetInt (loadBytes, fIdx);
							var tNumbers = new List.<TileInfo>(tNumberCount);
							for (var k = 0; k < tNumberCount; k++) {
								tNumbers.Add (TileInfo(SpriteFile.GetShort (loadBytes, fIdx), SpriteFile.GetShort (loadBytes, fIdx)) );
							}
							// mapData
							thisMapData = SpriteFile.GetMapData (loadBytes, fIdx, thisVersion, tileManager);
							if (thisVersion < 3) {
								// texture
								var thisTexSize = Int2(SpriteFile.GetInt (loadBytes, fIdx), SpriteFile.GetInt (loadBytes, fIdx));
								var thisTex = new Texture2D(thisTexSize.x, thisTexSize.y, TextureFormat.RGB24, false);
								var thisCols32 = new Color32[thisTexSize.x * thisTexSize.y];
								for (k = 0; k < thisCols32.Length; k++) {
									thisCols32[k] = SpriteFile.GetColor32 (loadBytes, fIdx);
								}
								thisTex.SetPixels32 (thisCols32);
								thisTex.Apply();
								var thisGroupTileSize = Vector2(tileSizeX, tileSizeY);
							}
							else {
								doSetupGroupTextures = true;
								thisGroupTileSize = Vector2(SpriteFile.GetFloat (loadBytes, fIdx), SpriteFile.GetFloat (loadBytes, fIdx));
							}
							thisGDatas.Add (new SpriteGroup (tNumbers, thisMapData, GUIContent((thisGDatas.Count).ToString(), thisTex), thisGroupTileSize));
						}
						thisGSets.Add (new SpriteGroupData(thisGDatas));
					}
					foundStandardGroups = true;
					break;
				case "rndsets":
					var thisRGSets = SpriteFile.LoadRandomGroups (loadBytes, fIdx);
					if (thisRGSets == null) {
						throw "Can't find rnddata tag";
					}
					foundRandomGroups = true;
					break;
				case "tersets":
					var thisTGSets = SpriteFile.LoadTerrainGroups (loadBytes, fIdx);
					if (thisTGSets == null) {
						throw "Can't find terdata tag";
					}
					foundTerrainGroups = true;
					break;
				case "prvsize":
					var thisDisplaySize = SpriteFile.GetInt (loadBytes, fIdx);
					foundPreviewSize = true;
					break;
				case "tintclr":
					var thisTintColor = SpriteFile.GetColor32 (loadBytes, fIdx);
					foundTint = true;
					break;
				case "divposn":
					var thisDivPercent = SpriteFile.GetFloat (loadBytes, fIdx);
					foundDivPosition = true;
					break;
				case "stlsize":
					var thisUseSingleTile = SpriteFile.GetBool (loadBytes, fIdx);
					foundSingletile = true;
					break;
				case "curlayr":
					var thisCurrentLayer = SpriteFile.GetInt (loadBytes, fIdx);
					foundCurrentLayer = true;
					break;
				case "bkmarks":
					var numberOfBookmarks = SpriteFile.GetInt (loadBytes, fIdx);
					var thisBookmarks = new Bookmark[numberOfBookmarks];
					for (i = 0; i < numberOfBookmarks; i++) {
						var bookmarkScrollPos = Vector2(SpriteFile.GetFloat (loadBytes, fIdx), SpriteFile.GetFloat (loadBytes, fIdx));
						thisBookmarks[i] = Bookmark(bookmarkScrollPos, SpriteFile.GetInt (loadBytes, fIdx), SpriteFile.GetBool (loadBytes, fIdx));
					}
					foundBookmarks = true;
					break;
				case "panelwd":
					var thisPanelWidth = SpriteFile.GetInt (loadBytes, fIdx);
					foundPanelWidth = true;
					break;
				case "numsets":
					var setsInFile = SpriteFile.GetInt (loadBytes, fIdx);
					if (MapData.setCount != setsInFile) {
						Debug.LogWarning ("The max number of tile sets referenced in the file (" + setsInFile + ") does not match the max number of tile sets in this project (" + MapData.setCount + "). Tiles may not be correct.");
					}
					break;
				case "srtlyrs":
					var sortLayerCount = SpriteFile.GetInt (loadBytes, fIdx);
					var thisSortingLayerIndices = new List.<int>(sortLayerCount);
					for (i = 0; i < sortLayerCount; i++) {
						thisSortingLayerIndices.Add (SpriteFile.GetInt (loadBytes, fIdx));
					}
					foundSortingLayerIndices = true;
					break;
				case "litinfo":
					var thisLightList = SpriteFile.GetLightInfoList (loadBytes, fIdx);
					foundLightList = true;
					break;
			}
			tagCount++;
		}
	}
	catch (err) {
		EditorUtility.DisplayDialog ("Can't load file", "Error: " + err.Message, "", "");
		return;
	}
	
	if (!openGroups) {
		// Check to see if sprites exist for all tiles
		var missingList = new List.<TileInfo>();
		for (j = 0; j < numberOfLayers; j++) {
			var thisMap = thisMapLayers[j].mapData.map;
			for (i = 0; i < thisMap.Length; i++) {
				var t = thisMap[i];
				if (t < 0) {
					continue;
				}
				if (!TileExists (t)) {
					var tInfo = TileInfo(t >> MapData.setShift, t & MapData.tileShiftI);
					if (!missingList.Contains (tInfo)) {
						missingList.Add (tInfo);
					}
					thisMap[i] = MapData.missingTile;
				}
			}
		}
		if (missingList.Count > 0) {
			var listString = "";
			var end = Mathf.Min (missingList.Count, 50);
			for (i = 0; i < end; i++) {
				listString += missingList[i];
				if (i < end-1) {
					listString += ", ";
				}
			}
			if (end < missingList.Count) {
				listString += ", and " + (missingList.Count - end) + " others";
			}
			EditorUtility.DisplayDialog ("Missing tiles", "The level references tiles that don't exist: " + listString, "", "");
		}
		
		if (foundPreviewSize) {
			oldTextureDisplaySize = textureDisplaySize = thisDisplaySize;
		}
		if (foundDivPosition) {
			divPercent = thisDivPercent;
		}
		if (foundTint) {
			colliderTint = thisTintColor;
		}
		if (foundSingletile) {
			useSingleTileSize = thisUseSingleTile;
		}
		else {
			useSingleTileSize = true;
		}
		if (foundLayers) {
			mapLayers = thisMapLayers;
			if (foundCurrentLayer) {
				currentLayer = oldCurrentLayer = thisCurrentLayer;
			}
			else {
				currentLayer = oldCurrentLayer = 0;
			}
			SetLayerInfo (Int2.zero, false);
			
			SetLayerNames();
			if (!CheckGroups (groupSets)) {
				InitializeGroups();
			}
			levelChanged = false;
			CalcMinMaxOrder (currentLayer);
			currentFileName = Path.GetFileNameWithoutExtension (path);
			savedLevelPath = path;
			EditorPrefs.SetString (basePathName + "LevelPath", savedLevelPath);
		}
		if (foundBookmarks) {
			bookmarks = thisBookmarks;
		}
		if (foundPanelWidth) {
			panelWidth = thisPanelWidth;
		}
		if (foundLightList) {
			lightList = thisLightList;
			if (lightList.Count == 0) {
				SetDefaultLight();
			}
			SetLightStringArray();
			selectedLight = Mathf.Min (selectedLight, lightList.Count-1);
			SetLightInfo (selectedLight);
			for (i = 0; i < mapLayers.Count; i++) {
				mapLayers[i].mapData.ResetLighting (lightList);
			}
		}
		else {
			SetDefaultLight();
		}
		if (foundSortingLayerIndices) {
			if (thisSortingLayerIndices.Count != mapLayers.Count) {
				Debug.LogWarning ("Sorting layer index count does not match layer count");
				SetDefaultSortingLayerIndices();
			}
			else {
				sortingLayerIndices = thisSortingLayerIndices;
			}
			var outOfRangeError = false;
			for (i = 0; i < mapLayers.Count; i++) {
				if (sortingLayerIndices[i] >= sortingLayerNames.Length) {
					outOfRangeError = true;
					sortingLayerIndices[i] = sortingLayerNames.Length-1;
				}
			}
			if (outOfRangeError) {
				Debug.LogWarning ("Sorting layer index exceeded sorting layer names array length");
			}
		}
		else {
			SetDefaultSortingLayerIndices();
		}
		sortingLayerIndex = oldSortingLayerIndex = sortingLayerIndices[currentLayer];
		pSizeX = oldPSizeX = mapLayers[currentLayer].previewSize;
		ComputePSizeY();
		UpdateShowLight();
		selectedLightPos = Int2(-1, -1);
		lightMode = false;
		editTile = false;
	}
	// Open groups
	else {
		var errorString = "The group file references tiles that don't exist";
		if (foundStandardGroups) {
			if (CheckGroups (thisGSets)) {
				groupSets = thisGSets;
				thisGroupSet = groupSets[0].spriteGroups;
				InitializeGroupDisplayData (path);
			}
			else {
				EditorUtility.DisplayDialog ("Can't load standard groups", errorString, "", "");
			}
		}
		if (foundRandomGroups) {
			if (CheckRandomGroups (thisRGSets)) {
				randomGroupSets = thisRGSets;
				thisRandomGroupSet = randomGroupSets[0].randomGroups;
				InitializeGroupDisplayData (path);
			}
			else {
				EditorUtility.DisplayDialog ("Can't load random groups", errorString, "", "");
			}
		}
		if (foundTerrainGroups) {
			if (CheckTerrainGroups (thisTGSets)) {
				terrainGroupSets = thisTGSets;
				thisTerrainGroupSet = terrainGroupSets[0].terrainGroups;
				SetTerrainGroupActiveList();
				InitializeGroupDisplayData (path);
			}
			else {
				EditorUtility.DisplayDialog ("Can't load terrain groups", errorString, "", "");
			}
		}
		if (doSetupGroupTextures) {
			CreateGroupTexture (false, 0, true, groupSets);
		}
	}
}

static function SetDefaultSortingLayerIndices () {
	sortingLayerIndices = new List.<int>(mapLayers.Count);
	for (var i = 0; i < mapLayers.Count; i++) {
		sortingLayerIndices.Add (i);
		if (sortingLayerIndices[i] >= sortingLayerNames.Length) {
			sortingLayerIndices[i] = sortingLayerNames.Length-1;
		}
	}
}

static function SetTerrainGroupActiveList () {
	terrainGroupActiveList = new List.<Int2>();
	for (var i = 0; i < terrainGroupSets.Count; i++) {
		for (var j = 0; j < terrainGroupSets[i].terrainGroups.Count; j++) {
			CheckTerrainGroupList (i, j);
		}
	}
}

static function InitializeGroupDisplayData (path : String) {
	currentGroupSet = selectedGroup = 0;
	groupScrollPos = Vector2.zero;
	SetGroupDisplayCounts();
	groupsChanged = false;
	savedGroupPath = path;
	EditorPrefs.SetString (basePathName + "GroupPath", savedGroupPath);
}

static function CheckGroups (gSets : List.<SpriteGroupData>) : boolean {
	for (var i = 0; i < gSets.Count; i++) {
		var thisGSet = gSets[i].spriteGroups;
		for (var j = 0; j < thisGSet.Count; j++) {
			for (var k = 0; k < thisGSet[j].tileNumbers.Count; k++) {
				var tNumber = thisGSet[j].tileNumbers[k];
				if (tNumber.set > tileSets.Count || tNumber.tile >= tileSets[tNumber.set].tileData.Count) {
					return false;
				}
			}
		}
	}
	return true;
}

static function CheckRandomGroups (gSets : List.<RandomGroupData>) : boolean {
	for (var i = 0; i < gSets.Count; i++) {
		var thisGSet = gSets[i].randomGroups;
		for (var j = 0; j < thisGSet.Count; j++) {
			for (var k = 0; k < thisGSet[j].Count; k++) {
				var tNumber = thisGSet[j][k];
				if (tNumber.set > tileSets.Count || tNumber.tile >= tileSets[tNumber.set].tileData.Count) {
					return false;
				}
			}
		}
	}
	return true;
}

static function CheckTerrainGroups (gSets : List.<TerrainGroupData>) : boolean {
	for (var i = 0; i < gSets.Count; i++) {
		var thisGSet = gSets[i].terrainGroups;
		for (var j = 0; j < thisGSet.Count; j++) {
			for (var k = 0; k < thisGSet[j].tileNumbers.Count; k++) {
				for (var ii = 0; ii < thisGSet[j].tileNumbers[k].Length; ii++) {
					var tNumber = thisGSet[j].tileNumbers[k][ii];
					if (tNumber.tile < 0) {
						continue;
					}
					if (tNumber.set > tileSets.Count || tNumber.tile >= tileSets[tNumber.set].tileData.Count) {
						return false;
					}
				}
			}
		}
	}
	return true;
}