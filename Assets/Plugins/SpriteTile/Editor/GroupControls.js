// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

#pragma strict
import System.Collections.Generic;
import UnityEngine.GUILayout;
import SpriteTile;
import TileEditor;

static function ChangeGroupSet (idx : int) {
	if (groupType == GroupType.Standard) {
		idx = Mathf.Repeat (currentGroupSet + idx, groupSets.Count);
		ChangeStandardGroupSet (idx);
	}
	else if (groupType == GroupType.Random) {
		idx = Mathf.Repeat (currentGroupSet + idx, randomGroupSets.Count);
		ChangeRandomGroupSet (idx);
	}
	else if (groupType == GroupType.Terrain) {
		idx = Mathf.Repeat (currentGroupSet + idx, terrainGroupSets.Count);
		ChangeTerrainGroupSet (idx);
	}
}

static function SetGroupDisplayCounts () {
	if (groupType == GroupType.Standard) {
		groupColumnCount = (panelWidth - 24) / (groupPixelSize + 5);
		groupRowCount = Mathf.Ceil (thisGroupSet.Count / parseFloat(groupColumnCount));
	}
	else if (groupType == GroupType.Random) {
		groupColumnCount = (panelWidth - 24) / (randomGroupPixelSize + 11);
		groupRowCount = Mathf.Ceil (thisRandomGroupSet.Count / parseFloat(groupColumnCount));
	}
	else if (groupType == GroupType.Terrain) {
		groupColumnCount = (panelWidth - 24) / (terrainGroupPixelSize + 18);
		groupRowCount = Mathf.Ceil (thisTerrainGroupSet.Count / parseFloat(groupColumnCount));
	}
}

static function GroupControls () {
	BeginHorizontal();
	Label ("<b><size=14>Groups</size></b>", richtextLabel);
	Space (20);
	Label (groupsChanged? "•" : "", Width(9));
	if (Button (content[Content.GroupSaveAs])) {
		Save (true, true);
	}
	if (Button (content[Content.GroupOpen])) {
		Open (true, true, savedGroupPath);
	}
	autoGroupLoad = Toggle (autoGroupLoad, content[Content.GroupAuto]);
	Space (10);
	if (Button ("Standard", buttonStyles[(groupType == GroupType.Standard)? 0 : 1])) {
		SwitchGroupType (GroupType.Standard);
	}
	if (Button ("Random", buttonStyles[(groupType == GroupType.Random)? 0 : 1])) {
		SwitchGroupType (GroupType.Random);
	}
	if (Button ("Terrain", buttonStyles[(groupType == GroupType.Terrain)? 0 : 1])) {
		SwitchGroupType (GroupType.Terrain);
	}
	FlexibleSpace();
	EndHorizontal();
	Space (5);
	
	switch (groupType) {
		case GroupType.Standard:
			StandardGroupControls(); break;
		case GroupType.Random:
			RandomGroupControls(); break;
		case GroupType.Terrain:
			TerrainGroupControls(); break;
	}
	if (oldAutoGroupLoad != autoGroupLoad) {
		oldAutoGroupLoad = autoGroupLoad;
		EditorPrefs.SetBool (basePathName + "AutoGroupLoad", autoGroupLoad);
	}
}

static function StandardGroupControls () {
	BeginHorizontal();
	GUI.enabled = (selectionActive || copyActive);
	if (Button (content[Content.GroupFromSelection])) {
		CreateGroupTexture (false, 0, false, null);
	}
	GUI.enabled = true;
	FlexibleSpace();
	Label ("Set:");
	if (Button (content[Content.GroupSetDelete])) {
		DeleteGroupSet (currentGroupSet);
	}
	if (Button (content[Content.GroupSetNew])) {
		AddGroupSet();
	}
	EndHorizontal();
	Space (2);
	
	var buttonNumber = ToolbarButtons (groupSets.Count, currentGroupSet);
	if (buttonNumber != -1) {
		ChangeStandardGroupSet (buttonNumber);
	}
	Space (1);
	
	if (thisGroupSet.Count > 0) {
		BeginHorizontal ("box");
		Label ("Group " + selectedGroup + ": ");
		Space (5);
		if (Button (content[Content.GroupCopyToBuffer])) {
			GroupToBuffer (selectedGroup);
		}
		GUI.enabled = (selectionActive || copyActive);
		if (Button (content[Content.GroupReplace])) {
			CreateGroupTexture (true, selectedGroup, false, null);
		}
		GUI.enabled = true;
		FlexibleSpace();
		if (Button ("Delete")) {
			DeleteGroup (currentGroupSet, selectedGroup);
		}
		EndHorizontal();
	}
	else {
		BeginHorizontal ("box", Height(24));
		GUI.color.a = 0.0;
		Button ("");	// Buttons here should match the number of buttons above, to fix scroll wheel not working bug
		Button ("");
		Button ("");
		GUI.color = Color.white;
		EndHorizontal();
	}
}

static function RandomGroupControls () {
	BeginHorizontal();
	if (thisRandomGroupSet.Count > 0 && thisRandomGroupSet[selectedGroup].Count > 1) {
		drawWithRandomGroup = Toggle (drawWithRandomGroup, "Draw with selected group");
	}
	else {
		GUI.enabled = false;
		Toggle (false, "Draw with selected group");
		GUI.enabled = true;
	}
	FlexibleSpace();
	if (Button ("+ Make New Group")) {
		AddRandomGroup();
	}
	FlexibleSpace();
	Label ("Set:");
	if (Button (content[Content.GroupSetDelete])) {
		DeleteRandomGroupSet (currentGroupSet);
	}
	if (Button (content[Content.GroupSetNew])) {
		AddRandomGroupSet();
	}
	EndHorizontal();
	Space (2);
	
	var buttonNumber = ToolbarButtons (randomGroupSets.Count, currentGroupSet);
	if (buttonNumber != -1) {
		ChangeRandomGroupSet (buttonNumber);
	}
	Space (1);
	
	if (thisRandomGroupSet.Count > 0) {
		BeginHorizontal ("box");
		Label ("Group " + selectedGroup + ": ");
		Space (5);
		GUI.enabled = (thisTileSet.Count > 0 && thisRandomGroupSet.Count > 0 && thisRandomGroupSet[selectedGroup].Count < 16);
		if (Button ("+ Add Selected Tile" + (doMultiSelect? "s" : ""))) {
			AddTileToRandomGroup();
		}
		GUI.enabled = (thisTileSet.Count > 0 && thisRandomGroupSet.Count > 0 && thisRandomGroupSet[selectedGroup].Count > 0);
		if (Button ("- Remove Selected Tile" + (doMultiSelect? "s" : ""))) {
			RemoveTileFromRandomGroup();
		}
		GUI.enabled = true;
		FlexibleSpace();
		if (Button ("Delete")) {
			DeleteRandomGroup (currentGroupSet, selectedGroup, true);
		}
		EndHorizontal();
	}
	else {
		BeginHorizontal ("box", Height(24));
		GUI.color.a = 0.0;
		Button ("");	// Buttons here should match the number of buttons above, to fix scroll wheel not working bug
		Button ("");
		GUI.color = Color.white;
		EndHorizontal();
	}
}

static function TerrainGroupControls () {
	BeginHorizontal();
	Space (50);
	FlexibleSpace();
	if (Button ("+ Make New Group")) {
		AddTerrainGroup();
	}
	FlexibleSpace();
	Label ("Set:");
	if (Button (content[Content.GroupSetDelete])) {
		DeleteTerrainGroupSet (currentGroupSet);
	}
	if (Button (content[Content.GroupSetNew])) {
		AddTerrainGroupSet();
	}
	EndHorizontal();
	Space (2);
	
	var buttonNumber = ToolbarButtons (terrainGroupSets.Count, currentGroupSet);
	if (buttonNumber != -1) {
		ChangeTerrainGroupSet (buttonNumber);
	}
	Space (1);
	
	if (thisTerrainGroupSet.Count > 0) {
		BeginHorizontal ("box");
		Label ("Group " + selectedGroup + ":");
		FlexibleSpace();
		GUI.color.a = .5;
		Label ("Drag and drop tiles above into terrain group below");
		GUI.color.a = 1.0;
		FlexibleSpace();
		if (Button ("Clear")) {
			ClearTerrainGroup (currentGroupSet, selectedGroup);
		}
		if (Button ("Delete")) {
			DeleteTerrainGroup (currentGroupSet, selectedGroup, true);
		}
		EndHorizontal();
	}
	else {
		BeginHorizontal ("box", Height(24));
		GUI.color.a = 0.0;
		Button ("");	// Buttons here should match the number of buttons above, to fix scroll wheel not working bug
		Button ("");
		GUI.color = Color.white;
		EndHorizontal();
	}
}

static function SwitchGroupType (newGroupType : GroupType) {
	if (groupType == GroupType.Standard) {
		currentGroupSet = Mathf.Clamp (currentGroupSet, 0, groupSets.Count-1);	// In case TileManager was messed around with
		groupSets[currentGroupSet].selectedGroup = selectedGroup;
	}
	else if (groupType == GroupType.Random) {
		currentGroupSet = Mathf.Clamp (currentGroupSet, 0, randomGroupSets.Count-1);
		randomGroupSets[currentGroupSet].selectedGroup = selectedGroup;
	}
	else if (groupType == GroupType.Terrain) {
		currentGroupSet = Mathf.Clamp (currentGroupSet, 0, terrainGroupSets.Count-1);
		terrainGroupSets[currentGroupSet].selectedGroup = selectedGroup;
	}
	
	storedSetNumbers[groupType] = currentGroupSet;
	storedScrollPositions[groupType] = groupScrollPos;
	groupType = newGroupType;
	currentGroupSet = storedSetNumbers[groupType];
	groupScrollPos = storedScrollPositions[groupType];
	
	if (groupType == GroupType.Standard) {
		selectedGroup = groupSets[currentGroupSet].selectedGroup;
	}
	else if (groupType == GroupType.Random) {
		selectedGroup = randomGroupSets[currentGroupSet].selectedGroup;
	}
	else if (groupType == GroupType.Terrain) {
		selectedGroup = terrainGroupSets[currentGroupSet].selectedGroup;
	}
	
	SetGroupDisplayCounts();
	EditorPrefs.SetInt (basePathName + "GroupType", groupType);
}

static function DrawGroups () {
	switch (groupType) {
		case GroupType.Standard:
			DrawStandardGroups();
			break;
		case GroupType.Random:
			DrawRandomGroups();
			break;
		case GroupType.Terrain:
			DrawTerrainGroups();
			break;
	}
}

static function DrawStandardGroups () {
	groupScrollPos = BeginScrollView (groupScrollPos, "box");
	var i = 0;
	for (var y = 0; y < groupRowCount; y++) {
		BeginHorizontal();
		for (var x = 0; x < groupColumnCount; x++) {
			if (i < thisGroupSet.Count) {
				if (Button (thisGroupSet[i].content, groupDisplayTypes[(i == selectedGroup)? 0 : 1],
							Width(groupPixelSize), Height(groupPixelSize + 14)) ) {
					selectedGroup = i;
					if (doubleClickedCount > 0) {
						doubleClickedCount = 0;
						GroupToBuffer (i);
					}
				}
				i++;
			}
		}
		EndHorizontal();
	}
	EndScrollView();
}

static function DrawRandomGroups () {
	groupScrollPos = BeginScrollView (groupScrollPos, "box");
	var i = 0;
	for (var y = 0; y < groupRowCount; y++) {
		BeginHorizontal();
		for (var x = 0; x < groupColumnCount; x++) {
			if (i < thisRandomGroupSet.Count) {
				if (Button (i.ToString(), groupDisplayTypes[(i == selectedGroup)? 0 : 1],
							Width(randomGroupPixelSize + 6), Height(randomGroupPixelSize + 20)) ) {
					selectedGroup = i;
					if (doubleClickedCount > 0) {
						doubleClickedCount = 0;
						drawWithRandomGroup = true;
					}
				}
				// Empty group
				if (thisRandomGroupSet[i].Count == 0) {
					GUI.color.a = .35;
					GUI.Box (Rect(((randomGroupPixelSize + 10) * x) + 7, ((randomGroupPixelSize + 24) * y) + 7,
									randomGroupPixelSize, randomGroupPixelSize), " ");
					GUI.color.a = 1.0;
				}
				// Not empty group
				else {
					if (thisRandomGroupSet[i].Count <= 4) {
						var tCount = 2;
					}
					else if (thisRandomGroupSet[i].Count <= 9) {
						tCount = 3;
					}
					else {
						tCount = 4;
					}
					var pixelSize = randomGroupPixelSize / tCount;
					var idx = 0;
					for (var yy = 0; yy < Mathf.Ceil (thisRandomGroupSet[i].Count / parseFloat(tCount)); yy++) {
						for (var xx = 0; xx < tCount; xx++) {
							var tInfo = thisRandomGroupSet[i][idx];
							var tile = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
							var ratio = tile.displayRatio;
							var xAdd = ((1.0 - ratio.x)/2) * pixelSize;
							var yAdd = ((1.0 - ratio.y)/2) * pixelSize;
							GUI.DrawTextureWithTexCoords (Rect(7 + ((randomGroupPixelSize + 10) * x) + xx*pixelSize + xAdd,
																7 + ((randomGroupPixelSize + 24) * y) + yy*pixelSize + yAdd,
																(pixelSize - 1)*ratio.x, (pixelSize - 1)*ratio.y),
															tile.texture, tile.textureRect);
							if (++idx == thisRandomGroupSet[i].Count) {
								break;
							}
						}
					}
				}
				i++;
			}
		}
		EndHorizontal();
	}
	EndScrollView();
}

static var terrainGroupH = 17;
static var terrainGroupV = 52;

static function DrawTerrainGroups () {
	groupScrollPos = BeginScrollView (groupScrollPos, "box");
	
	if (dragComplete && evt.type == EventType.Repaint) {
		TerrainSlotDragComplete();
	}
	
	// Draw groups
	var pixSize = terrainGroupPixelSize / 5;
	var i = 0;
	for (var y = 0; y < groupRowCount; y++) {
		BeginHorizontal();
		for (var x = 0; x < groupColumnCount; x++) {
			if (i < thisTerrainGroupSet.Count) {
				BeginVertical(Width(terrainGroupPixelSize + 9));
				if (Button (i.ToString(), groupDisplayTypes[(i == selectedGroup)? 0 : 1], Width(terrainGroupPixelSize + 9), Height(terrainGroupPixelSize + 24)) ) {
					selectedGroup = i;
					var terrainTile = thisTerrainGroupSet[i].tileNumbers[0][terrainLookups.Length-1];
					if (doubleClickedCount > 0 && terrainTile != TerrainGroup.defaultTile) {
						doubleClickedCount = 0;
						SetPickedTile (terrainTile);
						thisTerrainGroupSet[i].active = true;
					}
				}
				BeginHorizontal();
				FlexibleSpace();
				thisTerrainGroupSet[i].active = Toggle (thisTerrainGroupSet[i].active, "Active in level", Height(18));
				if (thisTerrainGroupSet[i].active != thisTerrainGroupSet[i].oldActive) {
					thisTerrainGroupSet[i].oldActive = thisTerrainGroupSet[i].active;
					CheckTerrainGroupList (currentGroupSet, i);
				}
				FlexibleSpace();
				EndHorizontal();
				EndVertical();
				var slotCount = 0;
				for (var yy = 0; yy < 5; yy++) {
					for (var xx = 0; xx < 5; xx++) {
						if (terrainSlotIndices[slotCount] >= 0) {
							var rect = Rect(11 + x*(terrainGroupPixelSize + terrainGroupH) + pixSize*xx, 7 + y*(terrainGroupPixelSize + terrainGroupV) + pixSize*yy, pixSize+1, pixSize+1);
							var thisTile = thisTerrainGroupSet[i].tileNumbers[0][terrainSlotIndices[slotCount]];
							if (thisTile != TerrainGroup.defaultTile) {
								thisTile.tile = GetTileNumber (thisTile);
								var tex = tileSets[thisTile.set].tileData[thisTile.tile].texture;
								var texRect = tileSets[thisTile.set].tileData[thisTile.tile].textureRect;
								var ratio = tileSets[thisTile.set].tileData[thisTile.tile].displayRatio;
							}
							else {
								tex = terrainBackgrounds[terrainSlotIndices[slotCount]];
								texRect = backgroundRect;
								ratio = Vector2.one;
								GUI.color.a = .4;
							}
							GUITexBox (rect, tex, texRect, ratio);
							GUI.color.a = 1.0;
						}
						slotCount++;
					}
				}
				i++;
			}
		}
		EndHorizontal();
	}
	EndScrollView();
}

static function CheckTerrainGroupList (groupSet : int, i : int) {
	if (terrainGroupSets[groupSet].terrainGroups[i].active) {
		if (!terrainGroupActiveList.Contains (Int2(groupSet, i))) {
			terrainGroupActiveList.Add (Int2(groupSet, i));
		}
	}
	else {
		terrainGroupActiveList.Remove (Int2(groupSet, i));
	}
}

static function TerrainSlotDragComplete () {
	dragComplete = false;
	if (thisTerrainGroupSet.Count > 0) {
		var pixSize = terrainGroupPixelSize / 5;
		var i = 0;
		for (var y = 0; y < groupRowCount; y++) {
			for (var x = 0; x < groupColumnCount; x++) {
				var xPos = 4 + x*(terrainGroupPixelSize + terrainGroupH);
				var yPos = 4 + y*(terrainGroupPixelSize + terrainGroupV);
				if (i < thisTerrainGroupSet.Count && Rect(xPos, yPos, terrainGroupPixelSize + 2, terrainGroupPixelSize + 2).Contains (evt.mousePosition)) {
					var thisPos = Int2(evt.mousePosition.x - xPos, evt.mousePosition.y - yPos);
					var slotNumber = (thisPos.y / pixSize) * 5 + (thisPos.x / pixSize);
					if (terrainSlotIndices[slotNumber] == -1) return;
					thisTerrainGroupSet[i].tileNumbers[0][terrainSlotIndices[slotNumber]] = TileInfo(currentSet, dragTileData.realTileNumber);
					groupsChanged = true;
					return;
				}
				i++;
			}
		}
	}
}

static function CreateGroupTexture (replace : boolean, groupReplaceNumber : int, doGroupSets : boolean, groupSets : List.<SpriteGroupData>) {
	var texPixelSize : int = groupPixelSize - 6;
	var name = "";
	var renderTex = new RenderTexture(texPixelSize, texPixelSize, 0, RenderTextureFormat.ARGB32);
	var renderCamera = new GameObject().AddComponent(Camera);
	renderCamera.orthographic = true;
	renderCamera.orthographicSize = texPixelSize / 2;
	renderCamera.nearClipPlane = .999;
	renderCamera.farClipPlane = 1.001;
	renderCamera.clearFlags = CameraClearFlags.SolidColor;
	renderCamera.backgroundColor = Color.clear;
	RenderTexture.active = renderTex;
	renderCamera.targetTexture = renderTex;
	var setsEnd = doGroupSets? groupSets.Count : 1;
	var doBreak = false;
	
	for (var i = 0; i < setsEnd; i++) {
		if (doBreak) break;
		var groupsEnd = doGroupSets? groupSets[i].spriteGroups.Count : 1;
		for (var j = 0; j < groupsEnd; j++) {
			if (!doGroupSets) {
				var groupSize = copyActive? copyBufferSize : Int2(selectionEnd.x - selectionStart.x + 1, selectionEnd.y - selectionStart.y + 1);
				if (groupSize.x >= groupPixelSize || groupSize.y >= groupPixelSize) {
					EditorUtility.DisplayDialog ("Selection too large", "Maximum size for selection is " + groupPixelSize + " x " + groupPixelSize + " tiles", "", "");
					doBreak = true;
					break;
				}
				var tileSize : Vector2 = thisLayer.tileSize;
				var groupTileNumbers = new List.<TileInfo>();
				var groupMapData = new MapData(groupSize, false);
				if (copyActive) {
					copyBuffer.CopyBlockTo (Int2.zero, Int2(copyBufferSize.x-1, copyBufferSize.y-1), groupMapData, Int2.zero, false, false);
				}
				else {
					thisLayer.mapData.CopyBlockTo (selectionStart, selectionEnd, groupMapData, Int2.zero, false, false);
				}
			}
			else {
				tileSize = groupSets[i].spriteGroups[j].tileSize;
				groupMapData = groupSets[i].spriteGroups[j].mapData;
				groupSize = groupMapData.mapSize;
			}
			var maxDimension : int = (groupSize.x * tileSize.x > groupSize.y * tileSize.y)? groupSize.x * tileSize.x : groupSize.y * tileSize.y;
			var tilePixelSize : float = texPixelSize / parseFloat(maxDimension);
			var groupTex = new Texture2D(texPixelSize, texPixelSize, TextureFormat.ARGB32, false);
			var tempParentTransform = new GameObject("T").transform;
			
			// Make sprites in scene and add tile numbers to group
			var p : Int2;
			for (p.y = 0; p.y < groupSize.y; p.y++) {
				for (p.x = 0; p.x < groupSize.x; p.x++) {
					var tInfo = groupMapData.GetTile (p);
					if (tInfo.set >= 0 && tInfo.tile >= 0) {
						var sortingOrder = groupMapData.GetOrder (p);
						var rotation = groupMapData.GetRotation (p);
						var flip = groupMapData.GetFlip (p);
						var tileScale : float = tilePixelSize / tileSize.x;
						var scale = Vector3(tileScale, tileScale, 1.0);
						var position = Vector3(p.x * tileSize.x * tileScale, p.y * tileSize.y * tileScale, 1000000);
						CreateTileSceneObject (0, tInfo, position, sortingOrder, rotation, flip, scale, tempParentTransform, name);
						
						if (!doGroupSets && !groupTileNumbers.Contains (tInfo)) {
							groupTileNumbers.Add (tInfo);
						}
					}
				}
			}
			
			// Make group texture
			var ratioX = (tileSize.y > tileSize.x)? 1 : tileSize.x / parseFloat(tileSize.y);
			var ratioY = (tileSize.y > tileSize.x)? tileSize.y / parseFloat(tileSize.x) : 1;
			var xPos = (groupSize.x / 2) * tilePixelSize * ratioX - ((groupSize.x % 2 == 0)? tilePixelSize/2 : 0) * ratioX;
			var yPos = (groupSize.y / 2) * tilePixelSize * ratioY - ((groupSize.y % 2 == 0)? tilePixelSize/2 : 0) * ratioY;
			renderCamera.transform.position = Vector3(xPos, yPos, 999999);
			renderCamera.Render();
			groupTex.ReadPixels (Rect(0, 0, texPixelSize, texPixelSize), 0, 0, false);
			groupTex.Apply();
			DestroyImmediate (tempParentTransform.gameObject);
			
			// Replace or add group if not loading groups
			if (!doGroupSets) {
				if (replace) {
					thisGroupSet[groupReplaceNumber] = new SpriteGroup(groupTileNumbers, groupMapData, GUIContent(groupReplaceNumber.ToString(), groupTex), thisLayer.tileSize);
				}
				else {
					thisGroupSet.Add (new SpriteGroup(groupTileNumbers, groupMapData, GUIContent((thisGroupSet.Count).ToString(), groupTex), thisLayer.tileSize));
					SetGroupDisplayCounts();
				}
				groupsChanged = true;
			}
			else {
				groupSets[i].spriteGroups[j].content = GUIContent(j.ToString(), groupTex);
			}
		}
	}
	renderCamera.targetTexture = null;
	RenderTexture.active = null;
	DestroyImmediate (renderCamera.gameObject);
	DestroyImmediate (renderTex);
}

static function GroupToBuffer (idx : int) {
	if (thisGroupSet.Count == 0) return;
	
	selectionActive = false;		
	var thisGroupSize = thisGroupSet[idx].mapData.mapSize;
	if (copyBuffer == null || copyBuffer.mapSize.x < thisGroupSize.x || copyBuffer.mapSize.y < thisGroupSize.y) {
		copyBuffer = new MapData (thisGroupSize, false);
	}
	copyBufferSize = thisGroupSize;
	thisGroupSet[idx].mapData.CopyBlockTo (Int2.zero, Int2(thisGroupSize.x-1, thisGroupSize.y-1), copyBuffer, Int2.zero, false, false);
	copyActive = true;
}

static function DeleteGroup (gSet : int, idx : int) {
	var thisGSet = groupSets[gSet].spriteGroups;
	thisGSet.RemoveAt (idx);
	if (gSet == currentGroupSet && selectedGroup == thisGSet.Count && thisGSet.Count >= 1) {
		selectedGroup--;
	}
	for (var i = idx; i < thisGSet.Count; i++) {
		thisGSet[i].content.text = i.ToString();
	}
	SetGroupDisplayCounts();
	groupsChanged = true;
}

static function ChangeStandardGroupSet (idx : int) {
	groupSets[currentGroupSet].selectedGroup = selectedGroup;
	currentGroupSet = idx;
	thisGroupSet = groupSets[idx].spriteGroups;
	selectedGroup = groupSets[idx].selectedGroup;
	SetGroupDisplayCounts();
}

static function DeleteGroupSet (idx : int) {
	var isSure = true;
	if (groupSets[idx].spriteGroups.Count > 0) {
		isSure = EditorUtility.DisplayDialog ("Delete set?", "Do you really want to delete set " + idx + "?", "Delete Set", "Cancel");
	}
	if (!isSure) return;
	
	if (groupSets.Count > 1) {
		groupSets.RemoveAt (idx);
		if (currentGroupSet == groupSets.Count) {
			currentGroupSet--;
		}
		thisGroupSet = groupSets[currentGroupSet].spriteGroups;
		SetGroupDisplayCounts();
	}
	else {
		thisGroupSet.Clear();
	}
	groupsChanged = true;
}

static function AddGroupSet () {
	if (groupSets.Count < 32) {
		groupSets.Add (new SpriteGroupData());
		ChangeStandardGroupSet (groupSets.Count - 1);
	}
}

static function ChangeRandomGroupSet (idx : int) {
	randomGroupSets[currentGroupSet].selectedGroup = selectedGroup;
	currentGroupSet = idx;
	thisRandomGroupSet = randomGroupSets[idx].randomGroups;
	selectedGroup = randomGroupSets[idx].selectedGroup;
	SetGroupDisplayCounts();
}

static function DeleteRandomGroupSet (idx : int) {
	var isSure = true;
	if (randomGroupSets[idx].randomGroups.Count > 0) {
		isSure = EditorUtility.DisplayDialog ("Delete set?", "Do you really want to delete set " + idx + "?", "Delete Set", "Cancel");
	}
	if (!isSure) return;
	
	if (randomGroupSets.Count > 1) {
		randomGroupSets.RemoveAt (idx);
		if (currentGroupSet == randomGroupSets.Count) {
			currentGroupSet--;
		}
		thisRandomGroupSet = randomGroupSets[currentGroupSet].randomGroups;
		SetGroupDisplayCounts();
	}
	else {
		thisRandomGroupSet.Clear();
	}
	groupsChanged = true;
}

static function AddRandomGroupSet () {
	if (randomGroupSets.Count < 32) {
		randomGroupSets.Add (new RandomGroupData());
		ChangeRandomGroupSet (randomGroupSets.Count - 1);
	}
}

static function AddRandomGroup () {
	thisRandomGroupSet.Add (new List.<TileInfo>());
	selectedGroup = thisRandomGroupSet.Count-1;
	SetGroupDisplayCounts();
}

static function DeleteRandomGroup (gSet : int, idx : int, askToDelete : boolean) {
	if (askToDelete) {
		var isSure = true;
		if (randomGroupSets[gSet].randomGroups[idx].Count > 0) {
			isSure = EditorUtility.DisplayDialog ("Delete group?", "Do you really want to delete group " + idx + "?", "Delete Group", "Cancel");
		}
		if (!isSure) return;
	}
	
	var thisGSet = randomGroupSets[gSet].randomGroups;
	thisGSet.RemoveAt (idx);
	if (gSet == currentGroupSet && selectedGroup == thisGSet.Count && thisGSet.Count >= 1) {
		selectedGroup--;
	}
	SetGroupDisplayCounts();
	groupsChanged = true;
}

static function AddTileToRandomGroup () {
	var alreadyAddedCount = 0;
	for (var i = 0; i < tileMultiSelectList.Count; i++) {
		if (thisRandomGroupSet[selectedGroup].Count < 16) {
			var tileInfo = new TileInfo(currentSet, thisTileSet[tileMultiSelectList[i]].realTileNumber);
			if (!thisRandomGroupSet[selectedGroup].Contains (tileInfo)) {
				thisRandomGroupSet[selectedGroup].Add (tileInfo);
			}
			else {
				alreadyAddedCount++;
			}
		}
	}
	if (alreadyAddedCount == tileMultiSelectList.Count) {
		EditorUtility.DisplayDialog ("Tile already added", "This group already contains the selected tile" + (doMultiSelect? "s" : ""), "", "");
	}
	else {
		groupsChanged = true;
	}
}

static function RemoveTileFromRandomGroup () {
	var alreadyRemovedCount = 0;
	for (var i = 0; i < tileMultiSelectList.Count; i++) {
		var tileInfo = new TileInfo(currentSet, thisTileSet[tileMultiSelectList[i]].realTileNumber);
		if (thisRandomGroupSet[selectedGroup].Contains (tileInfo)) {
			thisRandomGroupSet[selectedGroup].Remove (tileInfo);
		}
		else {
			alreadyRemovedCount++;
		}
	}
	if (alreadyRemovedCount != tileMultiSelectList.Count) {
		groupsChanged = true;
	}
	else {
		EditorUtility.DisplayDialog ("Group does not contain tile", "No action taken since this group does not contain the selected tile" + (doMultiSelect? "s" : ""), "", "");
	}
}

static function AddTerrainGroupSet () {
	if (terrainGroupSets.Count < 32) {
		terrainGroupSets.Add (new TerrainGroupData());
		ChangeTerrainGroupSet (terrainGroupSets.Count - 1);
	}
}

static function AddTerrainGroup () {
	thisTerrainGroupSet.Add (new TerrainGroup());
	selectedGroup = thisTerrainGroupSet.Count-1;
	SetGroupDisplayCounts();
}

static function ChangeTerrainGroupSet (idx : int) {
	terrainGroupSets[currentGroupSet].selectedGroup = selectedGroup;
	currentGroupSet = idx;
	thisTerrainGroupSet = terrainGroupSets[idx].terrainGroups;
	selectedGroup = terrainGroupSets[idx].selectedGroup;
	SetGroupDisplayCounts();
}

static function ClearTerrainGroup (gSet : int, idx : int) {
	var isSure = true;
	if (terrainGroupSets[gSet].terrainGroups[idx].Changed()) {
		isSure = EditorUtility.DisplayDialog ("Clear group?", "This will remove all tiles in group " + idx, "Clear Group", "Cancel");
	}
	if (!isSure) return;
	
	var tileNumbers = terrainGroupSets[gSet].terrainGroups[idx].tileNumbers;
	for (var i = 0; i < tileNumbers.Count; i++) {
		for (var j = 0; j < tileNumbers[i].Length; j++) {
			tileNumbers[i][j] = TerrainGroup.defaultTile;
		}
	}
	groupsChanged = true;
	SetTerrainGroupActiveList();
}

static function DeleteTerrainGroup (gSet : int, idx : int, askToDelete : boolean) {
	if (askToDelete) {
		var isSure = true;
		if (terrainGroupSets[gSet].terrainGroups[idx].Changed()) {
			isSure = EditorUtility.DisplayDialog ("Delete group?", "Do you really want to delete group " + idx + "?", "Delete Group", "Cancel");
		}
		if (!isSure) return;
	}
	
	var thisGSet = terrainGroupSets[gSet].terrainGroups;
	thisGSet.RemoveAt (idx);
	if (gSet == currentGroupSet && selectedGroup == thisGSet.Count && thisGSet.Count >= 1) {
		selectedGroup--;
	}
	SetGroupDisplayCounts();
	groupsChanged = true;
	SetTerrainGroupActiveList();
}

static function DeleteTerrainGroupSet (idx : int) {
	var isSure = true;
	if (terrainGroupSets[idx].terrainGroups.Count > 0) {
		isSure = EditorUtility.DisplayDialog ("Delete set?", "Do you really want to delete set " + idx + "?", "Delete Set", "Cancel");
	}
	if (!isSure) return;
	
	if (terrainGroupSets.Count > 1) {
		terrainGroupSets.RemoveAt (idx);
		if (currentGroupSet == terrainGroupSets.Count) {
			currentGroupSet--;
		}
		thisTerrainGroupSet = terrainGroupSets[currentGroupSet].terrainGroups;
		SetGroupDisplayCounts();
	}
	else {
		thisTerrainGroupSet.Clear();
	}
	groupsChanged = true;
	SetTerrainGroupActiveList();
}

static function TileInTerrainGroup (tInfo : TileInfo) : boolean {
	if (tileSets[tInfo.set].tileData.Count == 0) {
		return false;
	}
	tInfo.tile = tileSets[tInfo.set].tileData[tInfo.tile].realTileNumber;
	for (var i = 0; i < terrainGroupActiveList.Count; i++) {
		if (terrainGroupSets[terrainGroupActiveList[i].x].terrainGroups[terrainGroupActiveList[i].y].Contains (tInfo)) {
			return true;
		}
	}
	return false;
}