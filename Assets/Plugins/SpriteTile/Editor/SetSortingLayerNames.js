#pragma strict
import UnityEditorInternal;
import System.Reflection;
import System.Linq;
 
class SetLayerNames extends EditorWindow {
	@MenuItem ("Assets/Set SpriteTile Sorting Layer Names")
	static function ShowWindow () {
		EditorWindow.GetWindow (SetLayerNames);
		var tileManager = Resources.Load ("TileManager", TileManager) as TileManager;
		if (tileManager == null) {
			message = "No TileManager found in Resources. Please use the TileEditor to set it up.";
			return;
		}
		var internalEditorUtilityType : System.Type = InternalEditorUtility;
		var sortingLayersProperty : PropertyInfo = internalEditorUtilityType.GetProperty ("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
		var sortingLayerNames = sortingLayersProperty.GetValue (null, new System.Object[0]) as String[];
		
		if (sortingLayerNames.Distinct().Count() == sortingLayerNames.Length) {
			message = "Sorting layer names set successfully.";
			buttonLabel = "Close";
		}
		else {
			message = "Please ensure that all sorting layer names are unique.";
			buttonLabel = "Cancel";
			return;
		}
		
		tileManager.sortingLayerNames = sortingLayerNames;
		EditorUtility.SetDirty (tileManager);
		
		if (TileEditor.window != null) {
			TileEditor.SetSortingLayerNames();
		}
	}
	
	static var message : String;
	static var buttonLabel : String;
	
	function OnGUI () {
		GUILayout.Label (message);
		GUILayout.Space (10);
		if (GUILayout.Button (buttonLabel)) {
			EditorWindow.GetWindow (SetLayerNames).Close();
		}
	}
}