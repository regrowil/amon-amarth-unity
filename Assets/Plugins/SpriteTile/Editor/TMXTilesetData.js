#pragma strict

class TMXTilesetData {
	var firstgid : int;
	var name : String;
	var tileset : int;
	
	function TMXTilesetData (firstgid : int, name : String) {
		this.firstgid = firstgid;
		this.name = name;
		this.tileset = 0;
	}
}