// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

#pragma strict
import SpriteTile;
import TileEditor;

static function TerrainGroupCheck (p : Int2, map : MapData) {
	CheckPosition (p, map);
	CheckPosition (p+Int2.up, map);
	CheckPosition (p+Int2.down, map);
	CheckPosition (p+Int2.left, map);
	CheckPosition (p+Int2.right, map);
	CheckPosition (p+Int2.upLeft, map);
	CheckPosition (p+Int2.upRight, map);
	CheckPosition (p+Int2.downLeft, map);
	CheckPosition (p+Int2.downRight, map);
}

static function CheckPosition (p : Int2, map : MapData) {
	if (p.x < 0 || p.y < 0 || p.x >= map.mapSize.x || p.y >= map.mapSize.y) {
		return;
	}
	var newTInfo = CheckAdjacentPositions (p, map.GetTile (p), map);
	if (newTInfo.set >= 0 && newTInfo.tile >= 0) {
		SetUndo (p, Int2.zero, false, currentLayer);
		var thisTData = tileSets[newTInfo.set].tileData[GetTileNumber (newTInfo)];
		var thisTrigger = (thisTData.trigger != 0)? thisTData.trigger : map.GetTrigger (p);
		map.SetTile (p, newTInfo.set, thisTData.realTileNumber, thisTData.orderInLayer, thisTData.rotation, thisTData.useCollider, thisTrigger, map.GetFlip (p));
		SetMinMaxOrder (thisTData.orderInLayer);
	}
}

static function CheckAdjacentPositions (p : Int2, tInfo : TileInfo, map : MapData) : TileInfo {
	for (var i = 0; i < terrainGroupActiveList.Count; i++) {
		var tGroup = terrainGroupSets[terrainGroupActiveList[i].x].terrainGroups[terrainGroupActiveList[i].y];
		if (tGroup.Contains (tInfo)) {
			for (var j = 0; j < terrainLookups.Length; j++) {
				if (terrainLookups[j].FillsContain (p, map, tGroup) && !terrainLookups[j].EmptiesContain (p, map, tGroup)) {
					return tGroup.tileNumbers[0][j];
				}
			}
		}
	}
	return TileInfo(-1, -1);
}