// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

#pragma strict
import System.Collections.Generic;
import System.IO;
import UnityEngine.GUILayout;
import SpriteTile;
import TileEditor;

static function SetSelectedTile (i : int) {
	selectedTile = i;
	tileMultiSelectList.Clear();
	tileMultiSelectList.Add (i);
	doMultiSelect = false;
}

static function TileSetControls () {
	GUI.enabled = !isMovingTile;
	BeginHorizontal();
	Label ("<b><size=14>Tiles</size></b>", richtextLabel);
	Space (20);
	if (Button (content[Content.TileLoad])) {
		LoadSprite (false);
	}
	if (Button (content[Content.TileFromFolder])) {
		LoadSpritesInFolder();
	}
	if (selectedTile >= thisTileSet.Count) {
		selectedTile = 0;
	}
	GUI.enabled = (thisTileSet.Count > 0 && selectedTile < thisTileSet.Count && tileMultiSelectList.Count == 1 && thisTileSet[selectedTile].spriteAtlasCount < 2 && !isMovingTile);
	if (Button (content[Content.TileReplace])) {
		LoadSprite (true);
	}
	GUI.enabled = (thisTileSet.Count > 0 && !isMovingTile);
	if (Button (content[Content.TileRefresh])) {
		RefreshSprites();
	}
	GUI.enabled = !isMovingTile;
	FlexibleSpace();
	Label ("Set:");
	if (Button (content[Content.SetDelete])) {
		DeleteSpriteSet (currentSet);
	}
	if (Button (content[Content.SetNew])) {
		CreateSpriteSet();
	}
	EndHorizontal();
	Space (2);
	
	var buttonNumber = ToolbarButtons (tileSets.Count, currentSet);
	if (buttonNumber != -1) {
		ChangeSet (buttonNumber);
	}
}

static function ToolbarButtons (setCount : int, currentlySelected : int) : int {
	var buttonNumber = -1;
	for (var j = 0; j < Mathf.Ceil(setCount/8.0); j++) {
		BeginHorizontal();
		var end = Mathf.Min (j*8 + 8, setCount);
		Space (4);
		for (var i = j*8; i < end; i++) {
			if (Button ("Set " + i, buttonStyles[(i == currentlySelected)? 0 : 1], Width(56))) {
				buttonNumber = i;
			}
		}
		
		Space (1);
		Label (" ", toolbarBackground, Width((panelWidth - minPanelWidth) + (j*8 + 8 - i) * 56));
		EndHorizontal();
	}
	return buttonNumber;
}
	
static function TileControls () {
	// This bit should be in TilePreviewControls, but there's some kind of problem/bug if it's drawn after the tiles, and it works correctly if drawn first
	GUI.enabled = !isMovingTile;
	textureDisplaySize = EditorGUI.IntSlider (Rect(230, window.position.height * divPercent + 101 + Mathf.Ceil(tileSets.Count/8.0) * 21, 223, 16), textureDisplaySize, 30, 240);
	GUI.enabled = true;
	
	// We now continue with our regularly scheduled function
	if (tileSets.Count == 1 && tileSets[0].tileData.Count == 0) {
		SetupTileCountControls();
	}
	else if (showTileOptions) {
		TileOptionControls();
	}
	else if (thisTileSet.Count > 0) {
		BeginVertical("box");
		Space (1);
		BeginHorizontal();
		if (!doMultiSelect) {
			Label ("<b>Tile " + selectedTile + " defaults:</b>", richtextLabel);
		}
		else {
			Label ("<b>Multi defaults:</b>", richtextLabel);
		}
		useCollider = Toggle (useCollider, "Collider");
		Label ("Trigger");
		triggerDefault = EditorGUILayout.FloatField (triggerDefault, Width(35));
		Label ("Order");
		orderDefault = EditorGUILayout.IntField (orderDefault, Width(40));
		Label ("Rotation");
		rotationDefault = EditorGUILayout.FloatField (rotationDefault, Width(35));
		FlexibleSpace();
		EndHorizontal();
		Space (2);
		
		BeginHorizontal();
		usePhysics = Toggle (usePhysics, "Physics collider");
		Space (5);
		Label ("Material:");
		useNonTransparent = Toggle (useNonTransparent, "Non-transparent");
		useLighting = Toggle (useLighting, "Pixel lighting");
		FlexibleSpace();
		EndHorizontal();
		Space (1);
		
		GUI.enabled = usePhysics;
		BeginHorizontal();
		Label ("Collider:");
		useAutoCollider = Toggle (useAutoCollider, "Auto", radioButton);
		Space (5);
		useCustomCollider = Toggle (useCustomCollider, "Custom", radioButton);
		var objectSelected = (useCustomCollider && (Selection.activeObject as GameObject) != null); 
		GUI.enabled = (useCustomCollider && objectSelected);
		if (Button (content[Content.FromObject], Height(18))) {
			ApplyCustomCollider (currentSet, selectedTile);
		}
		GUI.enabled = (useCustomCollider && objectSelected && !doMultiSelect);
		if (Button (content[Content.ToObject], Height(18))) {
			ExportCustomCollider (currentSet, selectedTile);
		}
		FlexibleSpace();
		
		GUI.enabled = thisTileSet.Count >= 1;
		GUI.color.a = 0;	// Strange work-around for very strange GUI bug where the scroll wheel won't work for tile list after clicking "Move"
		Button ("", Width(1));		// Apparently the number of buttons has to match, though closing/re-opening the window fixes the issue without this work-around.
		Button ("", Width(1));
		GUI.color = Color.white;
		if (Button ("Options >", Height(18))) {
			showTileOptions = true;
		}
		GUI.enabled = true;
		
		EndHorizontal();
		Space (3);
		GUI.enabled = true;		
		EndVertical();
		if (evt.type == EventType.Repaint) {
			tileDefaultRect = GUILayoutUtility.GetLastRect();
			tileDefaultRect.x += tileWindowRect.x;
		}
		
		// Toggle "use physics"
		if (oldUsePhysics != usePhysics) {
			oldUsePhysics = usePhysics;
			for (var i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].usePhysics = usePhysics;
				if (!thisTileSet[tileMultiSelectList[i]].useCollider) {
					thisTileSet[tileMultiSelectList[i]].useCollider = true;
					useCollider = oldUseCollider = true;
				}
			}
			SetManagerSprite();
		}
		// Toggle "non-transparent"
		if (oldUseNonTransparent != useNonTransparent) {
			oldUseNonTransparent = useNonTransparent;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].useNonTransparent = useNonTransparent;
			}
			SetManagerSprite();
		}
		// Toggle "use lighting"
		if (oldUseLighting != useLighting) {
			oldUseLighting = useLighting;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].useLighting = useLighting;
			}
			SetManagerSprite();
		}
		// Toggle collider
		if (oldUseCollider != useCollider) {
			oldUseCollider = useCollider;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].useCollider = useCollider;
			}
			SetManagerSprite();
		}
		// Set order in layer
		if (oldOrderDefault != orderDefault) {
			orderDefault = Mathf.Clamp (orderDefault, short.MinValue, short.MaxValue);
			oldOrderDefault = orderDefault;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].orderInLayer = orderDefault;
			}
			SetManagerSprite();
		}
		// Set rotation
		if (oldRotationDefault != rotationDefault) {
			rotationDefault = Mathf.Repeat (rotationDefault, 360.0);
			oldRotationDefault = rotationDefault;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].rotation = rotationDefault;
			}
			SetManagerSprite();
		}
		// Set trigger number
		if (oldTriggerDefault != triggerDefault) {
			triggerDefault = Mathf.Clamp (triggerDefault, 0, 255);
			oldTriggerDefault = triggerDefault;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].trigger = triggerDefault;
			}
			SetManagerSprite();
		}
		// If selected texture was changed
		if (oldSelectedTexture != selectedTile) {
			oldSelectedTexture = selectedTile;
			usePhysics = oldUsePhysics = thisTileSet[selectedTile].usePhysics;
			useNonTransparent = oldUseNonTransparent = thisTileSet[selectedTile].useNonTransparent;
			useLighting = oldUseLighting = thisTileSet[selectedTile].useLighting;
			useCollider = oldUseCollider = thisTileSet[selectedTile].useCollider;
			useAutoCollider = oldUseAutoCollider = !thisTileSet[selectedTile].customCollider;
			useCustomCollider = oldUseCustomCollider = thisTileSet[selectedTile].customCollider;
			orderDefault = oldOrderDefault = thisTileSet[selectedTile].orderInLayer;
			rotationDefault = oldRotationDefault = thisTileSet[selectedTile].rotation;
			triggerDefault = oldTriggerDefault = thisTileSet[selectedTile].trigger;
		}
		// Radio buttons
		if (oldUseAutoCollider != useAutoCollider) {
			oldUseAutoCollider = useAutoCollider;
			useCustomCollider = oldUseCustomCollider = !useAutoCollider;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].customCollider = !useAutoCollider;
			}
			if (useAutoCollider) {
				SetManagerSprite();
			}
		}
		if (oldUseCustomCollider != useCustomCollider) {
			oldUseCustomCollider = useCustomCollider;
			useAutoCollider = oldUseAutoCollider = !useCustomCollider;
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				thisTileSet[tileMultiSelectList[i]].customCollider = useCustomCollider;
				if (useCustomCollider) {
					SetManagerSpriteCustom (tileMultiSelectList[i], true);
				}
			}
		}
	}
	else {	// Empty tile set
		BeginHorizontal("box", Height(67));
		Label (" ");
		EndHorizontal();
	}
}

static function TileOptionControls () {
	BeginVertical("box");
	Space (1);
	BeginHorizontal();
	GUI.color = Color.red;
	GUI.enabled = !isMovingTile;
	if (Button ((tileMultiSelectList.Count > 1)? "Delete Selected Tiles" : "Delete Selected Tile")) {
		DeleteTiles();
		showTileOptions = false;
	}
	GUI.color = Color.white;
	FlexibleSpace();
	if (Button ("Sort Tiles By Atlas Position")) {
		SortTiles();
	}
	FlexibleSpace();
	GUI.enabled = true;
	if (!isMovingTile) {
		if (Button ((tileMultiSelectList.Count > 1)? "Move Tiles" : "Move Tile")) {
			isMovingTile = true;
		}
	}
	else {
		if (Button ("Cancel Move")) {
			isMovingTile = false;
		}
	}
	EndHorizontal();
	
	Space (19);
	BeginHorizontal();
	if (isMovingTile) {
		Label ("Click a tile to move selected tile" + ((tileMultiSelectList.Count > 1)? "s" : "") + " to that position");
	}
	else {
		Label (" ");
	}
	FlexibleSpace();
	if (Button ("< Tile Defaults")) {
		showTileOptions = false;
		isMovingTile = false;
	}
	EndHorizontal();
	Space (2);
	EndVertical();
}

static function SetupTileCountControls () {
	BeginVertical("box", Height(67));
	BeginHorizontal();
	Label ("Max number of tile sets:", Width(170));
	setCount = System.Convert.ToInt32 (EditorGUILayout.EnumPopup (setCount, Width(50)));
	if (setCount <= 1) {	// According to a report, this could be 0 somehow
		setCount = 32;
	}
	FlexibleSpace();
	EndHorizontal();
	BeginHorizontal();
	Label ("Max number of tiles per set:", Width(170));
	Label ((32768 / System.Convert.ToInt32 (setCount)).ToString());
	FlexibleSpace();
	EndHorizontal();
	EndVertical();
	
	if (oldSetCount != setCount) {
		oldSetCount = setCount;
		var thisSetCount = System.Convert.ToInt32 (setCount);
		MapData.SetCounts (thisSetCount);
		tileManager.tilesetCount = thisSetCount;
	}
}

static function GetExtraVerticalSpace () : int {
	var vAdd = showTextureName? 14 : 0;
	if (showTextureNumber) {
		vAdd += 14;
	}
	return vAdd;
}

static function DrawTiles () {
	if (isMovingTile) {
		GUI.color = Color(.9, .9, .9);
	}
	texScrollPos = BeginScrollView (texScrollPos, "box", Height(window.position.height * divPercent));
	
	var texSize = textureDisplaySize - 6;
	var vAdd = GetExtraVerticalSpace();
	var vSize = textureDisplaySize + vAdd;
	var start : float = Mathf.Max (parseInt(texScrollPos.y) / vSize, 0);
	var end : float = Mathf.Min (start + (window.position.height * divPercent) / vSize + 1, textureRowCount);
	
	// Drag from tiles area
	if (evt.type == EventType.MouseDrag && evt.button == 0 && !evt.command && groupType == GroupType.Terrain && !dragging) {
		StartDrag (start, end, vAdd);
	}
	
	// Draw tiles
	Label ("", Height((textureDisplaySize + vAdd) * textureRowCount) );
	var i = start * textureColumnCount;
	for (var y = start; y < end; y++) {
		for (var x = 0; x < textureColumnCount; x++) {
			if (i < thisTileSet.Count) {
				if (GUI.Button (Rect (4 + x*textureDisplaySize, 4 + y*vSize, textureDisplaySize, textureDisplaySize + vAdd), 
								thisTileSet[i].content, textureDisplayTypes[tileMultiSelectList.Contains (i)? 0 : 1]) ) {
					if (!isMovingTile) {
						if (evt.shift) {
							if (i < selectedTile || i > selectedTile) {
								var iStart = (i < selectedTile)? i : selectedTile;
								var iEnd = (i < selectedTile)? selectedTile : i;
								tileMultiSelectList.Clear();
								for (var j = iStart; j <= iEnd; j++) {
									tileMultiSelectList.Add (j);
								}
								doMultiSelect = true;
							}
						}
						else if (evt.alt) {
							AddToMultiSelection (i);
						}
						else {
							SetSelectedTile (i);
							copyActive = false;
						}
					}
					else {
						MoveTiles (currentSet, i);
					}
				}
				
				var ratio = thisTileSet[i].displayRatio;
				if (thisTileSet[i].texture != null) {
					GUI.DrawTextureWithTexCoords (Rect(7 + x*textureDisplaySize + (textureDisplaySize*((1.0 - ratio.x)/2)),
														7 + y*vSize + (textureDisplaySize*((1.0 - ratio.y)/2)),
														texSize*ratio.x, texSize*ratio.y),
												  thisTileSet[i].texture, thisTileSet[i].textureRect);
				}
				i++;
			}
		}
	}
	
	EndScrollView();
	if (evt.type == EventType.Repaint) {
		tileSelectRect = GUILayoutUtility.GetLastRect();
		tileSelectRect.x += tileWindowRect.x;
	}
	GUI.color = Color.white;
}

static function StartDrag (start : float, end : float, vAdd : int) {
	var i = start * textureColumnCount;
	var thisMousePos = Vector2(evt.mousePosition.x, evt.mousePosition.y - texScrollPos.y);
	if (Rect(0, 0, panelWidth, window.position.height * divPercent).Contains (thisMousePos)) {
		for (var y = start; y < end; y++) {
			for (var x = 0; x < textureColumnCount; x++) {
				if (i < thisTileSet.Count && Rect (4 + x*textureDisplaySize, 4 + y*(textureDisplaySize + vAdd) - texScrollPos.y, textureDisplaySize, textureDisplaySize + vAdd).Contains (thisMousePos)) {
					dragging = true;
					dragTileData = thisTileSet[i];
					evt.Use();
				}
				i++;
			}
		}
	}
}

static function TilePreviewControls () {
	GUI.enabled = !isMovingTile;
	BeginHorizontal();
	showTextureName = Toggle (showTextureName, " Name");
	showTextureNumber = Toggle (showTextureNumber, " Number");
	Label ("    Preview size");
	FlexibleSpace();	// The textureDisplaySize slider is in TileControls because it doesn't work right here
	EndHorizontal();
	
	// Toggle texture names
	if (oldShowTextureName != showTextureName) {
		oldShowTextureName = showTextureName;
		EditorPrefs.SetBool (basePathName + "ShowName", showTextureName);
		SetTileDisplayNames (0);
	}
	// Toggle texture numbers
	if (oldShowTextureNumber != showTextureNumber) {
		oldShowTextureNumber = showTextureNumber;
		EditorPrefs.SetBool (basePathName + "ShowNumber", showTextureNumber);
		SetTileDisplayNames (0);
	}
	// Change preview size
	if (oldTextureDisplaySize != textureDisplaySize) {
		oldTextureDisplaySize = textureDisplaySize;
		// Clear multi select list if resizing
		if (tileMultiSelectList.Count > 1) {
			selectedTile = tileMultiSelectList[0];
			tileMultiSelectList.Clear();
			tileMultiSelectList.Add (selectedTile);
			doMultiSelect = false;
		}
		EditorPrefs.SetInt (basePathName + "TextureDisplaySize", textureDisplaySize);
		SetTextureDisplayCounts();
	}
	GUI.enabled = true;
}

static function SetMultiSelection (pos1 : Vector2, pos2 : Vector2, clear : boolean) {
	var startPos = Int2(pos1);
	var endPos = Int2(pos2);
	if (startPos.x > endPos.x) {
		startPos.x = pos2.x;
		endPos.x = pos1.x;
	}
	if (startPos.y > endPos.y) {
		startPos.y = pos2.y;
		endPos.y = pos1.y;
	}
	var modPos = Int2(tileWindowRect.x + 10, tileWindowRect.y);
	startPos -= modPos;
	endPos -= modPos;
	var vAdd = GetExtraVerticalSpace();
	startPos.x = startPos.x / textureDisplaySize;
	startPos.y = parseInt(texScrollPos.y + startPos.y) / (textureDisplaySize + vAdd);
	endPos.x = Mathf.Min (endPos.x / textureDisplaySize, textureColumnCount-1);
	endPos.y = parseInt(texScrollPos.y + endPos.y) / (textureDisplaySize + vAdd);
	
	if (startPos.y >= textureRowCount && endPos.y >= textureRowCount) {
		return;
	}
	if (startPos == endPos) {
		SetSelectedTile (startPos.x + startPos.y*textureColumnCount);
		return;
	}
	startPos.y = Mathf.Min (startPos.y, textureRowCount-1);
	endPos.y = Mathf.Min (endPos.y, textureRowCount-1);
	
	if (clear) {
		tileMultiSelectList.Clear();
	}
	for (var y = startPos.y; y <= endPos.y; y++) {
		for (var x = startPos.x; x <= endPos.x; x++) {
			var tileNumber = x + y*textureColumnCount;
			if (tileNumber < thisTileSet.Count) {
				tileMultiSelectList.Add (tileNumber);
			}
		}
	}
	
	if (clear) {
		SetMultiSelectCopyBuffer (startPos, endPos);
	}
	else {
		SetMultiSelectCopyBuffer();
	}
	doMultiSelect = true;
	copyActive = true;
}

static function AddToMultiSelection (tileNum : int) {
	var index = tileMultiSelectList.IndexOf (tileNum);
	if (index != -1) {
		tileMultiSelectList.RemoveAt (index);
	}
	else {
		tileMultiSelectList.Add (tileNum);
	}
	// Prevent de-selecting only existing entry
	if (tileMultiSelectList.Count == 0) {
		tileMultiSelectList.Add (tileNum);
		return;
	}
	if (tileMultiSelectList.Count == 1) {
		doMultiSelect = false;
		return;
	}
	
	SetMultiSelectCopyBuffer();
	doMultiSelect = true;
	copyActive = true;
}

static function SetMultiSelectCopyBuffer () {
	var startPos = Int2(99999, 99999);
	var endPos = Int2(0, 0);
	for (var i = 0; i < tileMultiSelectList.Count; i++) {
		var pos = Int2(tileMultiSelectList[i] % textureColumnCount, tileMultiSelectList[i] / textureColumnCount);
		if (pos.x < startPos.x) {
			startPos.x = pos.x;
		}
		else if (pos.x > endPos.x) {
			endPos.x = pos.x;
		}
		if (pos.y < startPos.y) {
			startPos.y = pos.y;
		}
		else if (pos.y > endPos.y) {
			endPos.y = pos.y;
		}
	}
	SetMultiSelectCopyBuffer (startPos, endPos);
}

static function SetMultiSelectCopyBuffer (startPos : Int2, endPos : Int2) {
	copyBufferSize = Int2(endPos.x - startPos.x + 1, endPos.y - startPos.y + 1);
	copyBuffer = new MapData (copyBufferSize, false);
	var yIdx = 0;
	for (var y = endPos.y; y >= startPos.y; y--) {
		var xIdx = 0;
		for (var x = startPos.x; x <= endPos.x; x++) {
			var thisTileNumber = x + y*textureColumnCount;
			if (thisTileNumber >= thisTileSet.Count || !tileMultiSelectList.Contains (thisTileNumber)) {
				copyBuffer.SetTile (Int2(xIdx, yIdx), 0, -1);
			}
			else {
				copyBuffer.SetTile (Int2(xIdx, yIdx), currentSet, thisTileSet[thisTileNumber].realTileNumber);
				if (thisTileSet[thisTileNumber].useCollider) {
					copyBuffer.SetCollider (Int2(xIdx, yIdx), true);
				}
			}
			xIdx++;
		}
		yIdx++;
	}
}

static function MoveTiles (setNumber : int, newPos : int) {
	isMovingTile = false;
	
	var tData = tileSets[setNumber].tileData;
	if (tileMultiSelectList.Count == tData.Count) return;
	
	tileMultiSelectList.Sort();
	var multiListCopy = tileMultiSelectList.GetRange (0, tileMultiSelectList.Count);
	
	// Remove selected items from set
	var tempList = new List.<TileData>();
	for (var i = 0; i < multiListCopy.Count; i++) {
		tempList.Add (tData[multiListCopy[i]]);
		tData.RemoveAt (multiListCopy[i]);
		for (var j = i+1; j < multiListCopy.Count; j++) {
			multiListCopy[j] = multiListCopy[j]-1;
		}
	}
	
	// Insert selected items at new position
	if (newPos > tData.Count) {
		newPos = tData.Count;
	}
	for (i = 0; i < tempList.Count; i++) {
		var offset = newPos + (tileMultiSelectList[i] - tileMultiSelectList[0]);
		if (offset > tData.Count) {
			offset = tData.Count;
		}
		tData.Insert (offset, tempList[i]);
	}
	
	tileManager.MatchTileDataList (currentSet, thisTileSet);
	EditorUtility.SetDirty (tileManager);
	
	SetRefNumbers (currentSet);
	SetSelectedTile (newPos);
	SetTileDisplayNames (0);
}

static function SortTiles () {
	if (thisTileSet.Count < 2) return;
	
	for (var i = 0; i < thisTileSet.Count; i++) {
		if (thisTileSet[i].spriteAtlasCount < 2) {
			EditorUtility.DisplayDialog ("Set contains single sprite", "Sorting by rect only works if all the sprites in the set are part of an atlas", "", "");
			return;
		}
	}
	thisTileSet.Sort (TileDataRectSort);
	tileManager.MatchTileDataList (currentSet, thisTileSet);
	EditorUtility.SetDirty (tileManager);
}

static function TileDataRectSort (a : TileData, b : TileData) : int {
	if (a.textureRect.y < b.textureRect.y) {
		return 1;
	}
	if (a.textureRect.y > b.textureRect.y) {
		return -1;
	}
	if (a.textureRect.x < b.textureRect.x) {
		return -1;
	}
	if (a.textureRect.x > b.textureRect.x) {
		return 1;
	}
	return 0;
}

static function SetRefNumbers (setNumber : int) {
	var refNumbers = new int[tileSets[setNumber].tileData.Count];
	for (var i = 0; i < refNumbers.Length; i++) {
		refNumbers[tileSets[setNumber].tileData[i].realTileNumber] = i;
	}
	refNumbersList[setNumber] = refNumbers;
}
	
static function SetManagerSprite () {
	for (var j = 0; j < tileMultiSelectList.Count; j++) {
		var i = tileMultiSelectList[j];
		var customCollider = tileSets[currentSet].tileData[i].customCollider;
		var paths = (usePhysics && !customCollider)? GetColliderPaths (currentSet, i) : null;
		tileManager.SetSpriteData (currentSet, i, useCollider, orderDefault, rotationDefault, triggerDefault, paths, usePhysics, useNonTransparent, useLighting, customCollider);
		EditorUtility.SetDirty (tileManager);
	}
}

static function SetManagerSpriteCustom (tileIdx : int, customCollider : boolean) {
	tileManager.SetCustomCollider (currentSet, tileIdx, customCollider);
	EditorUtility.SetDirty (tileManager);
}

static function ApplyCustomCollider (thisSet : int, thisTile : int) {
	var collider = (Selection.activeObject as GameObject).GetComponent(PolygonCollider2D);
	if (collider == null) {
		var boxCollider = (Selection.activeObject as GameObject).GetComponent(BoxCollider2D);
		if (boxCollider == null) {
			EditorUtility.DisplayDialog ("No collider", "The selected object needs to have a PolygonCollider2D or BoxCollider2D component", "", "");
			return;
		}
		// Box collider
		var center = boxCollider.offset;
		var size = boxCollider.size;
		var paths = new ColliderPath[1];
		paths[0] = new ColliderPath([Vector2(center.x - size.x/2, center.y - size.y/2), Vector2(center.x - size.x/2, center.y + size.y/2),
									 Vector2(center.x + size.x/2, center.y + size.y/2), Vector2(center.x + size.x/2, center.y - size.y/2)]);
	}
	else {
		// Polygon collider
		paths = new ColliderPath[collider.pathCount];
		for (var i = 0; i < paths.Length; i++) {
			paths[i] = new ColliderPath(collider.GetPath (i));
		}
	}
	for (i = 0; i < tileMultiSelectList.Count; i++) {
		tileManager.UpdateSpritePaths (thisSet, tileMultiSelectList[i], paths);		
	}
	EditorUtility.SetDirty (tileManager);
	EditorUtility.DisplayDialog ("Collider applied", "Custom collider from object \"" + collider.name + "\" applied successfully to tile " + selectedTile, "", "");
}

static function ExportCustomCollider (thisSet : int, thisTile : int) {
	var collider = (Selection.activeObject as GameObject).GetComponent(PolygonCollider2D);
	if (collider == null) {
		collider = (Selection.activeObject as GameObject).AddComponent(PolygonCollider2D);
	}
	var spriteData = tileManager.GetSpriteData (thisSet, thisTile);
	collider.pathCount = spriteData.paths.Length;
	for (var i = 0; i < spriteData.paths.Length; i++) {
		collider.SetPath (i, spriteData.paths[i].path);
	}
	EditorUtility.DisplayDialog ("Collider applied", "Collider from tile " + selectedTile + " applied to object \"" + collider.name + "\"", "", "");
}

static function GetColliderPaths (thisSet : int, thisTile : int) : ColliderPath[] {
	var tempSpr = new GameObject().AddComponent(SpriteRenderer);
	tempSpr.sprite = tileManager.GetSprite (thisSet, thisTile);
	var tempCollider = tempSpr.gameObject.AddComponent(PolygonCollider2D);
	var paths = new ColliderPath[tempCollider.pathCount];
	for (var i = 0; i < paths.Length; i++) {
		paths[i] = new ColliderPath(tempCollider.GetPath (i));
	}
	DestroyImmediate (tempSpr.gameObject);
	return paths;
}

static function SetTileDisplayNames (idx : int) {
	for (var i = idx; i < thisTileSet.Count; i++) {
		if (showTextureName) {
			thisTileSet[i].content.text = showTextureNumber? thisTileSet[i].baseName + "\n" + i : thisTileSet[i].baseName;
		}
		else if (showTextureNumber) {
			thisTileSet[i].content.text = i.ToString();
		}
	}
	if (textureDisplayTypes == null) {
		return;
	}
	textureDisplayTypes[0].imagePosition = textureDisplayTypes[1].imagePosition = 
			(showTextureName || showTextureNumber)? ImagePosition.ImageAbove : ImagePosition.ImageOnly;
}

static function ChangeSet (idx : int) {
	tileSets[currentSet].selectedTile = selectedTile;
	currentSet = idx;
	thisTileSet = tileSets[idx].tileData;
	SetSelectedTile (tileSets[idx].selectedTile);
	oldSelectedTexture = -1;
	SetTextureDisplayCounts();
	SetTileDisplayNames (0);
	showTileOptions = false;
	SetScrollPosToSelectedTile();
	EditorPrefs.SetInt (basePathName + "TileSet", currentSet);
}

static function CreateSpriteSet () {
	if (tileSets.Count >= MapData.setCount) return;
	tileSets.Add (new TilesetData());
	refNumbersList.Add (new int[0]);
	tileManager.AddNewList ();
	EditorUtility.SetDirty (tileManager);
	ChangeSet (tileSets.Count - 1);
	UpdateSetlist();
}

static function DeleteSpriteSet (idx : int) {
	var isSure = true;
	if (thisTileSet.Count > 0) {
		isSure = EditorUtility.DisplayDialog ("Delete set?", "Do you really want to delete set " + currentSet + "? Any groups containing the tiles in this set will be deleted", "Delete Set " + currentSet, "Cancel");
	}
	if (isSure) {
		for (var i = 0; i < thisTileSet.Count; i++) {
			DeleteAffectedGroups (TileInfo (currentSet, i));			
		}
		
		if (tileSets.Count > 1) {
			tileSets.RemoveAt (idx);
			refNumbersList.RemoveAt (idx);
			tileManager.DeleteSpriteSet (currentSet);
			if (currentSet == tileSets.Count) {
				currentSet--;
			}
			thisTileSet = tileSets[currentSet].tileData;
			SetSelectedTile (tileSets[currentSet].selectedTile);
			oldSelectedTexture = -1;
			SetTextureDisplayCounts();
		}
		else {
			thisTileSet.Clear();
			tileManager.ClearList (0);
		}
		
		for (var j = 0; j < mapLayers.Count; j++) {
			mapLayers[j].mapData.DeleteSpriteSetTiles (idx);
		}
		UpdateSetlist();
		
		EditorUtility.SetDirty (tileManager);
	}
}

static function UpdateSetlist () {
	setList = new String[tileSets.Count];
	for (var i = 0; i < tileSets.Count; i++) {
		setList[i] = i.ToString();
	}
}

static function DeleteTile (tSet : int, listIdx : int, idxAdd : int, deleteMultiple : boolean, manualDelete : boolean) : boolean {
	var tileIdx = tileSets[tSet].tileData[listIdx].realTileNumber;
	var tInfo = TileInfo (tSet, tileIdx);
	if (manualDelete) {
		var cancelButtonString = deleteMultiple? "Skip" : "Cancel";
		// Check if existing groups contain the tile to be deleted
		if (groupSets.Count > 0 || thisGroupSet.Count > 0 || randomGroupSets.Count > 0 || thisRandomGroupSet.Count > 0) {
			var groupsToDelete = "";
			var groupAdded = false;
			for (var j = 0; j < groupSets.Count; j++) {
				var thisGSet = groupSets[j].spriteGroups;
				for (var i = 0; i < thisGSet.Count; i++) {
					if (thisGSet[i].tileNumbers.Contains (tInfo)) {
						if (!groupAdded) {
							groupAdded = true;
							groupsToDelete += "Standard: ";
						}
						groupsToDelete += TileInfo(j, i) + " ";
					}
				}
			}
			groupAdded = false;
			for (j = 0; j < randomGroupSets.Count; j++) {
				var thisRandomGSet = randomGroupSets[j].randomGroups;
				for (i = 0; i < thisRandomGSet.Count; i++) {
					if (thisRandomGSet[i].Contains (tInfo)) {
						if (!groupAdded) {
							groupAdded = true;
							groupsToDelete += "Random: ";
						}
						groupsToDelete += TileInfo(j, i) + " ";
					}
				}
			}
			groupAdded = false;
			for (j = 0; j < terrainGroupSets.Count; j++) {
				var thisTerrainGSet = terrainGroupSets[j].terrainGroups;
				for (i = 0; i < thisTerrainGSet.Count; i++) {
					if (thisTerrainGSet[i].Contains (tInfo)) {
						if (!groupAdded) {
							groupAdded = true;
							groupsToDelete += "Terrain: ";
						}
						groupsToDelete += TileInfo(j, i) + " ";
					}
				}
			}
			if (groupsToDelete != "") {
				var isSure = EditorUtility.DisplayDialog ("Group contains tile", "Are you sure you want to delete tile " + (listIdx + idxAdd) + "? These groups contain the tile and will be removed: " + groupsToDelete, "Delete tile " + (listIdx + idxAdd), cancelButtonString);
				if (!isSure) return false;
				DeleteAffectedGroups (tInfo);
			}
		}
	}
	
	tileSets[tSet].tileData.RemoveAt (listIdx);
	tileManager.DeleteSprite (tSet, listIdx);
	
	// Update real tile numbers
	for (i = 0; i < tileSets[tSet].tileData.Count; i++) {
		if (tileSets[tSet].tileData[i].realTileNumber >= tileIdx) {
			tileSets[tSet].tileData[i].realTileNumber--;
			tileManager.UpdateSpriteNumber (tSet, i, tileSets[tSet].tileData[i].realTileNumber);
		}
	}
	
	if (manualDelete) {
		EditorUtility.SetDirty (tileManager);
		
		if (selectedTile == tileSets[tSet].tileData.Count && tileSets[tSet].tileData.Count >= 1) {
			selectedTile--;
		}
		
		// Remove tile if found in maps and adjust other tiles in the map as necessary
		for (i = 0; i < mapLayers.Count; i++) {
			if (mapLayers[i].mapData.RemoveTile (tSet, tileIdx)) {
				levelChanged = true;
			}
		}
		
		// Change tile number in groups as necessary, and in group maps
		for (i = 0; i < groupSets.Count; i++) {
			thisGSet = groupSets[i].spriteGroups;
			for (j = 0; j < thisGSet.Count; j++) {
				var tNumbers = thisGSet[j].tileNumbers;
				for (var k = 0; k < tNumbers.Count; k++) {
					if (tNumbers[k].set == tSet) {
						if (tNumbers[k].tile == tileIdx) {
							tNumbers.RemoveAt (k--);
							groupsChanged = true;
						}
						else if (tNumbers[k].tile > tileIdx) {
							tNumbers[k] = TileInfo(tNumbers[k].set, tNumbers[k].tile - 1);
							groupsChanged = true;
						}
					}
				}
				if (thisGSet[j].mapData.RemoveTile (tSet, tileIdx)) {
					groupsChanged = true;
				}
			}
		}
	}
	
	SetTextureDisplayCounts();
	SetTileDisplayNames (listIdx);
	SetRefNumbers (tSet);
	return true;
}

static function DeleteAffectedGroups (tInfo : TileInfo) {
	for (var j = 0; j < groupSets.Count; j++) {
		var thisGSet = groupSets[j].spriteGroups;
		var i = 0;
		while (i < thisGSet.Count) {
			if (thisGSet[i].tileNumbers.Contains (tInfo)) {
				DeleteGroup (j, i);
				continue;
			}
			i++;
		}
	}
	for (j = 0; j < randomGroupSets.Count; j++) {
		var thisRandomGSet = randomGroupSets[j].randomGroups;
		i = 0;
		while (i < thisRandomGSet.Count) {
			if (thisRandomGSet[i].Contains (tInfo)) {
				DeleteRandomGroup (j, i, false);
				continue;
			}
			i++;
		}
	}
	for (j = 0; j < terrainGroupSets.Count; j++) {
		var thisTerrainGSet = terrainGroupSets[j].terrainGroups;
		i = 0;
		while (i < thisTerrainGSet.Count) {
			if (thisTerrainGSet[i].Contains (tInfo)) {
				DeleteTerrainGroup (j, i, false);
				continue;
			}
			i++;
		}
	}
}

static function DeleteTiles () {
	if (thisTileSet.Count == 0) return;
	
	var word = (tileMultiSelectList.Count > 1)? "tiles" : "tile";
	if (tileMultiSelectList.Count == 1) {
		var dialogString = "tile " + selectedTile;
	}
	else {
		var sequentialNumbers = true;
		for (var i = 1; i < tileMultiSelectList.Count; i++) {
			if (tileMultiSelectList[i] != tileMultiSelectList[i-1] + 1) {
				sequentialNumbers = false;
				break;
			}
		}
		if (sequentialNumbers) {
			dialogString = "tiles " + tileMultiSelectList[0] + " - " + tileMultiSelectList[tileMultiSelectList.Count-1];
		}
		else {
			dialogString = "tiles ";
			for (i = 0; i < tileMultiSelectList.Count; i++) {
				dialogString += tileMultiSelectList[i];
				if (i < tileMultiSelectList.Count - 1) {
					dialogString += ", ";
				}
			}
		}
	}
	
	var isSure = EditorUtility.DisplayDialog ("Delete " + word, "Are you sure you want to delete " + dialogString + "?", "Delete " + word, "Cancel");
	if (!isSure) return;
	
	var idx = 0;
	for (i = 0; i < tileMultiSelectList.Count; i++) {
		if (DeleteTile (currentSet, tileMultiSelectList[i] - idx, i, true, true)) {
			idx++;
		}
	}
	selectedTile = Mathf.Min (selectedTile, thisTileSet.Count-1);
	SetSelectedTile (selectedTile);
}

static function GetTileSizeMax (tData : TileData) : float {
	return (tData.size.x > tData.size.y)? tData.size.x : tData.size.y;
}

static function LoadSprite (replace : boolean) {
	if (!replace && thisTileSet.Count >= MapData.tileShift) {
		EditorUtility.DisplayDialog ("Tileset capacity reached", "Maximum of " + MapData.tileShift + " tiles per set", "", "");
		return;
	}
	var path = EditorUtility.OpenFilePanel ((replace? "Replace" : "Load") + " sprite", savedSpritePath, "");
	if (path == "") return;
	
	savedSpritePath = Path.GetDirectoryName (path);
	EditorPrefs.SetString (basePathName + "SpritePath", savedSpritePath);
	
	var firstLoad = (thisTileSet.Count == 0 && tileSets.Count == 1 && tileSizeX == 1.0);
	var idx = path.IndexOf ("/Assets/") + 1;
	var sPath = path.Substring (idx, path.Length-idx);
	var tileSetCount = thisTileSet.Count;	// Since thisTileSet.Count can change
	var spriteCount = ImportSpriteFile (path, sPath, tileSetCount, replace, false);
	if (spriteCount == -1) {
		return;
	}
	if (spriteCount == 0) {
		EditorUtility.DisplayDialog ("Sprite not found", "No sprite found for asset:\n  " + sPath + "\nIf that's a texture, the texture type needs to be set to 'Sprite', or 'Advanced' with Sprite Mode as Single or Multiple", "", "");
		return;
	}
	if (spriteCount > MapData.tileShift) {
		EditorUtility.DisplayDialog ("Too many sprites", "> " + MapData.tileShift + " sprites found in:\n  " + sPath, "", "");
		return;
	}
	if ((tileSetCount + spriteCount) > MapData.tileShift) {
		EditorUtility.DisplayDialog ("Too many sprites", "Loading the sprites found in:\n  " + sPath + "\nwould exceed " + MapData.tileShift + " for this set", "", "");
		return;
	}
	EditorUtility.SetDirty (tileManager);
	SetRefNumbers (currentSet);
	SetTileDisplayNames (0);
	if (selectedTile < 0) {
		selectedTile = 0;
		SetSelectedTile (selectedTile);
	}
	
	if (firstLoad) {
		tileSizeX = tileSizeY = oldTileSizeX = oldTileSizeY = GetTileSizeMax (thisTileSet[0]);
	}
}

static function SpriteIndexInSet (thisSet : int, spr : Sprite) : int {
	for (var i = 0; i < tileSets[thisSet].tileData.Count; i++) {
		if (tileSets[thisSet].tileData[i].sprite == spr) {
			return i;
		}
	}
	return -1;
}

static function ImportSpriteFile (path : String, sPath : String, tileSetCount : int, replace : boolean, fromFolder : boolean) : int {
	var newSpriteCount = 0;
	var spriteCount = 0;
	var sprAssets = AssetDatabase.LoadAllAssetsAtPath (sPath);
	var foundTex = false;
	var foundSprite = false;
	var sprTexture : Texture2D;
		
	for (var i = 0; i < sprAssets.Length; i++) {
		if (!foundTex && typeof(sprAssets[i]).Equals (Texture2D)) {
			foundTex = true;
			sprTexture = sprAssets[i] as Texture2D;
		}
		if (typeof(sprAssets[i]).Equals (Sprite)) {
			foundSprite = true;
			spriteCount++;
			if (SpriteIndexInSet (currentSet, sprAssets[i] as Sprite) == -1) {
				newSpriteCount++;
			}
		}
	}
	if (!foundTex || !foundSprite) {
		return 0;
	}
	
	if ((tileSetCount + newSpriteCount) > MapData.tileShift) {
		return newSpriteCount;
	}
	if (newSpriteCount == 0 && !fromFolder) {
		if (spriteCount == 1) {
			var doUpdate = EditorUtility.DisplayDialog ("Sprite exists", "This sprite already exists in the current set. Do you want to update it?", "Update", "Cancel");
		}
		else {
			doUpdate = EditorUtility.DisplayDialog ("Sprites exist", "All the sprites in the atlas already exist in the current set. Do you want to update them?", "Update", "Cancel");
		}
		if (!doUpdate) {
			return -1;
		}
	}
	
	var originalReplace = replace;
	for (i = 0; i < sprAssets.Length; i++) {
		if (typeof(sprAssets[i]).Equals (Sprite)) {
			var spr = sprAssets[i] as Sprite;
			replace = originalReplace;
			var replaceIdx = selectedTile;	// If replacing a single texture
			var existsIdx = SpriteIndexInSet (currentSet, spr);
			if (existsIdx != -1) {
				replace = true;
				replaceIdx = existsIdx;
			}
			var realNumber = thisTileSet.Count;
			AddSprite (thisTileSet, spr.name, spr, sprTexture, 0, 0.0, 0, useColliderDefault, replace, replaceIdx, spriteCount, false, false, false, false, realNumber);
			if (replace) {
				var tInfo = TileInfo(currentSet, replaceIdx);
				tileManager.UpdateSprite (tInfo.set, tInfo.tile, spr);
				var thisTileData = tileSets[tInfo.set].tileData[tInfo.tile];
				if (thisTileData.usePhysics && !thisTileData.customCollider) {
					var paths = GetColliderPaths (tInfo.set, tInfo.tile);
					tileManager.UpdateSpritePaths (tInfo.set, tInfo.tile, paths);
				}
			}
			else {
				AddSpriteToManager (spr, newSpriteCount, realNumber);
			}
		}
	}
	if (tileSizeX == 0.0) {
		tileSizeX = tileSizeY = GetTileSizeMax (thisTileSet[0]);
	}
	SetTextureDisplayCounts();
	return spriteCount;
}

static function LoadSpritesInFolder () {
	var path = EditorUtility.OpenFolderPanel ("Load all sprites in folder", savedSpritePath, "");
	if (path == "") return;
	
	savedSpritePath = path;
	EditorPrefs.SetString (basePathName + "SpritePath", savedSpritePath);
	
	var firstLoad = (thisTileSet.Count == 0 && tileSets.Count == 1 && tileSizeX == 1.0);
	var filePaths = Directory.GetFiles (path);
	var spriteCount = 0;
	var totalSpriteCount = thisTileSet.Count;
	var foundDuplicate = false;
	for (filePath in filePaths) {
		if (!filePath.EndsWith(".meta") && !filePath.ToLower().EndsWith(".ds_store") && !filePath.EndsWith(".unity")) {
			var idx = filePath.IndexOf ("/Assets/") + 1;
			var sPath = filePath.Substring (idx, filePath.Length-idx);
			var thisCount = ImportSpriteFile (filePath, sPath, totalSpriteCount, false, true);
			if (thisCount != -1) {
				spriteCount += thisCount;
				totalSpriteCount += thisCount;
				if (totalSpriteCount > MapData.tileShift) {
					EditorUtility.DisplayDialog ("Too many sprites", "Loading the sprites found in:\n  " + sPath + "\nwould exceed " + MapData.tileShift + " for this set", "", "");
					break;
				}
			}
			else {
				foundDuplicate = true;
			}
		}
	}
	
	if (spriteCount == 0 && !foundDuplicate) {
		EditorUtility.DisplayDialog ("No sprites loaded", "No sprites found for the folder:\n  " + path, "", "");
		return;
	}
	if (totalSpriteCount <= MapData.tileShift) {
		EditorUtility.SetDirty (tileManager);
		SetRefNumbers (currentSet);
	}
	if (firstLoad) {
		tileSizeX = tileSizeY = oldTileSizeX = oldTileSizeY = GetTileSizeMax (thisTileSet[0]);
	}
}

static function LoadSpritesFromManager () {
	var numberOfSets = tileManager.SpriteSetCount();
	tileSets = new List.<TilesetData>(numberOfSets);
	var updateManager = false;
	
	for (var i = 0; i < numberOfSets; i++) {
		var spriteListCount = tileManager.SpriteListCount (i);
		
		// See if realTileNumber needs to be computed from list count due to upgrade
		var zeroCount = 0;
		for (var j = 0; j < spriteListCount; j++) {
			if (tileManager.GetSpriteData (i, j).realTileNumber == 0 && ++zeroCount > 1) {
				updateManager = true;
				break;
			}
		}
		
		var thisTDatas = new List.<TileData>(spriteListCount);
		for (j = 0; j < spriteListCount; j++) {
			var spr = tileManager.GetSpriteData (i, j);
			if (spr.sprite) {
				if (spr.spriteCount == 0) {
					spr.spriteCount = 1;
				}
				var realTileNumber = spr.realTileNumber;
				if (zeroCount > 1) {
					realTileNumber = thisTDatas.Count;
					tileManager.UpdateSpriteNumber (i, j, realTileNumber);
				}
				AddSprite (thisTDatas, spr.sprite.name, spr.sprite, spr.sprite.texture, spr.orderInLayer, spr.rotation, spr.trigger, spr.useCollider, false, 0, spr.spriteCount, spr.usePhysics, spr.useNonTransparent, spr.useLighting, spr.customCollider, realTileNumber);
			}
			else {
				var sprite = Sprite.Create (EditorGUIUtility.whiteTexture, Rect(0, 0, 1, 1), Vector2(.5, .5), 100);
				AddSprite (thisTDatas, "(missing)", sprite, EditorGUIUtility.whiteTexture, 0, 0.0, 0, false, false, 0, 0, false, false, false, false, thisTDatas.Count);
			}
		}
		tileSets.Add (new TilesetData(thisTDatas));
		refNumbersList.Add (new int[0]);
		SetRefNumbers (i);
	}
	
	if (updateManager) {
		EditorUtility.SetDirty (tileManager);
	}
}

static function RefreshSprites () {
	for (var i = 0; i < thisTileSet.Count; i++) {
		var spriteData = tileManager.GetSpriteData (currentSet, i);
		AddSprite (thisTileSet, spriteData.sprite.name, spriteData.sprite, spriteData.sprite.texture, spriteData.orderInLayer, spriteData.rotation, spriteData.trigger, spriteData.useCollider, true, i, spriteData.spriteCount, spriteData.usePhysics, spriteData.useNonTransparent, spriteData.useLighting, spriteData.customCollider, spriteData.realTileNumber);
		
		if (spriteData.useCollider && !spriteData.customCollider) {
			spriteData.paths = GetColliderPaths (currentSet, i);
		}
	}
	EditorUtility.SetDirty (tileManager);
}

static function AddSpriteToManager (spr : Sprite, spriteCount : int, realTileNumber : int) {
	tileManager.AddSprite (currentSet, spr, spriteCount, realTileNumber);
}

static function AddSprite (tSet : List.<TileData>, name : String, spr : Sprite, sprTexture : Texture2D, order : int, thisRotation : float, thisTrigger : int, useCol : boolean, replace : boolean, idx : int, spriteCount : int, usePhysics : boolean, useNonTransparent : boolean, useLighting : boolean, customCollider : boolean, realTileNumber : int) {
	var tileName = showTextureNumber? name + "\n" + (replace? idx : tSet.Count) : name;
	var size = Vector2(spr.bounds.size.x, spr.bounds.size.y);
	var pivot : Vector2 = spr.bounds.center;
	var displayRatio = (size.x > size.y)? Vector2(1.0, size.y / size.x) : Vector2(size.x / size.y, 1.0);
	
	if (replace) {
		tSet[idx].SetData (tileName, name, sprTexture, size, pivot, spr.rect, displayRatio, spr);
	}
	else {
		tSet.Add (new TileData(tileName, name, sprTexture, size, pivot, spr.rect, displayRatio, order, thisRotation, thisTrigger, useCol, spriteCount, usePhysics, useNonTransparent, useLighting, customCollider, realTileNumber, spr));
	}
}

static function SetTextureDisplayCounts () {
	textureColumnCount = (panelWidth - 24) / (textureDisplaySize + 1);
	textureRowCount = Mathf.Ceil (thisTileSet.Count / parseFloat(textureColumnCount));
}