// Version 3.1
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

#pragma strict
import System.Collections.Generic;
import System.Text;
import System.IO;
import UnityEngine.GUILayout;
import SpriteTile;
import GroupControls;
import TileControls;
import TerrainGroupLogic;
import FileIO;
import ToolTips;

class TileEditor extends EditorWindow {
	@MenuItem ("Window/SpriteTile Editor %&e")
	static function ShowWindow () {
		window = EditorWindow.GetWindow (TileEditor);
		window.minSize = Vector2(1070, 550);
		window.wantsMouseMove = true;
		// Get name of project folder
		basePathName = Application.dataPath.Substring (0, Application.dataPath.LastIndexOf ("/"));
		var idx = basePathName.LastIndexOf ("/");
		basePathName = "SpriteTile" + basePathName.Substring (idx + 1, basePathName.Length - idx - 1);
		
		showTextureName = oldShowTextureName = EditorPrefs.GetBool (basePathName + "ShowName", true);
		showTextureNumber = oldShowTextureNumber = EditorPrefs.GetBool (basePathName + "ShowNumber", false);
		syncLayerPos = oldSyncLayerPos = EditorPrefs.GetBool (basePathName + "SyncLayerPos", false);
		showGrid = oldShowGrid = EditorPrefs.GetBool (basePathName + "ShowGrid", true);
		showPrecedingLayer = oldShowPrecedingLayer = EditorPrefs.GetBool (basePathName + "ShowPrecedingLayer", false);
		showNextLayer = oldShowNextLayer = EditorPrefs.GetBool (basePathName + "ShowNextLayer", false);
		showAllLayers = oldShowAllLayers = EditorPrefs.GetBool (basePathName + "ShowAllLayers", false);
		panelWidth = EditorPrefs.GetInt (basePathName + "PanelWidth", minPanelWidth);
		divPercent = EditorPrefs.GetFloat (basePathName + "DivPercent", 0.35);
		textureDisplaySize = oldTextureDisplaySize = EditorPrefs.GetInt (basePathName + "TextureDisplaySize", 85);
		showExtra = oldShowExtra = EditorPrefs.GetBool (basePathName + "ShowExtra", false);
		showLight = oldShowLight = EditorPrefs.GetBool (basePathName + "ShowLight", false);
		showLayerControls = EditorPrefs.GetBool (basePathName + "ShowLayerControls", true);
		showLightControls = EditorPrefs.GetBool (basePathName + "ShowLightControls", false);
		showSelectionControls = EditorPrefs.GetBool (basePathName + "ShowSelectionControls", false);		
		showTips = oldShowTips = EditorPrefs.GetBool (basePathName + "ShowTips", true);
		
		if (tileMultiSelectList == null) {
			tileMultiSelectList = new List.<int>();
		}
		
		if (!tileSets) {
			InitializeTiles();
			InitializeLevel();
			
			richtextLabel = new GUIStyle("label");
			richtextLabel.richText = true;
			cField = new GUIStyle("textfield");
			cField.margin.top = 4;
			cLabel = new GUIStyle("label");
			cLabel.margin.top = 4;
			radioButton = new GUIStyle(EditorStyles.radioButton);
			radioButton.margin = RectOffset(4, 4, 2, 2);
			
			var box = new GUIStyle("box");
			box.imagePosition = ImagePosition.ImageAbove;
			box.wordWrap = false;
			box.alignment = TextAnchor.LowerCenter;
			box.normal.textColor = richtextLabel.normal.textColor;
			box.normal.background = EditorGUIUtility.LoadRequired (EditorGUIUtility.isProSkin? "SpriteTile/Box2.png" : "SpriteTile/Box.png") as Texture2D;
			box.border = RectOffset(3, 3, 3, 3);
			var label = new GUIStyle("label");
			label.padding = box.padding;
			label.border = box.border;
			label.margin = box.margin;
			label.imagePosition = ImagePosition.ImageAbove;
			label.wordWrap = false;
			label.alignment = TextAnchor.LowerCenter;
			textureDisplayTypes = [box, label];
			groupDisplayTypes = [new GUIStyle(box), new GUIStyle(label)];
			
			tipStyle = new GUIStyle("box");
			tipStyle.alignment = TextAnchor.MiddleLeft;
			tipStyle.padding.top = 1;
			tipStyle.normal.textColor = richtextLabel.normal.textColor;
			
			toolbarBackground = new GUIStyle(EditorStyles.toolbar);
			var toolbar = new GUIStyle(EditorStyles.toolbarButton);
			var toolbarSelected = new GUIStyle(EditorStyles.toolbarButton);
			var normalButton = new GUIStyle("button");
			toolbarSelected.normal.background = toolbar.onNormal.background;
			toolbarSelected.margin.top = toolbar.margin.top = toolbarBackground.margin.top = normalButton.margin.top;
			buttonStyles = [toolbarSelected, toolbar];
			
			var pressedButton = new GUIStyle("button");
			pressedButton.normal.background = normalButton.active.background;
			pressedButtonStyles = [normalButton, pressedButton];
			
			foldoutStyle = new GUIStyle("foldout");
			foldoutStyle.richText = true;
			
			divider = new GUIStyle("button");
			divider.border = RectOffset(4, 4, 4, 4);
			
			colliderTex = EditorGUIUtility.whiteTexture;
			
			functionKeys = [KeyCode.F1, KeyCode.F2, KeyCode.F3, KeyCode.F4, KeyCode.F5, KeyCode.F6, KeyCode.F7, KeyCode.F8, KeyCode.F9, KeyCode.F10, KeyCode.F11, KeyCode.F12, KeyCode.F13, KeyCode.F14, KeyCode.F15];
			bookmarks = new Bookmark[functionKeys.Length];
			for (var i = 0; i < bookmarks.Length; i++) {
				bookmarks[i] = Bookmark(Vector2.zero, 0, false);
			}
		}
		
		if (lineTex == null) {
			InitializeTex();
		}
		
		CleanGOList();
		SetPreviewDictionary();
		SetSortingLayerNames();
		
		var savedSet = EditorPrefs.GetInt (basePathName + "TileSet", 0);
		if (savedSet > 0 && savedSet < tileSets.Count) {
			ChangeSet (savedSet);
		}
		var savedGroupType = EditorPrefs.GetInt (basePathName + "GroupType", 0);
		if (savedGroupType > 0) {
			SwitchGroupType (savedGroupType);
		}
		SetTileDisplayNames (0);
		
		setCount = tileManager.tilesetCount;
		if (setCount == 0) {
			setCount = 32;
		}
		oldSetCount = setCount;
		MapData.SetCounts (setCount);
		
		mapMultiSelectList = new List.<Int2>();
		
		// Deal with any missing tiles
		var deleteList = new List.<List.<int> >();
		var missingCount = 0;
		for (i = 0; i < tileManager.SpriteSetCount(); i++) {
			var setList = new List.<int>();
			deleteList.Add (setList);
			for (var j = 0; j < tileManager.SpriteListCount (i); j++) {
				if (tileManager.GetSprite (i, j) == null) {
					setList.Add (j);
					missingCount++;
				}
			}
		}
		if (missingCount > 0) {
			var word = (missingCount == 1)? "sprite" : "sprites";
			var deleteMissing = !EditorUtility.DisplayDialog ("Missing " + word, missingCount + " " + word + " missing. Delete missing " + word + " from tile list?", "Continue", "Delete");
			if (deleteMissing) {
				for (i = 0; i < deleteList.Count; i++) {
					for (j = 0; j < deleteList[i].Count; j++) {
						DeleteTile (i, deleteList[i][j], 0, true, false);
						for (var k = j+1; k < deleteList[i].Count; k++) {
							deleteList[i][k] = deleteList[i][k] - 1;
						}
					}
				}
			}
			EditorUtility.SetDirty (tileManager);
		}
		
		savedSpritePath = EditorPrefs.GetString (basePathName + "SpritePath", Application.dataPath + "/");
		savedGroupPath = EditorPrefs.GetString (basePathName + "GroupPath", Application.dataPath + "/");
		if (!File.Exists (savedGroupPath)) {
			savedGroupPath = Application.dataPath + "/";
		}
		savedLevelPath = EditorPrefs.GetString (basePathName + "LevelPath", Application.dataPath + "/");
		if (!File.Exists (savedLevelPath)) {
			savedLevelPath = Application.dataPath + "/";
		}
		autoGroupLoad = oldAutoGroupLoad = EditorPrefs.GetBool (basePathName + "AutoGroupLoad", true);
		autoLevelLoad = oldAutoLevelLoad = EditorPrefs.GetBool (basePathName + "AutoLevelLoad", true);	
		if (autoGroupLoad) {
			Open (true, false, savedGroupPath);
		}
		if (autoLevelLoad) {
			Open (false, false, savedLevelPath);
		}
		
		mouseDown = false;
		UpdateShowLight();
		SetToolTips();
		SetTipSpace();
	}
	
	static function CleanGOList () {
		var goList = tileManager.goList;
		if (goList == null) return;
		
		// Attempt to find any missing GameObjects by name, otherwise make sure stored name is the current name
		var updateManager = false;
		for (var i = 0; i < goList.Count; i++) {
			if (goList[i].go == null) {
				var guids = AssetDatabase.FindAssets (goList[i].name);
				if (guids != null) {
					var asset : GameObject;
					for (var guid in guids) {
						asset = AssetDatabase.LoadAssetAtPath (AssetDatabase.GUIDToAssetPath (guid), GameObject);
						if (asset != null) {
							break;
						}
					}
					if (asset != null) {
						goList[i].go = asset;
						updateManager = true;
					}
				}
			}
			else if (goList[i].name != goList[i].go.name) {
				goList[i].name = goList[i].go.name;
				updateManager = true;
			}
		}
		// Clean up missing GameObjects
		i = 0;
		while (i < goList.Count) {
			if (goList[i].go == null) {
				goList.RemoveAt (i);
				updateManager = true;
				continue;
			}
			i++;
		}
		
		if (updateManager) {
			EditorUtility.SetDirty (tileManager);
		}
	}
	
	static function SetPreviewDictionary () {
		previewDictionary = new Dictionary.<int, Texture2D>();
		var goList = tileManager.goList;
		if (goList == null) {
			return;
		}
		for (var i = 0; i < goList.Count; i++) {
			previewDictionary[goList[i].guid] = null;	// Loaded as needed in DrawMap
		}
	}
	
	static function SetSortingLayerNames () {
		sortingLayerNames = tileManager.GetSortingLayerNames();
		if (sortingLayerIndices != null) {
			var clamped = false;
			for (var i = 0; i < sortingLayerIndices.Count; i++) {
				if (sortingLayerIndices[i] >= sortingLayerNames.Length) {
					sortingLayerIndices[i] = sortingLayerNames.Length-1;
					clamped = true;
				}
			}
			if (clamped) {
				sortingLayerIndex = oldSortingLayerIndex = sortingLayerIndices[currentLayer];
				levelChanged = true;
			}
		}
	}
	
	static function InitializeTiles () {
		tileSets = new List.<TilesetData>();
		refNumbersList = new List.<int[]>();
		tileManager = AssetDatabase.LoadAssetAtPath ("Assets/Plugins/SpriteTile/Resources/TileManager.asset", TileManager);
		if (!tileManager) {
			CreateTileManager();
		}
		if (!tileManager.DataListInitialized || tileManager.SpriteSetCount() == 0) {
			tileManager.InitializeList();
			CreateSpriteSet();
		}
		else {
			LoadSpritesFromManager();
		}
		thisTileSet = tileSets[0].tileData;
		InitializeGroups();
		SetTextureDisplayCounts();
		SetGroupDisplayCounts();
		currentSet = 0;
		SetSelectedTile (0);
		UpdateSetlist();
	}
	
	static function InitializeLevel () {
		if (mapSize == Int2.zero) {
			mapSize = newMapSize = new Int2(50, 50);
		}
		else {
			mapSize = newMapSize;
		}
		mapScrollPos = Vector2.zero;
		mapLayers = new List.<LayerData>();
		mapLayers.Add (new LayerData(mapSize, true));
		thisLayer = mapLayers[0];
		currentLayer = 0;
		zPos = oldZPos = 0.0;
		if (tileSizeX == 0) {
			tileSizeX = (thisTileSet.Count > 0)? GetTileSizeMax (thisTileSet[0]) : 1.0;
			oldTileSizeX = tileSizeX;
		}
		if (tileSizeY == 0) {
			tileSizeY = oldTileSizeY = tileSizeX;
		}
		thisLayer.tileSize = Vector2(tileSizeX, tileSizeY);
		lock = oldLock = LayerLock.None;
		addBorder = oldAddBorder = 0;
		SetLayerNames();
		selectionActive = false;
		mapMultiSelectActive = false;
		levelChanged = false;
		InitializeUndo();
		currentFileName = "";
		orderMin = orderMax = 0;
		savedGridPos = Int2(-1, -1);
		sortingLayerIndices = new List.<int>();
		sortingLayerIndices.Add (0);
		sortingLayerIndex = oldSortingLayerIndex = 0;
		editTile = false;
		lightAmbient = oldLightAmbient = Color(.5, .5, .5, 1.0);
		thisLayer.mapData.ambientColor = lightAmbient;
		thisLayer.mapData.ForceColorReset();
		SetDefaultLight();
		UpdateShowLight();
	}
	
	static function InitializeGroups () {
		groupSets = new List.<SpriteGroupData>();
		groupSets.Add (new SpriteGroupData());
		thisGroupSet = groupSets[0].spriteGroups;
		
		randomGroupSets = new List.<RandomGroupData>();
		randomGroupSets.Add (new RandomGroupData());
		thisRandomGroupSet = randomGroupSets[0].randomGroups;
		
		terrainGroupSets = new List.<TerrainGroupData>();
		terrainGroupSets.Add (new TerrainGroupData());
		thisTerrainGroupSet = terrainGroupSets[0].terrainGroups;
		terrainSlotIndices = [-1, 0, 8, 1, -1,   0, 7, 12, 6, 1,   9, 12, 12, 12, 11,   2, 5, 12, 4, 3,   -1, 2, 10, 3, -1];
		terrainGroupActiveList = new List.<Int2>();
		terrainLookups = TerrainLookup.InitializeTerrainLookups();
				
		currentGroupSet = selectedGroup = 0;
		SetGroupDisplayCounts();
		groupsChanged = false;
		storedSetNumbers = new int[System.Enum.GetNames (GroupType).Length];
		storedScrollPositions = new Vector2[storedSetNumbers.Length];
	}
	
	static function InitializeUndo () {
		undoList = new List.<UndoList>();
		undoIndex = -1;
	}
	
	static function InitializeTex () {
		lineTex = new Texture2D(1, 1);
		lineTex.SetPixel(0, 0, Color(0, 0, 0, .35));
		lineTex.Apply();
				
		selectionStyle = new GUIStyle();
		selectionStyle.normal.background = EditorGUIUtility.LoadRequired ("SpriteTile/SelectionBox.png") as Texture2D;
		var size = selectionStyle.normal.background.width / 2;
		selectionStyle.border = RectOffset(size, size, size, size);
		
		lightBox = new GUIStyle();
		lightBox.normal.background = EditorGUIUtility.LoadRequired ("SpriteTile/LightBox.png") as Texture2D;
		size = lightBox.normal.background.width / 2;
		lightBox.border = RectOffset(size, size, size, size);
				
		terrainBackgrounds = new Texture2D[13];
		for (var i = 0; i < terrainBackgrounds.Length; i++) {
			terrainBackgrounds[i] = EditorGUIUtility.LoadRequired ("SpriteTile/Terrain" + i + ".png") as Texture2D;
		}
		backgroundRect = Rect(0.0, 0.0, 1.0, 1.0);
		
		terrainCursor = EditorGUIUtility.LoadRequired ("SpriteTile/TerrainCursor.png") as Texture2D;
		fillCursor = EditorGUIUtility.LoadRequired ("SpriteTile/FillCursor.png") as Texture2D;
		colliderCursor = EditorGUIUtility.LoadRequired ("SpriteTile/ColliderCursor.png") as Texture2D;
		randomCursor = EditorGUIUtility.LoadRequired ("SpriteTile/RandomCursor.png") as Texture2D;
		lightCursor = EditorGUIUtility.LoadRequired ("SpriteTile/LightCursor.png") as Texture2D;
		pickCursor = EditorGUIUtility.LoadRequired ("SpriteTile/PickCursor.png") as Texture2D;
		
		orderFillIcons = new Texture2D[4];
		orderFillIcons[0] = EditorGUIUtility.LoadRequired ("SpriteTile/OrderLL.png") as Texture2D;
		orderFillIcons[1] = EditorGUIUtility.LoadRequired ("SpriteTile/OrderLR.png") as Texture2D;
		orderFillIcons[2] = EditorGUIUtility.LoadRequired ("SpriteTile/OrderUL.png") as Texture2D;
		orderFillIcons[3] = EditorGUIUtility.LoadRequired ("SpriteTile/OrderUR.png") as Texture2D;
		
		previewIconBackground = EditorGUIUtility.LoadRequired ("SpriteTile/PreviewIcon.png") as Texture2D;
		goIcon = EditorGUIUtility.LoadRequired ("SpriteTile/GameObjectIcon.png") as Texture2D;
		goMissingIcon = EditorGUIUtility.LoadRequired ("SpriteTile/GameObjectMissingIcon.png") as Texture2D;
		acceptIcon = EditorGUIUtility.LoadRequired ("SpriteTile/AcceptIcon.png") as Texture2D;
		cancelIcon = EditorGUIUtility.LoadRequired ("SpriteTile/CancelIcon.png") as Texture2D;
		lightIcon = EditorGUIUtility.LoadRequired ("SpriteTile/LightIcon.png") as Texture2D;
		lightMissingIcon = EditorGUIUtility.LoadRequired ("SpriteTile/LightMissingIcon.png") as Texture2D;
		
		if (groupSets != null && groupSets.Count > 0 && groupSets[0].spriteGroups != null && groupSets[0].spriteGroups.Count > 0 && groupSets[0].spriteGroups[0].content.image == null) {
			CreateGroupTexture (false, 0, true, groupSets);
		}
	}
	
	enum GroupType {Standard, Random, Terrain}
	enum SetCounts {_2 = 2, _4 = 4, _8 = 8, _16 = 16, _32 = 32}
	
	static var setCount : SetCounts;
	static var oldSetCount : SetCounts;
	
	static var window : EditorWindow;
	static var basePathName : String;
	static var panelWidth : int;
	static var minPanelWidth : int = 457;
	static var coordWidth = 70;
	static var savedSpritePath : String;
	static var richtextLabel : GUIStyle;
	static var cField : GUIStyle;
	static var cLabel : GUIStyle;
	static var radioButton : GUIStyle;
	static var selectionStyle : GUIStyle;
	static var divider : GUIStyle;
	static var buttonStyles : GUIStyle[];
	static var pressedButtonStyles : GUIStyle[];
	static var toolbarBackground : GUIStyle;
	static var lightBox : GUIStyle;
	static var lineTex : Texture2D;
	static var lightIcon : Texture2D;
	static var lightMissingIcon : Texture2D;
	static var functionKeys : KeyCode[];
	static var terrainCursor : Texture2D;
	static var fillCursor : Texture2D;
	static var colliderCursor : Texture2D;
	static var randomCursor : Texture2D;
	static var lightCursor : Texture2D;
	static var pickCursor : Texture2D;
	static var acceptIcon : Texture2D;
	static var cancelIcon : Texture2D;
	static var previewIconBackground : Texture2D;
	static var goIcon : Texture2D;
	static var goMissingIcon : Texture2D;
	static var go : GameObject;
	static var cursorCount = 0;
	static var selectionClick = false;
	
	static var currentSet : int;
	static var tileSets : List.<TilesetData>;
	static var thisTileSet : List.<TileData>;
	static var refNumbersList : List.<int[]>;
	static var textureDisplaySize : int;
	static var oldTextureDisplaySize : int;
	static var textureColumnCount : int;
	static var textureRowCount : int;
	static var selectedTile : int = 0;
	static var oldSelectedTexture : int = -1;
	static var textureDisplayTypes : GUIStyle[];
	static var groupDisplayTypes : GUIStyle[];
	static var showTextureName : boolean;
	static var oldShowTextureName : boolean;
	static var showTextureNumber : boolean;
	static var oldShowTextureNumber : boolean;
	static var texScrollPos : Vector2;
	static var setList : String[];
	
	static var usePhysics = false;
	static var oldUsePhysics = false;
	static var useNonTransparent = false;
	static var oldUseNonTransparent = false;
	static var useLighting = false;
	static var oldUseLighting = false;
	static var useCollider = false;
	static var oldUseCollider = false;
	static var useColliderDefault = false;
	static var orderDefault : int = 0;
	static var oldOrderDefault : int = 0;
	static var rotationDefault : float = 0.0;
	static var oldRotationDefault : float = 0.0;
	static var triggerDefault : int = 0;
	static var oldTriggerDefault : int = 0;
	static var useAutoCollider = true;
	static var oldUseAutoCollider = true;
	static var useCustomCollider = false;
	static var oldUseCustomCollider = false;
	
	static var mapSize : Int2;
	static var newMapSize : Int2;
	static var thisLayer : LayerData;
	static var mapLayers : List.<LayerData>;
	static var mapScrollPos : Vector2;
	static var layerNamesControls : GUIContent[];
	static var layerNames : String[];
	static var currentLayer : int = 0;
	static var oldCurrentLayer : int = 0;
	static var sortingLayerNames : String[];
	static var sortingLayerIndices : List.<int>;
	static var sortingLayerIndex : int = 0;
	static var oldSortingLayerIndex : int = 0;
	static var moveToLayer : int = 0;
	static var pSizeX : int = 64;
	static var oldPSizeX : int = 64;
	static var pSizeY : int = 64;
	static var zPos : float;
	static var oldZPos : float;
	static var tileSizeX : float;
	static var tileSizeY : float;
	static var oldTileSizeX : float;
	static var oldTileSizeY : float;
	static var useSingleTileSize = true;
	static var lock : LayerLock;
	static var oldLock : LayerLock;
	static var addBorder : int;
	static var oldAddBorder : int;
	static var syncLayerPos : boolean;
	static var oldSyncLayerPos : boolean;
	static var showPrecedingLayer : boolean;
	static var oldShowPrecedingLayer : boolean;
	static var showNextLayer : boolean;
	static var oldShowNextLayer : boolean;
	static var showAllLayers : boolean;
	static var oldShowAllLayers : boolean;
	
	static var showGrid = false;
	static var oldShowGrid = false;
	static var showColliders = false;
	static var oldShowColliders = false;
	static var showTriggers = false;
	static var oldShowTriggers = false;
	static var showOrder = false;
	static var oldShowOrder = false;
	static var showExtra = false;
	static var oldShowExtra = false;
	static var showLight = false;
	static var oldShowLight = false;
	static var startPos : Int2;
	static var endPos : Int2;
	static var gridPos : Int2;
	static var oldGridPos : Int2;
	static var savedGridPos : Int2;
	static var doRepaintCount : int = 0;
	static var mapRect = Rect(10, 0, 0, 0);
	static var mapRectHeightAdd : int;
	static var tileDefaultRect : Rect;
	static var tileSelectRect : Rect;
	static var tileWindowRect : Rect;
	static var colliderTint = Color(0, 0, 1, .4);
	static var oldColliderTint : Color;
	static var colliderTintX : Color;
	static var colliderTex : Texture2D;
	static var whiteColor = Color.white;
	static var thisTile : TileData;
	
	static var selectionActive = false;
	static var selectionInit = false;
	static var selectionTint = Color(.9, .9, .9, .5);
	static var previewTint = Color(.8, .8, 1, .5);
	static var selectionStart : Int2;
	static var selectionEnd : Int2;
	static var copyBuffer : MapData;
	static var copyActive = false;
	static var copyBufferSize : Int2;
	static var mapMultiSelectActive = false;
	static var multiSelecting = false;
	static var mapMultiSelectList : List.<Int2>;
	
	static var mouseDown : boolean;
	static var mouseClicked = false;
	static var mousePos : Vector2;
	static var originalMousePos : Vector2;
	static var lastMousePos : Vector2;
	static var previousMousePos : Vector2;
	static var mouseButton : int;
	static var evt : Event;
	static var drawTileBox = false;
	
	static var minZoom = 10;
	static var maxZoom = 200;
	static var scrollWheelSpeed = 1.0;
	static var newScrollPos : Vector2;
	static var zoomed = false;
		
	static var selectedGroup = 0;
	static var currentGroupSet = 0;
	static var groupColumnCount : int;
	static var groupRowCount : int;
	static var groupScrollPos : Vector2;
	static var groupSets : List.<SpriteGroupData>;
	static var thisGroupSet : List.<SpriteGroup>;
	static var groupPixelSize = 102;
	static var groupType = GroupType.Standard;
	static var storedSetNumbers : int[];
	static var storedScrollPositions : Vector2[];
	
	static var bookmarks : Bookmark[];
	static var setBookmarkCount : int = 0;
	static var setBookmark : int;
	
	static var randomGroupSets : List.<RandomGroupData>;
	static var thisRandomGroupSet : List.<List.<TileInfo> >;
	static var drawWithRandomGroup = false;
	static var randomGroupPixelSize = 96;
	
	static var terrainBackgrounds : Texture2D[];
	static var backgroundRect : Rect;
	static var terrainGroupSets : List.<TerrainGroupData>;
	static var thisTerrainGroupSet : List.<TerrainGroup>;
	static var drawWithTerrainGroup = false;
	static var terrainGroupPixelSize = 195;
	static var terrainSlotIndices : int[];
	static var terrainInverseLookups : int[];
	
	static var editTile = false;
	static var editPos : Int2;
	static var editOrder : int;
	static var editRotation : float;
	static var editTrigger : int;
	static var editFlip : Flip;
	static var editExtra = false;
	static var editProperty : PropertyInfo;
	static var isMultiEdit = false;
	static var applyOrder : boolean;
	static var applyRotation : boolean;
	static var applyTrigger : boolean;
	static var applyFlip : boolean;
	static var applyExtra : boolean;
	static var previewDictionary : Dictionary.<int, Texture2D>;
	
	static var orderMin : int;
	static var orderMax : int;
	
	static var doMultiSelect = false;
	static var tileMultiSelectList : List.<int>;
	
	static var dividerColor = Color(.65, .65, .65, .65);
	static var setHorizontalDivider = false;
	static var setVerticalDivider = false;
	static var divPercent : float;
	
	static var currentFileName : String;
	static var savedLevelPath : String;
	static var savedGroupPath : String;
	static var levelChanged = false;
	static var groupsChanged = false;
	
	static var undoList : List.<UndoList>;
	static var totalUndoElementCount : int;
	static var settingUndo : boolean;
	static var undoIndex : int;
	
	static var mouseUp : boolean;
	static var doubleClickedCount : int;
	static var mousePanView = false;
	static var isMovingTile = false;
	static var autoGroupLoad : boolean;
	static var oldAutoGroupLoad : boolean;
	static var autoLevelLoad : boolean;
	static var oldAutoLevelLoad : boolean;
	static var showTileOptions = false;
		
	static var previewContainer : GameObject;
	
	static var dragging = false;
	static var dragComplete = false;
	static var dragTileData : TileData;
	static var terrainGroupActiveList : List.<Int2>;
	static var terrainLookups : TerrainLookup[];
	static var terrainTileActive = false;
	
	static var fillArea = false;
	static var filled = false;
	static var pickMode = false;
	
	static var orderFillLL : int = 0;
	static var orderFillLR : int = 0;
	static var orderFillUL : int = 0;
	static var orderFillUR : int = 0;
	static var orderFillIcons : Texture2D[];
	
	static var lightMode = false;
	static var lightList : List.<LightInfo>;
	static var lightStringArray : GUIContent[];
	static var selectedLight : int = 0;
	static var oldSelectedLight : int = 0;
	static var selectedLightPos : Int2;
	static var lightIsSelected = false;
	static var lightPlacedDown = false;
	static var selectedLightInfo : LightInfo;
	static var lightColor : Color;
	static var lightDragging = false;
	static var lightDragPos = Int2(-1, -1);
	static var lightSize : Int2;
	static var lightStyle : LightStyle;
	static var lightOuterIntensity : float;
	static var lightInnerIntensity : float;
	static var lightCastsShadows = false;
	static var lightAmbient : Color;
	static var oldLightAmbient : Color;
	static var useRadiosity = false;
	static var oldUseRadiosity = false;
	
	static var selectingTilesets = false;
	static var setCollidersForImport = false;
	static var tmxTilesets : List.<TMXTilesetData>;
	static var tmxLines : String[];
	
	static var showSelectionControls = false;
	static var showLightControls = false;
	static var showLayerControls = false;
	static var foldoutStyle : GUIStyle;
	
	static var tileManager : TileManager;
	
	function OnGUI () {
		if (lineTex == null) {
			InitializeTex();
		}
		if (!window) {
			if (Application.isPlaying) {
				return;
			}
			ShowWindow();
		}
		
		evt = Event.current;
		if (evt.clickCount == 2) {	// Work-around for clickCount not ever being 2 inside GUI.Button
			doubleClickedCount = 6;
		}
		if (doubleClickedCount > 0) {
			doubleClickedCount--;
		}
		
		if (evt.type == EventType.MouseDown) {
			selectionClick = (evt.button == 1 || (evt.button == 0 && evt.command));
		}
		
		tileWindowRect = Rect(window.position.width-panelWidth-6, 135 + Mathf.Floor ((tileSets.Count - 1) / 8.0) * 21, panelWidth-22, window.position.height*divPercent);
		mouseUp = false;
		if (evt.type == EventType.MouseDown) {
			mouseClicked = true;
			mouseButton = evt.button;
			if (mousePanView) {
				previousMousePos = evt.mousePosition;
			}
			if (lightMode) {
				originalMousePos = evt.mousePosition;
			}
			// Initialize selection box
			if (selectionClick && mapRect.Contains (evt.mousePosition)) {
				selectionInit = true;
				selectionActive = true;
			}
		}
		// Scroll wheel zoom
		else if (evt.type == EventType.ScrollWheel && evt.alt) {
			ScrollWheelZoom();
			evt.type = EventType.Ignore;
			Repaint();
		}
		else if (evt.type == EventType.MouseDrag) {
			if (lightMode && evt.button == 0 && !lightDragging && Vector2.Distance (originalMousePos, evt.mousePosition) > 3 && lightDragPos != Int2(-1, -1)) {
				StartLightDrag();
			}
		}
		else if (evt.type == EventType.MouseUp) {
			mouseClicked = false;
			mouseUp = true;
			filled = false;
			mousePanView = false;
			multiSelecting = false;
			lightPlacedDown = false;
			if (dragging) {
				dragging = false;
				dragComplete = true;
			}
			if (lightDragging) {
				FinishLightDrag();
			}
			lightDragPos == Int2(-1, -1);
			if (selectionClick) {
				// Do multi-selection when right mouse button is up
				if (drawTileBox) {
					drawTileBox = false;
					if (tileWindowRect.Contains (lastMousePos)) {
						SetMultiSelection (originalMousePos, lastMousePos, !evt.alt);
					}
				}
				// Deselect by right-clicking
				else if (Vector2.Distance (lastMousePos, originalMousePos) < 3) {
					selectionActive = false;
					lightIsSelected = false;
					fillArea = false;
				}
			}
			selectionClick = false;
		}
		
		// If mouse was clicked but then moved out of the window and the button released
		if (mouseClicked && evt.type == EventType.MouseMove) {
			mouseClicked = false;
			mouseUp = true;
			filled = false;
			if (lightMode && lightDragging) {
				FinishLightDrag();
			}
		}
		
		// Vertical divider
		GUI.color = dividerColor;
		if (GUI.RepeatButton (Rect(mapRect.width+14, 10, 6, mapRect.height + mapRectHeightAdd), "", divider)) {
			setVerticalDivider = true;
		}
		GUI.color = whiteColor;
		if (setVerticalDivider) {
			panelWidth = Mathf.Clamp (window.position.width - 12 - evt.mousePosition.x, minPanelWidth, window.position.width - 613);
			SetTextureDisplayCounts();
			SetGroupDisplayCounts();
			if (evt.type == EventType.used) {
				setVerticalDivider = false;
			}
			Repaint();
		}
		else {
			panelWidth = Mathf.Clamp (panelWidth, minPanelWidth, window.position.width - 613);
		}
		
		mapRect.width = window.position.width - panelWidth - 26;
		mapRectHeightAdd = 262 - (!showSelectionControls? 21 : 0) - (!showLightControls? 23 : 0) - (!showLayerControls? 20 : 0);
		mapRect.height = window.position.height - (mapRectHeightAdd + 20) - tipSpace;
		mapRect.y = 208 - (!showSelectionControls? 21 : 0) - (!showLightControls? 23 : 0) - (!showLayerControls? 20 : 0);
		// Pan view
		if (mousePanView) {
			EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.Pan);
			if (mapRect.Contains (evt.mousePosition) && mouseDown && mouseButton == 0) {
				mapScrollPos -= (evt.mousePosition - previousMousePos);
				previousMousePos = evt.mousePosition;
			}
		}
		
		// Level editing area
		BeginArea (Rect(10, 10, mapRect.width, mapRect.height + mapRectHeightAdd));
		LevelControls();
		LayerControls();
		Space (2);
		LightControls();
		Space (2);
		SelectionControls();
		Tools();
		if (!selectingTilesets) {
			DrawMap();
		}
		else {
			DrawTilesetSelection();
		}
		MapInfo();
		MapPreviewControls();
		EndArea();
		
		// Tile/group area
		BeginArea (Rect(window.position.width-panelWidth-6, 10, panelWidth, window.position.height - 20 - tipSpace));
		TileSetControls();
		Space (1);
		TileControls();
		DrawTiles();
		TilePreviewControls();
		
		// Horizontal divider
		Space (2);
		GUI.color = dividerColor;
		if (RepeatButton (" ", divider, Height(6))) {
			setHorizontalDivider = true;
		}
		GUI.color = whiteColor;
		Space (2);
		if (setHorizontalDivider) {
			var rows : int = (tileSets.Count - 1) / 8;
			divPercent = Mathf.Clamp ((evt.mousePosition.y - (144 + rows*20)) / window.position.height, .1, .6);
			if (evt.type == EventType.used) {
				setHorizontalDivider = false;
			}
		}
		GroupControls();
		DrawGroups();
		
		EndArea();
		
		// Tooltip area
		if (showTips) {
			if (MouseInMap()) {
				GUI.tooltip = mouseInMapTip;
			}
			else if (tileDefaultRect.Contains (evt.mousePosition)) {
				if (GUI.tooltip == "") {
					GUI.tooltip = mouseInDefaultsTip;
				}
			}
			else if (tileSelectRect.Contains (evt.mousePosition)) {
				GUI.tooltip = mouseInTilesTip;
			}
			GUI.Box (Rect(10, window.position.height - tipSpace - 5, window.position.width - 20, tipSpace - 4), GUI.tooltip, tipStyle);
		}
		
		if (selectingTilesets) return;
		
		if (DoKeyboardEvents() || setHorizontalDivider || evt.type == EventType.MouseMove || evt.type == EventType.MouseDrag || evt.type == EventType.MouseUp) {
			Repaint();
		}
		else if (doRepaintCount > 0) {
			doRepaintCount--;
			Repaint();
		}
		if (mouseUp && settingUndo) {
			CompleteSettingUndo();
		}
		// Restore bookmarks over two frames, since it's possible the current zoom level will cause the new scrollPos to be clamped incorrectly
		if (setBookmarkCount > 0 && evt.type == EventType.Repaint) {
			setBookmarkCount--;
			pSizeX = oldPSizeX = bookmarks[setBookmark].zoom;
			mapScrollPos = bookmarks[setBookmark].scrollPos;
			Repaint();
		}
		
		// Cursors
		if (!mousePanView) {
			if (dragging) {
				EditorGUIUtility.AddCursorRect (Rect(0, 0, window.position.width, window.position.height), MouseCursor.Link);
				GUI.color.a = .5;
				GUITexBox (Rect(evt.mousePosition.x, evt.mousePosition.y, terrainGroupPixelSize/5, terrainGroupPixelSize/5), dragTileData.texture, dragTileData.textureRect, dragTileData.displayRatio);
			}
			if (drawWithRandomGroup) {
				Cursor.SetCursor (randomCursor, Vector2(8, 2), CursorMode.Auto);
				EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.CustomCursor);				
			}
			if (TerrainGroupActive() && TileInTerrainGroup (TileInfo(currentSet, selectedTile))) {
				Cursor.SetCursor (terrainCursor, Vector2(8, 8), CursorMode.Auto);
				EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.CustomCursor);
			}
			if (showColliders) {
				Cursor.SetCursor (colliderCursor, Vector2(8, 8), CursorMode.Auto);
				EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.CustomCursor);
			}
			else if (fillArea) {
				Cursor.SetCursor (fillCursor, Vector2(13, 13), CursorMode.Auto);
				EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.CustomCursor);
			}
			else if (lightMode) {
				Cursor.SetCursor (lightCursor, Vector2(8, 8), CursorMode.Auto);
				EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.CustomCursor);
			}
			else if (pickMode) {
				Cursor.SetCursor (pickCursor, Vector2(2, 14), CursorMode.Auto);
				EditorGUIUtility.AddCursorRect (mapRect, MouseCursor.CustomCursor);
			}
			if (isMovingTile) {
				EditorGUIUtility.AddCursorRect (tileWindowRect, MouseCursor.MoveArrow);
			}
		}
	}
	
	static function ScrollWheelZoom () {
		var centerPos = ComputeCenterPos (pSizeX);
		
		if (evt.delta.y > 0.0) {	// Out
			pSizeX = Mathf.Clamp (pSizeX - evt.delta.y * scrollWheelSpeed, minZoom, maxZoom);
		}
		else {						// In
			if (mapRect.Contains (evt.mousePosition)) {
				var scaledMousePos = Vector2((mapScrollPos.x + evt.mousePosition.x - mapRect.x)/pSizeX, (mapScrollPos.y + evt.mousePosition.y - mapRect.y)/pSizeY);
			}
			else {
				scaledMousePos = centerPos;
			}
			centerPos = Vector2.Lerp (centerPos, scaledMousePos, Mathf.Lerp (.2, .05, Mathf.InverseLerp (minZoom, maxZoom/2, pSizeX)));
			pSizeX = Mathf.Clamp (Mathf.Ceil (pSizeX - evt.delta.y * scrollWheelSpeed), minZoom, maxZoom);
		}
		
		ComputePSizeY();
		if (oldPSizeX != pSizeX) {
			oldPSizeX = pSizeX;
			SetNewScrollPosToPosition (centerPos);
			zoomed = true;	// Actual position is updated in DrawMap in order to set position accurately after the scrollview is updated
		}
	}
	
	static function ComputeCenterPos (sizeX : int) : Vector2 {
		var thisEndPosX = mapScrollPos.x + (mapRect.width-20);
		var thisEndPosY = mapScrollPos.y + (mapRect.height-20);
		return Vector2(((thisEndPosX - mapScrollPos.x) / 2 + mapScrollPos.x) / sizeX, ((thisEndPosY - mapScrollPos.y) / 2 + mapScrollPos.y) / pSizeY);
	}
	
	static function SetNewScrollPosToPosition (pos : Vector2) {
		newScrollPos.x = pos.x*pSizeX - (mapRect.width - 20)/2;
		newScrollPos.y = pos.y*pSizeY - (mapRect.height - 20)/2;
	}
	
	static function DoKeyboardEvents () : boolean {
		if (editTile && (evt.keyCode != KeyCode.Escape && evt.keyCode != KeyCode.Return)) {
			return;
		}
		if (evt.type == EventType.KeyDown && !dragging) {
			switch (evt.keyCode) {
				// Keyboard scrolling
				case KeyCode.UpArrow:
					if (evt.alt) {
						currentLayer = (currentLayer + 1) % mapLayers.Count;
						ChangeLayer (false);
					}
					else {
						mapScrollPos.y = Mathf.Max (0, mapScrollPos.y-pSizeY);
					}
					break;
				case KeyCode.DownArrow:
					if (evt.alt) {
						currentLayer = Mathf.Repeat (currentLayer - 1, mapLayers.Count);
						ChangeLayer (false);
					}
					else {
						mapScrollPos.y = Mathf.Min (mapSize.y*pSizeY, mapScrollPos.y+pSizeY);
					}
					break;
				case KeyCode.LeftArrow:
					mapScrollPos.x = Mathf.Max (0, mapScrollPos.x-pSizeX);
					break;
				case KeyCode.RightArrow:
					mapScrollPos.x = Mathf.Min (mapSize.x*pSizeX, mapScrollPos.x+pSizeX);
					break;
				case KeyCode.Space:
					mousePanView = true;
					break;
			}
			return true;
		}
		else if (evt.type == EventType.KeyUp) {
			switch (evt.keyCode) {
				case KeyCode.Space:
					mousePanView = false;
					break;
				// Decrease order in layer, or rotate by -5 degrees with alt/-90 degrees with shift
				case KeyCode.Comma:
					if (mapRect.Contains (evt.mousePosition)) {
						if (evt.shift) {
							RotateTile (gridPos, 90);
						}
						else if (evt.alt) {
							RotateTile (gridPos, 5);
						}
						else {
							if (selectionActive || mapMultiSelectActive) {
								CycleOrderBlock (selectionStart, selectionEnd, -1);
							}
							else {
								CycleOrder (gridPos, -1);
							}
						}
						CompleteSettingUndo();
					}
					break;
				// Increase order in layer, or rotate by 5 degrees with alt/90 degrees with shift
				case KeyCode.Period:
					if (mapRect.Contains (evt.mousePosition)) {
						if (evt.shift) {
							RotateTile (gridPos, -90);
						}
						else if (evt.alt) {
							RotateTile (gridPos, -5);
						}
						else {
							if (selectionActive || mapMultiSelectActive) {
								CycleOrderBlock (selectionStart, selectionEnd, 1);
							}
							else {
								CycleOrder (gridPos, 1);
							}
						}
						CompleteSettingUndo();
					}
					break;
				// Cycle through tiles
				case KeyCode.Semicolon:
					SetSelectedTile (Mathf.Repeat (selectedTile - 1, thisTileSet.Count));
					break;
				case KeyCode.Quote:
					SetSelectedTile ((selectedTile + 1) % thisTileSet.Count);
					break;
				// Cycle through tile sets
				case KeyCode.LeftBracket:
					if (evt.alt) {
						ChangeGroupSet (-1);
					}
					else {
						ChangeSet (Mathf.Repeat (currentSet - 1, tileSets.Count));
					}
					break;
				case KeyCode.RightBracket:
					if (evt.alt) {
						ChangeGroupSet (1);
					}
					else {
						ChangeSet ((currentSet + 1) % tileSets.Count);
					}
					break;
				// Finish editing tile properties
				case KeyCode.Return:
				case KeyCode.KeypadEnter:
					if (editTile) {
						FinishTileEdit();
						CompleteSettingUndo();
						levelChanged = true;
					}
					break;
				// Delete tile or tile/collider block
				case KeyCode.Delete:
				case KeyCode.Backspace:
					if (!editTile && MouseInMap()) {
						if (selectionActive || mapMultiSelectActive) {
							if (showColliders) {
								SetColliderBlock (selectionStart, selectionEnd, false);
							}
							else {
								SetTileBlock (selectionStart, selectionEnd, 0, -1);
							}
						}
						else if (copyActive) {
							PasteCopyBuffer (gridPos, !evt.shift, true);
						}
						else {
							if (showColliders) {
								SetCollider (gridPos, false);
							}
							else {
								SetTile (gridPos, 0, -1);
							}
						}
						CompleteSettingUndo();
					}
					break;
				// Clear selection
				case KeyCode.Escape:
					RemoveSelection();
					isMovingTile = false;
					lightIsSelected = false;
					editTile = false;
					pickMode = false;
					break;
				// Select all
				case KeyCode.A:
					if (evt.alt) {
						SelectAll();
					}
					break;
				// Copy group to buffer
				case KeyCode.B:
					GroupToBuffer (selectedGroup);
					break;
				// Copy/show colliders
				case KeyCode.C:
					if (evt.alt) {
						CopySelection (false);
					}
					else {
						showColliders = !showColliders;
					}
					break;
				// Toggle Extra overlay
				case KeyCode.E:
					showExtra = !showExtra;
					break;
				// Fill selection with tile/toggle fill area
				case KeyCode.F:
					if (!evt.shift) {
						if (selectionActive || mapMultiSelectActive) {
							if (showColliders) {
								SetColliderBlock (selectionStart, selectionEnd, true);
							}
							else {
								SetTileBlock (selectionStart, selectionEnd, currentSet, selectedTile);
							}
							CompleteSettingUndo();
						}
					}
					else {
						ToggleFillArea();
					}
					break;
				// Toggle map grid
				case KeyCode.G:
					showGrid = !showGrid;
					break;
				// Toggle Light overlay/toggle light mode
				case KeyCode.L:
					if (!evt.shift) {
						showLight = !showLight;
					}
					else {
						ToggleLightMode();
					}
					break;
				// Open level/toggle collider overlay
				case KeyCode.O:
					if (evt.alt) {
						Open (false, true, "");
					}
					else {
						showOrder = !showOrder;
					}
					break;
				// Set current selected tile to whatever's under the mouse pointer
				case KeyCode.P:
					PickTile (gridPos);
					break;
				// Save As/Save
				case KeyCode.S:
					if (evt.alt) {
						if (evt.shift) {
							Save (false, true);
						}
						else {
							Save (false, false);
						}
					}
					break;
				// Toggle trigger overlay
				case KeyCode.T:
					showTriggers = !showTriggers;
					break;
				// Cut/flip X
				case KeyCode.X:
					if (evt.alt) {
						CopySelection (true);
						CompleteSettingUndo();
					}
					else {
						FlipX (gridPos);
					}
					break;
				// Flip Y
				case KeyCode.Y:
					FlipY (gridPos);
					break;
				// Undo/Redo
				case KeyCode.Z:
					if (evt.alt) {
						if (evt.shift) {
							Redo();
						}
						else {
							Undo();
						}
					}
					break;
			}
			for (var i = 0; i < functionKeys.Length; i++) {
				if (evt.keyCode == functionKeys[i]) {
					if (evt.shift) {
						if (evt.alt) {
							DeleteBookmark (i);
						}
						else {
							SetBookmark (i);
						}
					}
					else {
						MoveToBookmark (i);
					}
					break;
				}
			}
			return true;
		}
		if (mouseDown) {
			// Click to draw selected tile or copy buffer
			if (mouseButton == 0 && !evt.command && (mouseClicked || oldGridPos != gridPos) && !multiSelecting) {
				oldGridPos = gridPos;
				// Double-click multi-select
				if (doubleClickedCount > 0) {
					doubleClickedCount = 0;
					MapMultiSelect (gridPos);
				}
				// Alt-click to edit tile properties
				else if (evt.alt) {
					StartTileEdit();
				}
				else if (!mousePanView) {
					if (copyActive) {
						PasteCopyBuffer (gridPos, !evt.shift, false);
					}
					else if (showColliders) {
						SetCollider (gridPos, true);
					}
					else {
						if (evt.control) {
							if (evt.shift) {
								SetTileLine (savedGridPos, gridPos, 0, -1);
							}
							else {
								SetTile (gridPos, 0, -1);
							}
						}
						else {
							if (evt.shift) {
								SetTileLine (savedGridPos, gridPos, currentSet, selectedTile);
							}
							else {
								SetTile (gridPos, currentSet, selectedTile);
							}
						}
					}
					editTile = false;
				}
			}
			// Middle button to delete tile or selection
			else if (mouseButton == 2 && (mouseClicked || oldGridPos != gridPos)) {
				oldGridPos = gridPos;
				if (copyActive) {
					PasteCopyBuffer (gridPos, !evt.shift, true);
				}
				else if (showColliders) {
					SetCollider (gridPos, false);
				}
				else {
					if (evt.shift) {
						SetTileLine (savedGridPos, gridPos, 0, -1);
					}
					else {
						SetTile (gridPos, 0, -1);
					}
				}
				editTile = false;
			}
			// Right button for selection box
			else if (selectionClick && !drawTileBox) {
				lastMousePos = mousePos;
				var x1 = originalMousePos.x / pSizeX;
				var y1 = originalMousePos.y / pSizeY;
				var x2 = lastMousePos.x / pSizeX;
				var y2 = lastMousePos.y / pSizeY;
				y1 = mapSize.y - y1;
				y2 = mapSize.y - y2;
				// Make x1/y1 always the smallest
				selectionStart.x = (x1 < x2)? x1 : x2;
				selectionEnd.x = (x1 < x2)? x2 : x1;
				selectionStart.y = (y1 < y2)? y1 : y2;
				selectionEnd.y = (y1 < y2)? y2 : y1;
				editTile = false;
				mapMultiSelectActive = false;
			}
			return true;
		}
		else {
			if (evt.type == EventType.Repaint) {
				// Draw selection box in tile window
				var thisMousePos = evt.mousePosition;
				if (mouseClicked && selectionClick && tileWindowRect.Contains (thisMousePos)) {
					if (!drawTileBox) {
						originalMousePos = thisMousePos;
						drawTileBox = true;
						selectionActive = false;
						mapMultiSelectActive = false;
					}
					else {
						lastMousePos = thisMousePos;
						if (Vector2.Distance (thisMousePos, originalMousePos) > 8) {
							var mPosX = (originalMousePos.x < thisMousePos.x)? originalMousePos.x : thisMousePos.x;
							var mPosY = (originalMousePos.y < thisMousePos.y)? originalMousePos.y : thisMousePos.y;
							GUI.Label (Rect(mPosX, mPosY, Mathf.Abs (thisMousePos.x-originalMousePos.x), Mathf.Abs (thisMousePos.y-originalMousePos.y)), "", selectionStyle);
						}
					}
				}
			}
		}
		return false;
	}
	
	static function UpdateShowLight () {
		EditorPrefs.SetBool (basePathName + "ShowLight", showLight);
		if (showLight) {
			for (var i = 0; i < mapLayers.Count; i++) {
				mapLayers[i].mapData.SetupColors();
			}
		}
		else {
			lightMode = false;
		}
	}
	
	static function SetBookmark (index : int) {
		bookmarks[index] = Bookmark(mapScrollPos, pSizeX, true);
		levelChanged = true;
		EditorUtility.DisplayDialog ("Bookmark set", "Bookmark set for F" + (index+1), "OK");
	}
	
	static function MoveToBookmark (index : int) {
		if (!bookmarks[index].defined) return;
		
		setBookmark = index;
		setBookmarkCount = 2;
	}
	
	static function DeleteBookmark (index : int) {
		bookmarks[index].defined = false;
		levelChanged = true;
		EditorUtility.DisplayDialog ("Bookmark deleted", "Bookmark deleted for F" + (index+1), "OK");
	}
	
	static function PickTile (pos : Int2) {
		if (!mapRect.Contains (Vector2(mousePos.x + mapRect.x - mapScrollPos.x, mousePos.y + mapRect.y - mapScrollPos.y))) {
			return;
		}
		
		pickMode = false;
		var thisTileInfo = thisLayer.mapData.GetTile (pos);
		if (thisTileInfo.tile >= 0) {
			SetPickedTile (thisTileInfo);
		}
	}
	
	static function SetPickedTile (tInfo : TileInfo) {
		if (tInfo.set != currentSet) {
			ChangeSet (tInfo.set);
		}
		SetSelectedTile (GetTileNumber (tInfo));
		
		SetScrollPosToSelectedTile();
	}
	
	static function SelectAll () {
		selectionActive = true;
		lastMousePos = Vector2(-10, -10);
		selectionStart = Int2.zero;
		selectionEnd = Int2(mapSize.x - 1, mapSize.y - 1);
	}
	
	static function SetScrollPosToSelectedTile () {
		var vAdd = GetExtraVerticalSpace();
		texScrollPos.y = (selectedTile / textureColumnCount) * (textureDisplaySize + vAdd);
	}
	
	static function GetTileNumber (tInfo : TileInfo) : int {
		return refNumbersList[tInfo.set][tInfo.tile];
	}

	static function LevelControls () {
		BeginHorizontal();
		Label ("<b><size=14>Level</size></b>", richtextLabel);
		Space (11);
		Label (levelChanged? "•" : "", Width(9));
		GUI.color.a = .75;
		Label (currentFileName);
		GUI.color.a = 1.0;
		Space (5);
		if (Button (content[Content.LevelSave])) {
			Save (false, true);
		}
		if (Button (content[Content.LevelOpen])) {
			Open (false, true, "");
		}
		autoLevelLoad = Toggle (autoLevelLoad, content[Content.AutoOpen]);
		if (Button (content[Content.LevelImport])) {
			Import();
		}
		if (Button (content[Content.New])) {
			NewLevel();
		}
		
		FlexibleSpace();
		EndHorizontal();
		
		if (oldAutoLevelLoad != autoLevelLoad) {
			oldAutoLevelLoad = autoLevelLoad;
			EditorPrefs.SetBool (basePathName + "AutoLevelLoad", autoLevelLoad);
		}
	}
	
	static function LayerControls () {
		Box ("", Height(2), ExpandWidth (true));
		
		BeginHorizontal();
		showLayerControls = EditorGUILayout.Foldout (showLayerControls, "<b>Layer:</b>", foldoutStyle);
		GUI.enabled = !selectingTilesets;
		currentLayer = EditorGUILayout.Popup (currentLayer, layerNamesControls, Width(60));
		if (showLayerControls) {
			GUI.enabled = (mapLayers.Count > 1 && currentLayer > 0);
			if (Button (content[Content.LayerUp], EditorStyles.miniButton)) {
				MoveLayer (currentLayer, -1);
			}
			GUI.enabled = (mapLayers.Count > 1 && currentLayer < mapLayers.Count - 1);
			if (Button (content[Content.LayerDown], EditorStyles.miniButton)) {
				MoveLayer (currentLayer, 1);
			}
			GUI.enabled = !selectingTilesets;
			syncLayerPos = Toggle (syncLayerPos, content[Content.SyncScroll]);
			
			Space (6);
			Label (content[Content.Sorting]);
			sortingLayerIndex = EditorGUILayout.Popup (sortingLayerIndex, sortingLayerNames, Width(75));
			
			Space (6);
			GUI.color.a = (mapLayers.Count > 1)? 1.0 : .5;
		}
		Label ("Show:");
		GUI.color.a = CanDrawPrecedingLayer()? 1.0 : .5;
		showPrecedingLayer = Toggle (showPrecedingLayer, content[Content.ShowPreceding]);
		GUI.color.a = CanDrawNextLayer()? 1.0 : .5;
		showNextLayer = Toggle (showNextLayer, content[Content.ShowNext]);
		GUI.color.a = (mapLayers.Count > 1)? 1.0 : .5;
		showAllLayers = Toggle (showAllLayers, content[Content.ShowAll]);
		GUI.color.a = 1.0;
		if (showLayerControls) {
			FlexibleSpace();
			EndHorizontal();
			
			BeginHorizontal();
			Label ("X", cLabel);
			newMapSize.x = EditorGUILayout.IntField (newMapSize.x, cField, Width(40));
			Label ("Y", cLabel);
			newMapSize.y = EditorGUILayout.IntField (newMapSize.y, cField, Width(40));
			if (Button (content[Content.LayerResize])) {
				ResizeMap();
				return;
			}
			Space (5);
			if (useSingleTileSize) {
				Label (content[Content.TileSize], cLabel);
				tileSizeX = EditorGUILayout.FloatField (tileSizeX, cField, Width(35));
				if (Button (content[Content.UseDualSize])) {
					useSingleTileSize = false;
				}
				if (tileSizeX != oldTileSizeX) {
					if (tileSizeX > .01) {
						oldTileSizeX = oldTileSizeY = tileSizeY = tileSizeX;
					}
					else {
						tileSizeX = tileSizeY = oldTileSizeX = oldTileSizeY = .01;
					}
					thisLayer.tileSize.x = thisLayer.tileSize.y = tileSizeX;
					levelChanged = true;
				}
			}
			else {
				Label ("Tile size X", cLabel);
				tileSizeX = EditorGUILayout.FloatField (tileSizeX, cField, Width(35));
				Label ("Y", cLabel);
				tileSizeY = EditorGUILayout.FloatField (tileSizeY, cField, Width(35));
				if (Button (content[Content.UseSingleSize])) {
					useSingleTileSize = true;
					if (tileSizeY != tileSizeX) {
						oldTileSizeY = tileSizeY = tileSizeX;
						levelChanged = true;
					}
				}
				if (tileSizeX != oldTileSizeX) {
					if (tileSizeX > .01) {
						oldTileSizeX = tileSizeX;
					}
					else {
						tileSizeX = oldTileSizeX = .01;
					}
					thisLayer.tileSize.x = tileSizeX;
					ComputePSizeY();
					levelChanged = true;
				}
				if (tileSizeY != oldTileSizeY) {
					if (tileSizeY > .01) {
						oldTileSizeY = tileSizeY;
					}
					else {
						tileSizeY = oldTileSizeY = .01;
					}
					thisLayer.tileSize.y = tileSizeY;
					ComputePSizeY();
					levelChanged = true;
				}
			}
			Space (5);
			Label (content[Content.Zpos], cLabel);
			zPos = EditorGUILayout.FloatField (zPos, cField, Width(35));
			Label (content[Content.LayerLock], cLabel);
			lock = System.Convert.ToInt32 (EditorGUILayout.EnumPopup (lock, Width(55)) );
			Label (content[Content.AddBorder], cLabel);
			addBorder = EditorGUILayout.IntField (addBorder, cField, Width(25));
		}
		else {
			GUI.enabled = true;
		}
		FlexibleSpace();
		EndHorizontal();
		
		if (oldCurrentLayer != currentLayer) {
			gridPos.x = gridPos.y = 0;	// Prevent potential out of bounds error
			ChangeLayer (true);
		}
		if (oldSyncLayerPos != syncLayerPos) {
			oldSyncLayerPos = syncLayerPos;
			EditorPrefs.SetBool (basePathName + "SyncLayerPos", syncLayerPos);
		}
		if (oldShowPrecedingLayer != showPrecedingLayer) {
			oldShowPrecedingLayer = showPrecedingLayer;
			EditorPrefs.SetBool (basePathName + "ShowPrecedingLayer", showPrecedingLayer);
			showNextLayer = oldShowNextLayer = false;
			EditorPrefs.SetBool (basePathName + "ShowNextLayer", showNextLayer);
			showAllLayers = oldShowAllLayers = false;
			EditorPrefs.SetBool (basePathName + "ShowAllLayers", showAllLayers);
		}
		if (oldShowNextLayer != showNextLayer) {
			oldShowNextLayer = showNextLayer;
			EditorPrefs.SetBool (basePathName + "ShowNextLayer", showNextLayer);
			showPrecedingLayer = oldShowPrecedingLayer = false;
			EditorPrefs.SetBool (basePathName + "ShowPrecedingLayer", showPrecedingLayer);
			showAllLayers = oldShowAllLayers = false;
			EditorPrefs.SetBool (basePathName + "ShowAllLayers", showAllLayers);
		}
		if (oldShowAllLayers != showAllLayers) {
			oldShowAllLayers = showAllLayers;
			EditorPrefs.SetBool (basePathName + "ShowAllLayers", showAllLayers);
			showPrecedingLayer = oldShowPrecedingLayer = false;
			EditorPrefs.SetBool (basePathName + "ShowPrecedingLayer", showPrecedingLayer);
			showNextLayer = oldShowNextLayer = false;
			EditorPrefs.SetBool (basePathName + "ShowNextLayer", showNextLayer);
		}
		if (zPos != oldZPos) {
			oldZPos = zPos;
			thisLayer.zPosition = zPos;
			levelChanged = true;
		}
		if (lock != oldLock) {
			oldLock = lock;
			thisLayer.layerLock = lock;
			levelChanged = true;
		}
		if (oldAddBorder != addBorder) {
			addBorder = Mathf.Max (addBorder, 0);
			oldAddBorder = addBorder;
			thisLayer.addBorder = addBorder;
			levelChanged = true;
		}
		if (sortingLayerIndex != oldSortingLayerIndex) {
			oldSortingLayerIndex = sortingLayerIndex;
			sortingLayerIndices[currentLayer] = sortingLayerIndex;
			levelChanged = true;
		}
	}
	
	static function SelectionControls () {
		Box ("", Height(2), ExpandWidth (true));
		
		BeginHorizontal();
		showSelectionControls = EditorGUILayout.Foldout (showSelectionControls, "<b>Selection:</b>", foldoutStyle);
		Space (22);
		
		GUI.enabled = (selectionActive && !editTile);
		if (showSelectionControls) {
			if (Button (content[Content.Cut])) {
				CopySelection (true);
				CompleteSettingUndo();
			}
		}
		if (Button (content[Content.Copy])) {
			CopySelection (false);
		}
		if (showSelectionControls) {
			GUI.enabled = ((selectionActive || mapMultiSelectActive) && !editTile);
			if (Button (content[Content.Fill])) {
				if (showColliders) {
					SetColliderBlock (selectionStart, selectionEnd, true);
				}
				else {
					SetTileBlock (selectionStart, selectionEnd, currentSet, selectedTile);
				}
				CompleteSettingUndo();
			}
			if (Button (content[Content.Erase])) {
				if (showColliders) {
					SetColliderBlock (selectionStart, selectionEnd, false);
				}
				else {
					SetTileBlock (selectionStart, selectionEnd, 0, -1);
				}
				CompleteSettingUndo();
			}
			var ena = GUI.enabled;
		    GUI.enabled = true;
			if (Button ("Delete Empty")) {
			    var x = TextureTester.GetToDelete();
			    Debug.Log(x.Count);
			    for (var j = 0; j < x.Count; j++) {
			        Debug.Log(x[j]);
			        SetTile(x[j], 0, -1);
			    }
			    TextureTester.Reset();
			    CompleteSettingUndo();
			}
		    GUI.enabled = ena;
			Space (10);
			GUI.enabled = (selectionActive || mapMultiSelectActive) && mapLayers.Count > 1 && !editTile;
			if (Button (content[Content.MoveTo])) {
				MoveSelectionToLayer (currentLayer, moveToLayer);
			}
			moveToLayer = EditorGUILayout.Popup (moveToLayer, layerNames, Width(60));
			FlexibleSpace();
			
			GUI.enabled = !editTile;
			if (Button (content[Content.SelectAll])) {
				SelectAll();
			}
		}
		GUI.enabled = (copyBufferSize != Int2.zero && !copyActive && !selectionActive && !mapMultiSelectActive && !editTile);
		if (Button (content[Content.Restore])) {
			copyActive = true;
		}
		GUI.enabled = ((selectionActive || copyActive || mapMultiSelectActive) && !editTile);
		if (Button (content[Content.Deactivate])) {
			RemoveSelection();
		}
		if (!showSelectionControls) {
			FlexibleSpace();
		}
		EndHorizontal();
		
		if (showSelectionControls) {
			BeginHorizontal();
			GUI.enabled = selectionActive;
			Label ("Scene preview:", cLabel);
			if (Button (content[Content.PreviewThisLayer])) {
				ShowEditorPreview (selectionStart, selectionEnd, false);
			}
			if (Button (content[Content.PreviewAllLayers])) {
				ShowEditorPreview (selectionStart, selectionEnd, true);
			}
			GUI.enabled = (previewContainer != null);
			if (Button (content[Content.PreviewDelete])) {
				DeletePreview();
			}
			
			Space (2);
			GUI.enabled = selectionActive && !editTile;
			Label ("Order fill:", cLabel);
			Label (orderFillIcons[0], cLabel);
			var fieldWidth = Mathf.Lerp (29, 39, Mathf.InverseLerp (583, 623, mapRect.width));
			orderFillLL = EditorGUILayout.IntField (orderFillLL, cField, Width(fieldWidth));
			Label (orderFillIcons[1], cLabel);
			orderFillLR = EditorGUILayout.IntField (orderFillLR, cField, Width(fieldWidth));
			Label (orderFillIcons[2], cLabel);
			orderFillUL = EditorGUILayout.IntField (orderFillUL, cField, Width(fieldWidth));
			Label (orderFillIcons[3], cLabel);
			orderFillUR = EditorGUILayout.IntField (orderFillUR, cField, Width(fieldWidth));
			if (Button (content[Content.OrderFill])) {
				OrderFill (currentLayer);
			}
			FlexibleSpace();
			EndHorizontal();
		}
		
		GUI.enabled = true;
	}
		
	static function OrderFill (layerNum : int) {
		orderFillLL = Mathf.Clamp (orderFillLL, -32768, 32767);
		orderFillLR = Mathf.Clamp (orderFillLR, -32768, 32767);
		orderFillUL = Mathf.Clamp (orderFillUL, -32768, 32767);
		orderFillUR = Mathf.Clamp (orderFillUR, -32768, 32767);
		
		var p : Int2;
		for (p.y = selectionStart.y; p.y <= selectionEnd.y; p.y++) {
			for (p.x = selectionStart.x; p.x <= selectionEnd.x; p.x++) {
				var xLerp = Mathf.InverseLerp (selectionStart.x, selectionEnd.x, p.x);
				var yLerp = Mathf.InverseLerp (selectionStart.y, selectionEnd.y, p.y);
				var o1 : int = Mathf.Round (Mathf.Lerp (orderFillLL, orderFillLR, xLerp));
				var o2 : int = Mathf.Round (Mathf.Lerp (orderFillUL, orderFillUR, xLerp));
				var thisOrder : int = Mathf.Round (Mathf.Lerp (o1, o2, yLerp));
				mapLayers[layerNum].mapData.SetOrder (p, thisOrder);
			}
		}
		
		CalcMinMaxOrder (layerNum);
		showOrder = true;
		levelChanged = true;
	}
	
	static function ComputePSizeY () {
		pSizeY = Mathf.Max (1, pSizeX * (tileSizeY / tileSizeX));
	}
	
	static function RemoveSelection () {
		if (selectionActive) {
			selectionActive = false;
		}
		if (copyActive) {
			copyActive = false;
		}
		if (mapMultiSelectActive) {
			mapMultiSelectActive = false;
		}
	}
	
	static function ShowEditorPreview (startPos : Int2, endPos : Int2, doAllLayers : boolean) {
		var selectionSize = (endPos - startPos) + Int2.one;
		if (selectionSize.x * selectionSize.y > 10000) {
			EditorUtility.DisplayDialog ("Preview size too large", "The selection needs to be smaller in order to generate an editor preview.", "Cancel");
			return;
		}
		if (previewContainer != null) {
			DestroyImmediate (previewContainer);
		}
		
		previewContainer = new GameObject("TileEditorPreview");
		if (doAllLayers) {
			var startLayer = 0;
			var endLayer = mapLayers.Count;
		}
		else {
			startLayer = currentLayer;
			endLayer = currentLayer + 1;
		}
		var thisStartPos : Int2;
		var thisEndPos : Int2;
		var p : Int2;
		for (var i = startLayer; i < endLayer; i++) {
			thisStartPos.x = Mathf.Min (startPos.x, mapLayers[i].mapData.mapSize.x - 1);
			thisStartPos.y = Mathf.Min (startPos.y, mapLayers[i].mapData.mapSize.y - 1);
			thisEndPos.x = Mathf.Min (endPos.x, mapLayers[i].mapData.mapSize.x - 1);
			thisEndPos.y = Mathf.Min (endPos.y, mapLayers[i].mapData.mapSize.y - 1);
			var tileSize = mapLayers[i].tileSize;
			var scale = Vector3.one;
			for (p.y = thisStartPos.y; p.y <= thisEndPos.y; p.y++) {
				for (p.x = thisStartPos.x; p.x <= thisEndPos.x; p.x++) {
					var tInfo = mapLayers[i].mapData.GetTile (p);
					if (tInfo.tile >= 0) {
						var sortingOrder = mapLayers[i].mapData.GetOrder (p);
						var rotation = mapLayers[i].mapData.GetRotation (p);
						var flip = mapLayers[i].mapData.GetFlip (p);
						var position = Vector3(p.x * tileSize.x, p.y * tileSize.y, mapLayers[i].zPosition);
						var name = "Layer " + i + " Tile " + p.x + " / " + p.y;
						CreateTileSceneObject (i, tInfo, position, sortingOrder, rotation, flip, scale, previewContainer.transform, name);
					}
				}
			}
		}
	}
	
	static function CreateTileSceneObject (layer : int, tInfo : TileInfo, position : Vector3, sortingOrder : int, zRotation : float, flip : Flip, scale : Vector3, parentTransform : Transform, name : String) {
		var tileObj = new GameObject(name, SpriteRenderer).transform;
		tileObj.position = position;
		tileObj.parent = parentTransform;
		var sprite = tileManager.GetSprite (tInfo.set, GetTileNumber (tInfo));
		tileObj.GetComponent (SpriteRenderer).sprite = sprite;
		tileObj.GetComponent (SpriteRenderer).sortingOrder = sortingOrder;
		tileObj.GetComponent (SpriteRenderer).sortingLayerName = sortingLayerNames[sortingLayerIndices[layer]];
		tileObj.eulerAngles = Vector3(0, 0, zRotation);
		tileObj.transform.localScale = scale;
		if (flip != Flip.None) {
			var rotation = tileObj.transform.eulerAngles;
			var pixelsPerUnit = sprite.pixelsPerUnit;
			var size = sprite.bounds.size;
			var pivot = sprite.pivot;
			var pos = tileObj.transform.localPosition;
			if (flip == Flip.X || flip == Flip.XY) {
				rotation.y = 180;
				var pivotXNormalized = ((1.0 / pixelsPerUnit) * pivot.x) / size.x;
				pos.x += (((1.0 - pivotXNormalized) * 2) - 1.0) * size.x;
			}
			if (flip == Flip.Y || flip == Flip.XY) {
				rotation.x = 180;
				var pivotYNormalized = ((1.0 / pixelsPerUnit) * pivot.y) / size.y;
				pos.y += (((1.0 - pivotYNormalized) * 2) - 1.0) * size.y;
			}
			tileObj.eulerAngles = rotation;
			tileObj.localPosition = pos;
		}
	}
	
	static function DeletePreview () {
		DestroyImmediate (previewContainer);
	}
	
	static function LightControls () {
		Box ("", Height(2), ExpandWidth (true));
		
		BeginHorizontal();
		showLightControls = EditorGUILayout.Foldout (showLightControls, "<b>Light:</b>", foldoutStyle);
		GUI.enabled = (lightMode && lightIsSelected);
		if (lightIsSelected) {
			Box (lightBox.normal.background, Width(14), Height(14));
		}
		else {
			Box ("", Width(14), Height(14));
		}
		GUI.enabled = true;
		selectedLight = EditorGUILayout.Popup (selectedLight, lightStringArray, Width(100));
		if (oldSelectedLight != selectedLight) {
			ChangeLight (false);
		}
		if (showLightControls) {
			GUI.enabled = (lightMode && lightIsSelected);
			if (Button (content[Content.MakeUnique])) {
				ChangeLight (true);
			}
			GUI.enabled = true;
			Space (10);
			Label (content[Content.LayerAmbient]);
			lightAmbient = EditorGUILayout.ColorField (lightAmbient, Width(40));
			Space (10);
			useRadiosity = Toggle (useRadiosity, content[Content.ShadowRadiosity]);
			FlexibleSpace();
			EndHorizontal();
			Space (3);
			
			BeginHorizontal();
			Label (" ", Width(48));
			Label ("Light " + selectedLight + ":");
			lightColor = EditorGUILayout.ColorField (lightColor, Width(40));
			Label (content[Content.LightSize]);
			lightSize.x = EditorGUILayout.IntField (lightSize.x, Width(25));
			Label ("x");
			lightSize.y = EditorGUILayout.IntField (lightSize.y, Width(25));
			lightStyle = System.Convert.ToInt32 (EditorGUILayout.EnumPopup (lightStyle, Width(50)));
			Label (content[Content.LightInner]);
			lightInnerIntensity = EditorGUILayout.FloatField (lightInnerIntensity, Width(30));
			Label (content[Content.LightOuter]);
			lightOuterIntensity = EditorGUILayout.FloatField (lightOuterIntensity, Width(30));
			Space (5);
			lightCastsShadows = Toggle (lightCastsShadows, content[Content.LightShadows]);
			if (Button (content[Content.LightAccept], Width(32))) {
				ApplyLight();
			}
		}
		FlexibleSpace();
		EndHorizontal();
		
		if (oldLightAmbient != lightAmbient) {
			oldLightAmbient = lightAmbient;
			ChangeAmbient();
		}
		if (oldUseRadiosity != useRadiosity) {
			oldUseRadiosity = useRadiosity;
			for (var i = 0; i < mapLayers.Count; i++) {
				mapLayers[i].mapData.useRadiosity = useRadiosity;
				mapLayers[i].mapData.ResetLighting (lightList);
			}
			levelChanged = true;
		}
	}
	
	static function ChangeLight (makeUnique : boolean) { 
		if (selectedLight == lightStringArray.Length-1 || makeUnique) {	// Add light
			lightInnerIntensity = Mathf.Max (0.0, lightInnerIntensity);
			lightOuterIntensity = Mathf.Max (0.0, lightOuterIntensity);
			lightList.Add (LightInfo(lightStyle, lightSize, lightColor, lightInnerIntensity, lightOuterIntensity, lightCastsShadows? ShadowType.Fast : ShadowType.None));
			SetLightStringArray();
			selectedLight = oldSelectedLight = lightList.Count-1;
			if (makeUnique) {
				thisLayer.mapData.SetLight (selectedLightPos, false, selectedLight, 1.0, lightList, false);
				thisLayer.mapData.SetLight (selectedLightPos, true, selectedLight, 1.0, lightList, false);
			}
			else {
				lightIsSelected = false;
			}
			levelChanged = true;
			return;
		}
		if (selectedLight == lightStringArray.Length-2) {	// Delete light
			var levelHasThisLight = false;
			var lightCount = 0;
			for (var i = 0; i < mapLayers.Count; i++) {
				for (var item in mapLayers[i].mapData.lightRefs) {
					if (item.Value == oldSelectedLight) {
						levelHasThisLight = true;
						lightCount++;
					}
				}
			}
			if (levelHasThisLight) {
				var isSure = !EditorUtility.DisplayDialog ("Delete light?", "Do you really want to delete light number " + oldSelectedLight + "?\n" + lightCount + (lightCount == 1? " light" : " lights") + " in the level with this light number will be removed", "Cancel", "Delete light");
				if (!isSure) {
					selectedLight = oldSelectedLight;
					return;
				}
			}
			for (i = 0; i < mapLayers.Count; i++) {
				mapLayers[i].mapData.RemoveLightNumber (oldSelectedLight);
				mapLayers[i].mapData.ResetLighting (lightList);
			}
			lightList.RemoveAt (oldSelectedLight);
			levelChanged = true;
			if (lightList.Count == 0) {
				SetDefaultLight();
				return;
			}
			selectedLight = oldSelectedLight = Mathf.Clamp (oldSelectedLight, 0, lightList.Count-1);
			SetLightStringArray();
			SetLightInfo (selectedLight);
			lightIsSelected = false;
			return;
		}
		if (selectedLight == lightStringArray.Length-3) {	// Divider
			selectedLight = oldSelectedLight;
			return;
		}
		
		// Change light number
		oldSelectedLight = selectedLight;
		if (lightMode && lightIsSelected && thisLayer.mapData.HasLight (selectedLightPos)) {
			var lightNumber = Mathf.Clamp (thisLayer.mapData.GetLightNumber (selectedLightPos), 0, lightList.Count-1);
			SetUndo (selectedLightPos, Int2.zero, false, currentLayer);
			thisLayer.mapData.SetLight (selectedLightPos, false, lightNumber, 1.0, lightList, false);
			thisLayer.mapData.SetLight (selectedLightPos, true, selectedLight, 1.0, lightList, false);
			levelChanged = true;
		}
		SetLightInfo (selectedLight);
	}
	
	static function ChangeAmbient () {
		thisLayer.mapData.ambientColor = lightAmbient;
		thisLayer.mapData.ResetLighting (lightList);
		levelChanged = true;
	}
	
	static function ApplyLight () {
		var thisLight = lightList[selectedLight];
		lightInnerIntensity = Mathf.Max (0.0, lightInnerIntensity);
		lightOuterIntensity = Mathf.Max (0.0, lightOuterIntensity);
		if (thisLight.size == lightSize && thisLight.style == lightStyle && thisLight.innerIntensity == lightInnerIntensity && thisLight.outerIntensity == lightOuterIntensity && thisLight.color == lightColor && thisLight.shadowType == (lightCastsShadows? ShadowType.Fast : ShadowType.None)) {
			return;
		}
		thisLight.style = lightStyle;
		thisLight.color = lightColor;
		thisLight.innerIntensity = lightInnerIntensity;
		thisLight.outerIntensity = lightOuterIntensity;
		thisLight.shadowType = (lightCastsShadows? ShadowType.Fast : ShadowType.None);
		thisLight.SetSize (Int2(Mathf.Clamp (lightSize.x, 1, thisLayer.mapData.mapSize.x), Mathf.Clamp (lightSize.y, 1, thisLayer.mapData.mapSize.y)));
		for (var i = 0; i < mapLayers.Count; i++) {
			mapLayers[i].mapData.ResetLighting (lightList);			
		}
		SetLightStringArray();
		levelChanged = true;
	}
	
	static function SetDefaultLight () {
		lightList = new List.<LightInfo>();
		lightList.Add (new LightInfo(LightStyle.Radial, Int2(7, 7), Color.white, 1.0, 0.1, ShadowType.None));
		SetLightStringArray();
		SetLightInfo (0);
		lightIsSelected = false;
	}
	
	static function SetLightStringArray () {
		lightStringArray = new GUIContent[lightList.Count + 3];
		for (var i = 0; i < lightList.Count; i++) {
			lightStringArray[i] = GUIContent(i + ": " + lightList[i].size.x + "x" + lightList[i].size.y + ((lightList[i].style == LightStyle.Radial)? " ◯" : " ▢"), lightPopupTip);
		}
		lightStringArray[i++] = GUIContent("____________");
		lightStringArray[i++] = GUIContent("Delete light");
		lightStringArray[i++] = GUIContent("New light");
	}
	
	static function SetLightInfo (index : int) {
		selectedLight = oldSelectedLight = index;
		lightSize = lightList[index].size;
		lightOuterIntensity = lightList[index].outerIntensity;
		lightInnerIntensity = lightList[index].innerIntensity;
		lightStyle = lightList[index].style;
		lightColor = lightList[index].color;
		lightCastsShadows = (lightList[index].shadowType == ShadowType.Fast);
	}
	
	static function Tools () {
		BeginHorizontal ("box");
		Label ("<b>Tools:</b>", richtextLabel);
		
		if (Button (content[Content.FillArea], pressedButtonStyles[fillArea? 1 : 0])) {
			ToggleFillArea();
		}		
		Space (15);
		
		if (Button (content[Content.LightMode], pressedButtonStyles[lightMode? 1 : 0])) {
			ToggleLightMode();
		}
		GUI.enabled = (lightMode && lightIsSelected);
		if (Button (content[Content.DeselectLight])) {
			lightIsSelected = false;
		}
		GUI.enabled = true;
		Space (15);
		
		if (Button (content[Content.PickTile], pressedButtonStyles[pickMode? 1 : 0])) {
			TogglePickMode();
		}
		Space (15);
		
		showTips = Toggle (showTips, content[Content.ShowTips]);
		FlexibleSpace();

		GUI.enabled = (undoIndex > -1);
		if (Button (content[Content.Undo])) {
			Undo();
		}
		GUI.enabled = (undoIndex < undoList.Count - 1);
		if (Button ("Redo")) {
			Redo();
		}
		GUI.enabled = true;
		EndHorizontal();
		
		if (oldShowTips != showTips) {
			oldShowTips = showTips;
			EditorPrefs.SetBool (basePathName + "ShowTips", showTips);
			SetTipSpace();
		}
	}
	
	static function TogglePickMode () {
		pickMode = !pickMode;
		if (pickMode) {
			lightMode = false;
			fillArea = false;
		}
	}
	
	static function ToggleFillArea () {
		fillArea = !fillArea;
		if (fillArea) {
			lightMode = false;
			pickMode = false;
		}
	}
	
	static function ToggleLightMode () {
		lightMode = !lightMode;
		if (lightMode) {
			fillArea = false;
			showLight = oldShowLight = true;
			showColliders = false;
			UpdateShowLight();
			lightIsSelected = false;
		}
	}
	
	static function SetTipSpace () {
		if (showTips) {
			tipSpace = 24;
			SetToolTips();
		}
		else {
			tipSpace = 0;
			RemoveToolTips();
		}
	}
	
	static function DrawMap () {
		GUI.SetNextControlName ("MapView");
		mapScrollPos = BeginScrollView (mapScrollPos, "box");
		if (zoomed) {
			zoomed = false;
			mapScrollPos = newScrollPos;
		}
		
		startPos.x = Mathf.Max (parseInt(mapScrollPos.x)/pSizeX - 1, 0);
		endPos.x = Mathf.Min (startPos.x + mapRect.width/pSizeX + 3, mapSize.x);
		endPos.y = Mathf.Min (mapSize.y - parseInt(mapScrollPos.y)/pSizeY + 1, mapSize.y);
		startPos.y = Mathf.Max (endPos.y - mapRect.height/pSizeY - 2, 0);
		
		GUI.Box (Rect(0, 0, mapSize.x*pSizeX, mapSize.y*pSizeY), "");
		
		// Tiles
		if (showPrecedingLayer && CanDrawPrecedingLayer()) {
			DrawLayer (mapLayers[currentLayer-1]);
		}
		else if (showAllLayers) {
			DrawLayers (0, currentLayer-1);
		}
		var p : Int2;
		for (var i = orderMin; i <= orderMax; i++) {
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				for (p.x = startPos.x; p.x < endPos.x; p.x++) {
					if (showLight) {
						GUI.color = thisLayer.mapData.GetColor (p);
					}
					var tInfo = thisLayer.mapData.GetTile (p);
					if (tInfo.tile >= 0) {
						if (thisLayer.mapData.GetOrder (p) == i) {
							thisTile = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
							DrawTileTex (p.x, mapSize.y - p.y - 1, thisTile, thisLayer.mapData, p, thisTile.texture);
						}
					}
					else {	// Empty
						if (tInfo.tile == MapData.missingTile) {
							GUI.Label (Rect(p.x*pSizeX, (mapSize.y - p.y - 1)*pSizeY, pSizeX, pSizeY), "X", "box");
						}
						if (showLight && (mapLayers.Count == 1 || currentLayer == 0 || (!showPrecedingLayer && !showNextLayer && !showAllLayers))) {
							GUI.DrawTexture (Rect(p.x*pSizeX, (mapSize.y - p.y - 1)*pSizeY, pSizeX, pSizeY), colliderTex);
						}
					}
				}
			}
		}
		GUI.color = whiteColor;
		if (showNextLayer && CanDrawNextLayer()) {
			DrawLayer (mapLayers[currentLayer+1]);
		}
		else if (showAllLayers) {
			DrawLayers (currentLayer+1, mapLayers.Count-1);
		}
		
		// Grid
		if (showGrid) {
			var rect = Rect(startPos.x*pSizeX, 0, (endPos.x - startPos.x)*pSizeX, 1);
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				rect.y = (mapSize.y - p.y)*pSizeY - 1;
				GUI.DrawTexture (rect, lineTex);
			}
			rect = Rect(0, (mapSize.y - endPos.y)*pSizeY, 1, (endPos.y-startPos.y)*pSizeY);
			for (p.x = startPos.x; p.x < endPos.x; p.x++) {
				rect.x = p.x*pSizeX - 1;
				GUI.DrawTexture (rect, lineTex);
			}
		}
		
		// Multi-select overlay
		if (mapMultiSelectActive) {
			for (i = 0; i < mapMultiSelectList.Count; i++) {
				if (mapMultiSelectList[i].x >= startPos.x && mapMultiSelectList[i].x < endPos.x && mapMultiSelectList[i].y >= startPos.y && mapMultiSelectList[i].y < endPos.y) {
					GUI.Label (Rect(mapMultiSelectList[i].x*pSizeX, (mapSize.y - mapMultiSelectList[i].y - 1)*pSizeY, pSizeX, pSizeY), "", selectionStyle);
				}
			}
		}
		
		// Collider overlay
		if (showColliders) {
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				for (p.x = startPos.x; p.x < endPos.x; p.x++) {
					if (thisLayer.mapData.GetCollider (p)) {
						tInfo = thisLayer.mapData.GetTile (p);
						if (tInfo.tile >= 0) {
							thisTile = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
							GUI.color = thisTile.usePhysics? colliderTint : colliderTintX;
							DrawTileTex (p.x, mapSize.y - p.y - 1, thisTile, thisLayer.mapData, p, colliderTex);
						}
						else {
							GUI.color = colliderTint;
							DrawColliderTex (p.x, mapSize.y - p.y - 1, thisLayer.mapData, p, colliderTex);
						}
					}
				}
			}
			GUI.color = whiteColor;
		}
		// Trigger overlay
		else if (showTriggers) {
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				for (p.x = startPos.x; p.x < endPos.x; p.x++) {
					var thisTrigger = thisLayer.mapData.GetTrigger (p);
					if (thisTrigger > 0) {
						GUI.color = colliderTint;
						GUI.DrawTexture (Rect(p.x*pSizeX, (mapSize.y - p.y - 1)*pSizeY, pSizeX, pSizeY), colliderTex);
						GUI.color = whiteColor;
						GUI.Label (Rect(p.x*pSizeX, (mapSize.y - p.y - 1)*pSizeY, 50, 30), thisTrigger.ToString(), EditorStyles.whiteLabel);
					}
				}
			}
			GUI.color = whiteColor;
		}
		// Order overlay
		else if (showOrder) {
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				for (p.x = startPos.x; p.x < endPos.x; p.x++) {
					var thisOrder = thisLayer.mapData.GetOrder (p);
					if (thisOrder != 0) {
						GUI.Label (Rect(p.x*pSizeX, (mapSize.y - p.y - 1)*pSizeY, 50, 30), thisOrder.ToString(), EditorStyles.whiteLabel);
					}
				}
			}
		}
		
		// Extra overlay
		if (showExtra) {
			var zoomFactor = Mathf.InverseLerp (10, 60, pSizeX);
			var iconSize = Mathf.Lerp (20, 64, zoomFactor);
			var textOffset = Mathf.Lerp (0, 55, zoomFactor);
			var bgSize = Mathf.Lerp (0, 5, zoomFactor);
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				for (p.x = startPos.x; p.x < endPos.x; p.x++) {
					if (thisLayer.mapData.HasProperty (p)) {
						var guid = thisLayer.mapData.GetPropertyID (p);
						if (guid != -1) {
							go = thisLayer.mapData.GetProperty.<GameObject> (p);
							if (go != null) {
								var yPos = (mapSize.y - p.y - 1)*pSizeY - iconSize/2 + pSizeY/2;
								GUI.Label (Rect(p.x*pSizeX - iconSize/2 + pSizeX/2, yPos, iconSize + bgSize, iconSize + bgSize), previewIconBackground);
								GUI.Label (Rect(p.x*pSizeX - iconSize/2 + pSizeX/2, yPos, iconSize, iconSize), PreviewTexture (guid, go));
								GUI.color = Color.black;
								GUI.Label (Rect(p.x*pSizeX + 1, yPos + textOffset + 1, 64, 20), go.name);
								GUI.color = whiteColor;
								GUI.Label (Rect(p.x*pSizeX, yPos + textOffset, 64, 20), go.name, EditorStyles.whiteLabel);
							}
							else {
								if (thisLayer.mapData.GetPropertyID (p) != null) {
									GUI.Label (Rect(p.x*pSizeX - iconSize/2 + pSizeX/2, (mapSize.y - p.y - 1)*pSizeY - iconSize/2 + pSizeY/2, iconSize, iconSize), goMissingIcon);
								}
							}
						}
						if (guid == -1 && (thisLayer.mapData.GetProperty.<float> (p) != 0.0 || !System.String.IsNullOrEmpty (thisLayer.mapData.GetProperty.<String> (p)))) {
							GUI.color = colliderTint;
							GUI.DrawTexture (Rect(p.x*pSizeX, (mapSize.y - p.y - 1)*pSizeY, pSizeX, pSizeY), colliderTex);
							GUI.color = whiteColor;
						}
					}
				}
			}
		}
		
		// Light overlay
		if (showLight) {
			iconSize = Mathf.Lerp (20, 44, Mathf.InverseLerp (10, 40, pSizeX));
			for (p.y = startPos.y; p.y < endPos.y; p.y++) {
				for (p.x = startPos.x; p.x < endPos.x; p.x++) {
					if (thisLayer.mapData.HasLight (p) || (lightDragging && p == selectedLightPos)) {
						GUI.Label (Rect(p.x*pSizeX - iconSize/2 + pSizeX/2, (mapSize.y - p.y - 1)*pSizeY - iconSize/2 + pSizeY/2, iconSize, iconSize), (thisLayer.mapData.GetLightNumber (p) < lightList.Count)? lightIcon : lightMissingIcon);
					}
				}
			}
			// Light outline box
			if (lightMode && lightIsSelected) {
				var lightp1 : Int2 = thisLayer.mapData.GetStartPosClamped (selectedLightPos, lightList[selectedLight]);
				var lightp2 : Int2 = thisLayer.mapData.GetEndPosClamped (selectedLightPos, lightList[selectedLight]);
				GUI.Label (Rect(lightp1.x*pSizeX, (mapSize.y - lightp2.y - 1)*pSizeY, (lightp2.x - lightp1.x + 1)*pSizeX, (lightp2.y - lightp1.y + 1)*pSizeY), "", lightBox);
			}
		}
		
		// Mouse stuff for level section done inside this scrollview to get the correct relative coords
		mousePos = evt.mousePosition;
		
		// Selection box
		if (selectionInit) {
			selectionInit = copyActive = false;
			originalMousePos = lastMousePos = mousePos;
		}
		if (selectionActive) {
			DrawSelectionBox();
		}
		
		if (MouseInMap()) {
			// Show box around current tile, or contents of copy buffer
			if (evt.type == EventType.Repaint) {
				ClampGridPos();
				if (!editTile) {
					GUI.FocusControl ("MapView");
				}
				if (lightDragging) {
					selectedLightPos = gridPos;
				}
			}
			if (copyActive) {
				GUI.color = selectionTint;
				DrawCopyBuffer();
			}
			else {
				if (selectedTile >= thisTileSet.Count) {
					selectedTile = 0;
				}
				// Show grid cell being hovered over, with ghosted version of selected tile if it exists
				GUI.color = previewTint;
				GUI.Box (Rect(gridPos.x*pSizeX, (mapSize.y - gridPos.y - 1)*pSizeY, pSizeX, pSizeY), "");
				if (thisTileSet.Count > 0 && !showColliders && !lightMode && !fillArea) {
					if (CanDrawWithRandom()) {
						tInfo = thisRandomGroupSet[selectedGroup][0];
						tInfo.tile = GetTileNumber (tInfo);
					}
					else {
						tInfo.set = currentSet;
						tInfo.tile = selectedTile;
					}
					var tX = (tileSets[tInfo.set].tileData[tInfo.tile].size.x / tileSizeX);
					var tY = (tileSets[tInfo.set].tileData[tInfo.tile].size.y / tileSizeY);
					var xShift = (tX*pSizeX - pSizeX) / 2 - (tileSets[tInfo.set].tileData[tInfo.tile].pivot.x / tileSizeX * pSizeX);
					var yShift = (tY*pSizeY - pSizeY) / 2 + (tileSets[tInfo.set].tileData[tInfo.tile].pivot.y / tileSizeY * pSizeY);
					GUI.color = selectionTint;
					DrawTex (gridPos.x, (mapSize.y - gridPos.y - 1), tileSets[tInfo.set].tileData[tInfo.tile].rotation, Flip.None, tX, tY, xShift, yShift, tileSets[tInfo.set].tileData[tInfo.tile].textureRect, tileSets[tInfo.set].tileData[tInfo.tile].texture);
				}
				if (lightMode) {
					GUI.color = Color (1, 1, 1, .3);
					lightp1 = thisLayer.mapData.GetStartPosClamped (gridPos, lightList[selectedLight]);
					lightp2 = thisLayer.mapData.GetEndPosClamped (gridPos, lightList[selectedLight]);
					GUI.Label (Rect(lightp1.x*pSizeX, (mapSize.y - lightp2.y - 1)*pSizeY, (lightp2.x - lightp1.x + 1)*pSizeX, (lightp2.y - lightp1.y + 1)*pSizeY), "", lightBox);
				}
			}
			GUI.color = whiteColor;
			if (mouseDown) {
				doRepaintCount = 1;
			}
		}
		
		mouseDown = RepeatButton ("", "label", Width(mapSize.x*pSizeX - 8), Height(mapSize.y*pSizeY - 6));
		
		EndScrollView();
	}
	
	static function CanDrawPrecedingLayer () : boolean {
		if (currentLayer-1 >= mapLayers.Count) {
			return false;
		}
		return currentLayer > 0 && thisLayer.mapData.mapSize == mapLayers[currentLayer-1].mapData.mapSize;
	}
	
	static function CanDrawNextLayer () : boolean {
		if (currentLayer+1 >= mapLayers.Count) {
			return false;
		}
		return currentLayer < mapLayers.Count-1 && thisLayer.mapData.mapSize == mapLayers[currentLayer+1].mapData.mapSize;
	}
	
	static function MouseInMap () {
		return (mousePos.x > 0 && mousePos.x < mapSize.x*pSizeX && mousePos.y > 0 && mousePos.y < mapSize.y*pSizeY) &&
				mapRect.Contains (Vector2(mousePos.x + mapRect.x - mapScrollPos.x, mousePos.y + mapRect.y - mapScrollPos.y));
	}
	
	static function DrawLayer (layer : LayerData) {
		GUI.color.a = .5;
		var p : Int2;
		for (p.y = startPos.y; p.y < endPos.y; p.y++) {
			for (p.x = startPos.x; p.x < endPos.x; p.x++) {
				if (showLight) {
					GUI.color = layer.mapData.GetColor (p);
					GUI.color.a = .5;
				}
				var tInfo = layer.mapData.GetTile (p);
				if (tInfo.tile >= 0) {
					thisTile = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
					DrawTileTex (p.x, layer.mapData.mapSize.y - p.y - 1, thisTile, layer.mapData, p, thisTile.texture);
				}
			}
		}
		GUI.color = whiteColor;
	}
	
	static function DrawLayers (startLayer : int, endLayer : int) {
		if (endLayer < 0 || startLayer >= mapLayers.Count) return;
		
		for (var i = startLayer; i <= endLayer; i++) {
			if (mapLayers[i].mapData.mapSize == thisLayer.mapData.mapSize) {
				DrawLayer (mapLayers[i]);
			}
		}
	}
	
	static function ClampGridPos () {
		gridPos.x = Mathf.Clamp (mousePos.x / pSizeX, 0, mapSize.x-1);
		gridPos.y = Mathf.Clamp (mousePos.y / pSizeY, 0, mapSize.y-1);
		gridPos.y = mapSize.y - gridPos.y - 1;
	}
	
	static function DrawTileTex (x : int, y : int, thisTile : TileData, mData : MapData, mapPos : Int2, tex : Texture) {
		if (evt.type != EventType.Repaint) {
			return;
		}
		
		var tX = (thisTile.size.x / tileSizeX);
		var tY = (thisTile.size.y / tileSizeY);
		var xShift = (tX*pSizeX - pSizeX) / 2 - (thisTile.pivot.x / tileSizeX * pSizeX);
		var yShift = (tY*pSizeY - pSizeY) / 2 + (thisTile.pivot.y / tileSizeY * pSizeY);
		var thisRotation = mData.GetRotation (mapPos);
		var thisFlip = mData.GetFlip (mapPos);
		DrawTex (x, y, thisRotation, thisFlip, tX, tY, xShift, yShift, thisTile.textureRect, tex);
		GetEmptyTiles(mapPos,Rect(Rect(x*pSizeX - xShift, y*pSizeY - yShift, tX*pSizeX, tY*pSizeY)),thisTile.textureRect,tex);

	}
	
	static function DrawColliderTex (x : int, y : int, mData : MapData, mapPos : Int2, tex : Texture) {
		if (evt.type != EventType.Repaint) {
			return;
		}
		
		var thisRotation = mData.GetRotation (mapPos);
		var thisFlip = mData.GetFlip (mapPos);
		DrawTex (x, y, thisRotation, thisFlip, 1.0, 1.0, 0.0, 0.0, Rect(0, 0, 1, 1), tex);
}
	
    static function GetEmptyTiles(mapPos: Int2,rect : Rect, texRect : Rect, tex : Texture) {
        var g = GUI.color;

        if (tex != null) {
            if (!TextureTester.IsCorrectTexture2DRect(rect, tex as Texture2D,texRect,mapPos))
            {
                GUI.color = new Color(1, 0, 0, 0.4);

                GUI.DrawTextureWithTexCoords(rect, EditorGUIUtility.whiteTexture, texRect);
                GUI.color = g;
            }
        }
    }

    static function GetAllNonEmptyTiles(rect : Rect, texRect : Rect) {
        var g = GUI.color;

            if (TextureTester.NonEmptyHighlightIsEnabled)
            {
		        GUI.color = new Color(1,0,1,0.2);

                GUI.DrawTextureWithTexCoords (rect, EditorGUIUtility.whiteTexture, texRect);
                GUI.color = g;
            }
    }

	static function DrawTex (x : int, y : int, thisRotation : float, thisFlip : Flip, tX : float, tY : float, xShift : float, yShift : float, texRect : Rect, tex : Texture) {
		if (thisFlip != Flip.None) {
			if (thisFlip == Flip.X || thisFlip == Flip.XY) {
				texRect.x = texRect.x + texRect.width;
				texRect.width = -texRect.width;
			}
			if (thisFlip == Flip.Y || thisFlip == Flip.XY) {
				texRect.y = texRect.y + texRect.height;
				texRect.height = -texRect.height;
			}			
		}
		
		var rect = Rect(x*pSizeX - xShift, y*pSizeY - yShift, tX*pSizeX, tY*pSizeY);
		if (tex != null) {
		    if (thisRotation == 0) {

		        GUI.DrawTextureWithTexCoords (rect, tex, texRect);
		        GetAllNonEmptyTiles(rect, texRect);


		    }
			else {
				var storedMatrix = GUI.matrix;
				GUIUtility.RotateAroundPivot (-thisRotation, Vector2(x*pSizeX + pSizeX/2, y*pSizeY + pSizeY/2));
                GUI.DrawTextureWithTexCoords (rect, tex, texRect);
				GUI.matrix = storedMatrix;
			}
		}
	}
	
	static function MapPreviewControls () {	
		BeginHorizontal();
		showGrid = Toggle (showGrid, content[Content.GridToggle]);
		showLight = Toggle (showLight, content[Content.LightToggle]);		
		showExtra = Toggle (showExtra, content[Content.GOToggle]);		
		showOrder = Toggle (showOrder, content[Content.OrderToggle]);
		showColliders = Toggle (showColliders, content[Content.ColliderToggle]);
		showTriggers = Toggle (showTriggers, content[Content.TriggerToggle]);
		Label (content[Content.Tint]);
		colliderTint = EditorGUILayout.ColorField (colliderTint, Width(40));
		Label (content[Content.PreviewSize]);
		pSizeX = EditorGUILayout.IntSlider (pSizeX, minZoom, maxZoom);
		if (oldPSizeX != pSizeX) {
			var centerPos = ComputeCenterPos (oldPSizeX);
			oldPSizeX = pSizeX;
			ComputePSizeY();
			SetNewScrollPosToPosition (centerPos);
			zoomed = true;
		}
		FlexibleSpace();
		EndHorizontal();
		
		if (oldColliderTint != colliderTint) {
			oldColliderTint = colliderTint;
			colliderTintX = colliderTint;
			colliderTintX.a = colliderTint.a / 2.25;
		}
		if (oldShowColliders != showColliders) {
			oldShowColliders = showColliders;
			if (showColliders) {
				showTriggers = false;
				showOrder = false;
				lightMode = false;
				showLight = false;
			}
		}
		if (oldShowTriggers != showTriggers) {
			oldShowTriggers = showTriggers;
			if (showTriggers) {
				showColliders = false;
				showOrder = false;
			}
		}
		if (oldShowOrder != showOrder) {
			oldShowOrder = showOrder;
			if (showOrder) {
				showColliders = false;
				showTriggers = false;
			}
			
		}
		if (oldShowGrid != showGrid) {
			oldShowGrid = showGrid;
			EditorPrefs.SetBool (basePathName + "ShowGrid", showGrid);
		}
		if (oldShowExtra != showExtra) {
			oldShowExtra = showExtra;
			EditorPrefs.SetBool (basePathName + "ShowExtra", showExtra);
		}
		if (oldShowLight != showLight) {
			oldShowLight = showLight;
			if (showLight) {
				showColliders = false;
				thisLayer.mapData.ResetLighting (lightList);
			}
			UpdateShowLight();
		}
	}
	
	static function StartTileEdit () {
		if (tileSets.Count == 1 && tileSets[0].tileData.Count == 0) return;
		
		editTile = true;
		EditorGUI.FocusTextInControl ("EditTrigger");
		isMultiEdit = (selectionActive || mapMultiSelectActive);
		if (selectionActive) {
			if (selectionEnd - selectionStart + Int2.one == Int2.one) {
				editPos = selectionStart;
				isMultiEdit = false;
			}
		}
		else if (mapMultiSelectActive) {
			if (mapMultiSelectList.Count == 1) {
				editPos = mapMultiSelectList[0];
				isMultiEdit = false;
			}
		}
		else {
			editPos = gridPos;
		}
		applyTrigger = applyOrder = applyRotation = applyFlip = applyExtra = true;
		
		// With multiple tiles selected, see if they all have the same properties
		if (isMultiEdit) {
			if (selectionActive) {
				var checkList = new List.<Int2>();
				for (var y = selectionStart.y; y <= selectionEnd.y; y++) {
					for (var x = selectionStart.x; x <= selectionEnd.x; x++) {
						checkList.Add (Int2(x, y));
					}
				}
			}
			else {
				checkList = mapMultiSelectList;
			}
			
			editTrigger = thisLayer.mapData.GetTrigger (checkList[0]);
			editOrder = thisLayer.mapData.GetOrder (checkList[0]);
			editRotation = thisLayer.mapData.GetRotation (checkList[0]);
			editFlip = thisLayer.mapData.GetFlip (checkList[0]);
			editProperty = thisLayer.mapData.GetPropertyInfo (checkList[0]);
			if (editProperty == null) {
				editProperty = new PropertyInfo();
			}
			
			for (var i = 1; i < checkList.Count; i++) {
				if (applyTrigger && thisLayer.mapData.GetTrigger (checkList[i]) != editTrigger) {
					applyTrigger = false;
					editTrigger = 0;
				}
				if (applyOrder && thisLayer.mapData.GetOrder (checkList[i]) != editOrder) {
					applyOrder = false;
					editOrder = 0;
				}
				if (applyRotation && thisLayer.mapData.GetRotation (checkList[i]) != editRotation) {
					applyRotation = false;
					editRotation = 0;
				}
				if (applyFlip && thisLayer.mapData.GetFlip (checkList[i]) != editFlip) {
					applyFlip = false;
					editFlip = Flip.None;
				}
				if (applyExtra && thisLayer.mapData.HasProperty (checkList[i]) != editExtra) {
					applyExtra = false;
					editExtra = false;
					editProperty = new PropertyInfo();
				}
				if (editExtra && (thisLayer.mapData.GetProperty.<float> (checkList[i]) != editProperty.f || thisLayer.mapData.GetProperty.<String> (checkList[i]) != editProperty.s || thisLayer.mapData.GetPropertyID (checkList[i]) != editProperty.guid)) {
					applyExtra = false;
					editExtra = false;
					editProperty = new PropertyInfo();
				}
			}
		}
		else {	// Not multi-edit
			editTrigger = thisLayer.mapData.GetTrigger (editPos);
			editOrder = thisLayer.mapData.GetOrder (editPos);
			editRotation = thisLayer.mapData.GetRotation (editPos);
			editFlip = thisLayer.mapData.GetFlip (editPos);
			editProperty = thisLayer.mapData.GetPropertyInfo (editPos);
			editExtra = thisLayer.mapData.HasProperty (editPos);
			if (editProperty == null) {
				editProperty = new PropertyInfo();
			}
		}
	}
	
	static function MapInfo () {
		if (isMultiEdit && !mapMultiSelectActive && !selectionActive) {
			editTile = false;
		}
		if (editTile) {
			GUI.color = Color(.6, 1, 1, 1);
			BeginVertical ("box");
			BeginHorizontal();
			if ((mapMultiSelectActive || selectionActive) && isMultiEdit) {
				Label ("<b>(Multi Selection)</b>", richtextLabel, Width(144));
			}
			else {
				Label ("X: <b>" + editPos.x + "</b>", richtextLabel, Width(coordWidth));
				Label ("Y: <b>" + editPos.y + "</b>", richtextLabel, Width(coordWidth));
			}
			Label ("Trigger: ");
			EditorGUI.showMixedValue = (!applyTrigger);
			GUI.SetNextControlName ("EditTrigger");
			EditorGUI.BeginChangeCheck();
			editTrigger = EditorGUILayout.IntField (editTrigger, Width(30));
			if (EditorGUI.EndChangeCheck()) {
				applyTrigger = true;
			}
			EditorGUI.showMixedValue = false;
			editTrigger = Mathf.Clamp (editTrigger, 0, Tile.TRIGGERNUMBER-1);
			
			Label ("Order: ");
			EditorGUI.showMixedValue = (!applyOrder);
			EditorGUI.BeginChangeCheck();
			editOrder = EditorGUILayout.IntField (editOrder, Width(38));
			if (EditorGUI.EndChangeCheck()) {
				applyOrder = true;
			}
			EditorGUI.showMixedValue = false;
			editOrder = Mathf.Clamp (editOrder, -32768, 32767);
			
			Label ("Rotation: ");
			EditorGUI.showMixedValue = (!applyRotation);
			EditorGUI.BeginChangeCheck();
			editRotation = EditorGUILayout.FloatField (editRotation, Width(38));
			if (EditorGUI.EndChangeCheck()) {
				applyRotation = true;
			}
			EditorGUI.showMixedValue = false;
			editRotation = Mathf.Clamp (editRotation, 0.0, 360.0);
			
			Label ("Flip: ");
			EditorGUI.showMixedValue = (!applyFlip);
			EditorGUI.BeginChangeCheck();
			editFlip = System.Convert.ToInt32 (EditorGUILayout.EnumPopup (editFlip, Width(45)));
			if (EditorGUI.EndChangeCheck()) {
				applyFlip = true;
			}
			EditorGUI.showMixedValue = false;
			FlexibleSpace();
			
			if (Button (content[Content.EditCancel], Width(32), Height(15))) {
				editTile = false;
			}
			if (Button (content[Content.EditAccept], Width(32), Height(15)) ) {
				FinishTileEdit();
				CompleteSettingUndo();
			}
			EndHorizontal();
			
			BeginHorizontal();
			if (selectionActive && isMultiEdit) {
				Label ((selectionEnd.x - selectionStart.x + 1) * (selectionEnd.y - selectionStart.y + 1) + " tiles", Width(144));
			}
			else if (mapMultiSelectActive && isMultiEdit) {
				Label (mapMultiSelectList.Count + " tiles", Width(144));	
			}
			else {
				Label ("    (" + (editPos.x*tileSizeX).ToString("f2") + ")", Width(coordWidth));
				Label ("    (" + (editPos.y*tileSizeY).ToString("f2") + ")", Width(coordWidth));
			}
			
			EditorGUI.BeginChangeCheck();
			EditorGUI.showMixedValue = (!applyExtra);
			editExtra = EditorGUILayout.Toggle (editExtra, Width(14));
			EditorGUI.showMixedValue = false;
			if (Button ("Extra:", EditorStyles.label)) {
				editExtra = !editExtra;
			}
			if (EditorGUI.EndChangeCheck()) {
				applyExtra = true;
			}
			if (editExtra) {
				Label ("num");
				editProperty.f = EditorGUILayout.FloatField (editProperty.f, Width(38));
				Label ("json");
				editProperty.s = EditorGUILayout.TextField (editProperty.s, Width(Mathf.Lerp (96, 196, Mathf.InverseLerp (583, 683, mapRect.width))));
				Label ("GO");
				editProperty.go = EditorGUILayout.ObjectField (editProperty.go, GameObject, false, Width(140)) as GameObject;
				if(editProperty.go != null && Button ("Instance Parameters")){
				    var instanceParametersEditor = EditorWindow.GetWindow(InstanceParametersEditor) as InstanceParametersEditor;
				    instanceParametersEditor.Initialize(editProperty);
				    instanceParametersEditor.Show();
				}
				
			}
			FlexibleSpace();
			EndHorizontal();
			GUI.color = whiteColor;
		}
		else {	// Not editing tile
			var stringFieldWidth = Mathf.Lerp (160, 260, Mathf.InverseLerp (583, 683, mapRect.width));
			var intFieldWidth = Mathf.Lerp (75, 100, Mathf.InverseLerp (583, 608, mapRect.width));
			BeginVertical (content[Content.InfoBox], "box");
			if (MouseInMap()) {
				BeginHorizontal();
				ClampGridPos();
				if (!selectionClick) {
					Label ("X: <b>" + gridPos.x + "</b>", richtextLabel, Width(coordWidth));
					Label ("Y: <b>" + gridPos.y + "</b>", richtextLabel, Width(coordWidth));
				}
				else {
					Label ("W: <b>" + (selectionEnd.x - selectionStart.x + 1) + "</b>", richtextLabel, Width(coordWidth));
					Label ("H: <b>" + (selectionEnd.y - selectionStart.y + 1) + "</b>", richtextLabel, Width(coordWidth));
				}
				Label ("Trigger: <b>" + thisLayer.mapData.GetTrigger (gridPos) + "</b>", richtextLabel, Width(75));
				Label ("Order: <b>" + thisLayer.mapData.GetOrder (gridPos) + "</b>", richtextLabel, Width(85));
				Label ("Rotation: <b>" + thisLayer.mapData.GetRotation (gridPos) + "</b>", richtextLabel, Width(90));
				Label ("Flip: <b>" + thisLayer.mapData.GetFlip (gridPos) + "</b>", richtextLabel, Width(70));
				FlexibleSpace();
				GUI.enabled = false;
				Button ("", "label", Height(15));	// Workaround for Unity 5.5 highlighting the Edit button for some reason
				GUI.enabled = true;
				Button (content[Content.EditProperties], Height(15));	// For show, since this can't be clicked if the mouse is in the map
				EndHorizontal();
				
				BeginHorizontal();
				if (!selectionClick) {
					Label ("    (" + (gridPos.x*tileSizeX).ToString("f2") + ")", Width(coordWidth));
					Label ("    (" + (gridPos.y*tileSizeY).ToString("f2") + ")", Width(coordWidth));
				}
				else {
					Label ("    (" + ((selectionEnd.x - selectionStart.x + 1)*tileSizeX).ToString("f2") + ")", richtextLabel, Width(coordWidth));
					Label ("    (" + ((selectionEnd.y - selectionStart.y + 1)*tileSizeY).ToString("f2") + ")", richtextLabel, Width(coordWidth));
				}
				Label ("Extra: ");
				if (thisLayer.mapData.HasProperty (gridPos)) {
					if (thisLayer.mapData.GetProperty.<float> (gridPos) != 0.0) {
						Label ("num: <b>" + thisLayer.mapData.GetProperty.<float> (gridPos) + "</b>", richtextLabel, Width(intFieldWidth));
					}
					else {
						GUI.color.a = .5;
						Label ("num: ", Width(intFieldWidth));
						GUI.color.a = 1.0;
					}
					if (thisLayer.mapData.GetProperty.<String> (gridPos) != null) {
						Label ("label: <b>" + thisLayer.mapData.GetProperty.<String> (gridPos) + "</b>", richtextLabel, Width(stringFieldWidth));
					}
					else {
						GUI.color.a = .5;
						Label ("label: ", Width(stringFieldWidth));
						GUI.color.a = 1.0;
					}
					if (thisLayer.mapData.GetProperty.<GameObject> (gridPos) != null) {
						var info = "";
						Label ("GO: <b>" + thisLayer.mapData.GetProperty.<GameObject> (gridPos).name + info+ "</b>",richtextLabel, Width(600) );
					}

					else {
						GUI.color.a = .5;
						Label ("GO: ", Width(600));
						GUI.color.a = 1.0;
					}

				}
				else {
					Label (" ", Width(intFieldWidth));
					Label (" ", Width(stringFieldWidth));
					Label (" ", Width(600));
				}
				FlexibleSpace();
				EndHorizontal();
			}
			else {	// Mouse not in map
				BeginHorizontal();
				Label ("X:", Width(coordWidth));
				Label ("Y:", Width(coordWidth));
				Label ("Trigger:", Width(75));
				Label ("Order:", Width(85));
				Label ("Rotation:", Width(90));
				Label ("Flip:", Width(70));
				FlexibleSpace();
				GUI.enabled = false;
				Button("", "label", Height(15));	// Workaround for Unity 5.5 highlighting the Edit button for some reason
				GUI.enabled = (selectionActive || mapMultiSelectActive);	
				if (Button (content[Content.EditProperties], Height(15))) {
					StartTileEdit();
				}
				GUI.enabled = true;
				EndHorizontal();
				
				BeginHorizontal();
				Label (" ", Width(coordWidth));
				Label (" ", Width(coordWidth));
				Label ("Extra: ");
				Label (" ", Width(intFieldWidth));
				Label (" ", Width(stringFieldWidth));
				Label (" ", Width(145));
				FlexibleSpace();
				EndHorizontal();
			}
		}
		EndVertical();
	}
	
	static function FinishTileEdit () {
		var setGOInManager = false;
		if (selectionActive) {
			SetUndo (selectionStart, selectionEnd, true, currentLayer);
			for (var y = selectionStart.y; y <= selectionEnd.y; y++) {
				for (var x = selectionStart.x; x <= selectionEnd.x; x++) {
					if (SetTileProperties (Int2(x, y))) {
						setGOInManager = true;
					}
				}
			}
		}
		else if (mapMultiSelectActive) {
			SetMultiUndo (currentLayer, false);
			for (var i = 0; i < mapMultiSelectList.Count; i++) {
				if (SetTileProperties (mapMultiSelectList[i])) {
					setGOInManager = true;
				}
			}
		}
		else {
			SetUndo (editPos, Int2.zero, false, currentLayer);
			setGOInManager = SetTileProperties (editPos);
		}
		
		if (setGOInManager) {
			EditorUtility.SetDirty (tileManager);
		}
		editTile = false;
		CalcMinMaxOrder (currentLayer);
		levelChanged = true;
	}
	
	static function SetTileProperties (pos : Int2) : boolean {
		var setGOInManager = false;
		if (applyOrder) {
			thisLayer.mapData.SetOrder (pos, editOrder);
		}
		if (applyRotation) {
			thisLayer.mapData.SetRotation (pos, editRotation);
		}
		if (applyTrigger) {
			thisLayer.mapData.SetTrigger (pos, editTrigger);
		}
		if (applyFlip) {
			thisLayer.mapData.SetFlip (pos, editFlip);
		}
		if (applyExtra) {
			thisLayer.mapData.SetHasProperty (pos, editExtra);
			if (editExtra) {
				thisLayer.mapData.SetProperty.<float> (pos, editProperty.f);
				thisLayer.mapData.SetProperty.<String> (pos, editProperty.s);
				thisLayer.mapData.SetProperty.<GameObject> (pos, editProperty.go);
				if (editProperty.go != null) {
					var id = tileManager.AddGO (editProperty.go);
					previewDictionary[id] = null;
					thisLayer.mapData.SetPropertyID (pos, id);
					setGOInManager = true;
				}
				else {
					thisLayer.mapData.SetPropertyID (pos, -1);
				}
			}
		}
		return setGOInManager;
	}
	
	static function PreviewTexture (i : int, go : GameObject) : Texture2D {
		if (previewDictionary[i] == null) {
			previewDictionary[i] = AssetPreview.GetAssetPreview (go);
			if (previewDictionary[i] == null) {	// Is async so might not have loaded yet
				if (AssetPreview.IsLoadingAssetPreview (go.GetInstanceID())) {
					return goIcon;
				}
				else {	// Failed somehow, use default icon
					previewDictionary[i] = goIcon;
					return goIcon;
				}
			}
		}
		return previewDictionary[i];
	}
	
	static function DrawSelectionBox () {
		if (Vector2.Distance (lastMousePos, originalMousePos) < 3) return;
		
		var w = selectionEnd.x - selectionStart.x + 1;
		var h = selectionEnd.y - selectionStart.y + 1;
		GUI.Label (Rect(selectionStart.x*pSizeX, (mapSize.y - selectionEnd.y - 1)*pSizeY, w*pSizeX, h*pSizeY), "", selectionStyle);
	}
	
	static function CopySelection (eraseOriginal : boolean) {
		if (!selectionActive) return;
		
		selectionActive = false;
		lightMode = false;
		copyBufferSize = Int2(selectionEnd.x - selectionStart.x + 1, selectionEnd.y - selectionStart.y + 1);
		copyBuffer = new MapData (copyBufferSize, false);
		thisLayer.mapData.CopyBlockTo (selectionStart, selectionEnd, copyBuffer, Int2.zero, false, false);
		copyActive = true;
		
		if (eraseOriginal) {
			var doResetLighting = thisLayer.mapData.AreaContainsLighting (selectionStart, selectionEnd);
			SetUndo (selectionStart, selectionEnd, true, currentLayer);
			thisLayer.mapData.SetTileBlock (selectionStart, selectionEnd, 0, -1, 0, 0, false, 0, Flip.None, false, null, true);
			if (doResetLighting) {
				thisLayer.mapData.ResetLighting (lightList);
			}
			levelChanged = true;
		}
	}
	
	static function DrawCopyBuffer () {
		var offset : Int2 = copyBufferSize / 2;
		var start = Int2(Mathf.Max (gridPos.x - offset.x, 0), Mathf.Max (gridPos.y - offset.y, 0));
		var end = Int2(Mathf.Min (gridPos.x + copyBufferSize.x - offset.x, mapSize.x), Mathf.Min (gridPos.y + copyBufferSize.y - offset.y, mapSize.y));
		
		GUI.Box (Rect(start.x*pSizeX, (mapSize.y - start.y - (end.y - start.y))*pSizeY, (end.x - start.x)*pSizeX, (end.y - start.y)*pSizeY), "");
		
		GUI.color.a = .3;
		var cPos : Int2;
		cPos.y = (gridPos.y - offset.y < 0)? -(gridPos.y - offset.y) : 0;
		for (var y = start.y; y < end.y; y++) {
			cPos.x = (gridPos.x - offset.x < 0)? -(gridPos.x - offset.x) : 0;
			for (var x = start.x; x < end.x; x++) {
				var tInfo = copyBuffer.GetTile (cPos);
				if (tInfo.tile >= 0) {
					thisTile = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
					DrawTileTex (x, mapSize.y - y - 1, thisTile, copyBuffer, cPos, thisTile.texture);
				}
				cPos.x++;
			}
			cPos.y++;
		}
		GUI.color.a = 1.0;
	}
	
	static function PasteCopyBuffer (p : Int2, skipEmpties : boolean, delete : boolean) {
		var offset : Int2 = copyBufferSize / 2;
		var p1 = p - offset;
		var p2 = p - offset + copyBufferSize - Int2.one;
		thisLayer.mapData.Clamp (p1, p2);
		SetUndo (p1, p2, true, currentLayer);
		var doResetLighting = thisLayer.mapData.AreaContainsLighting (p1, p2);
		var doResetBlockables = thisLayer.mapData.AreaContainsColliders (p1, p2);
		copyBuffer.CopyBlockTo (Int2.zero, copyBufferSize - Int2.one, thisLayer.mapData, p - offset, skipEmpties, delete);
		if (copyBuffer.AreaContainsColliders (Int2.zero, copyBufferSize - Int2.one)) {
			doResetBlockables = true;
		}
		if (copyBuffer.AreaContainsLighting (Int2.zero, copyBufferSize - Int2.one)) {
			doResetLighting = true;
		}
		if (doResetBlockables) {
			thisLayer.mapData.ResetShadowCastLights (lightList, false);
			doResetLighting = true;
		}
		if (doResetLighting) {
			thisLayer.mapData.ResetLighting (lightList);
		}

		levelChanged = true;
		CalcMinMaxOrder (currentLayer);
	}
	
	static function ResizeMap () {
		if (newMapSize.x == mapSize.x && newMapSize.y == mapSize.y) {
			return;
		}
		if (newMapSize.x < 1) {
			newMapSize.x = 1;
		}
		if (newMapSize.y < 1) {
			newMapSize.y = 1;
		}
		selectionActive = false;
		mapMultiSelectActive = false;
		
		// Check if map has been used, so as to not annoy users with resizing dialogs if there's nothing in it anyway
		var usedMap = false;
		var end = thisLayer.mapData.map.Length;
		for (var i = 0; i < end; i++) {
			if (thisLayer.mapData.map[i] >= 0) {
				usedMap = true;
				break;
			}
		}
		if (!usedMap) {
			mapSize = newMapSize;
			thisLayer.mapData.InitializeMap (mapSize, true);
			levelChanged = true;
			return;
		}
		
		if (newMapSize.x < mapSize.x || newMapSize.y < mapSize.y) {
			var isSure = EditorUtility.DisplayDialog ("Resize?", "The new map size is smaller; are you sure you want to resize?", "Resize", "Cancel");
			if (!isSure) {
				newMapSize = mapSize;
				return;
			}
			InitializeUndo();
		}
		
		var tempMap = new MapData(mapSize, false);
		thisLayer.mapData.CopyBlockTo (Int2.zero, Int2(mapSize.x-1, mapSize.y-1), tempMap, Int2.zero, false, false);
		thisLayer.mapData.InitializeMap (newMapSize, true);
		tempMap.CopyBlockTo (Int2.zero, Int2(Mathf.Min (newMapSize.x, mapSize.x)-1, Mathf.Min (newMapSize.y, mapSize.y)-1),
								thisLayer.mapData, Int2.zero, false, false);
		mapSize = newMapSize;
		ClampGridPos();
		thisLayer.mapData.ambientColor = lightAmbient;
		thisLayer.mapData.ResetLighting (lightList);
		levelChanged = true;
	}
	
	static function TerrainGroupActive () : boolean {
		return (terrainGroupActiveList.Count > 0);
	}
	
	static function Undo () {
		if (undoIndex < 0) return;
		
		SetUndoContents (undoList[undoIndex]);
		undoIndex--;
	}
	
	static function Redo () {
		if (undoIndex >= undoList.Count - 1) return;
		
		SetUndoContents (undoList[undoIndex + 1]);
		undoIndex++;
	}
	
	static function SetUndoContents (thisUndo : UndoList) {
		var layerNumber = thisUndo.layerNumber;
		for (var i = thisUndo.mapElementList.Count-1; i >= 0; i--) {
			var mapElement = thisUndo.mapElementList[i];
			if (!TileExists (mapElement.mapTile)) {
				mapElement.mapTile = -1;
			}
			thisUndo.mapElementList[i] = mapLayers[layerNumber].mapData.GetMapElement (mapElement.position);	// Replace with existing contents for Redo
			mapLayers[layerNumber].mapData.SetMapElement (mapElement);
		}
		if (showLight) {
			mapLayers[layerNumber].mapData.ResetLighting (lightList);
		}
		lightIsSelected = false;
		levelChanged = true;
		// Switch layer if it makes sense
		if (thisUndo.layerNumber != currentLayer) {
			if (showNextLayer && thisUndo.layerNumber == currentLayer + 1) {
				return;
			}
			if (showPrecedingLayer && thisUndo.layerNumber == currentLayer - 1) {
				return;
			}
			currentLayer = thisUndo.layerNumber;
			ChangeLayer (false);
		}
	}
	
	static function CompleteSettingUndo () {
		settingUndo = false;
	}
	
	static function SetUndo (p1 : Int2, p2 : Int2, doBlock : boolean, layerNumber : int) {
		for (var i = undoList.Count - 1; i > undoIndex; i--) {	// Remove undo stuff after current undo index
			totalUndoElementCount -= undoList[i].mapElementList.Count;
			undoList.RemoveAt (i);
		}
		if (!settingUndo) {
			undoList.Add (new UndoList(layerNumber));
		}
		
		if (mouseClicked) {
			mouseClicked = false;
			settingUndo = true;
		}
		
		var idx = undoList.Count-1;
		if (doBlock) {
			p1.x = Mathf.Max (p1.x, 0);
			p1.y = Mathf.Max (p1.y, 0);
			p2.x = Mathf.Min (p2.x, mapSize.x - 1);
			p2.y = Mathf.Min (p2.y, mapSize.y - 1);
			for (var y = p1.y; y <= p2.y; y++) {
				for (var x = p1.x; x <= p2.x; x++) {
					undoList[idx].Add (mapLayers[layerNumber].mapData.GetMapElement (Int2(x, y)));
					totalUndoElementCount++;
				}
			}
		}
		else {
			undoList[idx].Add (mapLayers[layerNumber].mapData.GetMapElement (p1));
			totalUndoElementCount++;
		}
		
		// Keep undo list under something vaguely approximating 2GB assuming 24 bytes/mapElement
		while (totalUndoElementCount > 80000000 && undoList.Count > 0) {
			totalUndoElementCount -= undoList[0].mapElementList.Count;
			undoList.RemoveAt (0);
		}
		undoIndex = undoList.Count - 1;
	}
	
	static function SetMultiUndo (layerNumber : int, addToExisting : boolean) {
		for (var i = 0; i < mapMultiSelectList.Count; i++) {
			if (i == 1 || addToExisting) {
				settingUndo = true;
			}
			SetUndo (mapMultiSelectList[i], Int2.zero, false, layerNumber);
		}
		settingUndo = false;
	}
	
	static function SetTile (p : Int2, thisSet : int, t : int) {
		if (!MouseInMap()) return;
		
		if (fillArea) {
			SetTileFillArea (p, thisSet, t);
			return;
		}
		if (lightMode) {
			PlaceLight (p, t != -1);
			return;
		}
		if (pickMode) {
			PickTile (p);
			return;
		}
		
		if ((t >= 0 && tileSets[thisSet].tileData.Count == 0)) return;
		
		if (p.x >= startPos.x && p.x < endPos.x && p.y >= startPos.y && p.y < endPos.y) {
			SetUndo (p, Int2.zero, false, currentLayer);
			savedGridPos = p;
			if (t == -1) {	// Delete tile
				var doReset = false;
				if (thisLayer.mapData.GetCollider (p)) {
					doReset = true;
				}
				thisLayer.mapData.SetTile (p, 0, -1, 0, 0, false, 0, Flip.None);
				thisLayer.mapData.SetHasProperty (p, false);
				if (TerrainGroupActive()) {
					TerrainGroupCheck (p, thisLayer.mapData);
				}
				if (doReset) {
					thisLayer.mapData.ResetShadowCastLights (lightList, showLight);
				}
			}
			else {
				t = SetTileInfo (p, thisSet, t);
				if (TerrainGroupActive()) {
					TerrainGroupCheck (p, thisLayer.mapData);
				}
				SetMinMaxOrder (tileSets[thisSet].tileData[t].orderInLayer);
			}
			levelChanged = true;
		}
	}
	
	static function SetTileFillArea (p : Int2, thisSet : int, t : int) {
		if (filled) {	// Prevent repeated calls while mousedown; filled set to false on mouseup
			return;
		}
		var targetTile = thisLayer.mapData.GetTile (p);
		if (targetTile == TileInfo(thisSet, t) && !CanDrawWithRandom()) {
			return;
		}
		// If filling with a random group that contains the target tile, fill with unused tile first, so the random fill will be correct
		if (t >= 0 && CanDrawWithRandom() && thisRandomGroupSet[selectedGroup].Contains (targetTile)) {
			drawWithRandomGroup = false;
			FillArea (p, targetTile, TileInfo(0, MapData.tempTile));
			targetTile = TileInfo(0, MapData.tempTile); 
			drawWithRandomGroup = true;
		}
		FillArea (p, targetTile, TileInfo(thisSet, t));
		if (CanDrawWithRandom() && t >= 0) {
			CalcMinMaxOrderWithRandomGroup (selectedGroup);
		}
		else {
			if (t >= 0) {
				SetMinMaxOrder (tileSets[thisSet].tileData[t].orderInLayer);
			}
		}
		filled = true;
		levelChanged = true;
		return;
	}
	
	static function PlaceLight (p : Int2, active : boolean) {
		if (active) {
			selectedLightPos = p;	// Set position for drag
		}
		if (lightDragging || lightPlacedDown) return;
		
		if (thisLayer.mapData.HasLight (p)) {
			var lightNumber = Mathf.Clamp (thisLayer.mapData.GetLightNumber (p), 0, lightList.Count-1);
			if (!active) {	// Delete light
				SetUndo (p, Int2.zero, false, currentLayer);
				thisLayer.mapData.SetLight (p, false, lightNumber, 1.0, lightList, false);
				lightIsSelected = false;
				levelChanged = true;
				lightDragPos = Int2(-1, -1);
				return;
			}
			else {	// If clicked on existing light, select it
				if (lightNumber < lightList.Count) {
					selectedLightInfo = lightList[lightNumber];
					SetLightInfo (lightNumber);
					lightDragPos = p;
					lightIsSelected = true;
					lightPlacedDown = true;
					return;
				}
			}
		}
		if (!active) return;
		// Place light if clicking where light doesn't exist
		SetUndo (p, Int2.zero, false, currentLayer);
		thisLayer.mapData.SetLight (p, true, selectedLight, 1.0, lightList, false);
		selectedLightInfo = lightList[selectedLight];
		lightDragPos = p;
		lightIsSelected = true;
		lightPlacedDown = true;
		levelChanged = true;
	}
	
	static function StartLightDrag () {
		lightDragging = true;
		SetUndo (lightDragPos, Int2.zero, false, currentLayer);
		thisLayer.mapData.SetLight (lightDragPos, false, selectedLight, 1.0, lightList, false);
	}
	
	static function FinishLightDrag () {
		lightDragging = false;
		SetUndo (selectedLightPos, Int2.zero, false, currentLayer);
		if (thisLayer.mapData.HasLight (selectedLightPos)) {	// If dragged onto an existing light, delete it first
			thisLayer.mapData.SetLight (selectedLightPos, false, thisLayer.mapData.GetLightNumber (selectedLightPos), 0.0, lightList, false);
		}
		thisLayer.mapData.SetLight (selectedLightPos, true, selectedLight, 1.0, lightList, false);
		lightDragPos = Int2(-1, -1);
		lightIsSelected = true;
		levelChanged = true;
	}
	
	static function SetTileInfo (p : Int2, thisSet : int, t : int) : int {
		if (CanDrawWithRandom() && t >= 0) {
			var tInfo = RandomTile();
			thisSet = tInfo.set;
			t = GetTileNumber (tInfo);
		}
		if (t >= 0) {
			var doReset = thisLayer.mapData.GetCollider (p);
			var thisTData = tileSets[thisSet].tileData[t];
			var thisTrigger = (thisTData.trigger != 0)? thisTData.trigger : thisLayer.mapData.GetTrigger (p);
			var thisOrder = (thisTData.orderInLayer != 0)? thisTData.orderInLayer : thisLayer.mapData.GetOrder (p);
			thisLayer.mapData.SetTile (p, thisSet, thisTData.realTileNumber, thisOrder, thisTData.rotation, thisTData.useCollider, thisTrigger, Flip.None);
			if (thisTData.useCollider || doReset) {
				thisLayer.mapData.ResetShadowCastLights (lightList, showLight);
			}
		}
		else {	// Delete
			if (thisLayer.mapData.GetCollider (p)) {
				thisLayer.mapData.ResetShadowCastLights (lightList, showLight);
			}
			thisLayer.mapData.SetTile (p, 0, t, 0, 0, false, 0, Flip.None);
		}
		return t;
	}

	static function SetTileBlock (p1 : Int2, p2 : Int2, thisSet : int, t : int) {
		if (thisTileSet.Count == 0) return;
		
		if (mapMultiSelectActive) {
			SetMultiUndo (currentLayer, false);
			var doReset = false;
			var thisTData = tileSets[thisSet].tileData[t];
			for (var i = 0; i < mapMultiSelectList.Count; i++) {
				if (thisLayer.mapData.GetCollider (mapMultiSelectList[i])) {
					doReset = true;
				}
				if (t == -1) {	// Erase
					thisLayer.mapData.SetTile (mapMultiSelectList[i], 0, -1, 0, 0, false, 0, Flip.None);
					continue;
				}
				if (CanDrawWithRandom()) {
					var tInfo = RandomTile();
					thisTData = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
					thisSet = tInfo.set;
				}
				if (thisTData.useCollider) {
					doReset = true;
				}
				thisLayer.mapData.SetTile (mapMultiSelectList[i], thisSet, thisTData.realTileNumber, thisTData.orderInLayer, thisTData.rotation, thisTData.useCollider, thisTData.trigger, Flip.None);
			}
			if (CanDrawWithRandom()) {
				CalcMinMaxOrderWithRandomGroup (selectedGroup);
			}
			else {
				SetMinMaxOrder (thisTData.orderInLayer);
			}
			if (doReset) {
				thisLayer.mapData.ResetShadowCastLights (lightList, showLight);
			}
			return;
		}
		
		SetUndo (p1, p2, true, currentLayer);
		doReset = thisLayer.mapData.AreaContainsColliders (p1, p2);
		if (t == -1) {	// Erase
			thisLayer.mapData.SetTileBlock (p1, p2, 0, -1, 0, 0, false, 0, Flip.None, false, null, false);
		}
		else {	// Random fill
			if (CanDrawWithRandom()) {
				for (var y = p1.y; y <= p2.y; y++) {
					for (var x = p1.x; x <= p2.x; x++) {
						tInfo = RandomTile();
						thisTData = tileSets[tInfo.set].tileData[GetTileNumber (tInfo)];
						if (thisTData.useCollider) {
							doReset = true;
						}
						thisLayer.mapData.SetTile (Int2(x, y), tInfo.set, thisTData.realTileNumber, thisTData.orderInLayer, thisTData.rotation, thisTData.useCollider, thisTData.trigger, Flip.None);
					}
				}
				CalcMinMaxOrderWithRandomGroup (selectedGroup);
			}
			else {	// Standard fill
				thisTData = tileSets[thisSet].tileData[t];
				if (thisTData.useCollider) {
					doReset = true;
				}
				thisLayer.mapData.SetTileBlock (p1, p2, thisSet, thisTData.realTileNumber, thisTData.orderInLayer, thisTData.rotation, thisTData.useCollider, thisTData.trigger, Flip.None, false, null, false);
				SetMinMaxOrder (thisTData.orderInLayer);
			}
		}
		if (doReset) {
			thisLayer.mapData.ResetShadowCastLights (lightList, showLight);
		}
		levelChanged = true;
	}
	
	static function MoveSelectionToLayer (oldLayer : int, newLayer : int) {
		if (oldLayer == newLayer) return;
		if (mapLayers[oldLayer].mapData.mapSize != mapLayers[newLayer].mapData.mapSize) {
			EditorUtility.DisplayDialog ("Layer sizes not compatible", "When moving the selected tiles between layers, the size of the two layers must be the same.", "Cancel");
			return;
		}
		
		if (mapMultiSelectActive) {
			SetMultiUndo (oldLayer, false);
			SetMultiUndo (newLayer, false);
			for (var i = 0; i < mapMultiSelectList.Count; i++) {
				var pos = mapMultiSelectList[i];
				var mapElement = mapLayers[oldLayer].mapData.GetMapElement (pos); 
				mapLayers[newLayer].mapData.SetMapElement (mapElement);
				mapLayers[oldLayer].mapData.SetTile (pos, 0, -1);
			}
		}
		else {
			SetUndo (selectionStart, selectionEnd, true, oldLayer);
			CompleteSettingUndo();
			SetUndo (selectionStart, selectionEnd, true, newLayer);
			CompleteSettingUndo();
			var mapData = mapLayers[oldLayer].mapData.GetBlock (selectionStart, selectionEnd);
			var doResetLighting = mapLayers[oldLayer].mapData.AreaContainsLighting (selectionStart, selectionEnd);
			mapData.CopyBlockTo (Int2.zero, selectionEnd - selectionStart, mapLayers[newLayer].mapData, selectionStart, !evt.shift, false);
			mapLayers[oldLayer].mapData.SetTileBlock (selectionStart, selectionEnd, 0, -1, 0, 0, false, 0, Flip.None, false, null, true);
			if (doResetLighting) {
				mapLayers[oldLayer].mapData.ResetLighting (lightList);
				mapLayers[newLayer].mapData.ResetLighting (lightList);
			}
		}
		
		levelChanged = true;
	}
	
	static function CalcMinMaxOrderWithRandomGroup (thisGroup : int) {
		for (var i = 0; i < thisRandomGroupSet[thisGroup].Count; i++) {
			var tInfo = thisRandomGroupSet[thisGroup][i];
			tInfo.tile = GetTileNumber (tInfo);
			SetMinMaxOrder (tileSets[tInfo.set].tileData[tInfo.tile].orderInLayer);
		}
	}
	
	static function SetTileLine (p1 : Int2, p2 : Int2, thisSet : int, t : int) {
		if (savedGridPos == Int2(-1, -1) || p1 == p2 || fillArea) return;
		
		savedGridPos = p2;
		var dy = p2.y - p1.y;
		var dx = p2.x - p1.x;
	 
		if (dy < 0) {
			dy = -dy;
			var stepy = -1;
		}
		else {
			stepy = 1;
		}
		if (dx < 0) {
			dx = -dx;
			var stepx = -1;
		}
		else {
			stepx = 1;
		}
		dy <<= 1;
		dx <<= 1;
	 
		if (dx > dy) {
			var fraction = dy - (dx >> 1);
			while (p1.x != p2.x) {
				if (fraction >= 0) {
					p1.y += stepy;
					fraction -= dx;
				}
				p1.x += stepx;
				fraction += dy;
				SetTile (p1, thisSet, t);
			}
		}
		else {
			fraction = dx - (dy >> 1);
			while (p1.y != p2.y) {
				if (fraction >= 0) {
					p1.x += stepx;
					fraction -= dy;
				}
				p1.y += stepy;
				fraction += dx;
				SetTile (p1, thisSet, t);
			}
		}
	}
	
	static function MapMultiSelect (p : Int2) {
		if (fillArea || lightMode) return;
		
		Undo();
		multiSelecting = true;	// Prevent mousedown from setting tiles; set false on mouseup
		mapMultiSelectList.Clear();
		var targetTile = thisLayer.mapData.GetTile (p);
		var pos : Int2;
		for (pos.y = 0; pos.y < mapSize.y; pos.y++) {
			for (pos.x = 0; pos.x < mapSize.x; pos.x++) {
				if (thisLayer.mapData.GetTile (pos) == targetTile) {
					mapMultiSelectList.Add (pos);
				}
			}
		}
		mapMultiSelectActive = true;
		selectionActive = false;
	}
	
	static function FillArea (p : Int2, fillTile : TileInfo, replaceTile : TileInfo) {
		var stack = new List.<Int2>();
		stack.Add (p);
		while (stack.Count > 0 && stack.Count < 10000000) {
			p = stack[stack.Count-1];
			stack.RemoveAt (stack.Count-1);
			if (p.x < 0 || p.x >= thisLayer.mapData.mapSize.x || p.y < 0 || p.y >= thisLayer.mapData.mapSize.y) {
				continue;
			}
			if (thisLayer.mapData.GetTile (p) == fillTile) {
				SetUndo (p, Int2.zero, false, currentLayer);
				SetTileInfo (p, replaceTile.set, replaceTile.tile);
				stack.Add (p + Int2.up);
				stack.Add (p + Int2.down);
				stack.Add (p + Int2.left);
				stack.Add (p + Int2.right);
			}
		}
	}
	
	static function CanDrawWithRandom () : boolean {
		return (groupType == GroupType.Random && drawWithRandomGroup && thisRandomGroupSet.Count > 0 && thisRandomGroupSet[selectedGroup].Count > 0);
	}
	
	static function RandomTile () : TileInfo {
		var randomGroup = thisRandomGroupSet[selectedGroup];
		return randomGroup[Random.Range(0, randomGroup.Count)];
	}
	
	static function CycleOrder (p : Int2, dir : int) {
		SetUndo (p, Int2.zero, false, currentLayer);
		var newOrder = Mathf.Clamp (thisLayer.mapData.GetOrder (p) + dir, -32768, 32767);
		thisLayer.mapData.SetOrder (p, newOrder);
		CalcMinMaxOrder (currentLayer);
		levelChanged = true;
	}

	static function CycleOrderBlock (p1 : Int2, p2 : Int2, dir : int) {
		if (mapMultiSelectActive) {
			SetMultiUndo (currentLayer, false);
			for (var i = 0; i < mapMultiSelectList.Count; i++) {
				var newOrder = Mathf.Clamp (thisLayer.mapData.GetOrder (mapMultiSelectList[i]) + dir, -32768, 32767);
				thisLayer.mapData.SetOrder (mapMultiSelectList[i], newOrder);
			}
			CalcMinMaxOrder (currentLayer);
			return;
		}
		SetUndo (p1, p2, true, currentLayer);
		newOrder = Mathf.Clamp (thisLayer.mapData.GetOrder (gridPos) + dir, -32768, 32767);
		thisLayer.mapData.SetOrderBlock (p1, p2, newOrder);
		CalcMinMaxOrder (currentLayer);
		levelChanged = true;
	}
	
	static function RotateTile (p : Int2, dir : int) {
		SetUndo (p, Int2.zero, false, currentLayer);
		var thisRotation = Mathf.Repeat (thisLayer.mapData.GetRotation (p) + dir, 360.0);
		thisLayer.mapData.SetRotation (p, thisRotation);
		levelChanged = true;
	}
	
	static function FlipX (p : Int2) {
		var flip = thisLayer.mapData.GetFlip (p);
		if (flip == Flip.None) {
			thisLayer.mapData.SetFlip (p, Flip.X);
		}
		else if (flip == Flip.X) {
			thisLayer.mapData.SetFlip (p, Flip.None);
		}
		else if (flip == Flip.Y) {
			thisLayer.mapData.SetFlip (p, Flip.XY);
		}
		else {
			thisLayer.mapData.SetFlip (p, Flip.Y);
		}
		levelChanged = true;
	}

	static function FlipY (p : Int2) {
		var flip = thisLayer.mapData.GetFlip (p);
		if (flip == Flip.None) {
			thisLayer.mapData.SetFlip (p, Flip.Y);
		}
		else if (flip == Flip.Y) {
			thisLayer.mapData.SetFlip (p, Flip.None);
		}
		else if (flip == Flip.X) {
			thisLayer.mapData.SetFlip (p, Flip.XY);
		}
		else {
			thisLayer.mapData.SetFlip (p, Flip.X);
		}
		levelChanged = true;
	}
	
	static function SetCollider (p : Int2, setCollider : boolean) {
		if (fillArea && setCollider && !thisLayer.mapData.GetCollider (p)) {
			FillColliderArea (p);
			return;
		}
		SetUndo (p, Int2.zero, false, currentLayer);
		thisLayer.mapData.SetCollider (p, setCollider);
		levelChanged = true;
	}
	
	static function SetColliderBlock (p1 : Int2, p2 : Int2, setCollider : boolean) {
		if (mapMultiSelectActive) {
			SetMultiUndo (currentLayer, false);
			for (var i = 0; i < mapMultiSelectList.Count; i++) {
				thisLayer.mapData.SetCollider (mapMultiSelectList[i], setCollider);
			}
			return;
		}

		SetUndo (p1, p2, true, currentLayer);
		thisLayer.mapData.SetColliderBlock (selectionStart, selectionEnd, setCollider);
		levelChanged = true;
	}
	
	static function FillColliderArea (p : Int2) {		
		var stack = new List.<Int2>();
		stack.Add (p);
		while (stack.Count > 0 && stack.Count < 10000000) {
			p = stack[stack.Count-1];
			stack.RemoveAt (stack.Count-1);
			if (p.x < 0 || p.x >= thisLayer.mapData.mapSize.x || p.y < 0 || p.y >= thisLayer.mapData.mapSize.y) {
				continue;
			}
			if (!thisLayer.mapData.GetCollider (p)) {
				SetUndo (p, Int2.zero, false, currentLayer);
				thisLayer.mapData.SetCollider (p, true);
				stack.Add (p + Int2.up);
				stack.Add (p + Int2.down);
				stack.Add (p + Int2.left);
				stack.Add (p + Int2.right);
			}
		}
		levelChanged = true;
	}
	
	static function CalcMinMaxOrder (layerNum : int) {
		orderMin = orderMax = 0;
		var orderData = thisLayer.mapData.orderData;
		var end = mapLayers[layerNum].mapData.orderData.Length;
		for (var i = 0; i < end; i++) {
			if (orderData[i] < orderMin) {
				orderMin = orderData[i];
			}
			else if (orderData[i] > orderMax) {
				orderMax = orderData[i];
			}
		}
	}
	
	static function SetMinMaxOrder (orderNumber : int) {
		if (orderNumber < orderMin) {
			orderMin = orderNumber;
		}
		else if (orderNumber > orderMax) {
			orderMax = orderNumber;
		}
	}
	
    static function ChangeLayer (fromMenu : boolean)
    {
		if (fromMenu) {
			if (currentLayer == layerNamesControls.Length-3) {
				currentLayer = oldCurrentLayer;
				return;
			}
			if (currentLayer == layerNamesControls.Length-2) {
				DeleteLayer();
				return;
			}
			if (currentLayer == layerNamesControls.Length-1) {
				CreateLayer();
				return;
			}
		}
		TextureTester.Reset();
		TextureTester.SetLayer(currentLayer);
		
		thisLayer.scrollPos = mapScrollPos;
		thisLayer.previewSize = pSizeX;
		thisLayer.tileSize.x = tileSizeX;
		thisLayer.tileSize.y = tileSizeY;
		thisLayer.zPosition = zPos;
		oldCurrentLayer = currentLayer;
		sortingLayerIndex = oldSortingLayerIndex = sortingLayerIndices[currentLayer];
		SetLayerInfo (thisLayer.mapData.mapSize, true);
		CalcMinMaxOrder (currentLayer);
		lightIsSelected = false;
	}

	static function SetLayerInfo (oldMapSize : Int2, changingLayer : boolean) {
		thisLayer = mapLayers[currentLayer];
		if (syncLayerPos && changingLayer && thisLayer.mapData.mapSize == oldMapSize) {
			thisLayer.scrollPos = mapScrollPos;
			thisLayer.previewSize = pSizeX;
		}
		else {
			mapScrollPos = thisLayer.scrollPos;
			pSizeX = oldPSizeX = thisLayer.previewSize;
			ComputePSizeY();
			selectionActive = false;
			mapMultiSelectActive = false;
		}
		mapSize = newMapSize = thisLayer.mapData.mapSize;
		zPos = oldZPos = thisLayer.zPosition;
		lock = oldLock = thisLayer.layerLock;
		tileSizeX = oldTileSizeX = thisLayer.tileSize.x;
		tileSizeY = oldTileSizeY = thisLayer.tileSize.y;
		addBorder = oldAddBorder = thisLayer.addBorder;
		lightAmbient = oldLightAmbient = thisLayer.mapData.ambientColor;
		useRadiosity = oldUseRadiosity = thisLayer.mapData.useRadiosity;
	}
	
	static function DeleteLayer () {
		currentLayer = oldCurrentLayer;
		if (mapLayers.Count == 1) {			
			return;
		}
		var isSure = !EditorUtility.DisplayDialog ("Delete layer?", "Do you really want to delete layer " + oldCurrentLayer + "?", "Cancel", "Delete layer");
		if (isSure) {
			mapLayers.RemoveAt (oldCurrentLayer);
			sortingLayerIndices.RemoveAt (oldCurrentLayer);
			if (currentLayer == mapLayers.Count) {
				currentLayer--;
			}
			SetLayerInfo (Int2.zero, false);
			SetLayerNames();
			sortingLayerIndex = oldSortingLayerIndex = sortingLayerIndices[currentLayer];
			levelChanged = true;
			selectionActive = false;
			mapMultiSelectActive = false;
		}
		InitializeUndo();
	}
	
	static function CreateLayer () {
		thisLayer.scrollPos = mapScrollPos;
		thisLayer.previewSize = pSizeX;
		thisLayer.tileSize.x = tileSizeX;
		thisLayer.tileSize.y = tileSizeY;
		thisLayer.zPosition = zPos;
		currentLayer = oldCurrentLayer = mapLayers.Count;
		var oldMapSize = mapSize;
		mapSize = newMapSize;
		mapLayers.Add (new LayerData(mapSize, true));
		thisLayer = mapLayers[currentLayer];
		if (syncLayerPos && oldMapSize == newMapSize) {
			thisLayer.scrollPos = mapScrollPos;
			thisLayer.previewSize = pSizeX;
			thisLayer.tileSize.x = tileSizeX;
			thisLayer.tileSize.y = tileSizeY;
		}
		else {
			mapScrollPos = Vector2.zero;
			pSizeX = oldPSizeX = thisLayer.previewSize;
			ComputePSizeY();
			tileSizeX = tileSizeY = (thisTileSet.Count > 0)? GetTileSizeMax (thisTileSet[0]) : 1.0;
			selectionActive = false;
			mapMultiSelectActive = false;
		}
		zPos = oldZPos = -currentLayer;
		thisLayer.zPosition = zPos;
		thisLayer.tileSize.x = thisLayer.tileSize.y = tileSizeX;
		SetLayerNames();
		var newLayerIndex = Mathf.Min (sortingLayerIndices.Count, sortingLayerNames.Length-1);
		sortingLayerIndices.Add (newLayerIndex);
		sortingLayerIndex = oldSortingLayerIndex = newLayerIndex;
		lightIsSelected = false;
		lightAmbient = oldLightAmbient = Color(.5, .5, .5, 1.0);
		thisLayer.mapData.ambientColor = lightAmbient;
		thisLayer.mapData.SetupColors();
		levelChanged = true;
	}
	
	static function MoveLayer (layer : int, dir : int) {
		var tempLayer = mapLayers[layer + dir];
		mapLayers[layer + dir] = mapLayers[layer];
		mapLayers[layer] = tempLayer;
		var tempIndex = sortingLayerIndices[layer + dir];
		sortingLayerIndices[layer + dir] = sortingLayerIndices[layer];
		sortingLayerIndices[layer] = tempIndex;
		currentLayer += dir;
		sortingLayerIndex = oldSortingLayerIndex = sortingLayerIndices[currentLayer];
		// Update undo list to reflect changed layers
		for (var i = 0; i < undoList.Count; i++) {
			if (undoList[i].layerNumber == layer) {
				undoList[i].layerNumber = layer + dir;
			}
			else if (undoList[i].layerNumber == layer + dir) {
				undoList[i].layerNumber = layer;
			}
		}
		levelChanged = true;
	}
	
	static function SetLayerNames () {
		layerNamesControls = new GUIContent[mapLayers.Count + 3];
		for (var i = 0; i < mapLayers.Count; i++) {
			layerNamesControls[i] = GUIContent("Layer " + i, layerPopupTip);
		}
		layerNamesControls[i++] = GUIContent("____________");
		layerNamesControls[i++] = GUIContent("Delete layer");
		layerNamesControls[i++] = GUIContent("New layer");
		
		layerNames = new String[mapLayers.Count];
		for (i = 0; i < layerNames.Length; i++) {
			layerNames[i] = layerNamesControls[i].text;
		}
		if (moveToLayer >= layerNames.Length) {
			moveToLayer = layerNames.Length-1;
		}
	}
	
	static function NewLevel () {
		var isSure = true;
		if (levelChanged) {
			isSure = EditorUtility.DisplayDialog ("New level?", "Do you really want to clear this level?", "Clear level", "Cancel");
		}
		if (isSure) {
			InitializeLevel ();
		}
	}
	
	static function TileExists (t : short) : boolean {
		if (t < 0) {
			return true;
		}
		return (t >> MapData.setShift) < tileSets.Count && (t & MapData.tileShiftI) < tileSets[t >> MapData.setShift].tileData.Count;
	}
	
	static function CreateTileManager () {
		if (!Directory.Exists (Application.dataPath + "/Plugins/SpriteTile/Resources")) {
			AssetDatabase.CreateFolder ("Assets/Plugins/SpriteTile", "Resources");			
		}
		tileManager = ScriptableObject.CreateInstance (TileManager) as TileManager;
		AssetDatabase.CreateAsset (tileManager, "Assets/Plugins/SpriteTile/Resources/TileManager.asset");
		AssetDatabase.SaveAssets();
	}
	
	// Draw a box with the texture scaled to fit, since GUI.Box only scales down
	static function GUITexBox (rect : Rect, tex : Texture, texRect : Rect, ratio : Vector2) {
		GUI.Box (rect, "");
		rect = Rect(2 + rect.x + (rect.width*((1.0 - ratio.x)/2)), 2 + rect.y + (rect.width*((1.0 - ratio.y)/2)), rect.width*ratio.x - 4, rect.width*ratio.y - 4);
		GUI.DrawTextureWithTexCoords (rect, tex, texRect);
	}
	
	function OnDestroy() {
		if (levelChanged) {
			var saveLevel = EditorUtility.DisplayDialog ("Unsaved level", "The level has unsaved changes; do you want to save it before closing?", "Save level", "Close without saving");
			if (saveLevel) {
				Save (false, false);
			}
		}
		
		// Prevent leaked textures
		DestroyImmediate (lineTex);
		if (groupSets) {
			for (var i = 0; i < groupSets.Count; i++) {
				for (var j = 0; j < groupSets[i].spriteGroups.Count; j++) {
					DestroyImmediate (groupSets[i].spriteGroups[j].content.image);
				}
			}
		}
		
		if (previewContainer != null) {
			DestroyImmediate (previewContainer);
		}
		
		EditorPrefs.SetBool (basePathName + "AutoGroupLoad", autoGroupLoad);
		EditorPrefs.SetBool (basePathName + "AutoLevelLoad", autoLevelLoad);
		if (autoGroupLoad) {
			EditorPrefs.SetString (basePathName + "GroupPath", savedGroupPath);
		}
		if (autoLevelLoad) {
			EditorPrefs.SetString (basePathName + "LevelPath", savedLevelPath);
		}
		EditorPrefs.SetInt (basePathName + "PanelWidth", panelWidth);
		EditorPrefs.SetFloat (basePathName + "DivPercent", divPercent);
		EditorPrefs.SetBool (basePathName + "ShowLayerControls", showLayerControls);
		EditorPrefs.SetBool (basePathName + "ShowLightControls", showLightControls);
		EditorPrefs.SetBool (basePathName + "ShowSelectionControls", showSelectionControls);
	}
}