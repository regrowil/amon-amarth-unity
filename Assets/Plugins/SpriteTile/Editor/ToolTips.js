// Version 3.0
// ©2016 Starscene Software. All rights reserved. Redistribution of source code without permission not allowed.

#pragma strict
import System.Collections.Generic;
import SpriteTile;
import TileEditor;

enum Content {LevelSave, LevelOpen, AutoOpen, LevelImport, New, ShowTips, Undo, Redo, LayerUp, LayerDown, SyncScroll, Sorting, ShowPreceding, ShowNext, ShowAll, FillArea, LayerResize, TileSize, UseDualSize, UseSingleSize, Zpos, LayerLock, AddBorder, Cut, Copy, Fill, Erase, MoveTo, Restore, SelectAll, Deactivate, PreviewThisLayer, PreviewAllLayers, PreviewDelete, OrderFill, GridToggle, ColliderToggle, TriggerToggle, OrderToggle, GOToggle, LightToggle, InfoBox, Tint, PreviewSize, TileLoad, TileFromFolder, TileReplace, TileRefresh, SetDelete, SetNew, FromObject, ToObject, GroupSaveAs, GroupOpen, GroupAuto, GroupSetDelete, GroupSetNew, GroupFromSelection, GroupCopyToBuffer, GroupReplace, EditProperties, LightMode, DeselectLight, MakeUnique, LayerAmbient, ShadowRadiosity, LightSize, LightInner, LightOuter, LightShadows, EditCancel, EditAccept, LightAccept, PickTile}

static var tipStyle : GUIStyle;
static var tipSpace : int;
static var showTips : boolean;
static var oldShowTips : boolean;
static var content : Dictionary.<Content, GUIContent>;
static var layerPopupTip : String;
static var lightPopupTip : String;
static var mouseInMapTip : String;
static var mouseInDefaultsTip : String;
static var mouseInTilesTip : String;

static function SetToolTips () {
	if (Application.platform == RuntimePlatform.OSXEditor) {
		mouseInMapTip = "Double-click: select all of same type, Right-click/Command-click: select area, Control-click/Del: erase tile, Alt-click: set tile properties, Shift-click: draw tile line, Space+click drag: pan view";
	}
	else {
		mouseInMapTip = "Double-click: select all of same type, Right-click: select area, Control-click/Del: erase tile, Alt-click: set tile properties, Shift-click: draw tile line, Space+click drag: pan view";
	}
	mouseInDefaultsTip = "Per-tile defaults that are applied when this tile is placed in the map. Editing values in the map overrides these defaults.";
	mouseInTilesTip = "Shift-click selects range of tiles, right-click draws selection box for selecting multiple tiles. Useful for assigning defaults to multiple tiles at once, adding tiles to random groups quickly, etc.";
	layerPopupTip = "Alt-up arrow: switch to previous layer, Alt-down arrow: switch to next layer. The popup list is used to switch to a specific layer, as well as adding and deleting layers";
	lightPopupTip = "The popup list is used to select different light styles, as well as adding and deleting styles. The styles are saved with the level. Changing a style changes all lights using that style.";
	
	content = new Dictionary.<Content, GUIContent>();
	content[Content.LevelSave] = GUIContent("Save As", "Alt-S: Save level, Alt-Shift-S: Save level as");
	content[Content.LevelOpen] = GUIContent("Open", "Alt-O: Open level");
	content[Content.AutoOpen] = GUIContent("Auto", "Automatically load the last saved level whenever the TileEditor window is opened");
	content[Content.LevelImport] = GUIContent("Import", "Import Tiled editor TMX file");
	content[Content.New] = GUIContent("New", "Create a new level (deletes all layers)");
	content[Content.ShowTips] = GUIContent("Show editor tooltips", "Show information about controls, including keyboard shortcuts where available");
	content[Content.Undo] = GUIContent("Undo", "Alt-Z: Undo last action in the map");
	content[Content.Redo] = GUIContent("Redo", "Shift-Alt-Z: Revert previous undo action");
	content[Content.LayerUp] = GUIContent("˄", "Move current layer up in the layer list. (If you want to switch layers, use the layer drop-down menu instead.)");
	content[Content.LayerDown] = GUIContent("˅", "Move current layer down in the layer list. (If you want to switch layers, use the layer drop-down menu instead.)");
	content[Content.SyncScroll] = GUIContent("Sync scroll", "When active, viewport scroll position is synced between all layers (that have the same dimensions). Otherwise each layer maintains its own scroll position");
	content[Content.Sorting] = GUIContent("Sorting:", "Use the popup to assign a Unity sorting layer to this layer. Must use Assets->Set SpriteTile Sorting Layer Names menu item");
	content[Content.ShowPreceding] = GUIContent("Preceding", "Show preceding layers in addition to the current layer (layers must have the same dimensions in order to be visible)");
	content[Content.ShowNext] = GUIContent("Next", "Show next layers in addition to the current layer (layers must have the same dimensions in order to be visible)");
	content[Content.ShowAll] = GUIContent("All", "Show all layers (layers must have the same dimensions in order to be visible)");
	content[Content.FillArea] = GUIContent("Fill Area", "When active, clicking fills an area with the selected tile. Can also use Shift-F to toggle on and off");
	content[Content.LayerResize] = GUIContent("Resize", "Resizes current layer to the specified X and Y dimensions");
	content[Content.TileSize] = GUIContent("Tile size", "Size in units of each tile");
	content[Content.UseDualSize] = GUIContent(">", "Use separate X and Y dimensions for the tile size");
	content[Content.UseSingleSize] = GUIContent("<", "Use the same number for X and Y tile size");
	content[Content.Zpos] = GUIContent("Z pos", "Z position in world space for this layer (used for perspective cameras)");
	content[Content.LayerLock] = GUIContent("Lock", "Use the popup to assign a layer lock (scroll constraint) to this layer");
	content[Content.AddBorder] = GUIContent("Add border", "Add a border of this many tiles to the screen; used to prevent pop-in of oversized tiles. Only effective in play mode");
	content[Content.Cut] = GUIContent("Cut", "Alt-X: Cut selection");
	content[Content.Copy] = GUIContent("Copy", "Alt-C: Copy selection");
	content[Content.Fill] = GUIContent("Fill", "F: Fill selection with current tile or random group");
	content[Content.Erase] = GUIContent("Erase", "Delete or Backspace: Erase selection");
	content[Content.MoveTo] = GUIContent("Move To:", "Moves contents of selection to the layer specified in the popup menu. Hold shift while clicking to force empty tiles to be included");
	content[Content.Restore] = GUIContent("Restore", "Restores a deactivated selection copy or group");
	content[Content.SelectAll] = GUIContent("All", "Alt-A: Selects everything in the current layer");
	content[Content.Deactivate] = GUIContent("X", "Esc: Deactivates selection. Right-clicking without moving the mouse will also deactivate the selection");
	content[Content.PreviewThisLayer] = GUIContent("This Layer", "Show contents of the selection (for this layer only) in the scene view");
	content[Content.PreviewAllLayers] = GUIContent("All Layers", "Show contents of the selection in this layer, and the corresponding areas in other layers, in the scene view");
	content[Content.PreviewDelete] = GUIContent("Delete", "Delete scene view preview");
	content[Content.OrderFill] = GUIContent("Fill", "Using the four input fields to the left, for lower left, lower right, upper left, and upper right values, fills the selection with computed order-in-layer values");
	content[Content.GridToggle] = GUIContent("Grid", "G: toggle grid lines in the map view");
	content[Content.ColliderToggle] = GUIContent("Collider", "C: toggle collider overlay, which shows any tiles that are set to be a collider. Tiles with a physics collider are shown in a darker tint");
	content[Content.TriggerToggle] = GUIContent("Trigger", "T: toggle trigger overlay, which shows any tiles with a non-zero trigger value");
	content[Content.OrderToggle] = GUIContent("Order", "O: toggle order-in-layer overlay, which shows any tiles with a non-zero order-in-layer value");
	content[Content.GOToggle] = GUIContent("Extra", "E: toggle extra property overlay, which shows any tiles with an extra property (int, string, GameObject)");
	content[Content.LightToggle] = GUIContent("Light", "L: toggle light overlay, show shows the layer ambient color and the effect of any lights that have been placed");
	content[Content.InfoBox] = GUIContent("", "Alt-click: change tile properties of highlighted tile, Alt-< >: rotate tile 5°, Shift-< >: rotate tile 90°, < >: decrease/increase tile order-in-layer value, X: flip tile on X, Y: flip tile on Y");
	content[Content.Tint] = GUIContent("Tint", "Change color for collider/trigger overlays");
	content[Content.PreviewSize] = GUIContent("Zoom", "Alt+scroll wheel: zoom in and out");
	content[Content.TileLoad] = GUIContent("Load", "Load a single sprite, or a sprite sheet. Re-loading an existing sprite sheet (which has had sprites added to it) will add only the new sprites");
	content[Content.TileFromFolder] = GUIContent("From Folder", "Load all sprites and sprite sheets from a folder");
	content[Content.TileReplace] = GUIContent("Replace", "Replace the selected sprite with a different sprite. Only works for single sprites, not sprites that are part of a spritesheet");
	content[Content.TileRefresh] = GUIContent("Refresh", "Refreshes all sprites, so changed properties such as the sprite name, pixels-to-units, and pivot point will be updated in the TileEditor");
	content[Content.SetDelete] = GUIContent("Delete", "Deletes the currently selected tileset");
	content[Content.SetNew] = GUIContent("New", "Adds a new tileset");
	content[Content.FromObject] = GUIContent("> From Object", "For tiles with a custom physics collider. This copies the polygon or box collider from a selected GameObject in the scene to the currently selected tile.");
	content[Content.ToObject] = GUIContent("< To Object", "For tiles with a custom physics collider. This copies the polygon collider from the currently selected tile to a selected GameObject in the scene (where it can be edited and copied back, if desired.)");
	content[Content.GroupSaveAs] = GUIContent("Save As", "Saves all standard, random, and terrain groups to a file");
	content[Content.GroupOpen] = GUIContent("Open", "Loads all standard, random, and terrain groups from a file");
	content[Content.GroupAuto] = GUIContent("Auto", "Automatically load the last saved group file when the TileEditor window is opened");
	content[Content.GroupSetDelete] = GUIContent("Delete", "Deletes the currently selected group set");
	content[Content.GroupSetNew] = GUIContent("New", "Adds a new group set");
	content[Content.GroupFromSelection] = GUIContent("> Make From Selection", "Makes a new group from the current selection in the Level window or the Tiles window");
	content[Content.GroupCopyToBuffer] = GUIContent("< Copy to Buffer", "Copies the selected group to the copy buffer, where it can be used in the Level window");
	content[Content.GroupReplace] = GUIContent("Replace with Selection", "Replaces the selected group with the contents of the current selection in the Level window or the Tiles window");
	content[Content.EditProperties] = GUIContent("Edit", "Edit properties of selected tiles. To edit properties of a single tile, alt-click the desired tile in the Level window");
	content[Content.LightMode] = GUIContent("Light Mode", "When active, left click to place lights, left click on existing light selects it (can be dragged to new location), and middle click or delete key deletes lights. Can toggle with Shift-L");
	content[Content.DeselectLight] = GUIContent("X", "Esc or right-click (while light is selected): Deselect light, so that the light style can be changed without affecting the selected light");
	content[Content.MakeUnique] = GUIContent("Make Unique", "Adds a new light style that copies the selected style, and sets the selected light to the new style");
	content[Content.LayerAmbient] = GUIContent("Layer ambient", "Sets the base color for all tiles in this layer. If ambient color is white, then no lights can be visible. Light overlay must be on to see ambient effect");
	content[Content.ShadowRadiosity] = GUIContent("Shadow radiosity", "Enabled or disables the radiosity effect for all shadow-casting lights");
	content[Content.LightSize] = GUIContent("Size", "The size in tiles of the light (width x height)");
	content[Content.LightInner] = GUIContent(" Inner", "Brightness at the center of the light. Range is 0.0 - 1.0, but can be set higher than 1.0");
	content[Content.LightOuter] = GUIContent("Outer", "Brightness at the outer edge of the light. Range is 0.0 - 1.0, but can be set higher than 1.0");
	content[Content.LightShadows] = GUIContent("Shadows", "Whether the light is blocked by collider cells or not");
	content[Content.EditCancel] = GUIContent(cancelIcon, "Cancels tile edit and keeps original values");
	content[Content.EditAccept] = GUIContent(acceptIcon, "Applies edited tile properties to the selected tile or tiles");
	content[Content.LightAccept] = GUIContent(acceptIcon, "Applies light properties to the current light style. All lights using this style will be changed");
	content[Content.PickTile] = GUIContent("Pick Tile", "P: Pick a tile from the map and use that as the selected tile");
}

static function RemoveToolTips () {
	for (var item in content) {
		item.Value.tooltip = "";
	}
}