#pragma strict

class UndoList {
	var mapElementList : List.<MapElement>;
	var layerNumber : int;
	
	function UndoList (layerNumber : int) {
		this.mapElementList = new List.<MapElement>();
		this.layerNumber = layerNumber;
	}
	
	function Add (mapElement : MapElement) {
		mapElementList.Add (mapElement);
	}
}