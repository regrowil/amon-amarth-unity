﻿using System;
using UnityEngine;

public class EmptyInstanceParameters : MonoBehaviour, IInstanceParameters
{
    public bool GetBool(string key, bool defaultValue)
    {
        return defaultValue;
    }

    public float GetFloat(string key, float defaultValue)
    {
        return defaultValue;
    }

    public int GetInt(string key, int defaultValue)
    {
        return defaultValue;
    }
}