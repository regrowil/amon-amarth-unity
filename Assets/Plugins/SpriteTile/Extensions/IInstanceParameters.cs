﻿public interface IInstanceParameters
{
    bool GetBool(string key, bool defaultValue);
    float GetFloat(string key, float defaultValue);
    int GetInt(string key, int defaultValue);
}