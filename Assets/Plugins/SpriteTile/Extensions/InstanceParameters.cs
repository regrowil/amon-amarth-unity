﻿using UnityEngine;

public class InstanceParameters : MonoBehaviour, IInstanceParameters
{
    private SerializableDictionary<string, float> dictionary;

    public bool GetBool(string key, bool defaultValue)
    {
        float value = defaultValue ? 1.0f : 0.0f;
        dictionary.TryGetValue(key, out value);
        return value > 0.0f;
    }

    public float GetFloat(string key, float defaultValue)
    {
        float value = defaultValue;
        dictionary.TryGetValue(key, out value);
        return value;
    }

    public int GetInt(string key, int defaultValue)
    {
        float value = defaultValue;
        dictionary.TryGetValue(key, out value);
        return (int)value;
    }

    public void ReadFromJson(string json)
    {
        dictionary = JsonUtility.FromJson<SerializableDictionary<string, float>>(json);
    }
}