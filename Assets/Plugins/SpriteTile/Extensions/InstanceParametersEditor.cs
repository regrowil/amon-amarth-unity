﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class InstanceParametersEditor : EditorWindow
{
    private SerializableDictionary<string, float> dictionary;
    private SpriteTile.PropertyInfo propertyInfo;
    private List<FieldInfo> instanceParameters;

    public void Initialize(SpriteTile.PropertyInfo propertyInfo)
    {
        this.propertyInfo = propertyInfo;
        dictionary = new SerializableDictionary<string, float>();
        if (!string.IsNullOrEmpty(propertyInfo.s))
        {
            EditorJsonUtility.FromJsonOverwrite(propertyInfo.s, dictionary);
        }
        FindInstanceParameters();
    }

    void OnGUI()
    {
        DrawEditor();
    }

    private void FindInstanceParameters()
    {
        instanceParameters = new List<FieldInfo>();
        GameObject gameObject = propertyInfo.go;
        foreach (MonoBehaviour component in gameObject.GetComponentsInChildren<MonoBehaviour>())
        {
            FieldInfo[] fields = component.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                if (field.IsDefined(typeof(InstanceParameter), false))
                {
                    instanceParameters.Add(field);
                }
            }
        }
    }

    private void DrawEditor()
    {
        DrawParameterInputs();
        DrawSaveButton();
    }

    private void DrawParameterInputs()
    {
        foreach (FieldInfo instanceParameter in instanceParameters)
        {
            DrawParameterInput(instanceParameter);
        }
    }

    private void DrawParameterInput(FieldInfo parameter)
    {
        string key = parameter.Name;
        float currentValue = 0.0f;
        dictionary.TryGetValue(key, out currentValue);
        if (!dictionary.ContainsKey(key))
        {
            currentValue = GetDefaultValue(parameter);
        }
        Type type = parameter.FieldType;
        if (type == typeof(float))
        {
            dictionary[key] = EditorGUILayout.FloatField(key, currentValue);
        }
        else if (type == typeof(int))
        {
            dictionary[key] = EditorGUILayout.IntField(key, (int)currentValue);
        }
        else if (type == typeof(bool))
        {
            dictionary[key] = EditorGUILayout.Toggle(key, currentValue > 0.0f) ? 1.0f : 0.0f;
        }
    }

    private void DrawSaveButton()
    {
        if (GUILayout.Button("Save"))
        {
            propertyInfo.s = EditorJsonUtility.ToJson(dictionary);
            Close();
        }
    }

    private float GetDefaultValue(FieldInfo field)
    {
        switch (field.Name)
        {
            case "movementSpeed":
                return 1;
            case "isFacingRight":
                return 1;
            case "isStationary":
                return 0;
            case "maxHP":
                return 30;
            default:
                return 0;
        }
    }
}
#endif