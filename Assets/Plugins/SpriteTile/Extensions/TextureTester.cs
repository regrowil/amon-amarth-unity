﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using SpriteTile;
using UnityEngine;

public class TextureTester : MonoBehaviour
{
    public static bool NonEmptyHighlightIsEnabled;
    public bool NonEmptyHighlightEnabled;

    public static bool IsEnabled;
    public bool ModuleEnabled;


    public static int LayerInt;
    public int Layer;

    private void OnValidate()
    {
        NonEmptyHighlightIsEnabled = NonEmptyHighlightEnabled;
        IsEnabled = ModuleEnabled;
        LayerInt = Layer;
        Reset();
    }

    void OnEnable()
    {
        gameObject.hideFlags = HideFlags.DontSaveInBuild;
    }


    public static Dictionary<Rect, bool> BadPlaced = new Dictionary<Rect, bool>();
    public static List<Int2> ToDelete = new List<Int2>();

    [ContextMenu("Reset")]
    public void ResetContext()
    {
        Reset();
    }

    public static void SetLayer(int i)
    {
        LayerInt = i;
        var tt = FindObjectOfType<TextureTester>();
        if(tt==null) return;
        
        tt.Layer = i;
    }

    public static void Reset()
    {
        BadPlaced = new Dictionary<Rect, bool>();
        ToDelete = new List<Int2>();
    }

    public static void AddToDelete(Int2 pos)
    {
        ToDelete.Add(pos);
    }

    public static List<Int2> GetToDelete()
    {
        return ToDelete;

    }

    [ContextMenu("DeleteAll")]
    public void DeleteAll()
    {
        
        for (int i = 0; i < ToDelete.Count; i++)
        {
            Tile.SetTile(ToDelete[i],LayerInt, 0, -1);
        }
        Reset();
    }

    public static bool IsCorrectTexture2DRect(Rect pos,Texture2D texture2d, Rect textureRect,Int2 mapPos)
    {
        if (!IsEnabled) return true;
        if (BadPlaced.ContainsKey(pos)) return BadPlaced[pos];
        var newTex = new Texture2D(texture2d.width, texture2d.height, texture2d.format, false);
        newTex.alphaIsTransparency = true;
        newTex.LoadRawTextureData(texture2d.GetRawTextureData());
        newTex.Apply();

        var x = newTex.GetPixels(Mathf.FloorToInt(textureRect.x * texture2d.width), Mathf.FloorToInt(textureRect.y * texture2d.height), 32, 32);

        bool t = false;
        for (int index = 0; index < x.Length; index++)
        {
            var color = x[index];

            if (color.a > 0)
            {
                t = true;
            }
        }
        BadPlaced.Add(pos,t);
        if(!t)AddToDelete(mapPos);
        return t;
    }


    public static bool IsCorrectTexture2D(Texture2D texture2d)
    {
        var newTex = new Texture2D(texture2d.width, texture2d.height, texture2d.format, false);
        newTex.alphaIsTransparency = true;
        newTex.LoadRawTextureData(texture2d.GetRawTextureData());
        newTex.Apply();

        var x = newTex.GetPixels();

        bool t = false;
        for (int index = 0; index < x.Length; index++)
        {
            var color = x[index];

            if (color.a > 0)
            {
                t = true;
            }
        }
        return t;
    }

    public static bool IsCorrectSprite(Sprite sprite)
    {
        var newTex = new Texture2D(sprite.texture.width, sprite.texture.height, sprite.texture.format, false);
        newTex.alphaIsTransparency = true;
        newTex.LoadRawTextureData(sprite.texture.GetRawTextureData());
        newTex.Apply();

        var x = newTex.GetPixels(Mathf.FloorToInt(sprite.textureRect.x), Mathf.FloorToInt(sprite.textureRect.y), 32, 32);

        bool t = false;
        for (int index = 0; index < x.Length; index++)
        {
            var color = x[index];

            if (color.a > 0)
            {
                t = true;
            }
        }
        return t;
    }
}
#endif