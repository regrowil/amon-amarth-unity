﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuntimeVersionInfo : MonoBehaviour
{

    public Text VersionText;
    public string Format = "{0} v.{1}";
    
    void Start()
    {
        var flags = "";
#if DEBUG
        flags += "dev";
#endif
        VersionText.text = string.Format(Format, flags, Application.version);

    }


}
