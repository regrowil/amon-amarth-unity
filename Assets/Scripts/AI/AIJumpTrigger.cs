﻿using UnityEngine;

public class AIJumpTrigger : MonoBehaviour
{
    [InstanceParameter]
    [SerializeField]
    private float jumpForceX;
    [InstanceParameter]
    [SerializeField]
    private float jumpForceY;
    [InstanceParameter]
    [SerializeField]
    private bool randomize;


    void Start()
    {
        ReadInstanceParameters();
    }

    private void ReadInstanceParameters()
    {
        jumpForceX = InstanceParameters.GetFloat("jumpForceX", jumpForceX);
        jumpForceY = InstanceParameters.GetFloat("jumpForceY", jumpForceY);
        randomize = InstanceParameters.GetBool("randomize", randomize);
    }

    private IInstanceParameters instanceParameters;
    protected IInstanceParameters InstanceParameters
    {
        get
        {
            if (instanceParameters == null)
            {
                instanceParameters = GetComponent<IInstanceParameters>();
                if (instanceParameters == null)
                {
                    instanceParameters = gameObject.AddComponent<EmptyInstanceParameters>();
                }
            }
            return instanceParameters;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        AIDispatcher AIDispatcher = other.GetComponent<AIDispatcher>();
        CharacterJumping characterJumping = other.GetComponent<CharacterJumping>();
        CharacterMovement characterMovement = other.GetComponent<CharacterMovement>();
        if (AIDispatcher != null && characterJumping != null)
        {
            if (randomize && Random.value < 0.5f) return; 
            if (Mathf.Sign(jumpForceX) != Mathf.Sign(characterMovement.isFacingRight?1:-1)) return;
            characterJumping.JumpForce = new Vector2(jumpForceX- characterMovement.movementSpeed*(characterMovement.isFacingRight?1:-1), jumpForceY);
            AIDispatcher.CommandRequested.Invoke(Command.Jump);
        }
        
    }
}