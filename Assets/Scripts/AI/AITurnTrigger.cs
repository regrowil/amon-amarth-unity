﻿using UnityEngine;

public class AITurnTrigger : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        AIDispatcher AIDispatcher = other.GetComponent<AIDispatcher>();
        if (AIDispatcher != null)
        {
            AIDispatcher.CommandRequested.Invoke(Command.Turn);
        }
    }
}