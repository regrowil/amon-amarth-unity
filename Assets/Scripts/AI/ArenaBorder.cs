﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaBorder : MonoBehaviour {
    [InstanceParameter]
    public bool IsRightBorder;

    private void ReadInstanceParameters()
    {
        IsRightBorder = InstanceParameters.GetBool("IsRightBorder", IsRightBorder);
    }

    public void Start()
    {
        ReadInstanceParameters();
    }


    private IInstanceParameters instanceParameters;
    protected IInstanceParameters InstanceParameters
    {
        get
        {
            if (instanceParameters == null)
            {
                instanceParameters = GetComponent<IInstanceParameters>();
                if (instanceParameters == null)
                {
                    instanceParameters = gameObject.AddComponent<EmptyInstanceParameters>();
                }
            }
            return instanceParameters;
        }
    }
}
