﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HelBoss : Boss
{
    public static VoidEvent BossAttack = new VoidEvent();
    public  VoidEvent OnSummon = new VoidEvent();

    public List<GameObject> Minions = new List<GameObject>();
    public Vector3 LeftPos;
    public Vector3 RightPos;
    public Camera cam;
    public Ray LeftRay;
    public Ray RightRay;

    public Vector3 LeftGroundPosition;
    public Vector3 RightGroundPosition;

    public GameObject SkeletonPrefab;
	public GameObject DraugrPrefab;

    public Animator Anim;
    public CharacterHP BossHP;

    public float SkeletonHeight = 1.04f;

    public Vector3 LeftArenaBorder;
    public Vector3 RightArenaBorder;

    public Collider2D HitCollider;

    public SpriteRenderer HelRenderer;

    private List<ArenaBorder> borders; 
    public void Start()
    {
        cam = Camera.main;
        BossAttack.AddListener(SpawnMinions);

        borders = GameObject.FindObjectsOfType<ArenaBorder>().ToList();
        if (borders.Count > 2)
        {
            Debug.LogWarning("There is too much arena Borders on This Map");
        }
    }

    [ContextMenu("Spawn Minions")]
    public void SpawnMinions()
    {
        LeftPos = cam.ViewportToWorldPoint(new Vector3(-0.1f, 0.5f));
        RightPos = cam.ViewportToWorldPoint(new Vector3(1.1f, 0.5f));

        if (borders.Count == 2 && (LeftArenaBorder==Vector3.zero || RightArenaBorder==Vector3.zero))
        {
            LeftArenaBorder = borders.First(border => border.IsRightBorder == false).transform.position;
            RightArenaBorder = borders.First(border => border.IsRightBorder == true).transform.position;
        }

        if (LeftArenaBorder != Vector3.zero)
        {
            LeftPos = new Vector3(Mathf.Max(LeftPos.x, LeftArenaBorder.x), LeftPos.y);
        }
        if (RightArenaBorder != Vector3.zero)
        {
            RightPos = new Vector3(Mathf.Min(RightPos.x, RightArenaBorder.x), RightPos.y);

        }

        LeftRay = new Ray(LeftPos, Vector3.down);
        RightRay = new Ray(RightPos, Vector3.down);

        LeftGroundPosition = Physics2D.Raycast(LeftRay.origin, LeftRay.direction).point;
        RightGroundPosition = Physics2D.Raycast(RightRay.origin, RightRay.direction).point;

        OnSummon.Invoke();

		//skeletons
        var skeletonLeft = Instantiate(SkeletonPrefab, new Vector3(LeftGroundPosition.x, LeftGroundPosition.y + (SkeletonHeight / 2f)), Quaternion.identity);
        var skeletonRight = Instantiate(SkeletonPrefab, new Vector3(RightGroundPosition.x, RightGroundPosition.y + (SkeletonHeight / 2f)), Quaternion.identity);
		skeletonLeft.GetComponent<EnemyScore>().setScore(0);
		skeletonRight.GetComponent<EnemyScore>().setScore(0);

        Minions.Add(skeletonLeft);
        Minions.Add(skeletonRight);

		skeletonRight.GetComponent<CharacterMovement>().isFacingRight = false;
		skeletonLeft.GetComponent<CharacterHP>().CharacterDied.AddListener(() => { RemoveMinion(skeletonLeft); });
		skeletonRight.GetComponent<CharacterHP>().CharacterDied.AddListener(() => { RemoveMinion(skeletonRight); });

		//draugr
		int rand = Random.Range(1,10);
		if(rand <= 5){
			var DraugrLeft = Instantiate(DraugrPrefab, new Vector3(LeftGroundPosition.x, LeftGroundPosition.y + (SkeletonHeight / 2f)), Quaternion.identity);
			DraugrLeft.GetComponent<EnemyScore>().setScore(0);
			Minions.Add(DraugrLeft);
			DraugrLeft.GetComponent<CharacterHP>().CharacterDied.AddListener(() => { RemoveMinion(DraugrLeft); });
		}else if(rand > 5){
			var DraugrRight = Instantiate(DraugrPrefab, new Vector3(RightGroundPosition.x, RightGroundPosition.y + (SkeletonHeight / 2f)), Quaternion.identity);
			DraugrRight.GetComponent<EnemyScore>().setScore(0);
			DraugrRight.GetComponent<CharacterMovement>().isFacingRight = false;
			Minions.Add(DraugrRight);
			DraugrRight.GetComponent<CharacterHP>().CharacterDied.AddListener(() => { RemoveMinion(DraugrRight); });
		}

        if (Minions.Count > 0)
        {
            MakeInVurnerable();
        }
    }

    public void Update()
    {
        LookAtThor();
    }

    public void LookAtThor()
    {
        var playerPos = Player.Instance.transform.position;
        HelRenderer.flipX = playerPos.x < transform.position.x;
    }

    public void MakeVurnerable()
    {
        Anim.SetBool("CanBeHurt", true);
        BossHP.IsInvulnerable = false;
        HitCollider.enabled = true;
    }

    public void MakeInVurnerable()
    {
        Anim.SetBool("CanBeHurt", false);
        BossHP.IsInvulnerable = true;
        HitCollider.enabled = false;
    }

    public void RemoveMinion(GameObject minion)
    {
		Minions.Remove(minion);
        if (Minions.Count == 0)
        {
            MakeVurnerable();
        }
    }

    //    private void OnDrawGizmos()
    //    {
    //        Gizmos.DrawSphere(LeftPos, 0.5f);
    //        Gizmos.DrawSphere(RightPos, 0.5f);
    //
    //
    //        Gizmos.DrawLine(LeftPos,LeftGroundPosition);
    //        Gizmos.DrawLine(RightPos,RightGroundPosition);
    //
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawSphere(LeftGroundPosition, 0.2f);
    //        Gizmos.DrawSphere(RightGroundPosition, 0.2f);
    //    }

}