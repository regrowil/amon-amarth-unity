﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NidhoggAttacks : MonoBehaviour {

    public VoidEvent OnAttack = new VoidEvent();

    public void AttackTop()
    {
        NidhoggBoss.NidhoggTopAttack.Invoke();
        OnAttack.Invoke();
    }

    public void AttackMiddle()
    {
        NidhoggBoss.NidhoggMiddleAttack.Invoke();
        OnAttack.Invoke();
    }

    public void AttackBottom()
    {
        NidhoggBoss.NidhoggBottomAttack.Invoke();
        OnAttack.Invoke();
    }

}
