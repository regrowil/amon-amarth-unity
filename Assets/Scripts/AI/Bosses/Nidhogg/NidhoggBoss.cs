﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NidhoggBoss : Boss
{
    public static VoidEvent NidhoggIsDead = new VoidEvent();
    public static VoidEvent NidhoggTopAttack = new VoidEvent();
    public static VoidEvent NidhoggMiddleAttack = new VoidEvent();
    public static VoidEvent NidhoggBottomAttack = new VoidEvent();


    public Transform TopSpawnPoint;
    public Transform MiddleSpawnPoint;
    public Transform BottomSpawnPoint;

    public GameObject NidhoggFirePrefab;


    public void Start()
    {
        NidhoggIsDead.AddListener(CompleteLevel);
        NidhoggTopAttack.AddListener(()=>Attack(2));
        NidhoggMiddleAttack.AddListener(()=>Attack(1));
        NidhoggBottomAttack.AddListener(()=>Attack(0));
    }


    public Animator Animator;

    public void Update()
    {
        if (!Animator.GetBool("StartAnimationDone") || Animator.GetBool("IsDead"))
        {
            return;
        }


        var pos = Player.Instance.transform.position;

        var t = Mathf.Abs(TopSpawnPoint.position.y - pos.y);
        var m = Mathf.Abs(MiddleSpawnPoint.position.y - pos.y);
        var b = Mathf.Abs(BottomSpawnPoint.position.y - pos.y);

        if (t < m)
        {
            Animator.SetInteger("HeightPosition", 2);
        }

        if (m < t && m < b)
        {
            Animator.SetInteger("HeightPosition", 1);
        }
        if (b < m)
        {
            Animator.SetInteger("HeightPosition", 0);
        }

    }


    private void Attack(int height)
    {
        Vector3 position = Vector3.zero;

        switch (height)
        {
            case 0:
                position = BottomSpawnPoint.position;
                break;
            case 1:
                position = MiddleSpawnPoint.position;

                break;
            case 2:
                position = TopSpawnPoint.position;

                break;
        }

        Instantiate(NidhoggFirePrefab, position, Quaternion.identity);

    }

    public void Death()
    {
        Debug.Log("#Nidhogg# is Dead");
        Animator.SetBool("IsDead", true);
    }
}
