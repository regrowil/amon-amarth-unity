﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NidhoggDead : BossAIState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        
        NidhoggBoss.NidhoggIsDead.Invoke();

        animator.SetBool("IsDead",false);

    }
}
