﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NighoggFire : MonoBehaviour {

    public float Speed;

    public Rigidbody2D Rigidbody;
    public Animator Animator;

    // Use this for initialization
    void Start () {
        Rigidbody.velocity = Vector2.left * Speed;
	}

    public void Explode()
    {
        StopAllCoroutines();

        Animator.SetTrigger("Explode");
        Rigidbody.velocity = Vector3.zero;
    }


 
}
