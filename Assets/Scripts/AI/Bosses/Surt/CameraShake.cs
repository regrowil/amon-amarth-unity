﻿using UnityEngine;
using UnityEngine.Events;

public class CameraShake : MonoBehaviour
{
    [SerializeField]
    private float strength = 0.25f;
    [SerializeField]
    private float decay = 1.0f;

    public Camera shakeCamera;
    private Vector3 originalPosition;
    private float currentShake;

    public VoidEvent OnShake;

    void Awake()
    {
        shakeCamera = Camera.main;
        
        originalPosition = shakeCamera.transform.position;
        SurtBoss.ShakeScreenEvent.AddListener(Shake);
    }

    private void Update()
    {
        ApplyDisplacement();
        ApplyDecay();
    }

    private void ApplyDisplacement()
    {
        if (currentShake > 0.0f)
        {
            Vector3 randomDisplacement = Random.insideUnitCircle * currentShake;
            shakeCamera.transform.position = originalPosition + randomDisplacement;
        }
    }

    private void ApplyDecay()
    {
        currentShake -= decay * Time.deltaTime;
    }
    [ContextMenu("Shake")]
    public void Shake()
    {
        originalPosition = shakeCamera.transform.position;
        currentShake = Mathf.Max(currentShake + strength, strength);
        if (OnShake != null) OnShake.Invoke();
    }


    
}