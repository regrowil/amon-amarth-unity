﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SurtBoss : Boss
{

    public static VoidEvent ShakeScreenEvent = new VoidEvent();
    public static VoidEvent SurtIsDead = new VoidEvent();

    public static VoidEvent SurtIsVurnerable = new VoidEvent();
    public static VoidEvent SurtIsInVurnerable = new VoidEvent();

    public VoidEvent OnScream = new VoidEvent();

    public Animator Animator;

    private GroundCheck playerGroundCheck;
    private CharacterHP playerCharacterHP;
    private CharacterHP bossHP;

    public void Start()
    {
        playerGroundCheck = Player.Instance.GetComponent<GroundCheck>();
        playerCharacterHP = Player.Instance.GetComponent<CharacterHP>();
        bossHP = GetComponent<CharacterHP>();

        SurtIsVurnerable.AddListener(() => { bossHP.IsInvulnerable = false; OnScream.Invoke();});
        SurtIsInVurnerable.AddListener(() => { bossHP.IsInvulnerable = true; });
        ShakeScreenEvent.AddListener(DealDamage);
        SurtIsDead.AddListener(CompleteLevel);
        SurtIsDead.AddListener(ShakeScreenEvent.Invoke);
    }

    public void DealDamage()
    {
        if (!Animator.GetBool("IsDead") && playerGroundCheck.IsGrounded)
        {
            playerCharacterHP.TakeDamage(GetComponentInChildren<DamageSource>());
        }
    }

    public void Death()
    {
        Animator.SetBool("IsDead", true);
    }
}
