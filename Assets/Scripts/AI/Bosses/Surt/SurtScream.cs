﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurtScream : BossAIState
{
   
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        SurtBoss.SurtIsVurnerable.Invoke();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        SurtBoss.SurtIsInVurnerable.Invoke();

    }

}
