﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurtSmash : BossAIState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        SurtBoss.ShakeScreenEvent.Invoke();

    }
}
