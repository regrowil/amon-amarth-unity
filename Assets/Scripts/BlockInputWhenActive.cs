﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockInputWhenActive : MonoBehaviour
{
    public bool ReverseLogic;
    private PlayerDispatcher Dispatcher;

    public void OnEnable()
    {
        Dispatcher = Player.Instance.GetComponent<PlayerDispatcher>();
        Dispatcher.ControlBlockToggled.Invoke(!ReverseLogic);
    }

    public void OnDisable()
    {
        Dispatcher.ControlBlockToggled.Invoke(ReverseLogic);
    }
}

