﻿using SpriteTile;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Vector2 offset;
    [SerializeField]
    private float speed = 1.0f;

    private Transform playerTransform;

    public float MinX, MaxX;
	public float MinY, MaxY;

    void Awake()
    {
        playerTransform = Player.Instance.transform;
    }

    void Start()
    {
        transform.position = TargetPosition;
    }

    public void UpdateRange(int mapSize)
    {
        float  mapAspect = 6.75f * GetComponent<Camera>().aspect;
        MaxX = mapSize-mapAspect-1;
        MinX = mapAspect;
    }

    void Update()
    {
        var cameraPosition = Vector3.Lerp(transform.position, TargetPosition, speed * Time.deltaTime);
        cameraPosition.x = Mathf.Clamp(cameraPosition.x, MinX, MaxX);
		cameraPosition.y = Mathf.Clamp(cameraPosition.y, MinY, MaxY);

        transform.position = cameraPosition;
    }

    private Vector3 TargetPosition
    {
        get
        {
            Vector3 targetPosition = playerTransform.position + (Vector3)offset;
            targetPosition.z = transform.position.z;
            return targetPosition;
        }
    }
}