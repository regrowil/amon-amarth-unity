﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ChapterClickArea : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IDragHandler, IBeginDragHandler
{

    public UnityAction Click;
    public UnityAction Up;
    public UnityAction Down;
    public UnityAction Drag;
    public UnityAction BeginDrag;

    public void SetUp(UnityAction down, UnityAction up, UnityAction click, UnityAction beginDrag, UnityAction drag)
    {
        Down = down;Up = up;Click = click;BeginDrag = beginDrag;Drag = drag;
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        if (Down != null)
        Down.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (Up != null)
            Up.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Click != null)
            Click.Invoke();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Drag != null)
            Drag.Invoke();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (BeginDrag != null)
            BeginDrag.Invoke();
    }
}
