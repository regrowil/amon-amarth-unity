﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BodyPart : MonoBehaviour
{

    private UnityEvent OnGroundHit = new UnityEvent();

    public  Rigidbody2D Rigidbody;


    public void SubscribeOnCollisionEnter2D(UnityAction action)
    {
        OnGroundHit.AddListener(action);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(OnGroundHit!=null)
        OnGroundHit.Invoke();
        StartCoroutine(ForceStop());
    }

    private IEnumerator ForceStop()
    {
        yield return new WaitForSeconds(2);
        Rigidbody.velocity = Vector2.zero;

    }
}
