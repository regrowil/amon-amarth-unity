﻿using UnityEngine;

public class CharacterAttackAnimation : MonoBehaviour
{
	public VoidEvent CharacterAttacked;
	private const string attackParameterKey = "Attack";
	private Animator animator;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	private bool visible;

	private void OnBecameInvisible()
	{
		visible = false;
	}

	private void OnBecameVisible()
	{
		visible = true;

	}

	void Update()
	{
		if (!visible) return;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(!gameObject.transform.root.gameObject.gameObject.GetComponent<CharacterHP>().IsDead){
			if(other.transform.root.gameObject.name == "Player" && !other.transform.root.gameObject.GetComponent<CharacterHP>().IsDead && !animator.GetBool(attackParameterKey)){ 
				animator.SetTrigger(attackParameterKey);
				CharacterAttacked.Invoke();
			}
		}
	}
}