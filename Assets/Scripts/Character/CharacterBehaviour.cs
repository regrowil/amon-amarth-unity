﻿using UnityEngine;

public class CharacterBehaviour : MonoBehaviour
{
    private CharacterDispatcher dispatcher;
    private IInstanceParameters instanceParameters;

    protected CharacterDispatcher Dispatcher
    {
        get
        {
            if (dispatcher == null)
            {
                dispatcher = GetComponentInParent<CharacterDispatcher>();
            }
            return dispatcher;
        }
    }

    protected IInstanceParameters InstanceParameters
    {
        get
        {
            if (instanceParameters == null)
            {
                instanceParameters = GetComponent<IInstanceParameters>();
                if (instanceParameters == null)
                {
                    instanceParameters = gameObject.AddComponent<EmptyInstanceParameters>();
                }
            }
            return instanceParameters;
        }
    }
}