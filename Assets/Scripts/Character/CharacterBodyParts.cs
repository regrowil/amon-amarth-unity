﻿using UnityEngine;
using UnityEngine.Events;

public class CharacterBodyParts : MonoBehaviour
{
    [SerializeField]
    private float force = 1.0f;
    [SerializeField]
    private float spreadAngle = 0.0f;

    public UnityEvent OnGroundHit;

    void Awake()
    {
        ExplodeBodyParts();

    }

    private void ExplodeBodyParts()
    {
        Rigidbody2D[] rigidBodies2D = GetComponentsInChildren<Rigidbody2D>();
        for (int i = 0; i < rigidBodies2D.Length; i++)
        {
            float angle = Mathf.Lerp(-spreadAngle, spreadAngle, i / (rigidBodies2D.Length - 1.0f));
            ApplyRandomForce(rigidBodies2D[i], angle);
            rigidBodies2D[i].GetComponent<BodyPart>().SubscribeOnCollisionEnter2D(OnGroundHit.Invoke);
        }
    }

    private void ApplyRandomForce(Rigidbody2D rigidBody2D, float angle)
    {
        Vector2 direction = -Physics2D.gravity.normalized;
        direction = Quaternion.AngleAxis(angle, transform.forward) * direction;
        rigidBody2D.AddForce(direction * force, ForceMode2D.Impulse);
        rigidBody2D.gravityScale = 1.3f;
    }
}