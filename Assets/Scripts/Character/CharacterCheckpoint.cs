﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterCheckpoint : MonoBehaviour
{

    public string RunestoneTag;
    public ParticleSystem LifeRegen;

    //    public void Start()
    //    {
    //    }
    [ContextMenu("Move To Checkpoint")]
    public void MoveToCheckpoint()
    {
        transform.position = Runestone.ActiveRunestonePos + Vector3.up * 0.3f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == RunestoneTag)
        {
            var rStone = collision.GetComponent<Runestone>();
            if (Runestone.ActiveRunestone != rStone)
            {
                var characterHP = GetComponent<CharacterHP>();

                 
                LifeRegen.Emit(characterHP.MaxHP - characterHP.CurrentHP);

            }
			if(!Runestone.ActiveRunestone.Default)
			{
				PlayerScore.IsActiveRunestoneDefault = false;
			}
                rStone.Activate();
            

        }
    }


}
