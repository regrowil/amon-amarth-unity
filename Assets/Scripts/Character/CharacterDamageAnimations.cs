﻿using UnityEngine;

public class CharacterDamageAnimations : MonoBehaviour
{
    private const string tookDamageKey = "TookDamage";

    [SerializeField]
    private float blinkDuration = 1.0f;
    [SerializeField]
    private int blinkLoops = 1;

    private Animator animator;
    private CharacterHP characterHP;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        animator = GetComponent<Animator>();
        characterHP = GetComponentInParent<CharacterHP>();
        characterHP.CharacterTookDamage.AddListener(OnCharacterTookDamage);
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    private void Blink()
    {
        LeanTween.cancel(gameObject);
        spriteRenderer.color = Color.white;
        LeanTween.alpha(gameObject, 0.0f, blinkDuration).setLoopPingPong(blinkLoops);
    }
    
    private void OnCharacterTookDamage()
    {
        animator.SetTrigger(tookDamageKey);
        Blink();
    }
}