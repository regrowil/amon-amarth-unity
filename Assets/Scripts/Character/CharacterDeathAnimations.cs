﻿using UnityEngine;

public class CharacterDeathAnimations : MonoBehaviour
{
    private const string isDeadParameterKey = "IsDead";

    [SerializeField]
    private Transform characterSoulPrefab;
    [SerializeField]
    private Transform characterBodyPartsPrefab;

    private Animator animator;
    private CharacterHP characterHP;
    private Rigidbody2D rigidBody2D;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        animator = GetComponent<Animator>();
        characterHP = GetComponentInParent<CharacterHP>();
        characterHP.CharacterDied.AddListener(OnCharacterDied);
        rigidBody2D = GetComponentInParent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnCharacterDied()
    {
        animator.SetBool(isDeadParameterKey, true);
        SpawnSoul();
        if (characterHP.IsExploded)
        {
            ExplodeBody();
        }
        else
        {
            EnableRagDoll();
        }
    }

    private void SpawnSoul()
    {
        if (characterSoulPrefab != null)
        {
            Instantiate(characterSoulPrefab, transform.position, Quaternion.identity);
        }
    }

    private void ExplodeBody()
    {
        if (characterBodyPartsPrefab != null)
        {
            rigidBody2D.gravityScale = 0.0f;
            spriteRenderer.enabled = false;
            Instantiate(characterBodyPartsPrefab, transform.position, Quaternion.identity);
        }
    }

    private void EnableRagDoll()
    {
        rigidBody2D.gravityScale = 1.0f;
        rigidBody2D.drag = 2;
    }
}