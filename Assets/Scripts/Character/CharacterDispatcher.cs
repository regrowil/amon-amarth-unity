﻿using UnityEngine;

public class CharacterDispatcher : MonoBehaviour
{
    public CommandEvent CommandRequested;
    public BoolEvent ControlBlockToggled;
}