﻿using System.Collections;
using UnityEngine;
using Firebase.Analytics;

public class CharacterHP : CharacterBehaviour
{
    public VoidEvent CharacterTookDamage;
    public VoidEvent CharacterDied;
	public VoidEvent CharacterExploded;
	public bool isBoss;

    [InstanceParameter]
    [SerializeField]
    private int maxHP;
    [SerializeField]
    private float blockControlDuration;

    public bool IsInvulnerable;

    [SerializeField] private ParticleSystem takeDamageParticle;
	[SerializeField] private ParticleSystem DeathParticle;
	[SerializeField] private ParticleSystem loseHPParticle;

    private PlayerLightning power;

    void Start()
    {
        ReadInstanceParameters();
        CurrentHP = maxHP;
        power = GetComponent<PlayerLightning>();
		//Debug.Log("name: "+this.gameObject.name+" , HP: "+CurrentHP);    
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        DamageSource damageSource = other.GetComponent<DamageSource>();

        if(IsInvulnerable)return;

        if (damageSource != null && damageSource.IsEnabled && !damageSource.IsDamageOverTime && !IsDead)
        {
            TakeDamage(damageSource);
            var thisMovement = GetComponent<CharacterMovement>();
            var otherMovement = other.GetComponentInParent<CharacterMovement>();
            if (thisMovement != null && thisMovement.isStationary) thisMovement.isStationary = false;
            if (otherMovement!= null && otherMovement.isStationary) otherMovement.isStationary = false;
        }
        else if (damageSource != null && damageSource.IsDamageOverTime)
        {
            StartCoroutine(Shock(1.75f));
            CharacterTookDamage.Invoke();

        }
        if (damageSource != null)
        {
            var fire = damageSource.GetComponent<NighoggFire>();
            if (fire != null)
            {
                fire.Explode();
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        DamageSource damageSource = other.GetComponent<DamageSource>();

        if (IsInvulnerable) return;


        if (damageSource != null && damageSource.IsEnabled && damageSource.IsDamageOverTime && !IsDead)
        {
            TakeDamage(damageSource,true);
        }
    }

    public int CurrentHP { get; set; }

    public int MaxHP { get { return maxHP; } }

    public bool IsDead { get; private set; }

    public bool IsExploded { get; private set; }

    private void ReadInstanceParameters()
    {
        maxHP = InstanceParameters.GetInt("maxHP", maxHP);
    }

    public void TakeDamage(DamageSource damageSource,bool overTime=false)
    {
		if(!isBoss){
        	IsExploded = damageSource.CausesExplosion;
		}
        CurrentHP = Mathf.Max(CurrentHP - damageSource.Damage, 0);

		if (takeDamageParticle && CurrentHP > 0) {
            takeDamageParticle.Play();
		}

		if (DeathParticle && CurrentHP <= 0) {
			DeathParticle.Play();
		}

        if (loseHPParticle != null)
        {
            loseHPParticle.Emit(damageSource.Damage);
        }

        if (CurrentHP == 0)
        {
			IsDead = true;
			PlayerScore pScore = GetComponent<PlayerScore>();
			if(pScore != null)
			{
				PlayerScore.TimerPaused = true;
				Parameter[] DeathParameters = 
				{
					new Parameter("Checkpoints_Reached", Player.HowManyCheckpointsReached),
					new Parameter(FirebaseAnalytics.ParameterLocation,"x: "+ transform.position.x + " y: "+ transform.position.y),
					new Parameter(FirebaseAnalytics.ParameterLevel,LevelManager.Instance.LevelID)
				};
				FirebaseAnalytics.LogEvent("Player_Died",DeathParameters);

				if(Runestone.ActiveRunestone.Default==false)
				{
					Player.HasDiedOnce = true;
				}
				Player.HowManyCheckpointsReached--;
			}
			DisableDamageSources();
            Dispatcher.ControlBlockToggled.Invoke(true);
            CharacterDied.Invoke();
			if(IsExploded){
				CharacterExploded.Invoke();
			}
       	}
    	else
        {
            if (blockControlDuration > 0.0f)
            {
                StartCoroutine(BlockControlCoroutine());
            }
            if(!overTime)
            CharacterTookDamage.Invoke();
        }
    }


    private IEnumerator Shock(float time)
    {
		if(!isBoss){
	//      yield break;//TODO remove it
	        var thisMovement = GetComponent<CharacterMovement>();
	        if (thisMovement != null) thisMovement.isShocked = true;
	        yield return new WaitForSeconds(time);
	        if (thisMovement != null) thisMovement.isShocked = false;
		}

    }

    private IEnumerator BlockControlCoroutine()
    {
		if(!isBoss){
	        Dispatcher.ControlBlockToggled.Invoke(true);
	        yield return new WaitForSeconds(blockControlDuration);
	        if(power!=null && !power.UsingLightningNow)
	        Dispatcher.ControlBlockToggled.Invoke(false);
		}
    }

    private void DisableDamageSources()
    {
        foreach (DamageSource damageSource in GetComponentsInChildren<DamageSource>())
        {
            damageSource.IsEnabled = false;
        }
    }
}