﻿using UnityEngine;

public class CharacterJumping : CharacterBehaviour
{
    public BoolEvent CharacterJumped;
    public VoidEvent CharacterDoubleJumped;

    [SerializeField]
    private Vector2 jumpForce = Vector2.up;
    [SerializeField]
    private Vector2 doubleJumpForce = Vector2.up;
    [SerializeField]
    private bool canDoubleJump = false;
    
    private bool isDoubleJumping = false;
    private Rigidbody2D rigidBody;
    private GroundCheck groundCheck;


    private bool canJump;

    [Header("Long Jump")]
    [SerializeField]
    private bool longJumpEnabled;
    private bool longJump;
    [Space]

    [SerializeField] private float longJumpDelay;
                     private float longJumpDelayRuntime;
    [SerializeField] private float maxLongJumping;


    [SerializeField] private float stepLongJumping;
                     private float maxRuntimeStepLongJumping;

    public void SwitchJump(bool pressed)
    {
        longJump = pressed;
    }

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        groundCheck = GetComponent<GroundCheck>();
        JumpForce = jumpForce;
        DoubleJumpForce = doubleJumpForce;
        Dispatcher.CommandRequested.AddListener(OnCommandRequested);
    }

    void FixedUpdate()
    {
        PerformGroundCheck();
        if (longJump && longJumpEnabled)
        {
            longJumpDelayRuntime -= Time.deltaTime;
        }
        if (longJump && longJumpEnabled && !isDoubleJumping && longJumpDelayRuntime <= 0)
        {
            var step = Mathf.Min(stepLongJumping, maxRuntimeStepLongJumping);
            rigidBody.AddForce(new Vector2(0, step), ForceMode2D.Impulse);
            maxRuntimeStepLongJumping = Mathf.Max(maxRuntimeStepLongJumping - step, 0);

        }
    }

    public Vector2 JumpForce { get; set; }

    public Vector2 DoubleJumpForce { get; set; }

    private void TryJumping()
    {
        if (canJump)
        {
            Jump();
            canJump = false;
            longJumpDelayRuntime = longJumpDelay;
            maxRuntimeStepLongJumping = maxLongJumping;
        }
        else
        {
            if (!isDoubleJumping && canDoubleJump)
            {
                isDoubleJumping = true;
                Jump();
            }
        }
    }

    private void Jump()
    {
        Vector2 force = isDoubleJumping ? DoubleJumpForce : JumpForce;
        rigidBody.AddForce(force, ForceMode2D.Impulse);
        rigidBody.velocity = new Vector2(rigidBody.velocity.x, Mathf.Min(rigidBody.velocity.y, 11));
        CharacterJumped.Invoke(isDoubleJumping);
        if (isDoubleJumping)
        {
            CharacterDoubleJumped.Invoke();
        }
    }

    private void PerformGroundCheck()
    {
        if (groundCheck.IsGrounded)
        {
            isDoubleJumping = false;
            canJump = true;
        }
    }

    private void OnCommandRequested(Command command)
    {
        if (command == Command.Jump)
        {
            TryJumping();
        }
    }
}