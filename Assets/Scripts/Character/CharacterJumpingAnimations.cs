﻿using UnityEngine;

public class CharacterJumpingAnimations : MonoBehaviour
{
    private const string verticalVelocityParameterKey = "VerticalVelocity";
    private const string jumpParameterKey = "Jump";
    private const string isGroundedParameterKey = "IsGrounded";

#pragma warning disable 108,114
    private SpriteRenderer renderer;
#pragma warning restore 108,114
    private Animator animator;
    private GroundCheck groundCheck;
    private CharacterJumping characterJumping;
    private Vector2 previousPosition;

    void Awake()
    {
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        groundCheck = GetComponentInParent<GroundCheck>();
        characterJumping = GetComponentInParent<CharacterJumping>();
        characterJumping.CharacterJumped.AddListener(OnCharacterJumped);
        previousPosition = transform.position;
    }

    private bool visible;

    private void OnBecameInvisible()
    {
        visible = false;
    }

    private void OnBecameVisible()
    {
        visible = true;

    }

    void Update()
    {
        if (!visible) return;

        UpdateVelocity();
        UpdateIsGrounded();
    }

    private void UpdateVelocity()
    {
        Vector2 positionDelta = (Vector2)transform.position - previousPosition;
        float verticalVelocity = positionDelta.y / Time.deltaTime;
        animator.SetFloat(verticalVelocityParameterKey, Mathf.Abs(verticalVelocity) > 0.7f ? verticalVelocity : 0);

        previousPosition = transform.position;
    }

    private void UpdateIsGrounded()
    {
        var grounded = groundCheck.IsGrounded;
        renderer.sortingLayerName = (!grounded ? "Front Front Deco" : "Player");
        animator.SetBool(isGroundedParameterKey, grounded);
    }

    private void OnCharacterJumped(bool isDoubleJumping)
    {
        if (!isDoubleJumping)
        {
            animator.SetTrigger(jumpParameterKey);
        }
    }
}