﻿using UnityEngine;

public class CharacterMovement : CharacterBehaviour
{
    [InstanceParameter]
    [SerializeField]
    public bool isFacingRight = true;
    [InstanceParameter]
    [SerializeField]
    public float movementSpeed = 1.0f;
    [SerializeField]
    private float baseMovement = 1.0f;
    [InstanceParameter]
    [SerializeField]
    public bool isStationary = false;
    [SerializeField]
    private bool canTurnInAir = true;

    private Vector2 currentDirection = Vector2.right;
    private Rigidbody2D rigidBody;
    private SpriteRenderer spriteRenderer;
    private GroundCheck groundCheck;

    public bool isShocked;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        groundCheck = GetComponent<GroundCheck>();
        Dispatcher.CommandRequested.AddListener(OnCommandRequested);
        Dispatcher.ControlBlockToggled.AddListener(OnControlBlockToggled);
    }

    void Start()
    {
        ReadInstanceParameters();
    }

    void FixedUpdate()
    {
        if (isShocked)
        {
            Vector2 velocity = rigidBody.velocity;
            velocity.x = 0.0f;
            rigidBody.velocity = velocity;

            return;
        }
        if (!isStationary && (canTurnInAir || groundCheck.IsGrounded))
        {
            Move();
        }
    }

    void OnDisable()
    {
        Vector2 velocity = rigidBody.velocity;
        velocity.x = 0.0f;
        rigidBody.velocity = velocity;
    }

    private void ReadInstanceParameters()
    {
        isFacingRight = InstanceParameters.GetBool("isFacingRight", isFacingRight);
		spriteRenderer.flipX = !isFacingRight;
        movementSpeed = baseMovement * InstanceParameters.GetFloat("movementSpeed", movementSpeed);
        isStationary = InstanceParameters.GetBool("isStationary", isStationary);
    }

    private void Move()
    {
        currentDirection = isFacingRight ? Vector2.right : Vector2.left;
        spriteRenderer.flipX = !isFacingRight;
        Vector2 velocity = rigidBody.velocity;
        velocity.x = currentDirection.x * movementSpeed;
        rigidBody.velocity = velocity;
    }

    private void Turn()
    {
        isFacingRight = !isFacingRight;
    }

    private void OnControlBlockToggled(bool isControlBlocked)
    {
        enabled = !isControlBlocked;
    }

    private void OnCommandRequested(Command command)
    {
        if (command == Command.Turn)
        {
            Turn();
        }
        
    }

    public void AllowMovement()
    {
        isStationary = false;
		PlayerScore.TimerPaused = false;
    }
}