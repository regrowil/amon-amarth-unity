﻿using UnityEngine;

public class CharacterMovementAnimations : MonoBehaviour
{
    private const string horizontalVelocityParameterKey = "HorizontalVelocity";

    private Animator animator;
    private Vector2 previousPosition;

    public bool ChangeZRotationOnTurn = false;



    void Awake()
    {
        animator = GetComponent<Animator>();
        previousPosition = transform.position;
    }
    private bool visible;

    private void OnBecameInvisible()
    {
        visible = false;
    }

    private void OnBecameVisible()
    {
        visible = true;

    }

    void Update()
    {
        if (!visible) return;

        UpdateVelocity();
    }

    private void UpdateVelocity()
    {
        Vector2 positionDelta = (Vector2)transform.position - previousPosition;
        float horizontalVelocity = positionDelta.x / Time.deltaTime;
        animator.SetFloat(horizontalVelocityParameterKey, horizontalVelocity);
        if (ChangeZRotationOnTurn)
            transform.rotation = Mathf.Abs(horizontalVelocity) > 1 ? Quaternion.Euler(0, 0, 10 * -Mathf.Sign(horizontalVelocity)) : Quaternion.Euler(Vector3.zero);
        previousPosition = transform.position;
    }
}