﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class CharacterPower : MonoBehaviour
{
    [SerializeField]
    private int maxPower;
    [SerializeField]
    private int startPower;

    [SerializeField]
    private int powerPerEnemy;

    private int currentPower;

    public ParticleSystem PowerRegen;

    void Awake()
    {
        currentPower = startPower;
    }

    public int CurrentPower
    {
        get
        {
            return currentPower;
        }
        set
        {
            currentPower = Mathf.Clamp(value, 0, maxPower);
        }
    }

    public int MaxPower { get { return maxPower; } }

    public void AddPower(Vector3 position)
    {
        StartCoroutine(Emit(powerPerEnemy,position)); //CurrentPower += powerPerEnemy;
    }

    private IEnumerator Emit(int amountToEmit,Vector3 positon)
    {

        var i = Mathf.Min(MaxPower - CurrentPower, amountToEmit);
        while (i > 0)
        {
            ParticleSystem.EmitParams emitParam = new ParticleSystem.EmitParams();
            emitParam.position = positon;
            emitParam.velocity = new Vector3(Random.Range(-1f, 2f), 1,0).normalized * PowerRegen.main.startSpeedMultiplier;
            emitParam.startLifetime = PowerRegen.main.startLifetimeMultiplier;
            emitParam.randomSeed = PowerRegen.randomSeed;
            emitParam.startSize = Random.Range(PowerRegen.main.startSize.constantMin, PowerRegen.main.startSize.constantMax);
//            emitParam.angularVelocity = PowerRegen.main.;
//            emitParam.rotation =0;
//            emitParam.rotation3D = new Vector3(45,45,45);

            PowerRegen.Emit(emitParam,1);
            i--;

            yield return new WaitForSeconds(1f / amountToEmit);
        }
    }
}