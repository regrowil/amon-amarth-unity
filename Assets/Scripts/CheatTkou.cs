﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatTkou : MonoBehaviour
{

    public bool manualControl;
    [ContextMenu("NoCLip")]
    public void Noclip()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0;
        GetComponent<CapsuleCollider2D>().enabled = false;
        GetComponent<CharacterMovement>().movementSpeed = 0;
        manualControl = true;
    }

    [ContextMenu("Normal Control")]
    public void Normal()
    {
        GetComponent<Rigidbody2D>().gravityScale = 1;
        GetComponent<CapsuleCollider2D>().enabled = true;
        GetComponent<CharacterMovement>().movementSpeed = 5;
        manualControl = false;
    }

    public void EnableCollisions()
    {
        GetComponent<CapsuleCollider2D>().enabled = true;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad8)) Noclip();
        if (Input.GetKeyDown(KeyCode.Keypad7)) Normal();
        if (Input.GetKeyDown(KeyCode.Keypad9)) EnableCollisions();
        if (!manualControl) return;

        float Y = Input.GetAxis("Vertical") * 0.5f;
        float X = Input.GetAxis("Horizontal") * 0.5f;
        transform.Translate(X, Y, 0);
    }

}
