﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AudioClipGroup : MonoBehaviour
{

    public AudioSource source;
    public AudioClip[] clips;

    public void Start()
    {
        if(source==null)
        source = GetComponent<AudioSource>();
    }
    public void PlayRandom()
    {
        source.PlayOneShot(clips[Random.Range(0,clips.Length)]);
    }

}
