﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
[Serializable]
public class Song
{
    public string Name;
    public AudioClip Clip;
}

public class AudioController : Singleton<AudioController>
{

    public AudioMixer GameAudioMixer;
    public AudioSource Source;
    public List<Song> Songs;
    public string MenuSong;
    void Start()
    {
        if (Instance != null && Instance != this)
        {
            FindAndDestroyOtherInstances();
            return;
        }
        Source = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
        ChangeSoundMute(Settings.GetBoolSettings("Sound"));
        ChangeMusicMute(Settings.GetBoolSettings("Music"));

        PlayMenuMusic();
    }

    public static void PlayMenuMusic()
    {
        if(Instance.Source.clip != GetSong(Instance.MenuSong))
        PlaySong(Instance.MenuSong);
    }

    public static void ChangeGroupMute(string groupName, bool enabled)
    {
        if (groupName == "Sound")
            ChangeSoundMute(enabled);
        else if (groupName == "Music")
            ChangeMusicMute(enabled);
    }
    public static void ChangeSoundMute(bool enabled)
    {
        Instance.GameAudioMixer.SetFloat("SoundVolume", Settings.GetVolume(enabled ? 100 : 0));

    }

    public static void ChangeMusicMute(bool enabled)
    {
        Instance.GameAudioMixer.SetFloat("MusicVolume", Settings.GetVolume(enabled ? 100 : 0));
        if (enabled) Instance.Source.time = 0;
    }

    public static string GetSongName(AudioClip clip)
    {
        return Instance.Songs.First(s => s.Clip == clip).Name;
    }

    public static AudioClip GetSong(string name)
    {
        return Instance.Songs.First(s => s.Name == name).Clip;
    }

    public static void PlaySong(string songName)
    {
        Instance.Source.clip = GetSong(songName);
        Instance.Source.Play();
    }

    public static void PlayAudioClip(AudioClip clip)
    {
        Instance.Source.PlayOneShot(clip);
    }
}
