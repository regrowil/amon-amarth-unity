﻿using UnityEngine;
[CreateAssetMenu]
public class ChapterData : ScriptableObject
{
    public Sprite Image;
    public Level[] Levels;
    public AudioClip[] Songs;

//    public bool Locked;

}