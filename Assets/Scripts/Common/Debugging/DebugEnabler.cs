﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DebugEnabler : MonoBehaviour
{
    public static bool DebugMode =false;

    public static UnityEvent<bool> OnDebugChange = new BoolEvent();

    private static bool initialized;

    public int TapToActivate = 5;

    public void Tap()
    {
        if(DebugMode)return;
        TapToActivate--;
        if (TapToActivate == 0) Enable();

    }

    public void Enable()
    {
        DebugMode = true;
        OnDebugChange.Invoke(true);
    }

    public static void Initialize()
    {
        if(initialized)return;
        var debug = Debug.isDebugBuild;
        initialized = true;
        DebugMode = debug;
    }
}
