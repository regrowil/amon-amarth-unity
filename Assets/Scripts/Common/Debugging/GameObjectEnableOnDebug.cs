﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectEnableOnDebug : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        SetActive(DebugEnabler.DebugMode);
    }

    void OnEnable()
    {
        DebugEnabler.OnDebugChange.AddListener(SetActive);
    }

    void OnDestroy()
    {
        DebugEnabler.OnDebugChange.RemoveListener(SetActive);
    }

    void SetActive(bool active)
    {
        gameObject.SetActive(active);

    }
}
