﻿using System.Collections;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    [SerializeField]
    private float delay;

    void Awake()
    {
        StartCoroutine(SelfDestructCoroutine());
    }

    private IEnumerator SelfDestructCoroutine()
    {
        yield return new WaitForSeconds(delay);
        StopAllCoroutines();
        Destroy();
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

}