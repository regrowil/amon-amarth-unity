﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

public static class Settings
{
    public static bool Initialized;

    
    public static Dictionary<string, bool> BoolSettings;
    public static Dictionary<string, UnityAction<bool>> BoolEvents;

    public static Dictionary<string, string> StringSettings;
    public static Dictionary<string, UnityAction<string>> StringEvents;

    public static void SubscribeOnChange(string name, UnityAction<bool> action)
    {
        if (!Initialized) Initialize();

        if (BoolEvents.ContainsKey(name))
        {
            BoolEvents[name] = action;
        }
    }

    public static void SubscribeOnChange(string name, UnityAction<string> action)
    {
        if (!Initialized) Initialize();

        if (StringEvents.ContainsKey(name))
        {
            StringEvents[name] = action;
        }
    }

    public static void ToggleSettings(string name)
    {
        if (!Initialized) Initialize();

        BoolSettings[name] = !BoolSettings[name];
        Debug.Log(name+"Setting: is now: "+BoolSettings[name]);
        PlayerPrefs.SetInt(name, BoolSettings[name] ? 1 : 0);
        PlayerPrefs.Save();

        if (BoolEvents[name] != null)
            BoolEvents[name].Invoke(BoolSettings[name]);
    }


    public static void ChangeStringSettings(string name,string newSetting)
    {
        if (!Initialized) Initialize();

        StringSettings[name] = newSetting;
//        Debug.Log(name + "Setting: is now: " + StringSettings[name]);
        PlayerPrefs.SetString(name, StringSettings[name]);
        PlayerPrefs.Save();

        if (StringEvents[name] != null)
            StringEvents[name].Invoke(StringSettings[name]);
    }

    public static bool GetBoolSettings(string name)
    {
        if(!Initialized)Initialize();

        return BoolSettings[name];
    }

    public static string  GetStringSettings(string name)
    {
        if (!Initialized) Initialize();

        return StringSettings[name];
    }

    private static void AddBoolSetting(string name, bool defaultValue = false)
    {
        if (!PlayerPrefs.HasKey(name))
            BoolSettings.Add(name, defaultValue);
        else
        {
            BoolSettings.Add(name, PlayerPrefs.GetInt(name) == 1);
        }
        BoolEvents.Add(name, null);
    }

    private static void AddStringSetting(string name, string defaultValue = "null")
    {
        if (!PlayerPrefs.HasKey(name))
            StringSettings.Add(name, defaultValue);
        else
        {
            StringSettings.Add(name, PlayerPrefs.GetString(name));
        }
        StringEvents.Add(name, null);
    }

    public static void Initialize()
    {
        BoolSettings = new Dictionary<string, bool>();
        BoolEvents = new Dictionary<string, UnityAction<bool>>();

        StringSettings = new Dictionary<string, string>();
        StringEvents = new Dictionary<string, UnityAction<string>>();

        AddBoolSetting("Music",true);
        AddBoolSetting("Sound",true);
        AddBoolSetting("LeftHand",true);
        AddStringSetting("Lang","English");

        Initialized = true;
    }
    
    public static float GetVolume(int percent)
    {
       return Mathf.Lerp(-80, 0, percent);
    }


}
