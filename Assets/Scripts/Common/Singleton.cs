﻿using UnityEngine;
using UnityEngine.Assertions;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    public static T Instance
    {
        get
        {

            if (instance == null)
            {
                instance = FindObjectOfType<T>();
                FindAndDestroyOtherInstances();
            }



            return instance;
        }
    }

    public static void FindAndDestroyOtherInstances()
    {
        if (FindObjectsOfType<T>().Length > 1)
        {
            for (int index = 0; index < FindObjectsOfType<T>().Length; index++)
            {
                if (FindObjectsOfType<T>()[index] != instance) Destroy(FindObjectsOfType<T>()[index].gameObject);
            }
        }
    }
}