﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CommandButton : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private Command command;

    public UnityEvent OnCommandInvoke;

    public void Start()
    {
        if(OnCommandInvoke!=null)
            EventManager.Instance.PlayerCommandRequested.AddListener(arg0 => {if(arg0==command) {OnCommandInvoke.Invoke();} });
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        EventManager.Instance.PlayerCommandRequested.Invoke(command);
    }
}