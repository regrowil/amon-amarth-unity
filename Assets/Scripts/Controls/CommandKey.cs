﻿using UnityEngine;

[System.Serializable]
public class CommandKey
{
    public Command Command;
    public KeyCode KeyCode;
}