﻿using UnityEngine;

public class KeyboardControls : MonoBehaviour
{
    [SerializeField]
    private CommandKey[] CommandKeys;

    void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        for (int i = 0; i < CommandKeys.Length; i++)
        {
            HandleKey(CommandKeys[i]);
        }
    }

    private void HandleKey(CommandKey commandKey)
    {
        if (Input.GetKeyDown(commandKey.KeyCode))
        {
            EventManager.Instance.PlayerCommandRequested.Invoke(commandKey.Command);
        }
    }
}