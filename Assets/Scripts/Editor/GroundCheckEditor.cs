﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GroundCheck))]
public class GroundCheckEditor : Editor
{
    void OnSceneGUI()
    {
        Handles.color = Color.red;
        Vector2 position = Selection.activeTransform.position;
        Vector2 offset = serializedObject.FindProperty("offset").vector2Value;
        float radius = serializedObject.FindProperty("radius").floatValue;
        Handles.DrawWireDisc(position + offset, Vector3.forward, radius);
    }


}