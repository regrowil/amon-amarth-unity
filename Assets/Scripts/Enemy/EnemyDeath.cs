﻿using UnityEngine;

public class EnemyDeath : MonoBehaviour
{
    private CharacterHP characterHP;
	private EnemyScore enemyScore;

    void Awake()
    {
        characterHP = GetComponent<CharacterHP>();
        characterHP.CharacterDied.AddListener(OnCharacterDied);
		enemyScore = GetComponent<EnemyScore>();
    }

    private void FillPlayerPower()
    {
        CharacterPower playerPower = Player.Instance.GetComponent<CharacterPower>();
        playerPower.AddPower(transform.position);
    }

	private void FillPlayerScore()
	{
		PlayerScore playerScore = Player.Instance.GetComponent<PlayerScore>();
		playerScore.AddScore(enemyScore.ScoreWorth);
	}

    private void OnCharacterDied()
    {
        FillPlayerPower();
		FillPlayerScore();
        gameObject.layer = 15;
    }
}