﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScore : MonoBehaviour {
	[SerializeField]
	private int scoreWorth;

	public int ScoreWorth
	{
		get{return scoreWorth;}
	}

	public void setScore(int x){
		scoreWorth = x;
	}
}
