﻿using UnityEngine.Events;

[System.Serializable]
public class CommandEvent : UnityEvent<Command>
{
}