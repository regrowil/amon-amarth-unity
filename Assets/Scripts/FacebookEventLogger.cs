﻿using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.Purchasing;

public class FacebookEventLogger : MonoBehaviour
{
    void Awake()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Handle FB.Init
            FB.Init(FB.ActivateApp);
        }

    }

    void Start()
    {
//        if (FB.IsInitialized)
//        {
//            Dictionary<string, object> d = new Dictionary<string, object> {{"version", "unity-1.0000"}};
//            FB.LogAppEvent("Launched", null, d);
//        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                FB.Init(FB.ActivateApp);
            }
        }
    }

    public static void OnPurchase(Product product)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("id", product.definition.id);
        param.Add("storeId", product.definition.storeSpecificId);
        param.Add("price", product.metadata.localizedPrice);
        FB.LogPurchase((float)product.metadata.localizedPrice, product.metadata.isoCurrencyCode, param);
    }

    public static void LogSharedEvent(string platform, string target, string level)
    {
        Dictionary<string, object> parameters = new Dictionary<string, object>();
        parameters.Add("platform", platform);
        parameters.Add("target", target);
        parameters.Add("level", level);
        FB.LogAppEvent("Shared", null, parameters);
    }

    public void ReceiveFromNative(string message)
    {
        Debug.Log(message);
    }
}
