﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Firebase.Messaging;
using Firebase.RemoteConfig;

public class MessageHandler : Singleton<MessageHandler>
{
    public void Start()
    {
        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.MessageReceived += OnMessageReceived;
        DontDestroyOnLoad(gameObject);
        SwitchFireBaseTopic(GameConfig.IsFullVersion);

        Dictionary<string, object> defaults = new Dictionary<string, object>();

        // These are the values that are used if we haven't fetched data from the
        // server
        // yet, or if we ask for values that the server doesn't have:
        defaults.Add("ChristmasCampaign", false);
        FirebaseRemoteConfig.SetDefaults(defaults);

        //        FirebaseRemoteConfig.Settings = new ConfigSettings { IsDeveloperMode = true };
        var task = FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
        task.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetch)
    {
        if (fetch.IsCanceled)
        {
            Debug.Log("#RemoteConfig# Fetch canceled.");
        }
        else if (fetch.IsFaulted)
        {
            Debug.Log("#RemoteConfig# Fetch encountered an error.");
        }
        else if (fetch.IsCompleted)
        {
            Debug.Log("#RemoteConfig# Fetch completed successfully!");
        }

        var info = FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus)
        {
            case LastFetchStatus.Success:
                FirebaseRemoteConfig.ActivateFetched();
                Debug.Log(String.Format("#RemoteConfig# Remote data loaded and ready (last fetch time {0}).",
                                       info.FetchTime));
                foreach (var key in FirebaseRemoteConfig.Keys)
                {
                    Debug.Log("#RemoteKey# " + key + " - " + FirebaseRemoteConfig.GetValue(key).StringValue);
                }
                break;
            case LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case FetchFailureReason.Error:
                        Debug.Log("#RemoteConfig# Fetch failed for unknown reason");
                        break;
                    case FetchFailureReason.Throttled:
                        Debug.Log("#RemoteConfig# Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case LastFetchStatus.Pending:
                Debug.Log("#RemoteConfig# Latest Fetch call still pending.");
                break;
        }
    }

    public void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        Debug.Log("From: " + e.Message.From);
        Debug.Log("Message ID: " + e.Message.MessageId);
    }

    private void OnDestroy()
    {
        FirebaseMessaging.TokenReceived -= OnTokenReceived;
        FirebaseMessaging.MessageReceived -= OnMessageReceived;
    }

    public void SuscribeAllTopicsDebug()
    {
        FirebaseMessaging.Subscribe("/topics/paid");
        FirebaseMessaging.Subscribe("/topics/free");

    }

    public static void SwitchFireBaseTopic(bool isFull)
    {
        if (GameConfig.IsFullVersion)
        {
            //PlayerPrefs.SetInt("PaidTopic", 1);
            FirebaseMessaging.Subscribe("/topics/paid");

            //PlayerPrefs.SetInt("FreeTopic", 0);
            FirebaseMessaging.Unsubscribe("/topics/free");
        }
        else
        {
            //PlayerPrefs.SetInt("PaidTopic", 0);
            FirebaseMessaging.Unsubscribe("/topics/paid");

            //PlayerPrefs.SetInt("FreeTopic", 1);
            FirebaseMessaging.Subscribe("/topics/free");
        }

        PlayerPrefs.Save();

    }
}
