﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fabric.Crashlytics;
using Firebase.Analytics;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;

public class GameConfig : MonoBehaviour
{

    public static bool IsFullVersion;
    public static bool FakeFull = false;

    //    public static Purchaser purchaser = new Purchaser();
    


    public void Awake()
    {
        DebugEnabler.Initialize();

        if (Application.isMobilePlatform)
        {
            Application.targetFrameRate = 60;
        }
        else
        {
            Application.targetFrameRate = 100;
        }
        Crashlytics.Log("FPS: " + Application.targetFrameRate);
        //        purchaser.Init();
        if (FakeFull)
            IsFullVersion = true;
        else
            IsFullVersion = PlayerPrefs.GetInt("Full", 0) == 1;

	}

 

    public static void UnlockFullVersion(string id)
    {
        PlayerPrefs.SetInt("Full", 1);
        GameConfig.IsFullVersion = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        MessageHandler.SwitchFireBaseTopic(IsFullVersion);

        Crashlytics.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", id));

        PlayerPrefs.Save();
    }

    public static void ForgotAboutFullVersion()
    {
        Crashlytics.Log("Removing Full Version");
        PlayerPrefs.SetInt("Full", 0);
        GameConfig.IsFullVersion = false;

        MessageHandler.SwitchFireBaseTopic(IsFullVersion);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        PlayerPrefs.Save();

    }

}
//
//public class Purchaser : IStoreListener
//{
//
//    private static IStoreController m_StoreController;          // The Unity Purchasing system.
//    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
//
//    public static string kProductIDNonConsumable = "unlock_all";
//    // Apple App Store-specific product identifier for the subscription product.
//    //    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";
//
//    // Google Play Store-specific product identifier subscription product.
//    //    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";
//
//    public void Init()
//    {
//        // If we haven't set up the Unity Purchasing reference
//        if (m_StoreController == null)
//        {
//            // Begin to configure our connection to Purchasing
//            InitializePurchasing();
//        }
//    }
//
//
//    public void InitializePurchasing()
//    {
//        // If we have already connected to Purchasing ...
//        if (IsInitialized())
//        {
//            // ... we are done here.
//            return;
//        }
//
//        // Create a builder, first passing in a suite of Unity provided stores.
//        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
//
//        // Add a product to sell / restore by way of its identifier, associating the general identifier
//        // with its store-specific identifiers.
//        //        builder.AddProduct(kProductIDConsumable, ProductType.Consumable);
//        // Continue adding the non-consumable product.
//        builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);
//        // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
//        // if the Product ID was configured differently between Apple and Google stores. Also note that
//        // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
//        // must only be referenced here. 
//        //        builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
//        //                { kProductNameAppleSubscription, AppleAppStore.Name },
//        //                { kProductNameGooglePlaySubscription, GooglePlay.Name },
//        //            });
//
//        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
//        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
//        UnityPurchasing.Initialize(this, builder);
//    }
//
//
//    private bool IsInitialized()
//    {
//        // Only say we are initialized if both the Purchasing references are set.
//        return m_StoreController != null && m_StoreExtensionProvider != null;
//    }
//
//
//    //    public void BuyConsumable()
//    //    {
//    //        // Buy the consumable product using its general identifier. Expect a response either 
//    //        // through ProcessPurchase or OnPurchaseFailed asynchronously.
//    //        BuyProductID(kProductIDConsumable);
//    //    }
//
//
//    public void BuyNonConsumable()
//    {
//        // Buy the non-consumable product using its general identifier. Expect a response either 
//        // through ProcessPurchase or OnPurchaseFailed asynchronously.
//        BuyProductID(kProductIDNonConsumable);
//    }
//
//
//    //    public void BuySubscription()
//    //    {
//    //        // Buy the subscription product using its the general identifier. Expect a response either 
//    //        // through ProcessPurchase or OnPurchaseFailed asynchronously.
//    //        // Notice how we use the general product identifier in spite of this ID being mapped to
//    //        // custom store-specific identifiers above.
//    //        BuyProductID(kProductIDSubscription);
//    //    }
//
//
//    void BuyProductID(string productId)
//    {
//        // If Purchasing has been initialized ...
//        if (IsInitialized())
//        {
//            // ... look up the Product reference with the general product identifier and the Purchasing 
//            // system's products collection.
//            Product product = m_StoreController.products.WithID(productId);
//
//            // If the look up found a product for this device's store and that product is ready to be sold ... 
//            if (product != null && product.availableToPurchase)
//            {
//                Crashlytics.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
//                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
//                // asynchronously.
//                m_StoreController.InitiatePurchase(product);
//            }
//            // Otherwise ...
//            else
//            {
//                // ... report the product look-up failure situation  
//                Crashlytics.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
//            }
//        }
//        // Otherwise ...
//        else
//        {
//            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
//            // retrying initiailization.
//            Crashlytics.Log("BuyProductID FAIL. Not initialized.");
//        }
//    }
//
//
//    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
//    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
//    public void RestorePurchases()
//    {
//        // If Purchasing has not yet been set up ...
//        if (!IsInitialized())
//        {
//            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
//            Crashlytics.Log("RestorePurchases FAIL. Not initialized.");
//            return;
//        }
//
//        // If we are running on an Apple device ... 
//        if (Application.platform == RuntimePlatform.IPhonePlayer ||
//            Application.platform == RuntimePlatform.OSXPlayer)
//        {
//            // ... begin restoring purchases
//            Crashlytics.Log("RestorePurchases started ...");
//
//            // Fetch the Apple store-specific subsystem.
//            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
//            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
//            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
//            apple.RestoreTransactions((result) =>
//            {
//                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
//                // no purchases are available to be restored.
//                Crashlytics.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
//            });
//        }
//        // Otherwise ...
//        else
//        {
//            // We are not running on an Apple device. No work is necessary to restore purchases.
//            Crashlytics.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
//        }
//    }
//
//
//    //  
//    // --- IStoreListener
//    //
//
//    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
//    {
//        // Purchasing has succeeded initializing. Collect our Purchasing references.
//        Crashlytics.Log("OnInitialized: PASS");
//
//        // Overall Purchasing system, configured with products for this application.
//        m_StoreController = controller;
//        // Store specific subsystem, for accessing device-specific store features.
//        m_StoreExtensionProvider = extensions;
//
//
//    }
//
//    public void ConsumeManaged()
//    {
//        
//    }
//
//    public void OnInitializeFailed(InitializationFailureReason error)
//    {
//        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
//        Crashlytics.Log("OnInitializeFailed InitializationFailureReason:" + error);
//    }
//
//    public void UnlockFullVersion(string id)
//    {
//        Crashlytics.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", id));
//        PlayerPrefs.SetInt("Full", 1);
//        GameConfig.IsFullVersion = true;
//    }
//
//    public void ForgotAboutFullVersion()
//    {
//        Crashlytics.Log("Removing Full Version");
//        PlayerPrefs.SetInt("Full", 0);
//        GameConfig.IsFullVersion = false;
//    }
//
//    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
//    {
//        // A consumable product has been purchased by this user.
//        //        if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
//        //        {
//        //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//        //            // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
//        //            ScoreManager.score += 100;
//        //        }
//        // Or ... a non-consumable product has been purchased by this user.
//        //        else
//
//        if (args.purchasedProduct.definition.id == "unlock_all" || args.purchasedProduct.definition.id == "test1" ||
//            args.purchasedProduct.definition.id == "test2" || args.purchasedProduct.definition.id == "test3")
//        {
//            UnlockFullVersion(args.purchasedProduct.definition.id);
//        }
////        if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
////        {
////
////
////            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
////        }
//        // Or ... a subscription product has been purchased by this user.
//        //        else 
//        //        if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
//        //        {
//        //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//        // TODO: The subscription item has been successfully purchased, grant this to the player.
//        //        }
//        // Or ... an unknown product has been purchased by this user. Fill in additional products here....
//        else
//        {
//            Crashlytics.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'",
//                args.purchasedProduct.definition.id));
//        }
//
//        // Return a flag indicating whether this product has completely been received, or if the application needs 
//        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
//        // saving purchased products to the cloud, and when that save is delayed. 
//        return PurchaseProcessingResult.Complete;
//    }
//
//
//    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
//    {
//        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
//        // this reason with the user to guide their troubleshooting actions.
//        Crashlytics.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
//        if (m_StoreController.products.all.Length > 0)
//        {
//            var prevPurchase = m_StoreController.products.WithStoreSpecificID(product.definition.storeSpecificId);
//            if (prevPurchase.hasReceipt)
//            {
//                PlayerPrefs.SetInt("Full", 1);
//                GameConfig.IsFullVersion = true;
//            }
//        }
//    }
//}
//
