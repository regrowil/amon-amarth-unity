﻿using UnityEngine;

public class DamageSource : MonoBehaviour
{
    [SerializeField]
    private int damage = 0;
    [SerializeField]
    private bool causesExplosion = false;
    [SerializeField]
    private bool isDamageOverTime = false;

    void Awake()
    {
        IsEnabled = true;
        Damage = damage;
        CausesExplosion = causesExplosion;
        IsDamageOverTime = isDamageOverTime;
    }

    public int Damage { get; set; }

    public bool CausesExplosion { get; set; }

    public bool IsDamageOverTime { get; set; }

    public bool IsEnabled { get; set; }
}