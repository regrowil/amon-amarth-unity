﻿using System.Collections;
using System.Collections.Generic;
using SpriteTile;
using UnityEngine;
[CreateAssetMenu]
public class Level : ScriptableObject
{

    [Header("Name")]
    public string LevelName;
    public string LevelSubName;
	public string LevelNr;
	public int LevelID;

    [Header("Images")]
    public Sprite Image;
    [Space]
    public List<GameObject> Backgrounds; 
    public List<GameObject> ParallaxPrefabs;
    [Header("Map")]
    public TextAsset LevelData;

    [Header("Paid Features")] public bool RequireFullVersion;
}
