﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpriteTile;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Globalization;
using Firebase.Analytics;


[Serializable]
public struct AnimatedTileRange
{
    public int Set;
    public int Tile;
    public int Range;
    public float Speed;
    public AnimType Type;
}

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    private Level InspectorLevelData;
    public static Level LevelData;

    [Header("Effects")]
    public GameObject Snow;

    [Header("Platforms")]
    public int PlatformLayer;
    public int EffectorSurfaceArc;

    [Header("Blocking Blocks")]
    public int BlockingLayer;
    public TileInfo BlockingTile;
    public GameObject BlockingPrefab;

    [Header("Spikes")]
    public int SpikesTileLayer;
    public string SpikesLayerName = "NeutralDamageSources";
    public int SpikesDamage;

    [Header("Title Card")]
    public Text Title;
    public Text SubTitle;
    public string LevelNr;
    public int LevelID;

    public Image BlackFade;
    public float FadeoutTime;

    public Transform ParallaxParent;

    public List<GameObject> RuntimeObjects = new List<GameObject>();

    public List<AnimatedTileRange> Animated;

    public UnityEvent OnStart, OnComplete;

    public CameraFollow CameraFollow;
    public Int2 MapSize;

    [Header("WinScreen Popup")]
    public GameObject PopupCamera;
    public GameObject SettingsScreen;
    public GameObject winscreen;
    public RatePopup rate;
    //	public GameObject Chapter1_Img;
    //	public GameObject Chapter2_Img;
    //	public GameObject Chapter3_Img;
    //	public GameObject Chapter4_Img;
    public GameObject Score;
    public GameObject TimeScore;
    //public GameObject HighScoreText;
    public GameObject levelNr;
    public GameObject NewRecordBadge;
    public Text LevelNameCenter;
    public Text LevelNameRight;
    public Text ChapterNameRight;

    public List<UIInGameScoreElement> WinScreenScore;

    public UnityStandardAssets.ImageEffects.BlurOptimized Blur;

    private TimeSpan compareableTime;

    public void Start()
    {
        //Load Default Level if null
        if (LevelData == null) LoadLevel(InspectorLevelData);
        else LoadLevel(LevelData);

        OnStart.Invoke();
    }

    private void OnDisable()
    {
        //        CompleteLevel();
    }
    [ContextMenu("complete")]
    public void CompleteLevel()
    {
        Runestone.Reset();
        PlayerScore.TimerPaused = true;
        StartCoroutine(CompleteWithFadeOut());

    }

    public IEnumerator CompleteWithFadeOut()
    {
        var startTime = 0f;

        var alpha = new Color(0, 0, 0, 0);
        var full = new Color(0, 0, 0, 1);

        OnComplete.Invoke();
        while (startTime < 1)
        {
            startTime += Time.deltaTime / FadeoutTime;

            BlackFade.color = Color.Lerp(alpha, full, startTime);
            yield return new WaitForEndOfFrame();
        }

        rate.Show(() =>
        {
            winscreen.SetActive(true);
        });
        SettingsScreen.SetActive(false);
        PopupCamera.SetActive(true);


        if (LevelData.LevelNr.Equals("1"))
        {
            levelNr.GetComponent<Text>().text = "I";
        }
        else if (LevelData.LevelNr.Equals("2"))
        {
            levelNr.GetComponent<Text>().text = "II";
        }
        else if (LevelData.LevelNr.Equals("3"))
        {
            levelNr.GetComponent<Text>().text = "III";
        }
        else
        {
            levelNr.GetComponent<Text>().text = "I";
        }

        //		int lvlID = LevelManager.Instance.LevelID;
        //		if(lvlID == 1 || lvlID == 2 || lvlID == 3 ){
        //			Chapter1_Img.SetActive(true);
        //		}
        //		else if(lvlID == 4 || lvlID == 5 || lvlID == 6 ){
        //			Chapter2_Img.SetActive(true);
        //		}
        //		else if(lvlID == 7 || lvlID == 8 || lvlID == 9 ){
        //			Chapter3_Img.SetActive(true);
        //		}
        //		else if(lvlID == 10 || lvlID == 11 || lvlID == 12 ){
        //			Chapter4_Img.SetActive(true);
        //		}

        String LevelSubName = LevelManager.LevelData.LevelSubName;
        //		WinText.GetComponent<Text>().text = "You've completed: \n"+LevelSubName+"";
        LevelNameCenter.text = LevelNameRight.text = LevelSubName;
        ChapterNameRight.text = LevelData.LevelName.Split(' ')[0];

        //check if player died at least once and reset timer if so
        if (Player.HasDiedOnce)
        {
            PlayerScore.timeText = "";
        }
        else
        {
            string formattedTime = "00:" + PlayerScore.timeText;
            compareableTime = TimeSpan.Parse(formattedTime);
        }
        TimeScore.GetComponent<Text>().text = "" + PlayerScore.timeText;

        //save highscore
        int lvlnr = LevelManager.Instance.LevelID;
        for (int spotNumber = 1; spotNumber < 6; spotNumber++)
        {
            string spotNumberKey = "highscore_lvl" + lvlnr + "_spot_" + spotNumber;
            if (PlayerPrefs.GetInt(spotNumberKey, 0) < PlayerPrefs.GetInt("currentscore", 0))
            {
                AddToLeaderboardAndSort(lvlnr, spotNumber, PlayerPrefs.GetInt("currentscore", 0), PlayerScore.timeText);
                break;
            }
            else if (PlayerPrefs.GetInt(spotNumberKey, 0) == PlayerPrefs.GetInt("currentscore", 0))
            {//if scores are the same, it will compare times.
                if (!PlayerScore.timeText.Equals(""))
                {//if the player has time bonus, check further
                    if (PlayerPrefs.GetString(spotNumberKey + "_time", "").Equals(""))
                    {//if the leaderboard doesnt have time bonus, the player replaces it.
                        AddToLeaderboardAndSort(lvlnr, spotNumber, PlayerPrefs.GetInt("currentscore", 0), PlayerScore.timeText);
                        break;
                    }
                    else
                    {//comparing timespans
                        string prefFormattedTime = "00:" + PlayerPrefs.GetString(spotNumberKey + "_time", "");
                        TimeSpan prefCompareableTime = TimeSpan.Parse(prefFormattedTime);
                        int compareResult = TimeSpan.Compare(compareableTime, prefCompareableTime);
                        if (compareResult == (-1))
                        {//player time is shorter than leaderboard's time, so player replaces it.
                            AddToLeaderboardAndSort(lvlnr, spotNumber, PlayerPrefs.GetInt("currentscore", 0), PlayerScore.timeText);
                            break;
                        }
                    }
                }
            }

        }
        var s = PlayerPrefs.GetInt("currentscore", 0);

        Score.GetComponent<Text>().text = "" + s;
        foreach (var uiInGameScoreElement in WinScreenScore)
        {
            uiInGameScoreElement.SetUp(LevelData.LevelID, s, PlayerScore.timeText);
        }
        PlayerPrefs.SetInt("currentscore", 0);
        Player.HowManyCheckpointsReached = 0;





        FirebaseAnalytics.LogEvent("Level_Completed", "LevelID", LevelID);

        BlackFade.color = alpha;
        Blur.enabled = true;
    }


    public void ShowNewRecord()
    {
        NewRecordBadge.SetActive(true);
    }

    public void ExitToMenu()
    {
        Player.HasDiedOnce = false;
        Player.HowManyCheckpointsReached = 0;
        PlayerScore.TimerPaused = true;
        PlayerPrefs.SetInt(LevelData.LevelName, 1);
        if (GameConfig.IsFullVersion)
        {
            if (LevelSelectUI.CurrentLevelId + 1 >= LevelSelectUI.LevelList.Count)
            {
                SceneManager.LoadScene(1);
                AudioController.PlayMenuMusic();
            }
            else
            {
                AudioController.Instance.Source.Stop();
                LevelSelectUI.CurrentLevelId++;
                LevelData = LevelSelectUI.LevelList[LevelSelectUI.CurrentLevelId];
                SceneManager.LoadScene(2);
            }
        }
        else
        {
            SceneManager.LoadScene(1);
            AudioController.PlayMenuMusic();
        }
    }

    public void LoadLevel(Level level)
    {
        LevelData = level;
        foreach (var parallax in level.ParallaxPrefabs)
        {
            var g = Instantiate(parallax);
            g.transform.SetParent(ParallaxParent, false);
        }
        PrepareLevel();
        foreach (var background in level.Backgrounds)
        {
            Instantiate(background, CameraFollow.transform, false);
        }
        Title.text = LevelData.LevelName;
        SubTitle.text = LevelData.LevelSubName;
        LevelNr = LevelData.LevelNr;
        LevelID = LevelData.LevelID;
        MapSize = Tile.GetMapSize();
        CameraFollow.UpdateRange(MapSize.x);

        if (LevelID == 7 || LevelID == 8 || LevelID == 9)
        {
            Snow.SetActive(true);
        }
    }

    public void PrepareLevel()
    {
        foreach (var runtimeObject in RuntimeObjects)
        {
            Destroy(runtimeObject);
        }
        RuntimeObjects = new List<GameObject>();
        // Set Camera and Load LevelData
        Tile.SetCamera();
        Tile.LoadLevel(LevelData.LevelData);

        //Find All Platforms and add effectors
        var platformColliders = Tile.Colliders[PlatformLayer];
        foreach (var platformCollider in platformColliders)
        {
            var effector = platformCollider.gameObject.AddComponent<PlatformEffector2D>();
            platformCollider.gameObject.AddComponent<Platform>();
            effector.surfaceArc = EffectorSurfaceArc;
        }
        //Find All BlockingBlocks and Prepare GameObjects
        var blockingTransforms = new List<Int2>();
        Tile.GetTilePositions(BlockingTile, BlockingLayer, ref blockingTransforms);
        var blockParent = new GameObject("BlockingBlockParent");
        RuntimeObjects.Add(blockParent);
        foreach (var blockingTransform in blockingTransforms)
        {
            var blockingBlock = Instantiate(BlockingPrefab);
            blockingBlock.transform.position = new Vector3(blockingTransform.x, blockingTransform.y);
            blockingBlock.transform.SetParent(blockParent.transform, true);
            RuntimeObjects.Add(blockingBlock);

        }
        //Hide BlockingBlocks
        Tile.SetLayerActive(BlockingLayer, false);

        SetupSpikes();
        SetupAnimateTiles();
        PlayMusic();
    }

    public void PlayMusic()
    {

        if (!AudioController.Instance.Source.isPlaying)
            AudioController.PlaySong(AudioController.GetSongName(MenuController.LastChapter.Songs[int.Parse(LevelData.LevelNr) - 1]));
        Invoke("PlayMusic", AudioController.Instance.Source.clip.length + 2);

    }

    private void SetupSpikes()
    {
        foreach (Transform spike in Tile.Colliders[SpikesTileLayer])
        {
            Collider2D collider = spike.GetComponent<Collider2D>();
            collider.isTrigger = true;
            DamageSource damageSource = spike.gameObject.AddComponent<DamageSource>();
            damageSource.Damage = SpikesDamage;
            damageSource.CausesExplosion = true;
            spike.gameObject.layer = LayerMask.NameToLayer(SpikesLayerName);
        }
    }

    private void SetupAnimateTiles()
    {
        foreach (var animatedTileRange in Animated)
        {
            Tile.AnimateTile(new TileInfo(animatedTileRange.Set, animatedTileRange.Tile), animatedTileRange.Range, animatedTileRange.Speed, animatedTileRange.Type);

        }
    }
    private void AddToLeaderboardAndSort(int lvlNr, int playerSpot, int playerScore, string playerTime)
    {
        for (int currentSpot = 5; currentSpot > playerSpot; currentSpot--)
        {
            string currentSpotKey = "highscore_lvl" + lvlNr + "_spot_" + currentSpot;
            string NewCurrentSpotKey = "highscore_lvl" + lvlNr + "_spot_" + (currentSpot - 1);

            PlayerPrefs.SetInt(currentSpotKey, PlayerPrefs.GetInt(NewCurrentSpotKey, 0));
            PlayerPrefs.SetString(currentSpotKey + "_name", PlayerPrefs.GetString(NewCurrentSpotKey + "_name", "none"));
            PlayerPrefs.SetString(currentSpotKey + "_time", PlayerPrefs.GetString(NewCurrentSpotKey + "_time", ""));
        }
        string playerSpotKey = "highscore_lvl" + lvlNr + "_spot_" + playerSpot;
        PlayerPrefs.SetInt(playerSpotKey, playerScore);
        PlayerPrefs.SetString(playerSpotKey + "_name", "You");
        PlayerPrefs.SetString(playerSpotKey + "_time", playerTime);
    }
}