﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxManager : MonoBehaviour
{
    public bool ParalaxEnabled;
    public bool AutoScroll;

    public float BackgroundSize = 9.6f;

    private Transform cameraTransform;

    private List<Transform> layers = new List<Transform>();

    [SerializeField]
    private float viewZone;

    private int leftIndex, rightIndex;

    public float ParalaxSpeed;

    private float lastCameraX;

    public void Start()
    {

        cameraTransform = Camera.main.transform;
        lastCameraX = cameraTransform.position.x;
        for (int i = 0; i < transform.childCount; i++)
        {
            layers.Add(transform.GetChild(i));
        }

        leftIndex = 0;
        rightIndex = layers.Count - 1;

    }

    public void ScrollLeft()
    {
        layers[rightIndex].localPosition = Vector3.right * (layers[leftIndex].localPosition.x - BackgroundSize);
        leftIndex = rightIndex;
        rightIndex--;
        if (rightIndex < 0)
        {
            rightIndex = layers.Count - 1;
        }
    }

    public void ScrollRight()
    {
        layers[leftIndex].localPosition = Vector3.right * (layers[rightIndex].localPosition.x + BackgroundSize);
        rightIndex = leftIndex;
        leftIndex++;
        if (leftIndex == layers.Count)
        {
            leftIndex = 0;
        }
    }


    public void Update()
    {
        if (AutoScroll)
        {
            transform.position += Vector3.right * (Time.deltaTime * ParalaxSpeed);
        }
        if (ParalaxEnabled)
        {
            float deltaX = cameraTransform.transform.position.x - lastCameraX;
            transform.position += Vector3.right*(deltaX*ParalaxSpeed);
            lastCameraX = cameraTransform.position.x;
        }

        if (cameraTransform.position.x < layers[leftIndex].transform.position.x + viewZone)
            ScrollLeft();

        if (cameraTransform.position.x > layers[rightIndex].transform.position.x - viewZone)
            ScrollRight();

    }

}
