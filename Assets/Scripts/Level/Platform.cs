﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    void Awake()
    {
        SetEffector();
        //        ChangeHeightOfPolygonCollider();
        //ConvertPolygonToEdgeCollider();
    }

    [ContextMenu("Convert")]
    private void ConvertPolygonToEdgeCollider()
    {
        PolygonCollider2D polygonCollider2D = GetComponent<PolygonCollider2D>();
        EdgeCollider2D edgeCollider2D = gameObject.AddComponent<EdgeCollider2D>();
        Vector2 start = polygonCollider2D.bounds.max - new Vector3(polygonCollider2D.bounds.size.x, 0.0f, 0.0f) - transform.position;
        Vector2 end = polygonCollider2D.bounds.max - transform.position;
        edgeCollider2D.points = new Vector2[] { start, end };
        edgeCollider2D.usedByEffector = true;
        Destroy(polygonCollider2D);
    }

    [ContextMenu("Convert")]
    private void SetEffector()
    {
        PolygonCollider2D polygonCollider2D = GetComponent<PolygonCollider2D>();
        polygonCollider2D.usedByEffector = true;

    }



    [ContextMenu("Manual ChangeHeight")]

    private void ChangeHeightOfPolygonCollider()
    {
        PolygonCollider2D polygonCollider2D = GetComponent<PolygonCollider2D>();
        polygonCollider2D.usedByEffector = true;

        var top = (polygonCollider2D.points[0].y);
        for (int i = 0; i < polygonCollider2D.pathCount; i++)
        {
            var path = polygonCollider2D.GetPath(i);
                path[0] = new Vector2(path[0].x, top);
                path[1] = new Vector2(path[1].x, top - 0.1f);
                path[2] = new Vector2(path[2].x, top - 0.1f);
                path[3] = new Vector2(path[3].x, top);
            

            polygonCollider2D.SetPath(i, path);
        }


    }
}