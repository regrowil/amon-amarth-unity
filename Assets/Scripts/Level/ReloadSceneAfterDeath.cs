﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadSceneAfterDeath : MonoBehaviour
{
    [SerializeField]
    private float delay = 0.0f;

    private CharacterHP playerHP;

    void Awake()
    {
        playerHP = Player.Instance.GetComponent<CharacterHP>();
        playerHP.CharacterDied.AddListener(OnPlayerDied);
    }

    private IEnumerator ReloadCoroutine()
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    private void OnPlayerDied()
    {
		PlayerPrefs.SetInt("currentscore", 0);
        StartCoroutine(ReloadCoroutine());
    }
}