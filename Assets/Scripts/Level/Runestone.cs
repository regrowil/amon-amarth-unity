﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Runestone : MonoBehaviour
{
    public bool Default;
    public static Runestone ActiveRunestone;
    public static Vector3 ActiveRunestonePos = Vector3.zero;

    public static UnityAction OnDefaultActivate;
    public UnityEvent OnActivate;

    public void Start()
    {
        if (Default && ActiveRunestonePos == Vector3.zero || ActiveRunestonePos == transform.position)
        {
            Activate();
        }
        if (OnDefaultActivate != null)
        {
            OnDefaultActivate.Invoke();
           // OnDefaultActivate = null;
        }

    }

    public static void Reset()
    {
        ActiveRunestone = null;
        ActiveRunestonePos=Vector3.zero;
    }

    public bool Activate()
    {
        if (ActiveRunestone != this)
        {
			Player.HowManyCheckpointsReached++;
			if (ActiveRunestone != null)
                ActiveRunestone.GetComponent<Animator>().Play("Idle");
            ActiveRunestone = this;
            ActiveRunestonePos = ActiveRunestone.transform.position;
            Runestone.OnDefaultActivate = () => Player.Instance.transform.position = Runestone.ActiveRunestonePos + Vector3.up * 0.3f;
            
            GetComponent<Animator>().SetTrigger("Startup");

            OnActivate.Invoke();
            return true;
        }
        return false;
    }

}
