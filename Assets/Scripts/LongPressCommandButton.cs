﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LongPressCommandButton : CommandButton, IPointerUpHandler
{

    public BoolEvent OnPointerChange;
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        OnPointerChange.Invoke(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnPointerChange.Invoke(false);
    }
}
