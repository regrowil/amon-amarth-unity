﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPOnParticleCollision : MonoBehaviour
{

    public bool Add;
    public float HPToAdd = 0;

    CharacterHP hp;
    ParticleSystem ps;
    List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

    void Start()
    {
        ps = GetComponent<ParticleSystem>();

        hp = Player.Instance.GetComponent<CharacterHP>();

    }

    void Update()
    {
        if (HPToAdd > 0 && Add)
        {
            hp.CurrentHP = Mathf.Min(hp.CurrentHP + 1, hp.MaxHP);
            HPToAdd -= 1;
        }
    }

    private void OnParticleCollision(GameObject other)
    {

        ps.GetCollisionEvents(other, collisionEvents);

        foreach (var particleCollisionEvent in collisionEvents)
        {
            HPToAdd += 1;
        }
        collisionEvents = new List<ParticleCollisionEvent>();

    }




}

