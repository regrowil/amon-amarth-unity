using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleAttractorMove : MonoBehaviour
{
    public Transform Target;
    public float Speed = 5f;
    public float Treshold;

    new ParticleSystem  particleSystem;
    ParticleSystem.Particle[] particles;
    int numParticlesAlive;

    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];
        numParticlesAlive = particleSystem.GetParticles(particles);
        for (int i = 0; i < numParticlesAlive; i++)
        {
            if (particles[i].startLifetime - particles[i].remainingLifetime > Treshold)
            {
                var t = Target.position;
                t.z = transform.position.z;
                particles[i].velocity += (t - particles[i].position).normalized * Speed / 10 *
                                           Math.Max(1, (10 - Vector3.Distance(t, particles[i].position)));
                if (Math.Max(1, (10 - Vector3.Distance(t, particles[i].position))) > 7)
                {
                    particles[i].velocity = (t - particles[i].position).normalized * Speed / 10 *
                           Math.Max(1, (10 - Vector3.Distance(t, particles[i].position)));
                }
            }
        }
        particleSystem.SetParticles(particles, numParticlesAlive);
    }
}
