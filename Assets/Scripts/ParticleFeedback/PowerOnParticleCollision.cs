﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerOnParticleCollision : MonoBehaviour {


    public bool Add;
    public float PowerToAdd = 0;

    CharacterPower power;
    ParticleSystem ps;
    List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

    void Start()
    {
        ps = GetComponent<ParticleSystem>();

        power = Player.Instance.GetComponent<CharacterPower>();

    }

    void Update()
    {
        if (PowerToAdd > 0 && Add)
        {
            power.CurrentPower = Mathf.Min(power.CurrentPower + 1, power.MaxPower);
            PowerToAdd -= 1;
            if (power.CurrentPower == power.MaxPower) PowerToAdd = 0;
        }
    }

    private void OnParticleCollision(GameObject other)
    {

        ps.GetCollisionEvents(other, collisionEvents);

        foreach (var particleCollisionEvent in collisionEvents)
        {
            PowerToAdd += 1;
        }
        collisionEvents = new List<ParticleCollisionEvent>();

    }
}
