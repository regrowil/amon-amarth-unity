﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[ExecuteInEditMode]
[RequireComponent(typeof(ParticleSystem))]
public class SetPositionFromImageFill : MonoBehaviour
{
    public ParticleSystem ParticleSystem;
    public Image Bar;

	void Update ()
	{
	    var percent = Bar.fillAmount;
	    var width = Bar.rectTransform.sizeDelta.x;
	    var lowerPos = Bar.rectTransform.sizeDelta.y;

        transform.localPosition = new Vector3(width*percent,-lowerPos);
	}

}
