﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseOnActivate : MonoBehaviour {

    public UnityEvent OnActivate;
    public UnityEvent OnDeActivate;

     void OnEnable()
    {
        if (OnActivate != null) OnActivate.Invoke();
    }
     void OnDisable() {

        if (OnDeActivate != null) OnDeActivate.Invoke();
    }

    public void ChangeGameSpeed(float speed)
    {
        Time.timeScale = speed;

    }
}
