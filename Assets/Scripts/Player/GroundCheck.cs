﻿using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [SerializeField]
    private Vector2 offset = Vector2.down;
    [SerializeField]
    private float radius = 1.0f;
    [SerializeField]
    private LayerMask layerMask;

    private Rigidbody2D rigidBody2D;

    void Awake()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    public bool IsGrounded
    {
        get
        {
            return Mathf.Approximately(rigidBody2D.velocity.y, 0.0f) && Physics2D.OverlapCircle((Vector2)transform.position + offset, radius, layerMask);
        }
    }
}