﻿using UnityEngine;

public class Player : Singleton<Player>
{
	public static bool HasDiedOnce;
	public static int HowManyCheckpointsReached;

    private bool levelCompleted = false;

	void Start () {
		//set playerHP a bit lower on level 1 so the player can pick up the barrel on the turorial
		if(LevelManager.LevelData.LevelID == 1 && !Player.HasDiedOnce)
        {
			GetComponent<CharacterHP>().CurrentHP = GetComponent<CharacterHP>().CurrentHP - 30;
		}	
	}

    public void Update()
    {
        if (!levelCompleted && transform.position.x > LevelManager.Instance.MapSize.x)
        {
            levelCompleted = true;
            LevelManager.Instance.CompleteLevel();
            var r= GetComponent<Rigidbody2D>();
            r.isKinematic = true;
            r.velocity = Vector2.zero;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var startTrigger = other.GetComponent<StartTrigger>();
        if (!startTrigger) return;
        startTrigger.Raise();
    }
}