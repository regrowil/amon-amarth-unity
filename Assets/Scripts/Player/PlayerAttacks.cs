﻿using UnityEngine;

public class PlayerAttacks : CharacterBehaviour
{
    [SerializeField]
    private PlayerWeapon playerWeapon;
    [SerializeField]
    private RuntimeAnimatorController noWeaponAnimatorController;

    private Animator animator;
    private RuntimeAnimatorController originalAnimatorController;


    static int drinking = Animator.StringToHash("Drink");
    static int death = Animator.StringToHash("Death");
    void Awake()
    {
        Dispatcher.CommandRequested.AddListener(OnPlayerCommandRequested);
        animator = GetComponentInChildren<Animator>();
        originalAnimatorController = animator.runtimeAnimatorController;
    }

    void LateUpdate()
    {
        if (playerWeapon.State == PlayerWeaponState.Available)
        {
            var currentBaseState = animator.GetCurrentAnimatorStateInfo(0);

            if (currentBaseState.shortNameHash != drinking && currentBaseState.shortNameHash != death)
                animator.runtimeAnimatorController = originalAnimatorController;
        }
    }

    private void TryAttacking()
    {
        if (playerWeapon.State == PlayerWeaponState.Available)
        {
            Attack();
        }
    }

    private void Attack()
    {
        playerWeapon.Throw();
        animator.runtimeAnimatorController = noWeaponAnimatorController;
    }

    private void OnPlayerCommandRequested(Command command)
    {
        if (command == Command.Attack)
        {
            TryAttacking();
        }
    }
}