﻿public class PlayerDispatcher : CharacterDispatcher
{
    private bool isControlBlocked = true;

    void Awake()
    {
        EventManager.Instance.PlayerCommandRequested.AddListener(OnPlayerCommandRequested);
        ControlBlockToggled.AddListener(OnControlBlockedToggled);
    }

    private void OnPlayerCommandRequested(Command command)
    {
        if (!isControlBlocked)
        {
            CommandRequested.Invoke(command);
        }
        else if(command == Command.StrikeLightning)
        {
            CommandRequested.Invoke(command);

        }
    }

    private void OnControlBlockedToggled(bool isControlBlocked)
    {
        this.isControlBlocked = isControlBlocked;
    }
}