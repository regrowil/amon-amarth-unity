﻿using System.Collections;
using UnityEngine;

public class PlayerDrinking : CharacterBehaviour
{
    public AudioClip DrinkingClip;

    public VoidEvent DrinkingStarted;
    public VoidEvent DrinkingCompleted;
    public VoidEvent BarrelCrush;

    private CharacterHP characterHP;
    private PlayerLightning playerLightning;
    private GroundCheck groundCheck;
    private bool stayInAir;
    public bool DrinkingNow;

    public ParticleSystem LifeRegen;

    void Awake()
    {
        characterHP = GetComponent<CharacterHP>();
        groundCheck = GetComponent<GroundCheck>();
        playerLightning = GetComponent<PlayerLightning>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Barrel barrel = other.GetComponent<Barrel>();
        if (playerLightning.UsingLightningNow) return;
        if (barrel != null && characterHP.CurrentHP < characterHP.MaxHP)
        {
            if (groundCheck.IsGrounded)
            {
                Drink(barrel);
            }
            else
            {
                stayInAir = true;
            }
        }

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (!stayInAir) return;
        if (playerLightning.UsingLightningNow) return;
        Barrel barrel = other.GetComponent<Barrel>();
        if (barrel != null && characterHP.CurrentHP < characterHP.MaxHP)
        {
            if (groundCheck.IsGrounded)
            {
                Drink(barrel);
                stayInAir = false;
            }
        }
    }

    private void Drink(Barrel barrel)
    {

        Debug.Log("#Emit#" + (characterHP.MaxHP - characterHP.CurrentHP));
        StartCoroutine(Emit((characterHP.MaxHP - characterHP.CurrentHP)));    
        Destroy(barrel.gameObject);
        DrinkingStarted.Invoke();
        DrinkingNow = true;

        Drinking();
    }

    private IEnumerator Emit(int amountToEmit)
    {
        var i = amountToEmit;
        while (i > 0)
        {
            LifeRegen.Emit(1);
            i--;

            yield return new WaitForSeconds(3f / amountToEmit);
        }
    }

    private void Drinking()
    {

        GetComponent<AudioSource>().PlayOneShot(DrinkingClip);
        Dispatcher.ControlBlockToggled.Invoke(true);
    }

    public void OnDrinkComplete()
    {
        Dispatcher.ControlBlockToggled.Invoke(false);
        DrinkingCompleted.Invoke();
        DrinkingNow = false;

    }

}