﻿using UnityEngine;

public class PlayerDrinkingAnimations : MonoBehaviour
{
    private const string drinkParameterKey = "Drink";

    private Animator animator;
    private PlayerDrinking playerDrinking;

    void Awake()
    {
        animator = GetComponent<Animator>();
        playerDrinking = GetComponentInParent<PlayerDrinking>();
        playerDrinking.DrinkingStarted.AddListener(OnDrinkingStarted);
    }

    private void OnDrinkingStarted()
    {
        animator.SetTrigger(drinkParameterKey);
    }

    private void OnDrinkingCompeted()
    {
        playerDrinking.OnDrinkComplete();
    }

    private void OnBarrelCrush()
    {
        playerDrinking.BarrelCrush.Invoke();
    }
}