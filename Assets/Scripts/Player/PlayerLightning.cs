﻿using System.Collections;
using UnityEngine;

public class PlayerLightning : CharacterBehaviour
{
    public VoidEvent LightningStruck;

    [SerializeField]
    private Transform lightningPrefab;
    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    private float radius = 1.0f;
    [SerializeField]
    private float preDelay = 0.0f;
    [SerializeField]
    private float postDelay = 0.0f;

    private CharacterPower playerPower;
    private GroundCheck groundCheck;
    private CharacterHP playerHP;

    public bool UsingLightningNow;

    public float playerYOffset;

    void Awake()
    {
        playerPower = GetComponent<CharacterPower>();
        playerHP = GetComponent<CharacterHP>();
        groundCheck = GetComponent<GroundCheck>();
        Dispatcher.CommandRequested.AddListener(OnCommandRequested);
    }

    private void TryStrikingLightning()
    {
        if (playerPower.CurrentPower == playerPower.MaxPower && groundCheck.IsGrounded && !GetComponent<PlayerDrinking>().DrinkingNow)
        {
            StrikeLightning();
        }
    }

    private void StrikeLightning()
    {
        playerPower.CurrentPower = 0;
        StartCoroutine(StrikeLightningCoroutine());
    }

    private IEnumerator StrikeLightningCoroutine()
    {
        LightningStruck.Invoke();
        UsingLightningNow = true;
        Dispatcher.ControlBlockToggled.Invoke(true);
        playerHP.IsInvulnerable = true;
        yield return new WaitForSeconds(preDelay);
        SpawnLightning();
        yield return new WaitForSeconds(postDelay);
        Dispatcher.ControlBlockToggled.Invoke(false);
        playerHP.IsInvulnerable = false;
        UsingLightningNow = false;

    }

    private void SpawnLightning()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius, layerMask);
        foreach (Collider2D collider in colliders)
        {
            var hp = collider.gameObject.GetComponent<CharacterHP>() ??
                     collider.gameObject.GetComponentInParent<CharacterHP>();
            if (hp != null)
                if (hp.IsDead) continue;
            var lightning = Instantiate(lightningPrefab, collider.transform.position - new Vector3(0, collider.bounds.extents.y - collider.offset.y - playerYOffset, 0), Quaternion.identity);
            lightning.GetComponent<SelfDestruct>().StartCoroutine(SetLightningPosition(lightning, collider));
        }
    }

    private IEnumerator SetLightningPosition(Transform lightning, Collider2D target)
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            lightning.position = target.transform.position - new Vector3(0, target.bounds.extents.y - target.offset.y - playerYOffset, 0);
        }

    }

    private void OnCommandRequested(Command command)
    {
        if (command == Command.StrikeLightning)
        {
            TryStrikingLightning();
        }
    }
}