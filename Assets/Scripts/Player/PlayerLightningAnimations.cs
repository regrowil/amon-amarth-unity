﻿using UnityEngine;

public class PlayerLightningAnimations : MonoBehaviour
{
    private const string strikeLightningParameterKey = "StrikeLightning";

    private Animator animator;
    private PlayerLightning playerLightning;

    void Awake()
    {
        animator = GetComponent<Animator>();
        playerLightning = GetComponentInParent<PlayerLightning>();
        playerLightning.LightningStruck.AddListener(OnLightningStruck);
    }

    private void OnLightningStruck()
    {
        animator.SetTrigger(strikeLightningParameterKey);
    }
}