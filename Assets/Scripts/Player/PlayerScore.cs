﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerScore : MonoBehaviour {

	public static bool IsActiveRunestoneDefault = true;
	public static bool TimerPaused = true;
	public bool TimerPausedinspect = TimerPaused;
	public static string timeText;

	[SerializeField]
	private int currentScore;
	[SerializeField]
	private float minutes;
	[SerializeField]
	private float seconds;
	[SerializeField]
	private float miliseconds;

	private float timer;

	void Awake()
	{
		//reset score on start
		PlayerPrefs.SetInt("currentscore",0);
		currentScore = PlayerPrefs.GetInt("currentscore",0);
	}
	void Start()
	{
		minutes = 0f;
		seconds = 0f;
		miliseconds = 0f;
		timeText = "00:00.000";
	}
	void Update()
	{
		TimerPausedinspect = TimerPaused;
		if(!TimerPaused)
		{
			timer += Time.deltaTime;
			minutes = Mathf.Floor(timer / 60);
			seconds = Mathf.Floor(timer % 60);
			miliseconds = Mathf.Floor(timer * 1000 % 1000);
			timeText = String.Format("{0:00}:{1:00}.{2:00}",minutes,seconds,miliseconds);
		}
	}
	public int CurrentScore
	{
		get{return currentScore;}
	}

	public void AddScore(int Score)
	{
		currentScore += Score;
		PlayerPrefs.SetInt("currentscore", currentScore);
	}
}
