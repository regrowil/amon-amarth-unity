﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PlayerWeapon : CharacterBehaviour
{
    [SerializeField]
    private float throwSpeed = 1.0f;
    [SerializeField]
    private float throwDuration = 1.0f;
    [SerializeField]
    private float spinSpeed = 1.0f;

    public GameObject Particles;

    public UnityEvent OnThrow;
    public UnityEvent OnGroundHit;

    private Vector2 throwDirection = Vector2.right;
    private Vector3 spinAxis = Vector3.back;
    private Vector2 originalPosition;
    private SpriteRenderer spriteRenderer;
    private CircleCollider2D circleCollider2D;
    private Transform originalParent;
    private DamageSource damageSource;

    void Awake()
    {
        Dispatcher.CommandRequested.AddListener(OnPlayerCommandRequested);
        originalPosition = transform.localPosition;
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        circleCollider2D = GetComponent<CircleCollider2D>();
        originalParent = transform.parent;
        damageSource = GetComponentInChildren<DamageSource>();
    }

    void Start()
    {
        Deactivate();
    }

    void Update()
    {
        Spin();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.isTrigger && other.GetComponent<Player>() == null)
        {
            HandleNonPlayerCollision();
            if (other.gameObject.layer != LayerMask.NameToLayer("Enemies") && other.gameObject.layer != LayerMask.NameToLayer("BodyParts"))
            {
                OnGroundHit.Invoke();
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (!other.isTrigger && other.GetComponent<Player>() != null)
        {
            HandlePlayerCollision();
        }
    }

    public PlayerWeaponState State { get; private set; }

    public void Throw()
    {
        if (State == PlayerWeaponState.Available)
        {
            OnThrow.Invoke();
            StartCoroutine(ThrowCoroutine());
        }
    }

    private void Return()
    {
        if (State == PlayerWeaponState.Thrown)
        {
            StartCoroutine(ReturnCoroutine());
        }
    }

    private void Spin()
    {
        spriteRenderer.transform.Rotate(spinAxis, spinSpeed * Time.deltaTime);
    }

    private void Activate()
    {
        transform.localPosition = Vector2.Scale(originalPosition, throwDirection);
        transform.localRotation = Quaternion.identity;
        transform.parent = null;
        spriteRenderer.enabled = true;
        circleCollider2D.enabled = true;
        damageSource.IsEnabled = true;
        Particles.SetActive(true);
    }

    private void Deactivate()
    {
        State = PlayerWeaponState.Available;
        transform.parent = originalParent;
        spriteRenderer.enabled = false;
        circleCollider2D.enabled = false;
        damageSource.IsEnabled = false;
        Particles.SetActive(false);

    }

    private void HandlePlayerCollision()
    {
        if (State == PlayerWeaponState.Returning)
        {
            Deactivate();
        }
    }

    private void HandleNonPlayerCollision()
    {
        if (State == PlayerWeaponState.Thrown)
        {
            Return();
        }
    }

    private IEnumerator ThrowCoroutine()
    {
        State = PlayerWeaponState.Thrown;
        Activate();
        float currentDuration = 0.0f;
        Vector2 currentDirection = throwDirection;
        while (State == PlayerWeaponState.Thrown && currentDuration < throwDuration)
        {
            Vector2 position = (Vector2)transform.position + currentDirection * throwSpeed * Time.deltaTime;
            transform.position = position;
            currentDuration += Time.deltaTime;
            yield return null;
        }
        Return();
    }

    private IEnumerator ReturnCoroutine()
    {
        State = PlayerWeaponState.Returning;
        while (State == PlayerWeaponState.Returning)
        {
            Vector2 toPlayer = originalParent.position - transform.position;
            Vector2 position = (Vector2)transform.position + toPlayer.normalized * throwSpeed * Time.deltaTime;
            transform.position = position;
            yield return null;
        }
    }

    private void Turn()
    {
        throwDirection = -throwDirection;
        spinAxis = -spinAxis;
        spriteRenderer.flipY = !spriteRenderer.flipY;
    }

    private void OnPlayerCommandRequested(Command command)
    {
        if (command == Command.Turn)
        {
            Turn();
        }
    }
}