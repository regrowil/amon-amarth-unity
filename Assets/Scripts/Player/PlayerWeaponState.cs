﻿public enum PlayerWeaponState
{
    Unavailable,
    Available,
    Thrown,
    Returning
}