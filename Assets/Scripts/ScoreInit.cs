﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreInit : MonoBehaviour {

	// Use this for initialization
	void Start()
	{
		//level 1
		if(PlayerPrefs.GetString("highscore_lvl1_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl1_spot_1", 600); PlayerPrefs.SetString("highscore_lvl1_spot_1_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl1_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl1_spot_2", 500); PlayerPrefs.SetString("highscore_lvl1_spot_2_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl1_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl1_spot_3", 400); PlayerPrefs.SetString("highscore_lvl1_spot_3_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl1_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl1_spot_4", 300); PlayerPrefs.SetString("highscore_lvl1_spot_4_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl1_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl1_spot_5", 200); PlayerPrefs.SetString("highscore_lvl1_spot_5_name", "Johan");
		}

		//Level 2
		if(PlayerPrefs.GetString("highscore_lvl2_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl2_spot_1", 730); PlayerPrefs.SetString("highscore_lvl2_spot_1_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl2_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl2_spot_2", 610); PlayerPrefs.SetString("highscore_lvl2_spot_2_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl2_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl2_spot_3", 520); PlayerPrefs.SetString("highscore_lvl2_spot_3_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl2_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl2_spot_4", 430); PlayerPrefs.SetString("highscore_lvl2_spot_4_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl2_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl2_spot_5", 350); PlayerPrefs.SetString("highscore_lvl2_spot_5_name", "Johan (vocals)");
		}


		//Level 3
		if(PlayerPrefs.GetString("highscore_lvl3_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl3_spot_1", 900); PlayerPrefs.SetString("highscore_lvl3_spot_1_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl3_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl3_spot_2", 800); PlayerPrefs.SetString("highscore_lvl3_spot_2_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl3_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl3_spot_3", 700); PlayerPrefs.SetString("highscore_lvl3_spot_3_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl3_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl3_spot_4", 600); PlayerPrefs.SetString("highscore_lvl3_spot_4_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl3_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl3_spot_5", 500); PlayerPrefs.SetString("highscore_lvl3_spot_5_name", "Olavi");
		}


		//level 4
		if(PlayerPrefs.GetString("highscore_lvl4_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl4_spot_1", 780); PlayerPrefs.SetString("highscore_lvl4_spot_1_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl4_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl4_spot_2", 760); PlayerPrefs.SetString("highscore_lvl4_spot_2_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl4_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl4_spot_3", 640); PlayerPrefs.SetString("highscore_lvl4_spot_3_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl4_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl4_spot_4", 550); PlayerPrefs.SetString("highscore_lvl4_spot_4_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl4_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl4_spot_5", 500); PlayerPrefs.SetString("highscore_lvl4_spot_5_name", "Ted");
		}	

		//Level 5
		if(PlayerPrefs.GetString("highscore_lvl5_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl5_spot_1", 980); PlayerPrefs.SetString("highscore_lvl5_spot_1_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl5_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl5_spot_2", 900); PlayerPrefs.SetString("highscore_lvl5_spot_2_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl5_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl5_spot_3", 860); PlayerPrefs.SetString("highscore_lvl5_spot_3_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl5_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl5_spot_4", 740); PlayerPrefs.SetString("highscore_lvl5_spot_4_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl5_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl5_spot_5", 630); PlayerPrefs.SetString("highscore_lvl5_spot_5_name", "Jocke");
		}

		//Level 6
		if(PlayerPrefs.GetString("highscore_lvl6_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl6_spot_1", 1000); PlayerPrefs.SetString("highscore_lvl6_spot_1_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl6_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl6_spot_2", 900); PlayerPrefs.SetString("highscore_lvl6_spot_2_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl6_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl6_spot_3", 800); PlayerPrefs.SetString("highscore_lvl6_spot_3_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl6_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl6_spot_4", 700); PlayerPrefs.SetString("highscore_lvl6_spot_4_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl6_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl6_spot_5", 600); PlayerPrefs.SetString("highscore_lvl6_spot_5_name", "Olavi");
		}

		//level 7
		if(PlayerPrefs.GetString("highscore_lvl7_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl7_spot_1", 990); PlayerPrefs.SetString("highscore_lvl7_spot_1_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl7_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl7_spot_2", 940); PlayerPrefs.SetString("highscore_lvl7_spot_2_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl7_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl7_spot_3", 800); PlayerPrefs.SetString("highscore_lvl7_spot_3_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl7_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl7_spot_4", 730); PlayerPrefs.SetString("highscore_lvl7_spot_4_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl7_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl7_spot_5", 660); PlayerPrefs.SetString("highscore_lvl7_spot_5_name", "Johan (vocals)");
		}	

		//Level 8
		if(PlayerPrefs.GetString("highscore_lvl8_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl8_spot_1", 1000); PlayerPrefs.SetString("highscore_lvl8_spot_1_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl8_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl8_spot_2", 900); PlayerPrefs.SetString("highscore_lvl8_spot_2_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl8_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl8_spot_3", 800); PlayerPrefs.SetString("highscore_lvl8_spot_3_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl8_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl8_spot_4", 700); PlayerPrefs.SetString("highscore_lvl8_spot_4_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl8_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl8_spot_5", 600); PlayerPrefs.SetString("highscore_lvl8_spot_5_name", "Johan (guitars)");
		}

		//Level 9
		if(PlayerPrefs.GetString("highscore_lvl9_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl9_spot_1", 1000); PlayerPrefs.SetString("highscore_lvl9_spot_1_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl9_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl9_spot_2", 900); PlayerPrefs.SetString("highscore_lvl9_spot_2_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl9_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl9_spot_3", 800); PlayerPrefs.SetString("highscore_lvl9_spot_3_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl9_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl9_spot_4", 700); PlayerPrefs.SetString("highscore_lvl9_spot_4_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl9_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl9_spot_5", 600); PlayerPrefs.SetString("highscore_lvl9_spot_5_name", "Jocke");
		}


		//level 10
		if(PlayerPrefs.GetString("highscore_lvl10_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl10_spot_1", 1000); PlayerPrefs.SetString("highscore_lvl10_spot_1_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl10_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl10_spot_2", 900); PlayerPrefs.SetString("highscore_lvl10_spot_2_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl10_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl10_spot_3", 800); PlayerPrefs.SetString("highscore_lvl10_spot_3_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl10_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl10_spot_4", 700); PlayerPrefs.SetString("highscore_lvl10_spot_4_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl10_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl10_spot_5", 600); PlayerPrefs.SetString("highscore_lvl10_spot_5_name", "Johan (guitars)");
		}	

		//Level 11
		if(PlayerPrefs.GetString("highscore_lvl11_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl11_spot_1", 1000); PlayerPrefs.SetString("highscore_lvl11_spot_1_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl11_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl11_spot_2", 900); PlayerPrefs.SetString("highscore_lvl11_spot_2_name", "Ted");
		}
		if(PlayerPrefs.GetString("highscore_lvl11_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl11_spot_3", 800); PlayerPrefs.SetString("highscore_lvl11_spot_3_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl11_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl11_spot_4", 700); PlayerPrefs.SetString("highscore_lvl11_spot_4_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl11_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl11_spot_5", 600); PlayerPrefs.SetString("highscore_lvl11_spot_5_name", "Jocke");
		}
		
		//Level 12
		if(PlayerPrefs.GetString("highscore_lvl12_spot_1_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl12_spot_1", 1000); PlayerPrefs.SetString("highscore_lvl12_spot_1_name", "Jocke");
		}
		if(PlayerPrefs.GetString("highscore_lvl12_spot_2_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl12_spot_2", 900); PlayerPrefs.SetString("highscore_lvl12_spot_2_name", "Johan (vocals)");
		}
		if(PlayerPrefs.GetString("highscore_lvl12_spot_3_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl12_spot_3", 800); PlayerPrefs.SetString("highscore_lvl12_spot_3_name", "Johan (guitars)");
		}
		if(PlayerPrefs.GetString("highscore_lvl12_spot_4_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl12_spot_4", 700); PlayerPrefs.SetString("highscore_lvl12_spot_4_name", "Olavi");
		}
		if(PlayerPrefs.GetString("highscore_lvl12_spot_5_name", "none").Equals("none")){
			PlayerPrefs.SetInt("highscore_lvl12_spot_5", 600); PlayerPrefs.SetString("highscore_lvl12_spot_5_name", "Ted");
		}

		//all 12 levels time prefs
		for(int j = 1; j < 13; j++)
		{
			for(int i = 1; i < 6; i++)
			{
				string key = "highscore_lvl" + j + "_spot_" + i;
				if(PlayerPrefs.GetString(key + "_name","").Equals(""))
				{
					PlayerPrefs.SetString(key + "_time","0"+i+":");
				}
			}
		}
        PlayerPrefs.Save();
	}
}
