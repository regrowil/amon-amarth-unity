﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartTrigger : MonoBehaviour
{

    private static Dictionary<int, UnityEvent> Triggers = new Dictionary<int, UnityEvent>();

    public bool Done;
    [InstanceParameter]
    public int Key;
    public UnityEvent DefaultAction;

    public static void AddListener(int key,UnityAction action)
    {
        if (Triggers.ContainsKey(key)) Triggers[key].AddListener(action);
        else
        {
            var newEvent = new UnityEvent();
            newEvent.AddListener(action);
            Triggers.Add(key,newEvent);
        }
    }

    public static void Invoke(int key)
    {
        Triggers[key].Invoke();
    }

    public static void Clear()
    {
        Triggers.Clear();
    }

    public void Start()
    {
        ReadInstanceParameters();
    }

    public void Raise()
    {
        if(Done)return;
        Done = true;
        DefaultAction.Invoke();
        Invoke(Key);
    }

    private void ReadInstanceParameters()
    {
        Key = InstanceParameters.GetInt("Key", Key);
    }

    private IInstanceParameters instanceParameters;
    protected IInstanceParameters InstanceParameters
    {
        get
        {
            if (instanceParameters == null)
            {
                instanceParameters = GetComponent<IInstanceParameters>();
                if (instanceParameters == null)
                {
                    instanceParameters = gameObject.AddComponent<EmptyInstanceParameters>();
                }
            }
            return instanceParameters;
        }
    }

 
}
