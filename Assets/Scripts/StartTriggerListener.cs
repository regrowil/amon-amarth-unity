﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartTriggerListener : MonoBehaviour
{
    [InstanceParameter]
    public int Key;
    public UnityEvent Event;
    public Animator animator;

    private string animName = "";

    void Start()
    {
        ReadInstanceParameters();
        StartTrigger.AddListener(Key, Event.Invoke);
    }

    void Update()
    {
        if (animName != "" && animator.GetBool("StartAnimationDone"))
        {
            var movement = GetComponent<CharacterMovement>();
            if (movement != null)
                movement.isStationary = false;
        }
    }

    public void PlayAudio(AudioClip clip)
    {
        GetComponent<AudioSource>().PlayOneShot(clip);
    }

    public void PlayAnimation(string name)
    {
        animName = name;
        animator.SetTrigger(name);
    }

    public void ShowDebug(string text)
    {
        Debug.Log(text);
    }

    private void ReadInstanceParameters()
    {
        Key = InstanceParameters.GetInt("Key", Key);
    }

    private IInstanceParameters instanceParameters;
    protected IInstanceParameters InstanceParameters
    {
        get
        {
            if (instanceParameters == null)
            {
                instanceParameters = GetComponent<IInstanceParameters>();
                if (instanceParameters == null)
                {
                    instanceParameters = gameObject.AddComponent<EmptyInstanceParameters>();
                }
            }
            return instanceParameters;
        }
    }

}
