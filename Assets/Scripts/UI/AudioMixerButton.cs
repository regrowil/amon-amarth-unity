﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

class AudioMixerButton : SettingsButton
{
    public override void SubscribeEvent()
    {
        Settings.SubscribeOnChange(SettingsName, (enabled) =>
        {
            SetOptionButton(enabled);
            AudioController.ChangeGroupMute(SettingsName, enabled);
            SwitchEvent.Invoke();
        });
    }

}
