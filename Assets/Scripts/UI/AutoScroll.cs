﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AutoScroll : MonoBehaviour
{
    [SerializeField]
    private float scrollSpeed;
    [SerializeField]
    private float startNormalizedPosition;
    [SerializeField]
    private float endNormalizedPosition;

    private ScrollRect scrollRect;

    void Awake()
    {
        scrollRect = GetComponent<ScrollRect>();
    }

    void Start()
    {
        StartCoroutine(ScrollingCoroutine());
    }

    private IEnumerator ScrollingCoroutine()
    {
        float currentScrollValue = 0.0f;
        while (true)
        {
            currentScrollValue += scrollSpeed * Time.deltaTime;
            if (currentScrollValue > 1.0f)
            {
                currentScrollValue = 0.0f;
            }
            scrollRect.verticalNormalizedPosition = Mathf.Lerp(startNormalizedPosition, endNormalizedPosition, currentScrollValue);
            yield return null;
        }
    }
}