﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BlinkButton : MonoBehaviour
{

    public List<BlinkButton> BlinkOnClick;
    private List<Image> blinkImages; 

    public float BlinkTime;
    public Gradient BlinkColor;
    public void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(Blink);
        blinkImages = BlinkOnClick.Select(blinkButton => blinkButton.GetComponent<Image>()).ToList();

    }

    public void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveListener(Blink);

    }

    public void Blink()
    {
        StartCoroutine( ColorTween(blinkImages));
    }

    public IEnumerator ColorTween(List<Image> images)
    {
        var startTime = 0f;
        while (startTime <= 1)
        {
            startTime += Time.deltaTime / BlinkTime;

            foreach (var image in images)
            {
                image.color = BlinkColor.Evaluate(startTime);

            }
            yield return new WaitForEndOfFrame();
        }
    }


}
