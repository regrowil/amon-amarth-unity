﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ButtonSpriteOverrider))]
public class ButtonRotator : MonoBehaviour
{

    public List<ButtonSpriteSwap> Buttons;

    public int Current;

    public void SetButton(int index)
    {
        Current = index;
        GetComponent<ButtonSpriteOverrider>().SetButton(Buttons[Current]);

    }

    public void Next()
    {
        if (Buttons.Count - 1 == Current) Current = 0; else Current++;
        GetComponent<ButtonSpriteOverrider>().SetButton(Buttons[Current]);

    }



}
