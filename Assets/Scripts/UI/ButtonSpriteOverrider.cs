﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSpriteOverrider : MonoBehaviour
{
    public ButtonSpriteSwap ButtonSpriteSwap;
    private Button button;
#if UNITY_EDITOR
    
    void OnValidate()
    {
        if(Application.isPlaying)return;
        SetButton(ButtonSpriteSwap);

        GetComponent<RectTransform>().sizeDelta = new Vector2(ButtonSpriteSwap.Normal.texture.width,
        ButtonSpriteSwap.Normal.texture.height);
    }
#endif
    public void SetButton(ButtonSpriteSwap buttonData)
    {
        ButtonSpriteSwap = buttonData;
        button = GetComponent<Button>();
        GetComponent<Image>().sprite = ButtonSpriteSwap.Normal;
        button.transition = Selectable.Transition.SpriteSwap;
        button.spriteState = ButtonSpriteSwap;
    }

}
