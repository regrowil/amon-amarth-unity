﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class ButtonSpriteSwap : ScriptableObject
{
    public Sprite Normal;
    public Sprite Pressed;
    [Header("Additional")]
    public Sprite Highlighted;
    public Sprite Disabled;

    public static implicit operator SpriteState(ButtonSpriteSwap buttonSprite)
    {
        return new SpriteState {disabledSprite = buttonSprite.Disabled,highlightedSprite = buttonSprite.Highlighted,pressedSprite = buttonSprite.Pressed};
    }
    [ContextMenu("Auto set Highlighted and Disabled")]
    public void AutoSetHighlightedAndDisabled()
    {
        Highlighted = Disabled = Normal;
    }
}