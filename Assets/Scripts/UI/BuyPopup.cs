﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;
using Firebase.Analytics;
using UnityEngine.Events;

public class BuyPopup : Singleton<BuyPopup>
{

    public GameObject popup;

    public UnityAction AfterView;
//    public BlurOptimized blur;

    public void SetUp( UnityAction afterView)
    {

        AfterView = afterView;
        Show();
    }

    public void Show()
    {
        popup.SetActive(true);

//        blur.enabled = true;

    }
    public void Close()
    {
        popup.SetActive(false);
//        blur.enabled = false;

    }

    public void ViewVideo()
    {
        AfterView.Invoke();
    }

    public void Buy(Product p)
    {
        GameConfig.UnlockFullVersion(p.definition.id);

        FacebookEventLogger.OnPurchase(p);
		Debug.Log("Purchase_Completed");
		FirebaseAnalytics.LogEvent("Purchase_Completed");
        FacebookEventLogger.OnPurchase(p);

    }

    public void OnFailed(Product p, PurchaseFailureReason reason)
    {
        Debug.Log("Purchase FAILED!");
        Debug.Log("Reason: "+reason.ToString());
        Debug.Log(p);
        if (p.hasReceipt)
        {
            Debug.Log("Product has receipt => unlocking");
            Buy(p);
            return;
        }
        if (reason == PurchaseFailureReason.DuplicateTransaction)
        {
            Debug.Log("Duplicate Transaction => unlocking");
            Buy(p);
            return;
        }
        if (reason == PurchaseFailureReason.ExistingPurchasePending)
        {
            Debug.Log("Existing Purchase Pending  => unlocking");
            Buy(p);
            return;
        }
        Debug.Log("there is no reason to fix purchase");
    }

    public void RemoveFullVersion()
    {
        GameConfig.ForgotAboutFullVersion();
    }

    public void FakeBuy()
    {
        GameConfig.FakeFull = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

}
