﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class ChapterSelectUI : MonoBehaviour
{

    public ChapterData[] Chapters;

    public GameObject ChapterPrefab;
    public Transform ChapterList;

    public LevelSelectUI LevelSelect;

    public Sprite GoldMedal, GrayMedal;

    public UnityEvent OnChapterClick;

    public void Start()
    {
        bool unlocked =true;
        foreach (var chapter in Chapters)
        {
            var g = Instantiate(ChapterPrefab);
            g.transform.SetParent(ChapterList, false);
            var chapterElement = g.GetComponent<ChapterUIElement>();
            chapterElement.SetUp(chapter, LevelSelect,unlocked,OnChapterClick.Invoke);
            chapterElement.SetCompleted(GetChapterProgress(chapter), GoldMedal, GrayMedal);
            unlocked = !GetChapterProgress(chapter).Any(b => b==false);  // GameConfig.IsFullVersion;
        }
    }

    public static bool[] GetChapterProgress(ChapterData chapter)
    {
        if (chapter.Levels.Length == 0) { return new bool[3]; }
        var l1 = PlayerPrefs.GetInt(chapter.Levels[0].LevelName, 0);
        var l2 = PlayerPrefs.GetInt(chapter.Levels[1].LevelName, 0);
        var l3 = PlayerPrefs.GetInt(chapter.Levels[2].LevelName, 0);
        return new bool[] { l1 == 1, l2 == 1, l3 == 1 };
    }

    public void RefreshProgress()
    {
        for (int i = 0; i < ChapterList.childCount; i++)
        {
            var x = ChapterList.GetChild(i).GetComponent<ChapterUIElement>();
            if(!x.gameObject.activeSelf)continue;

            x.SetCompleted(GetChapterProgress(x.CurrentChapter), GoldMedal, GrayMedal);

        }
    }

}
