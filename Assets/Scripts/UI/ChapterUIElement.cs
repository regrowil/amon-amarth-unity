﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChapterUIElement : MonoBehaviour
{
    public Image Border;

    public Sprite LockedBorder, ClickedBorder;

    public Text Name;
    public Image Image;
    public Image[] Medals;
    public Text[] Songs;

    public bool Locked;

    public ChapterData CurrentChapter;

    private LevelSelectUI levelSelectUI;

    private bool[] completedLevels;
    private UnityAction OnClick;

    public void SetUp(ChapterData chapter, LevelSelectUI levelSelect,bool isUnlocked, UnityAction onClick)
    {
        levelSelectUI = levelSelect;
        CurrentChapter = chapter;
        Name.text = chapter.name;
        Locked = !isUnlocked;

        if (Locked) Border.overrideSprite = LockedBorder;

        for (int index = 0; index < Songs.Length; index++)
        {
            Songs[index].text = AudioController.GetSongName(chapter.Songs[index]);
        }
        Image.sprite = chapter.Image;
        OnClick = onClick;

    }

    public void OnEnable()
    {
        GetComponentInChildren<ChapterClickArea>().SetUp(OnPointerDown,OnPointerUp,OnPointerClick,null,null);
    }

    public void SetCompleted(bool[] completed, Sprite goldMedal, Sprite grayMedal)
    {
        completedLevels = completed;
        for (int index = 0; index < completed.Length; index++)
        {
            Medals[index].sprite = completed[index] ? goldMedal : grayMedal;
        }
    }

    public void OnPointerDown()
    {
        if (Locked) return;
        Border.overrideSprite = ClickedBorder;
    }

    public void OnPointerUp()
    {
        if (Locked) return;
        Border.overrideSprite = null;
    }

    public void OnPointerClick()
    {
        if (Locked)
        {
//            BuyPopup.Instance.Show();//GameConfig.BuyFullVersion();
        }
        else
        {
            MenuController.LastChapter = CurrentChapter;
            levelSelectUI.SetUp(CurrentChapter, completedLevels);
            OnClick.Invoke();
        }
    }

}
