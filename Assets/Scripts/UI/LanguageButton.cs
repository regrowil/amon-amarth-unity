﻿using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using UnityEngine;

public class LanguageButton : SettingsButton
{
    [Header("Language button")]

    public List<string> Languages;

    public SetLanguage LangChanger;

    public void SetLangButton(string lang)
    {
        SetButton(Languages.IndexOf(lang));
        ChangeGameLang(lang);
    }

    public void ChangeGameLang(string lang)
    {
        if (LangChanger == null) LangChanger = GetComponent<SetLanguage>();
        if (LangChanger == null) return;

        LangChanger._Language = lang;
        LangChanger.ApplyLanguage();
    }

    public override void SubscribeEvent()
    {
        Settings.SubscribeOnChange(SettingsName, (lang) =>
        {
            SetLangButton(lang);
            SwitchEvent.Invoke();
        });
    }

    public override void LoadButton()
    {
        SetLangButton(Settings.GetStringSettings(SettingsName));
    }

    public override void OnClick()
    {
        Next();
        Settings.ChangeStringSettings(SettingsName, Languages[Current]);
    }
}
