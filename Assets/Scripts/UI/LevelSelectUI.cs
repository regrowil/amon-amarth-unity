﻿using System.Collections;
using System.Collections.Generic;
using SpriteTile;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectUI : MonoBehaviour
{
    public static int CurrentLevelId = 0;
    public static List<Level> LevelList = new List<Level>();

    public Text HeaderText;
    public LevelUIElement[] Levels;
	public Sprite Clicked, GoldMedal_1, GrayMedal_1, GoldMedal_2, GrayMedal_2, GoldMedal_3, GrayMedal_3;

    public Text[] Songs;

    public void ResetCurrentChapter()
    {
        MenuController.LastChapter = null;
    }

    public void SetUp(ChapterData chapter, bool[] completed)
    {
        gameObject.SetActive(true);
        HeaderText.text = chapter.name;
        LevelList.Clear();
        for (int i = 0; i < 3; i++)
        {
            var song = AudioController.GetSongName(chapter.Songs[i]);
            Songs[i].text = song;

            if (chapter.Levels.Length < i) return;
            LevelList.Add(chapter.Levels[i]);

			if(i == 0) {
				Levels[i].SetUp(i, chapter.Levels[i], this, Clicked, GoldMedal_1, GrayMedal_1, song);
			}
			else if(i == 1) {
				Levels[i].SetUp(i, chapter.Levels[i], this, Clicked, GoldMedal_2, GrayMedal_2, song);
			}
			else if(i == 2) {
				Levels[i].SetUp(i, chapter.Levels[i], this, Clicked, GoldMedal_3, GrayMedal_3, song);
			}
			else {
				Levels[i].SetUp(i, chapter.Levels[i], this, Clicked, GoldMedal_1, GrayMedal_1, song);
			}
            Levels[i].SetCompleted(completed[i], i == 0 || completed[i - 1], i == 0);
        }
    }

    public void CheatCompleteChapter()
    {
        foreach (var level in LevelList)
        {
            PlayerPrefs.SetInt(level.LevelName, 1);
        }

    }
}


