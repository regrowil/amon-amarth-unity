﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelUIElement : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,IPointerClickHandler
{
    Level thisLevel;
    string levelSong;
    public Image LevelImage;
    public Image Medal;
    public Image Border;
    public Text Completed;
    public int ID;
	public GameObject Subtitle;

    Sprite Clicked, GoldMedal, GrayMedal;

    public AudioClip OnClickSound;
    public bool IsLocked = false;

    public bool RequireFull;

    public void SetUp(int id,Level level, LevelSelectUI levelSelect,Sprite clicked,Sprite gold,Sprite gray,string song)
    {
        ID = id;
        thisLevel = level;
        RequireFull = thisLevel.RequireFullVersion;
        LevelImage.sprite = level.Image;

        Clicked = clicked;
        GoldMedal = gold;
        GrayMedal = gray;
        levelSong = song;
		Subtitle.GetComponent<Text>().text = song;
    }

    public void SetCompleted(bool completed,bool previousCompleted,bool isFirst)
    {
        if (completed)
        {
            Medal.sprite = GoldMedal;
            Completed.text = "Completed!";
        }
        else if(previousCompleted)
        {
            Medal.sprite = GrayMedal;
            Completed.text = isFirst?"Start":"Continue...";
        }
        else
        {
            Medal.sprite = GrayMedal;
            Completed.text = "Locked";
            IsLocked = true;
            LevelImage.color = new Color(0.5f, 0.5f, 0.5f, 1);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(IsLocked)return;
        Border.overrideSprite = Clicked;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (IsLocked) return;
        Border.overrideSprite = null;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(IsLocked)return;
        if (RequireFull && !GameConfig.IsFullVersion)
        {
            BuyPopup.Instance.SetUp(() =>
            {
                Player.HasDiedOnce = false;
                Player.HowManyCheckpointsReached = 0;
                PlayerScore.TimerPaused = true;
                AudioController.Instance.Source.PlayOneShot(OnClickSound);
                StartCoroutine(LoadDelayed(0.8f));

            });
            return;
        }
		Player.HasDiedOnce = false;
		Player.HowManyCheckpointsReached = 0;
		PlayerScore.TimerPaused = true;
        AudioController.Instance.Source.PlayOneShot(OnClickSound);
        StartCoroutine(LoadDelayed(0.8f));

    }

    public IEnumerator LoadDelayed(float delay)
    {
        Border.overrideSprite = Clicked;
        yield return new WaitForSeconds(delay);
        LevelSelectUI.CurrentLevelId = ID;
        LevelManager.LevelData = thisLevel;
        SceneManager.LoadScene(2);
        AudioController.PlaySong(levelSong);
        AudioController.Instance.Source.Stop();

    }

}
