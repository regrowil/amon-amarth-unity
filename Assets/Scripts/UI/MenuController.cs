﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

    public ChapterSelectUI ChapterSelectorPopup;
    public LevelSelectUI LevelSelectorPopup;
    public GameObject OptionPopup;

    public static ChapterData LastChapter;

    public void Start()
    {
        if (LastChapter != null)
        {
            ChapterSelectorPopup.gameObject.SetActive(true);
            LevelSelectorPopup.gameObject.SetActive(true);
            LevelSelectorPopup.SetUp(LastChapter,ChapterSelectUI.GetChapterProgress(LastChapter));
        }
    }
}
