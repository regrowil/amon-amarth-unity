﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class PlayAudioOnClick : MonoBehaviour
{

    public AudioClip AudioOnClick;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(PlayClip);
    }

    void PlayClip()
    {
        AudioController.PlayAudioClip(AudioOnClick);
    }
}
