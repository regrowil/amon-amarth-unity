﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerStatusPanel : MonoBehaviour
{
    [SerializeField]
    private Image healthBar;
    [SerializeField]
    private Image powerBar;
    [SerializeField]
    private Image powerBarGlow;
	[SerializeField]
	private Image TimeCard;
	[SerializeField]
	private Text ScoreCounter;
	[SerializeField]
	private Text TimeCounter;


	private CharacterHP playerHP;
	private PlayerScore playerScore;
    private CharacterPower playerPower;

    public UnityEvent OnMaxPower;

    void Awake()
    {
        playerHP = Player.Instance.GetComponent<CharacterHP>();
        playerPower = Player.Instance.GetComponent<CharacterPower>();
		playerScore = Player.Instance.GetComponent<PlayerScore>();
		if(Player.HasDiedOnce == true)
		{
			TimeCard.gameObject.SetActive(false);
			TimeCounter.gameObject.SetActive(false);
		}
    }

    void Update()
    {
        UpdateHealthBar();
        UpdatePowerBar();
		UpdateScoreCounter();
		UpdateTimeCounter();
    }

    private void UpdateHealthBar()
    {
        healthBar.fillAmount = (float)playerHP.CurrentHP / playerHP.MaxHP;
    }

    private void UpdatePowerBar()
    {
        powerBar.fillAmount = (float)playerPower.CurrentPower / playerPower.MaxPower;
        if (playerPower.CurrentPower == playerPower.MaxPower)
        {
            powerBarGlow.enabled = true;
            if(OnMaxPower != null) OnMaxPower.Invoke();
        }
        else
        {
            powerBarGlow.enabled = false;
        }
    }

	private void UpdateScoreCounter()
	{
		ScoreCounter.text = "" + playerScore.CurrentScore;
	}
	private void UpdateTimeCounter()
	{
		TimeCounter.text = PlayerScore.timeText;
	}
}