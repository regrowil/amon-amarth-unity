﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUISwitcher : MonoBehaviour
{


    void Start()
    {
        Switch();
    }
    // Use this for initialization
    public void Switch()
    {
        var side = Settings.GetBoolSettings("LeftHand") ? -1 : 1;
        
        transform.localScale = new Vector3(side, transform.localScale.y, transform.localScale.z);
        for (int index = 0; index < transform.childCount; index++)
        {
            var children = transform.GetChild(index);
            var scale = children.localScale;
            children.localScale = new Vector3(side,scale.y,scale.z);

            
        }
    }

}
