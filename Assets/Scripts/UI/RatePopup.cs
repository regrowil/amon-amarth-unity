﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RatePopup : MonoBehaviour
{

    public string GoogleLink;
    public string AppleLink;

    public UnityEvent OnNo;
    public UnityEvent OnYes;
    public UnityEvent OnLater;

    private UnityAction OnClose;

    public void ClickYes()
    {
        OnYes.Invoke();
    }

    public void ClickNo()
    {
        OnNo.Invoke();
    }

    public void ClickLater()
    {
        OnLater.Invoke();
    }


    public void Show(UnityAction onClose)
    {
        if (GameConfig.IsFullVersion)
        {
            if (PlayerPrefs.GetInt("GameRated", 0) == 0)
            {
                OnClose = onClose;
                ShowUI();
            }
            else
            {
                onClose.Invoke();
            }
        }
        else
        {
            onClose.Invoke();
        }
    }

    public void ShowUI()
    {
        GetComponent<CanvasGroup>().alpha = 1;
        GetComponent<CanvasGroup>().interactable = true;
        GetComponent<CanvasGroup>().blocksRaycasts = true;

    }

    public void HideUI()
    {
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().interactable = false;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        if(OnClose !=null) OnClose.Invoke();
    }

    public void OpenShop()
    {
        PlayerPrefs.SetInt("GameRated", 1);
        PlayerPrefs.Save();
#if UNITY_ANDROID
        OpenGooglePlay();
#elif UNITY_IOS
        OpenItunes();
#endif
        HideUI();
    }

    public void OpenItunes()
    {
        var url = "itms://itunes.apple.com/app/apple-store/" + AppleLink + "?mt=8";
        Application.OpenURL(url);

    }

    public void OpenGooglePlay()
    {
        var url = "market://details?id=" + GoogleLink;
        Application.OpenURL(url);

    }

    public void OnLaterClick()
    {
        HideUI();
    }

    public void OnCloseClick()
    {
        PlayerPrefs.SetInt("GameRated", 1);
        PlayerPrefs.Save();
        HideUI();

    }
}
