﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleIf : MonoBehaviour
{
    public Vector2 ratio;
    public Vector2 scale;
    public Vector2 size;
    public Vector2 preferedLayout;
    public int fontSize;
    // Use this for initialization
    void Start()
    {
        if (Math.Abs(Camera.main.aspect - ratio.x / ratio.y) < 0.1f)
        {
            if (size != Vector2.zero)
            {
                GetComponent<RectTransform>().sizeDelta = size;
            }
            if (scale != Vector2.zero)
            {
                GetComponent<RectTransform>().localScale = scale;

            }
            if (preferedLayout != Vector2.zero)
            {
                if (GetComponent<LayoutElement>())
                {
                    if (preferedLayout.x != 0)
                        GetComponent<LayoutElement>().preferredWidth = preferedLayout.x;
                    if (preferedLayout.y != 0)
                        GetComponent<LayoutElement>().preferredHeight = preferedLayout.y;
                };

            }
            if (fontSize != 0)
            {
                if (GetComponent<Text>())
                {
                    GetComponent<Text>().fontSize = fontSize;
                };

            }
        }
    }


}
