﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SettingsButton : ButtonRotator
{

    [Header("Settings button")]
    public string SettingsName;
    public UnityEvent SwitchEvent;

    public void OnValidate()
    {
        if(GetComponent<ButtonRotator>())
        {
            var br = GetComponent<ButtonRotator>();
            Buttons = br.Buttons;
        }
    }

    public void SetOptionButton(bool enabled)
    {
        SetButton(enabled ? 1 : 0);
    }

    public void Start()
    {
        LoadButton();
        SubscribeEvent();
    }

    public virtual void SubscribeEvent()
    {
        Settings.SubscribeOnChange(SettingsName, (enabled) =>
            {
                SetOptionButton(enabled);
                SwitchEvent.Invoke();
            });
    }

    public virtual void LoadButton()
    {
        SetOptionButton(Settings.GetBoolSettings(SettingsName));
    }

    public virtual void OnClick()
    {
        Settings.ToggleSettings(SettingsName);
    }

   
}
