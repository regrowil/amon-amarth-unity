﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Events;
using UnityStandardAssets.ImageEffects;
using Firebase.Analytics;
using FreshRocket;

public class ShareController : MonoBehaviour
{
	//[SerializeField]
	//NativeShare nativeShare;

	[SerializeField]
	private List<Camera> cameras = new List<Camera>();
	[SerializeField]
	public Camera screenshotCamera;

	private Texture2D screenTexture;
	private byte[] dataToSave;
	private string screenshotName = "screenshot.png";

    public void ShareScreenFromTextureWithText(string text, Texture2D screen)
    {
        SimpleNativeSharing.ShareTextAndImage(screen,text);

    }

    public void ShareScreenshotWithText(string text, string screenshotName, bool fromResources)
    {
        FirebaseAnalytics.LogEvent("Share_Method_Initialized");

        if (fromResources)
        {
             NativeShare.Share(text, "", "");
        }
        else
        {
            string screenShotPath = Path.Combine(Application.persistentDataPath, screenshotName);
            StartCoroutine(delayedShare(screenShotPath, text));
        }
    }

    //CaptureScreenshot runs asynchronously, so you'll need to either capture the screenshot early and wait a fixed time
    //for it to save, or set a unique image name and check if the file has been created yet before sharing.
    IEnumerator delayedShare(string screenShotPath, string text)
    {
        while (!File.Exists(screenShotPath))
        {
            yield return new WaitForSeconds(.05f);
        }
        while (!File.Exists(screenShotPath))
        {
            yield return new WaitForSeconds(.05f);
        }
        NativeShare.Share(text, screenShotPath, "");
    }

    public void ShareClick()
	{
        //        ShareScreenshotWithText("Get the Amon Amarth mobile game here: http://tosto.re/Amonamarth", screenshotName, false);
		ShareScreenFromTextureWithText("Get the Amon Amarth mobile game here: http://tosto.re/amonamarthgame", screenTexture);

        var lvl = LevelManager.Instance.LevelNr;
        FacebookEventLogger.LogSharedEvent(Application.platform.ToString(), "Unknown", lvl);
    }

    [Range(0,2)]
	public int downsample = 1;

	public enum BlurType
	{
		StandardGauss = 0,
		SgxGauss = 1,
	}

	[Range(0.0f,10.0f)]
	public float blurSize = 3.0f;

	[Range(1,4)]
	public int blurIterations = 2;

	public BlurType blurType = BlurType.StandardGauss;

	public Shader blurShader = null;
	private Material blurMaterial = null;

	private RenderTexture renderTexture;

	protected Material CheckShaderAndCreateMaterial(Shader s,Material m2Create)
	{
		if(!s)
		{
			Debug.Log("Missing shader in " + ToString());
			enabled = false;
			return null;
		}

		if(s.isSupported && m2Create && m2Create.shader == s)
			return m2Create;

		if(!s.isSupported)
		{
			Debug.Log("The shader " + s.ToString() + " on effect " + ToString() + " is not supported on this platform!");
			return null;
		}

		m2Create = new Material(s);
		m2Create.hideFlags = HideFlags.DontSave;

		return m2Create;
	}

	public RenderTexture Blur(RenderTexture source,RenderTexture destination)
	{
		blurMaterial = CheckShaderAndCreateMaterial(blurShader,blurMaterial);



		float widthMod = 1.0f / (1.0f * (1 << downsample));

		blurMaterial.SetVector("_Parameter",new Vector4(blurSize * widthMod,-blurSize * widthMod,0.0f,0.0f));
		source.filterMode = FilterMode.Bilinear;

		int rtW = source.width >> downsample;
		int rtH = source.height >> downsample;

		// downsample
		RenderTexture rt = RenderTexture.GetTemporary(rtW,rtH,0,source.format);
		rt.filterMode = FilterMode.Bilinear;
		Graphics.Blit(source,rt,blurMaterial,0);

		var passOffs = 0;

		for(int i = 0; i < blurIterations; i++)
		{
			float iterationOffs = (i * 1.0f);
			blurMaterial.SetVector("_Parameter",new Vector4(blurSize * widthMod + iterationOffs,-blurSize * widthMod - iterationOffs,0.0f,0.0f));

			// vertical blur
			RenderTexture rt2 = RenderTexture.GetTemporary(rtW,rtH,0,source.format);
			rt2.filterMode = FilterMode.Bilinear;
			Graphics.Blit(rt,rt2,blurMaterial,1 + passOffs);
			RenderTexture.ReleaseTemporary(rt);
			rt = rt2;

			// horizontal blur
			rt2 = RenderTexture.GetTemporary(rtW,rtH,0,source.format);
			rt2.filterMode = FilterMode.Bilinear;
			Graphics.Blit(rt,rt2,blurMaterial,2 + passOffs);
			RenderTexture.ReleaseTemporary(rt);
			rt = rt2;
		}

		Graphics.Blit(rt,destination);
		return rt;

	}

    public bool AlreadyCaptured;

	public GameObject[] buttons;
	[ContextMenu("Share")]
	public void TakeScreenshot()
	{
		FirebaseAnalytics.LogEvent("Share_Button_Clicked");
	    if (AlreadyCaptured)
	    {
            ShareClick();
            return;
        }
		//Enable Overlay
		screenshotCamera.gameObject.SetActive(true);
		//Hide Share/OK buttons
		foreach(var button in buttons)
		{
			button.gameObject.SetActive(false);

		}
		//Create new renderTexture
		renderTexture = new RenderTexture(Screen.width,Screen.height,24);

		//Render every Camera
		for(int index = 0; index < cameras.Count; index++)
		{
			//Skip Rendering Main Camera and create copy
			if(index == 0)
			{
				//Create new Camera and copy settings from MainCamera
				var x = new GameObject("shotcam");
				var c = x.AddComponent<Camera>();
				c.CopyFrom(cameras[0]);
				c.targetTexture = renderTexture;
				c.Render();
				//Create Blurred texture
				var blurred = Blur(renderTexture,null);
				//Combine Blurred Texture with RenderTexture
				Graphics.Blit(blurred,renderTexture);
				c.targetTexture = null;

				Destroy(x);
				continue;
			}

			Camera camera = cameras[index];
			camera.targetTexture = renderTexture;
			camera.Render();
			camera.targetTexture = null;
		}

		//Reenable buttons
		foreach(var button in buttons)
		{
			button.gameObject.SetActive(true);

		}
		// Load Texture from RenderTexture
		screenTexture = new Texture2D(renderTexture.width,renderTexture.height,TextureFormat.RGB24,true);
		screenTexture.ReadPixels(new Rect(0f,0f,renderTexture.width,renderTexture.height),0,0);
		screenTexture.Apply();


//        dataToSave = screenTexture.EncodeToPNG();
//		string destination = Path.Combine(Application.persistentDataPath,screenshotName);
//		Debug.Log(destination);

		// Save Texture to file
//		File.WriteAllBytes(destination,dataToSave);

		RenderTexture.active = null;
		Destroy(renderTexture);
		screenshotCamera.gameObject.SetActive(false);
        AlreadyCaptured = true;
		ShareClick();
	}
}
