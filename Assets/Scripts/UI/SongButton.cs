﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SongButton : MonoBehaviour, IPointerClickHandler
{

    public static SongButton CurrentSong;
    public Text Text;

    public void OnPointerClick(PointerEventData eventData)
    {
        if(!Settings.GetBoolSettings("Music"))return;

        if(CurrentSong)
        CurrentSong.Text.color = Color.black;
        if (CurrentSong == this)
        {
            CurrentSong = null;
            AudioController.PlayMenuMusic();
            return;
        }

        var song = Text.text;
        CurrentSong = this;
        CurrentSong.Text.color = Color.blue;
        
        AudioController.PlaySong(song);
    }

    public void DisableSongOnBackClicked()
    {
        if(CurrentSong==null) return;
        
        AudioController.PlayMenuMusic();
        CurrentSong.Text.color = Color.black;
        
    }

}
