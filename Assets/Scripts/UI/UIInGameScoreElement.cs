﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIInGameScoreElement : MonoBehaviour
{

    public Text name;
    public Text score;
    public Text time;

    public Image background;
    public Image marger;

    public Color you;
    public Color enemy;

    public int position;


    public void SetUp(int level, int points, string time)
    {
        SetChapter(level, points, time);
    }

    public string GetHighScoreData(int level, int positon, bool isName = false)
    {
        return new StringBuilder("highscore_lvl").Append(level).Append("_spot_").Append(positon).Append(isName ? "_name" : "").ToString();
    }

    public void SetChapter(int l, int points, string timeString)
    {
        //  var offset = (world - 1) * 3;
        //        for (int i = 0; i < scores.Length; i++)
        //        {
        //            int l = (i / 5) + 1;
        //            int p = (i % 5) + 1;
        score.text = PlayerPrefs.GetInt(GetHighScoreData(l, position), 0).ToString();
        name.text = PlayerPrefs.GetString(GetHighScoreData(l, position, true), "none");
        time.text = PlayerPrefs.GetString(GetHighScoreData(l, position) + "_time", "");
        name.color = (name.text == "You") ? you : enemy;

        if (score.text == points.ToString() && time.text == timeString)
        {
            marger.enabled = true;
            background.enabled = true;
            if (position == 1)
            {
                LevelManager.Instance.ShowNewRecord();
            }
        }
        else
        {
            marger.enabled = false;
            background.enabled = false;

        }

    }
}
