﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using SpriteTile;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UISceneSwitcher : MonoBehaviour
{

    public void LoadScene(int scene)
    {
		if (scene == 1)
            AudioController.PlayMenuMusic();

        if (scene == 3)
        {
            FB.LogAppEvent("Credits");
        }

        SceneManager.LoadScene(scene);
        //        Disabled Because problem with not loaded Tiles        
        //        Tile.EraseLevel();
    }

	public void resetScore(){
		PlayerPrefs.SetInt("currentscore", 0);
	}

    public void ResetRunestone()
    {
        Runestone.Reset();
    }
}
