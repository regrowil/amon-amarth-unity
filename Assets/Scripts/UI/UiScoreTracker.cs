﻿using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UiScoreTracker : MonoBehaviour
{

    [Header("Controlls")]
    public Text ChapterName;
    public Color DefaultColor;

    public GameObject SpeedRunButton;
    public Image Header;
    public Sprite SpeedHeader;

    public Text[] scores;
    public Text[] names;
    public Text[] timeTexts;

    public Text[] emptyDesc;

    private string Chapter;

    public UnityEvent OnBackClicked;
    public void LoadFromView(Text chap)
    {
        Chapter = chap.text.ToUpper();
        ChapterName.text = Chapter;

        SetChapter(LoadChapter(Chapter));
    }

    int LoadChapter(string ShowChapter)
    {
        switch (ShowChapter)
        {
            case "MIDGARD":
//                SetChapter(1);
                return 1;
            case "MUSPELHEIM":
//                SetChapter(2);
                return 2;
            case "NIFLHEIM":
//                SetChapter(3);
                return 3;
            case "HELHEIM":
//                SetChapter(4);
                return 4;
            default:
//                SetChapter(1);
                Debug.Log("Default loaded!");
                return 1;
        }

    }

    public string GetHighScoreData(int offset, int level, int positon, bool isName = false)
    {
        return new StringBuilder("highscore_lvl").Append(offset + level).Append("_spot_").Append(positon).Append(isName ? "_name" : "").ToString();
    }

    public void SetChapter(int world)
    {
        var offset = (world - 1) * 3;
        for (int i = 0; i < scores.Length; i++)
        {

            int l = (i / 5) + 1;
            int p = (i % 5) + 1;

            scores[i].transform.parent.gameObject.SetActive(true);
            


            scores[i].text = PlayerPrefs.GetInt(GetHighScoreData(offset, l, p), 0).ToString();
            names[i].text = PlayerPrefs.GetString(GetHighScoreData(offset, l, p, true), "none");
            timeTexts[i].text = PlayerPrefs.GetString(GetHighScoreData(offset, l, p) + "_time", "");
            names[i].color = (names[i].text == "You") ? Color.red : DefaultColor;
        }
        for (int i = 0; i < 3; i++)
        {
            emptyDesc[i].gameObject.SetActive(false);
        }
    }
    public void SetChapterSpeedRun(int world)
    {
        bool[] empty = {true, true, true};
        var offset = (world - 1) * 3;
        for (int i = 0; i < scores.Length; i++)
        {
            int l = (i / 5) + 1;
            int p = (i % 5) + 1;
            var time = PlayerPrefs.GetString(GetHighScoreData(offset, l, p) + "_time", "");
            if (time == "")
            {
                scores[i].transform.parent.gameObject.SetActive(false);
            }
            else
            {
                scores[i].transform.parent.gameObject.SetActive(true);
                empty[l-1] = false;
            }
            scores[i].text = PlayerPrefs.GetInt(GetHighScoreData(offset, l, p), 0).ToString();
            names[i].text = PlayerPrefs.GetString(GetHighScoreData(offset, l, p, true), "none");
            timeTexts[i].text = time;
            names[i].color = (names[i].text == "You") ? Color.red : DefaultColor;
        }

        for (int i = 0; i < emptyDesc.Length; i++)
        {
            emptyDesc[i].gameObject.SetActive(empty[i]);
        }

    }
    public void LoadSpeedRuns()
    {
        Header.overrideSprite = SpeedHeader;
        SpeedRunButton.SetActive(false);
        SetChapterSpeedRun(LoadChapter(Chapter));

    }

    public void Back()
    {
        if (Header.overrideSprite == SpeedHeader)
        {
            Header.overrideSprite = null;
            SetChapter( LoadChapter(Chapter));
            SpeedRunButton.SetActive(true);

        }
        else
        {
            OnBackClicked.Invoke();
        }
    }

}