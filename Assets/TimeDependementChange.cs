﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeDependementChange : MonoBehaviour
{

    // Use this for initialization
    [Header("Use this format: \"2017-12-21T00:00:00\"")]
    public string StartDate;
    public string EndDate;


    public UnityEvent IsInRange;

    private void OnValidate()
    {
        var start = DateTime.Parse(StartDate);
        var end = DateTime.Parse(EndDate);

        Debug.Log("StartDate is:" + start);
        Debug.Log("EndDate is:" + end);


    }

    void Start()
    {

        var start = DateTime.Parse(StartDate);
        var end = DateTime.Parse(EndDate);
        if (start < DateTime.Now && DateTime.Now < end)
        {
            IsInRange.Invoke();
        }


    }
}
